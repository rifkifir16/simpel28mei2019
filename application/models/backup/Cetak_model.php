<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_model extends CI_Model {
    
    function getPenelitian($param){
        $this->db->select('p.ID_PENELITIAN,p.JUDUL_PENELITIAN,p.BIAYA_PENELITIAN,p.SUMBER_BIAYA_PENELITIAN,b.JENIS_BIDANG,j.NAMA_JENIS,d.NIP,d.NAMA_DOSEN,jr.NAMA_JURUSAN,pk.NAMA_GOLONGAN,pk.PANGKAT,d.NIDN,d.NO_TLP,d.EMAIL');
        $this->db->from('penelitian p');
        $this->db->join('jenis_penelitian j','p.ID_JENIS=j.ID_JENIS');
        $this->db->join('bidang b','p.ID_BIDANG=b.ID_BIDANG');
        $this->db->join('dosen d','p.NIP=d.NIP');
        $this->db->join('jurusan jr','d.ID_JURUSAN=jr.ID_JURUSAN');
        $this->db->join('pangkat pk','d.ID_PANGKAT=pk.ID_PANGKAT');
        $this->db->where('p.ID_PENELITIAN',$param);
        $query=$this->db->get();

        return $query->row_array();
    }

    function anggotaPenelitian($param){
        $this->db->select('d.NAMA_DOSEN,d.NIDN,jr.NAMA_JURUSAN');
        $this->db->from('penelitian p');
        $this->db->join('anggota_penelitian ap','p.ID_PENELITIAN=ap.ID_PENELITIAN','left');
        $this->db->join('dosen d','ap.NIP=d.NIP','left');
        $this->db->join('jurusan jr','d.ID_JURUSAN=jr.ID_JURUSAN');
        $this->db->where('p.ID_PENELITIAN',$param);
        $query=$this->db->get();

        return $query->result_array();
    }

    function getPKM($param){
        $this->db->select('p.ID_PKM,p.JUDUL_PKM,p.MITRA_PKM,p.MAHASISWA_PKM,kota.NAMA_KOTA,propinsi.NAMA_PROP,p.WIL_MITRA_PKM,p.JARAK_PKM,p.OUTPUT_PKM,p.PELAKSANAAN_PKM,p.BIAYA_PKM,p.SUMBER_BIAYA_PKM,b.JENIS_BIDANG,j.NAMA_JENIS,d.NIP,d.NAMA_DOSEN,jr.NAMA_JURUSAN,pk.NAMA_GOLONGAN,pk.PANGKAT,d.NIDN,d.NO_TLP,d.EMAIL');
        $this->db->from('pkm p');
        $this->db->join('jenis_penelitian j','p.ID_JENIS=j.ID_JENIS');
        $this->db->join('bidang b','p.ID_BIDANG=b.ID_BIDANG');
        $this->db->join('dosen d','p.NIP=d.NIP');
        $this->db->join('kota','kota.ID_KOTA=p.ID_KOTA');
        $this->db->join('propinsi','kota.ID_PROP=propinsi.ID_PROP');
        $this->db->join('jurusan jr','d.ID_JURUSAN=jr.ID_JURUSAN');
        $this->db->join('pangkat pk','d.ID_PANGKAT=pk.ID_PANGKAT');
        $this->db->where('p.ID_PKM',$param);
        $query=$this->db->get();

        return $query->row_array();
    }

    function anggotaPKM($param){
        $this->db->select('d.NAMA_DOSEN,d.NIDN,b.JENIS_BIDANG');
        $this->db->from('pkm p');
        $this->db->join('anggota_pkm ap','p.ID_PKM=ap.ID_PKM','left');
        $this->db->join('dosen d','ap.NIP=d.NIP','left');
        $this->db->join('bidang b','ap.ID_BIDANG=b.ID_BIDANG');
        $this->db->where('p.ID_PKM',$param);
        $query=$this->db->get();

        return $query->result_array();
    }
	
    
    

}