<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen_model extends CI_Model {

    private $table = 'dosen';
    var $column_order = array('NIP','NIDN',null,'NAMA_DOSEN',null,null); 
    var $column_search = array('NIP','NIDN','NAMA_DOSEN'); 
    var $order = array('NIP' => 'ASC'); 

    private function _get_datatables_query($where=null)
    {
        $this->db->select('dosen.NIP,dosen.NIDN,user.ID_USER,dosen.NAMA_DOSEN,dosen.STATUS_DOSEN');
        $this->db->from($this->table);
        $this->db->join('user ','dosen.ID_USER=user.ID_USER','left');
        $i = 0;
     
        foreach ($this->column_search as $item) {
            if(isset($_POST['search']['value'])) {
                if($i===0) {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getData($id=null,$where=array(),$start=0,$limit=10){
        $this->_get_datatables_query();
        if(isset($_POST['length']))
            if($_POST['length'] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);
            else
                $this->db->limit(10, 0);
        else
            $this->db->limit(10, 0);
            
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($where=array())
    {        
        $this->_get_datatables_query();
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($where=array())
    {
        $this->db->from($this->table)->where($where);
        return $this->db->count_all_results();
    }

    function ajaxDatax($q){
        $this->db->select('ID_PRODI as id,NAMA_PRODI as text');
        $this->db->limit(10);
        $this->db->from('prodi');
        $this->db->where(array('STATUS_PRODI'=>1));
        $this->db->like('NAMA_PRODI',$q);
        $query=$this->db->get();
		
        return $query->result_array();
    }

    function selectData($id=null){
		$filter=is_null($id)?'': ' WHERE NIP='.$id;
        $q="SELECT dosen.NIP,dosen.NIDN,user.ID_USER,user.NAMA_USER,dosen.NAMA_DOSEN,dosen.STATUS_DOSEN FROM dosen LEFT JOIN user ON dosen.ID_USER=user.ID_USER".$filter;
        $query= $this->db->query($q);
        return $query->result_array();
    }

    function ajaxData($id=null,$q){
		$filter=is_null($id)?'': " WHERE NIP!=".$id." AND dosen.NAMA_DOSEN LIKE '%".$q."%'";
        $q="SELECT dosen.NIP,dosen.NIDN,user.ID_USER,user.NAMA_USER,dosen.NAMA_DOSEN,dosen.STATUS_DOSEN FROM dosen LEFT JOIN user ON dosen.ID_USER=user.ID_USER".$filter;
        $query= $this->db->query($q);
        return $query->result_array();
    }

    function check($where){
        $this->db->where($where);
        $this->db->from('dosen');
        $count = $this->db->count_all_results();

        return $count;
    }

    function getEdit($id=null){
		$filter=is_null($id)?'': ' WHERE NIP='.$id;
        $q="SELECT * FROM dosen LEFT JOIN user ON dosen.ID_USER=user.ID_USER LEFT JOIN jabatan ON dosen.ID_JABATAN=jabatan.ID_JABATAN LEFT JOIN pangkat ON dosen.ID_PANGKAT=pangkat.ID_PANGKAT LEFT JOIN jurusan ON dosen.ID_JURUSAN=jurusan.ID_JURUSAN".$filter;
        $query= $this->db->query($q);
        return $query->row_array();
    }

    function getDosen($id){
		$filter=is_null($id)?'': ' WHERE NIP='.$id;
        $q="SELECT NIP,NIDN,NAMA_DOSEN,EMAIL,NO_TLP,ALAMAT_RUMAH,ALAMAT_KANTOR,CONCAT(PANGKAT,'/',NAMA_GOLONGAN) AS NAMA_PANGKAT,NAMA_JABATAN,NAMA_JURUSAN FROM dosen LEFT JOIN user ON dosen.ID_USER=user.ID_USER LEFT JOIN pangkat p ON dosen.ID_PANGKAT=p.ID_PANGKAT LEFT JOIN jabatan j ON dosen.ID_JABATAN=j.ID_JABATAN LEFT JOIN jurusan jr ON dosen.ID_JURUSAN=jr.ID_JURUSAN".$filter;
        $query= $this->db->query($q);
        return $query->row_array();
    }
	
    function insert($data,$reqId=false,$table='dosen')
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        
        if($this->db->affected_rows()>0){
            if($reqId) 
                return $insert_id;
            else 
                return true;
        }
        else
            return false;
    }
    
    function update($data,$where,$table='dosen')
    {
        $this->db->where($where);
        $this->db->update($table,$data);
        
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }
    

}