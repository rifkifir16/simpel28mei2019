<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_model extends CI_Model {
   
    function getPenelitian() {
        $input=$this->input->post();
        $where=array();
        $where['p.STATUS_PENELITIAN >']=0;
        
        if($input['bulan']!='0')
            $where['MONTH(CREATED_AT_PENELITIAN)']=$input['bulan'];

        $where['YEAR(CREATED_AT_PENELITIAN)']=$input['tahun'];
        $this->db->select("NAMA_JURUSAN,dosen.NIDN as NIDA,dosen.NAMA_DOSEN,JUDUL_PENELITIAN,GROUP_CONCAT(n.NIDN separator '\n') AS NIDN,GROUP_CONCAT(n.NAMA_DOSEN separator '\n') as ANGGOTA, BIAYA_PENELITIAN, SUMBER_BIAYA_PENELITIAN, NAMA_JENIS,STATUS_PENELITIAN");
        $this->db->from('penelitian AS p');
        $this->db->join('(SELECT a.ID_PENELITIAN,a.NIP,d.NIDN, d.NAMA_DOSEN FROM anggota_penelitian a JOIN dosen d ON a.NIP=d.NIP) as n','p.ID_PENELITIAN=n.ID_PENELITIAN','left');
        $this->db->join('dosen','p.NIP=dosen.NIP','left');
        $this->db->join('jenis_penelitian AS jp','p.ID_JENIS=jp.ID_JENIS','left');
        $this->db->join('jurusan AS j','dosen.ID_JURUSAN=j.ID_JURUSAN','left');
        $this->db->where($where);
        $this->db->group_by('p.ID_PENELITIAN'); 
        $query = $this->db->get();
        //print_r($this->db->last_query());
        return $query->result();
    }

    function getPkm() {
        $input=$this->input->post();
        $where=array();
        $where['STATUS_PKM >']=0;
        
        if($input['bulan']!='0')
            $where['MONTH(CREATED_AT_PKM)']=$input['bulan'];

        $where['YEAR(CREATED_AT_PKM)']=$input['tahun'];
        $this->db->select("NAMA_JURUSAN,dosen.NIDN as NIDA,dosen.NAMA_DOSEN,JUDUL_PKM,GROUP_CONCAT(n.NIDN separator '\n') AS NIDN,GROUP_CONCAT(n.NAMA_DOSEN separator '\n') as ANGGOTA, BIAYA_PKM, SUMBER_BIAYA_PKM, NAMA_JENIS,STATUS_PKM");
        $this->db->from('pkm AS p');
        $this->db->join('(SELECT a.ID_PKM,a.NIP,d.NIDN, d.NAMA_DOSEN FROM anggota_pkm a JOIN dosen d ON a.NIP=d.NIP) as n','p.ID_PKM=n.ID_PKM','left');
        $this->db->join('dosen','p.NIP=dosen.NIP','left');
        $this->db->join('jenis_penelitian AS jp','p.ID_JENIS=jp.ID_JENIS','left');
        $this->db->join('jurusan AS j','dosen.ID_JURUSAN=j.ID_JURUSAN','left');
        $this->db->where($where);
        $this->db->group_by('p.ID_PKM'); 
        $query = $this->db->get();
        
        return $query->result();
    }

}