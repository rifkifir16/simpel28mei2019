<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

    function getData(){
        $this->db->select('*');
        $this->db->from('settings');
        $query= $this->db->get();
        return $query->result_array();
    }

    function selectData($where=array()){
        $this->db->select();
        $this->db->from('user');
        $this->db->where($where);
		/*$filter=is_null($id)?'': ' WHERE ID_USER='.$id;
        $q="SELECT * FROM user".$filter;*/
        $query= $this->db->get();
        return $query->result_array();
    }

    function check($where){
        $this->db->where($where);
        $this->db->from('user');
        $count = $this->db->count_all_results();

        return $count;
    }
	
    function insert($data,$reqId=false,$table='user')
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        
        if($this->db->affected_rows()>0){
            if($reqId) 
                return $insert_id;
            else 
                return true;
        }
        else
            return false;
    }
    
    function update($data,$where,$table='settings')
    {
        $this->db->where($where);
        $this->db->update($table,$data);
        
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }
    

}