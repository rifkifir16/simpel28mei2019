<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota_model extends CI_Model {

    function __construct() {
        
    }

    function getData($id=null){
		$filter=is_null($id)?'': ' WHERE NIP='.$id;
        $q="SELECT dosen.NIP,user.ID_USER,user.NAMA_USER,dosen.NAMA_DOSEN,dosen.STATUS_DOSEN FROM dosen LEFT JOIN user ON dosen.ID_USER=user.ID_USER".$filter;
        $query= $this->db->query($q);
        return $query->result_array();
    }

    function getEdit($id=null){
		$filter=is_null($id)?'': ' WHERE NIP='.$id;
        $q="SELECT * FROM dosen LEFT JOIN user ON dosen.ID_USER=user.ID_USER".$filter;
        $query= $this->db->query($q);
        return $query->row_array();
    }
	
    function insert($data,$reqId=false,$table='anggota_penelitian')
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        
        if($this->db->affected_rows()>0){
            if($reqId) 
                return $insert_id;
            else 
                return true;
        }
        else
            return false;
    }
    
    function update($data,$where,$table='anggota_penelitian')
    {
        $this->db->where($where);
        $this->db->update($table,$data);
        
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }
    

}