<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penelitian_model extends CI_Model {
    private $table = 'penelitian';
    var $column_order = array(null,'JUDUL_PENELITIAN'); //field yang ada di table user
    var $column_search = array('JUDUL_PENELITIAN','dosen.NAMA_DOSEN','n.NIDN','n.NAMA_DOSEN'); //field yang diizin untuk pencarian 
    var $order = array('p.ID_PENELITIAN' => 'ASC'); // default order 
    private  $joinTable="(SELECT a.ID_PENELITIAN,a.NIP,d.NIDN, d.NAMA_DOSEN FROM anggota_penelitian a JOIN dosen d ON a.NIP=d.NIP) as n";
    private $joinFilter="p.ID_PENELITIAN=n.ID_PENELITIAN";
    /*
    Status : 
    0=dibatalkan, hak user
    1= belum disetujui, default
    2= perlu revisi, hak admin
    3= sudah direvisi, hak user
    4= disetujui, hak admin
    ditolak? (by admin)
    dibatalkan? (by proposer)
     */
    function __construct() {
		parent::__construct();
		$qq='set sql_mode="NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"';
		$query= $this->db->query($qq);
	}

    function allowReg($nip,$year){
        
		$sql="SELECT p.NIP,p.JUDUL_PENELITIAN FROM penelitian p WHERE NIP='".$nip."' AND YEAR(CREATED_AT_PENELITIAN)=".$year." AND STATUS_PENELITIAN>0 UNION SELECT p.NIP,p.JUDUL_PKM FROM pkm p WHERE NIP='".$nip."' AND YEAR(CREATED_AT_PKM)=".$year." AND STATUS_PKM>0";
		$query=$this->db->query($sql);
        //$query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query($where=null)
    {
       
        $this->db->select("ptot.jumlah,DATE_FORMAT(CREATED_AT_PENELITIAN,'%d-%m-%Y') AS TANGGAL, p.ID_PENELITIAN,p.JUDUL_PENELITIAN,dosen.NAMA_DOSEN,NAMA_JENIS,STATUS_PENELITIAN,GROUP_CONCAT('(',n.NIDN,')' ,' <br/> ',n.NAMA_DOSEN SEPARATOR '<br/>') as ANGGOTA,LOCATION_PPEN AS PROPOSAL_PENELITIAN");
        $this->db->from($this->table.' AS p');
        $this->db->join($this->joinTable,$this->joinFilter,'left');
        $this->db->join('dosen','p.NIP=dosen.NIP','left');
        $this->db->join('jenis_penelitian jp','p.ID_JENIS=jp.ID_JENIS','left');
        $this->db->join('proposal_penelitian pp','p.ID_PPENELITIAN=pp.ID_PPEN AND p.ID_PENELITIAN=pp.ID_PENELITIAN','left');
        $this->db->join('(SELECT ID_PENELITIAN,COUNT(*) as jumlah FROM proposal_penelitian GROUP BY ID_PENELITIAN) AS ptot','p.ID_PENELITIAN=ptot.ID_PENELITIAN','left');
        $this->db->where(array('STATUS_PENELITIAN'>0));
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if(isset($_POST['search']['value'])) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by("p.ID_PENELITIAN");
    }

    function getDatas($where=array()){
        $this->_get_datatables_query();
                
        if(isset($_POST['length']))
            if($_POST['length'] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);
            else
                $this->db->limit(10, 0);
        else
            $this->db->limit(10, 0);
        //$this->db->limit($length, $start);
        $this->db->where($where);
        $query = $this->db->get();
        
        return $query->result();
    }

    function count_filtered($where=array())
    {   
        $this->_get_datatables_query();
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($where=array())
    {
        $this->db->select("*");
        $this->db->from($this->table.' AS p');
        $this->db->join($this->joinTable,$this->joinFilter,'left');
        $this->db->join('dosen','p.NIP=dosen.NIP','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    function getData($id=null,$customFilter=''){
        //belum anggota
        $filter=is_null($id)?'': ' AND p.ID_PENELITIAN='.$id;
        $filter.=$customFilter;
        //$q="SELECT ID_PENELITIAN,JUDUL_PENELITIAN,JENIS_PENELITIAN,NAMA_DOSEN,'-' AS ANGGOTA,STATUS_PENELITIAN FROM penelitian LEFT JOIN dosen on penelitian.NIP=dosen.NIP".$filter;
        //$q="SELECT p.ID_PENELITIAN,p.JUDUL_PENELITIAN,dosen.NAMA_DOSEN,NAMA_JENIS,STATUS_PENELITIAN,GROUP_CONCAT('(',n.NIDN,')' ,' <br/> ',n.NAMA_DOSEN SEPARATOR '<br/>') as ANGGOTA,PROPOSAL_PENELITIAN FROM penelitian p JOIN (SELECT a.ID_PENELITIAN,a.NIP,d.NIDN, d.NAMA_DOSEN FROM `anggota` a JOIN dosen d ON a.NIP=d.NIP) as n ON p.ID_PENELITIAN=n.ID_PENELITIAN LEFT JOIN dosen on p.NIP=dosen.NIP JOIN jenis_penelitian jp ON p.ID_JENIS=jp.ID_JENIS WHERE STATUS_PENELITIAN>0 ".$filter." GROUP BY n.ID_PENELITIAN";
        
        $q="SELECT p.ID_PENELITIAN,DATE_FORMAT(p.CREATED_AT_PENELITIAN,'%d/%m/%Y') AS CREATED_AT_PENELITIAN,p.JUDUL_PENELITIAN,dosen.NAMA_DOSEN,NAMA_JENIS,STATUS_PENELITIAN,GROUP_CONCAT('(',n.NIDN,')' ,' <br/> ',n.NAMA_DOSEN SEPARATOR '<br/>') as ANGGOTA,LOCATION_PPEN AS PROPOSAL_PENELITIAN FROM penelitian p JOIN (SELECT a.ID_PENELITIAN,a.NIP,d.NIDN, d.NAMA_DOSEN FROM anggota_penelitian a JOIN dosen d ON a.NIP=d.NIP) as n ON p.ID_PENELITIAN=n.ID_PENELITIAN LEFT JOIN dosen on p.NIP=dosen.NIP JOIN jenis_penelitian jp ON p.ID_JENIS=jp.ID_JENIS LEFT JOIN proposal_penelitian pp ON p.ID_PPENELITIAN=pp.ID_PPEN AND p.ID_PENELITIAN=pp.ID_PENELITIAN WHERE STATUS_PENELITIAN>0 ".$filter." GROUP BY n.ID_PENELITIAN";

        $query= $this->db->query($q);
        return $query->result_array();
    }

    function getEdit($id=null){
		$filter=is_null($id)?'': ' WHERE p.ID_PENELITIAN='.$id;
        //$q="SELECT * FROM penelitian LEFT JOIN dosen ON penelitian.NIP=dosen.NIP".$filter;
        $q="SELECT p.ID_PENELITIAN,p.SUMBER_BIAYA_PENELITIAN,p.JUDUL_PENELITIAN,jp.ID_JENIS,NAMA_JENIS,GROUP_CONCAT(n.NIP,';',n.NAMA_DOSEN SEPARATOR '#') as ANGGOTA,b.ID_BIDANG,b.JENIS_BIDANG,p.BIAYA_PENELITIAN FROM penelitian p LEFT JOIN (SELECT a.ID_PENELITIAN,a.NIP,d.NAMA_DOSEN FROM anggota_penelitian a LEFT JOIN dosen d ON a.NIP=d.NIP) as n ON p.ID_PENELITIAN=n.ID_PENELITIAN LEFT JOIN dosen on p.NIP=dosen.NIP LEFT JOIN bidang b ON p.ID_BIDANG=b.ID_BIDANG LEFT JOIN jenis_penelitian jp ON p.ID_JENIS=jp.ID_JENIS".$filter." GROUP BY n.ID_PENELITIAN";
        $query= $this->db->query($q);
        return $query->row_array();
    }
	
    function insert($data,$reqId=false,$table='penelitian')
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        
        if($this->db->affected_rows()>0){
            if($reqId) 
                return $insert_id;
            else 
                return true;
        }
        else
            return false;
    }
    
    function update($data,$where,$table='penelitian')
    {
        $this->db->where($where);
        $this->db->update($table,$data);
        
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }

    function delAnggota($where,$table='anggota_penelitian'){
        $this->db->where($where);
        $this->db->delete($table);
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }
    

}