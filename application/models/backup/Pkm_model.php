<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkm_model extends CI_Model {
    private $table = 'pkm';
    var $column_order = array(null,'JUDUL_PKM'); //field yang ada di table user
    var $column_search = array('JUDUL_PKM'); //field yang diizin untuk pencarian 
    var $order = array('p.ID_PKM' => 'ASC'); // default order 
    private  $joinTable="(SELECT a.ID_PKM,a.NIP,d.NIDN, d.NAMA_DOSEN FROM anggota_pkm a JOIN dosen d ON a.NIP=d.NIP) as n";
    private $joinFilter="p.ID_PKM=n.ID_PKM";
    /*
    Status : 
    0=dibatalkan, hak user
    1= belum disetujui, default
    2= perlu revisi, hak admin
    3= sudah direvisi, hak user
    4= disetujui, hak admin
    ditolak? (by admin)
    dibatalkan? (by proposer)
     */
    function __construct() {
        
    }

    function allowReg($nip,$year){
        //$year=$year-1;
        $this->db->select("p.NIP,p.JUDUL_PENELITIAN");
        $this->db->from("penelitian p");
        $this->db->where(array("NIP"=>$nip,"YEAR(CREATED_AT_PENELITIAN)"=>$year, "STATUS_PENELITIAN" => '!= 0' ));
        $query1 = $this->db->get_compiled_select();

        $this->db->select("p.NIP,p.JUDUL_PKM");
        $this->db->from("pkm p");
        $this->db->where(array("NIP"=>$nip,"YEAR(CREATED_AT_PKM)"=>$year, "STATUS_PKM" => '!= 0' ));
        $query2 = $this->db->get_compiled_select();
        $query = $this->db->query($query1 . ' UNION ' . $query2);

        //$query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query($where=null)
    {
       
        $this->db->select("DATE_FORMAT(CREATED_AT_PKM,'%d-%m-%Y') AS TANGGAL, p.ID_PKM,p.JUDUL_PKM,dosen.NAMA_DOSEN,NAMA_JENIS,STATUS_PKM,GROUP_CONCAT('(',n.NIDN,')' ,' <br/> ',n.NAMA_DOSEN SEPARATOR '<br/>') as ANGGOTA,LOCATION_PPKM AS PROPOSAL_PKM");
        $this->db->from($this->table.' AS p');
        $this->db->join($this->joinTable,$this->joinFilter,'left');
        $this->db->join('dosen','p.NIP=dosen.NIP','left');
        $this->db->join('jenis_penelitian jp','p.ID_JENIS=jp.ID_JENIS','left');
        $this->db->join('proposal_pkm pp','p.ID_PPKM=pp.ID_PPKM AND p.ID_PKM=pp.ID_PKM','left');
        $this->db->join('(SELECT ID_PKM,COUNT(*) as jumlah FROM proposal_pkm GROUP BY ID_PKM) AS ptot','p.ID_PKM=ptot.ID_PKM','left');
        $this->db->where(array('STATUS_PKM'>0));
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if(isset($_POST['search']['value'])) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by("p.ID_PKM");
    }

    function getDatas($where=array()){
        $this->_get_datatables_query();
        //$length=isset($_POST['length'])? $_POST['length']:10;
        //$start=isset($_POST['start'])? $_POST['start'] : 0;
        //if($length==-1) $length=10;
        
        if(isset($_POST['length']))
            if($_POST['length'] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);
            else
                $this->db->limit(10, 0);
        else
            $this->db->limit(10, 0);
        //$this->db->limit($length, $start);
        $this->db->where($where);
        $query = $this->db->get();
        
        return $query->result();
    }

    function count_filtered($where=array())
    {   
        $this->_get_datatables_query();
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($where=array())
    {
        $this->db->select("*");
        $this->db->from($this->table.' AS p');
        $this->db->join($this->joinTable,$this->joinFilter,'left');
        $this->db->join('dosen','p.NIP=dosen.NIP','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    function getData($id=null,$customFilter=''){
        //belum anggota
        $filter=is_null($id)?'': ' AND p.ID_PENELITIAN='.$id;
        $filter.=$customFilter;
        //$q="SELECT ID_PENELITIAN,JUDUL_PENELITIAN,JENIS_PENELITIAN,NAMA_DOSEN,'-' AS ANGGOTA,STATUS_PENELITIAN FROM penelitian LEFT JOIN dosen on penelitian.NIP=dosen.NIP".$filter;
        $q="SELECT p.ID_PENELITIAN,p.JUDUL_PENELITIAN,dosen.NAMA_DOSEN,NAMA_JENIS,STATUS_PENELITIAN,GROUP_CONCAT('(',n.NIDN,')' ,' <br/> ',n.NAMA_DOSEN SEPARATOR '<br/>') as ANGGOTA,PROPOSAL_PENELITIAN FROM penelitian p JOIN (SELECT a.ID_PENELITIAN,a.NIP,d.NIDN, d.NAMA_DOSEN FROM `anggota` a JOIN dosen d ON a.NIP=d.NIP) as n ON p.ID_PENELITIAN=n.ID_PENELITIAN LEFT JOIN dosen on p.NIP=dosen.NIP JOIN jenis_penelitian jp ON p.ID_JENIS=jp.ID_JENIS WHERE STATUS_PENELITIAN>0 ".$filter." GROUP BY n.ID_PENELITIAN";
        
        $query= $this->db->query($q);
        return $query->result_array();
    }

    function getEdit($id=null){
		$filter=is_null($id)?'': ' WHERE p.ID_PKM='.$id;
        //$q="SELECT * FROM penelitian LEFT JOIN dosen ON penelitian.NIP=dosen.NIP".$filter;
        $q="SELECT p.ID_PKM,p.JUDUL_PKM,p.MITRA_PKM,b.ID_BIDANG,b.JENIS_BIDANG,GROUP_CONCAT(n.NIP,';',n.NAMA_DOSEN,';',n.ID_BIDANG,';',n.JENIS_BIDANG SEPARATOR '#') as ANGGOTA,p.MAHASISWA_PKM,jp.ID_JENIS,NAMA_JENIS,p.BIAYA_PKM,p.SUMBER_BIAYA_PKM,p.OUTPUT_PKM,p.ID_KOTA,NAMA_KOTA,WIL_MITRA_PKM,JARAK_PKM,PELAKSANAAN_PKM FROM pkm p JOIN (SELECT a.ID_PKM,a.NIP,d.NAMA_DOSEN,a.ID_BIDANG,JENIS_BIDANG FROM anggota_pkm a JOIN dosen d ON a.NIP=d.NIP JOIN bidang ON a.ID_BIDANG=bidang.ID_BIDANG) as n ON p.ID_PKM=n.ID_PKM LEFT JOIN dosen on p.NIP=dosen.NIP LEFT JOIN bidang b ON p.ID_BIDANG=b.ID_BIDANG JOIN jenis_penelitian jp ON p.ID_JENIS=jp.ID_JENIS JOIN kota ON p.ID_KOTA=kota.ID_KOTA".$filter."  GROUP BY n.ID_PKM";
        $query= $this->db->query($q);
        return $query->row_array();
    }
	
    function insert($data,$reqId=false,$table='pkm')
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        
        if($this->db->affected_rows()>0){
            if($reqId) 
                return $insert_id;
            else 
                return true;
        }
        else
            return false;
    }
    
    function update($data,$where,$table='pkm')
    {
        $this->db->where($where);
        $this->db->update($table,$data);
        
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }

    function delAnggota($where,$table='anggota'){
        $this->db->where($where);
        $this->db->delete($table);
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }
    

}