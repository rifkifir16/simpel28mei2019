<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap_model extends CI_Model
{

	function data($where)
	{
		$this->db->select("NO_SP_SPPD,DATE_FORMAT(TGL_NOTA_SPPD,'%d-%m-%Y') AS TGL_NOTA_SPPD,NAMA_PEG,NO_JMK_SPPD,DATE_FORMAT(TGL_JMK_SPPD,'%d-%m-%Y') AS TGL_JMK_SPPD,NO_SPPD,DATE_FORMAT(TGL_SPPD,'%d-%m-%Y') AS TGL_SPPD,DATE_FORMAT(TGL_BRKT_SPPD,'%d-%m-%Y') AS TGL_BRKT_SPPD,DATE_FORMAT(TGL_KMBL_SPPD,'%d-%m-%Y') AS TGL_KMBL_SPPD,DATEDIFF(sppd.TGL_KMBL_SPPD,sppd.TGL_BRKT_SPPD)+1 AS LAMA_SPPD,NAMA_LOKASI,TUJUAN_SPPD,JENIS_SPPD,PEMK_SPPD,TRANSPORT_SPPD,(UNIT_PESAWAT_SPPD*BIAYA_PESAWAT_SPPD)+(UNIT_BUS_SPPD*BIAYA_BUS_SPPD) AS BIAYA_SPPD,(UNIT_TLOKAL_SPPD*BIAYA_TLOKAL_SPPD) AS TLOKAL_SPPD,TAX_BANDARA_SPPD,BIAYA_HARIAN_SPPD");
		$this->db->from('sppd');
		$this->db->join('pegawai', 'pegawai.NIP_PEG=sppd.NIP_PEG');
		$this->db->join('lokasi', 'sppd.ASAL_SPPD=lokasi.ID_LOKASI');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}


	function lokasi($where, $table = 'lokasi')
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row_array();
	}
}
