<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sppd_model extends CI_Model {

    private $table = 'sppd s';
    var $column_order = array('NO_SPPD','TGL_NOTA_SPPD','NAMA_PEG'); 
    var $column_search = array('NO_SPPD',"DATE_FORMAT(TGL_SPPD,'%d-%m-%Y')",'NAMA_PEG'); 
    var $order = array('ID_SPPD' => 'ASC'); 

    private function _get_datatables_query($where=null)
    {
        $this->db->select("ID_SPPD, NO_SPPD, DATE_FORMAT(TGL_SPPD,'%d-%m-%Y') AS TGL_SPPD, NAMA_PEG, FOTO_SPPD, STATUS_SPPD");
        $this->db->from($this->table);
        $this->db->join('pegawai','s.NIP_PEG=pegawai.NIP_PEG');
        $this->db->order_by('ID_SPPD');
        $i = 0;
     
        foreach ($this->column_search as $item) {
            if(isset($_POST['search']['value'])) {
                if($i===0) {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getData($id=null,$where=array(),$start=0,$limit=10){
        $this->_get_datatables_query();
        if(isset($_POST['length']))
            if($_POST['length'] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);
            else
                $this->db->limit(10, 0);
        else
            $this->db->limit(10, 0);
            
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($where=array())
    {        
        $this->_get_datatables_query();
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($where=array())
    {
        $this->db->from($this->table)->where($where);
        return $this->db->count_all_results();
    }

    function ajaxData($q){
        $this->db->select('NIP_PEG as id,NAMA_PEG as text');
        $this->db->limit(10);
        $this->db->from('pegawai');
        $this->db->where(array('KERRES_PEG >'=>'Bekerja'));
        $this->db->like('NAMA_PEG',$q);
        $query=$this->db->get();
        return $query->result_array();
    }

    function check($where,$table='pegawai'){
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function singleData($where){
        $this->db->select("s.*,p.*,l.*,l1.NAMA_LOKASI as TUJUAN_LOKASI,l2.NAMA_LOKASI as PEMK_LOKASI");
        $this->db->from('sppd s');
        $this->db->join('pegawai p','p.NIP_PEG=s.NIP_PEG');
        $this->db->join('lokasi l','l.ID_LOKASI=s.ASAL_SPPD');
        $this->db->join('lokasi l1','l1.ID_LOKASI=s.TUJUAN_SPPD');
        $this->db->join('lokasi l2','l2.ID_LOKASI=s.PEMK_SPPD');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detail($where){
        $this->db->select("s.ID_SPPD,s.NIP_PEG,s.NO_SPPD,s.TGL_NOTA_SPPD,s.NO_JMK_SPPD,s.JENIS_SPPD,s.TGL_SPPD,s.URAIAN_SPPD,s.TRANSPORT_SPPD,TGL_BRKT_SPPD,s.TGL_KMBL_SPPD,DATEDIFF(s.TGL_KMBL_SPPD,s.TGL_BRKT_SPPD) AS LAMA_SPPD,(SELECT KODE_LOKASI FROM lokasi WHERE ID_LOKASI=s.ASAL_SPPD) as KODE_LOKASI,(SELECT NAMA_LOKASI FROM lokasi WHERE ID_LOKASI=s.ASAL_SPPD) as ASAL_SPPD,(SELECT NAMA_LOKASI FROM lokasi WHERE ID_LOKASI=s.TUJUAN_SPPD) AS TUJUAN_SPPD,s.UNIT_PESAWAT_SPPD,s.BIAYA_PESAWAT_SPPD,s.UNIT_BUS_SPPD,s.BIAYA_BUS_SPPD,s.UNIT_TLOKAL_SPPD,s.BIAYA_TLOKAL_SPPD,s.TAX_BANDARA_SPPD,s.BIAYA_HARIAN_SPPD,s.TOTAL_BIAYA_SPPD,s.STATUS_SPPD,p.NAMA_PEG,NAMA_KUALIFIKASI,j.NAMA_JABATAN");
        $this->db->from('sppd s');
        $this->db->join('pegawai p','p.NIP_PEG=s.NIP_PEG');
        $this->db->join('jabatan j','p.ID_JABATAN=j.ID_JABATAN');
        $this->db->join('kualifikasi k','p.ID_KUALIFIKASI=k.ID_KUALIFIKASI');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row_array();
    }

    function rincian($where){
        $this->db->select("*");
        $this->db->from('rincian_sppd');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function cetak($where){
        $this->db->select("sppd.ID_SPPD,sppd.NIP_PEG,sppd.NO_SPPD,sppd.TGL_NOTA_SPPD,sppd.NO_JMK_SPPD,sppd.JENIS_SPPD,sppd.TGL_SPPD,sppd.URAIAN_SPPD,sppd.TRANSPORT_SPPD,TGL_BRKT_SPPD,sppd.TGL_KMBL_SPPD,DATEDIFF(sppd.TGL_KMBL_SPPD,sppd.TGL_BRKT_SPPD) AS LAMA_SPPD,(SELECT KODE_LOKASI FROM lokasi WHERE ID_LOKASI=sppd.ASAL_SPPD) as KODE_LOKASI,(SELECT NAMA_LOKASI FROM lokasi WHERE ID_LOKASI=sppd.ASAL_SPPD) as ASAL_SPPD,(SELECT NAMA_LOKASI FROM lokasi WHERE ID_LOKASI=sppd.TUJUAN_SPPD) AS TUJUAN_SPPD,sppd.UNIT_PESAWAT_SPPD,sppd.BIAYA_PESAWAT_SPPD,sppd.UNIT_BUS_SPPD,sppd.BIAYA_BUS_SPPD,sppd.UNIT_TLOKAL_SPPD,sppd.BIAYA_TLOKAL_SPPD,sppd.TAX_BANDARA_SPPD,sppd.BIAYA_HARIAN_SPPD,sppd.TOTAL_BIAYA_SPPD,sppd.STATUS_SPPD,sppd.BIAYA_OLEH_SPPD,sppd.OTORISASI_SPPD,pegawai.NAMA_PEG,NAMA_KUALIFIKASI,jabatan.NAMA_JABATAN");
        $this->db->from("sppd");
        $this->db->join("pegawai","sppd.NIP_PEG=pegawai.NIP_PEG");
        $this->db->join("jabatan","pegawai.ID_JABATAN=jabatan.ID_JABATAN");
        $this->db->join("kualifikasi","pegawai.ID_KUALIFIKASI=kualifikasi.ID_KUALIFIKASI");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row_array();
    }

    function insert($data,$reqId=false,$table='pegawai')
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        
        if($this->db->affected_rows()>0){
            if($reqId) 
                return $insert_id;
            else 
                return true;
        }
        else
            return false;
    }
    
    function update($data,$where,$table='pegawai')
    {
        $this->db->where($where);
        $this->db->update($table,$data);
        
        if($this->db->affected_rows()>0)
            return true;
        else
            return false;
    }
    

}