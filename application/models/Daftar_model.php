<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Daftar_model extends CI_Model
{

	function __construct()
	{ }
	function check($where)
	{
		$this->db->where($where);
		return $this->db->get('pelamar')->num_rows();
	}

	function tambahPelamar($createdPath = '')
	{
		$_input = $this->input->post();
		$dbPelamar = array('ID_KOTA', 'ID_AGAMA', 'POSISI_PELAMAR', 'NAMA_PELAMAR', 'TPT_LAHIR_PELAMAR', 'TGL_LAHIR_PELAMAR', 'STATUS_PELAMAR', 'JK_PELAMAR', 'ALAMAT_PELAMAR', 'NOKTP_PELAMAR', 'NOTELP_PELAMAR', 'TKTPENDTERAKHIR_PELAMAR', 'SPESIALISASI_PELAMAR', 'KEAHLIAN_PELAMAR', 'BIDSTUDFAV_PELAMAR', 'JDLSKRIPSI_PELAMAR', 'TB_PELAMAR', 'BB_PELAMAR', 'HOBI_PELAMAR', 'OLAHRAGA_PELAMAR', 'SAKIT_PELAMAR', 'TGLSAKIT_PELAMAR', 'JNSSAKIT_PELAMAR', 'KECELAKAAN_PELAMAR', 'TGLKEC_PELAMAR', 'JNSKEC_PELAMAR', 'AKIBATKEC_PELAMAR', 'TERLARANG_PELAMAR', 'KEPOLISIAN_PELAMAR', 'KETKEPOL_PELAMAR', 'ASTEK_PELAMAR', 'NOASTEK_PELAMAR', 'SIM_PELAMAR', 'NOSIM_PELAMAR', 'EMAIL_PELAMAR');

		$fieldPelamar = array('domisili', 'agama', 'posisi', 'nama', 'kotalahir', 'tgllhr', 'sn', 'jk', 'alamat', 'ktp', 'telp', 'pendidikan', 'spesialisasi', 'keahlian', 'bidstud', 'judul', 'tb', 'bb', 'hobi', 'or', 'sakit', 'tglsakit', 'sakitapa', 'kecelakaan', 'tglkecelakaan', 'kecelakaanapa', 'akibatkecelakaan', 'terlarang', 'kepolisian', 'jelaskepol', 'astek', 'noastek', 'sim', 'nosim', 'email');

		$data = array();
		$i = 0;
		for ($i = 0; $i < count($dbPelamar); $i++) {
			$data[$dbPelamar[$i]] = $_input[$fieldPelamar[$i]];
		}

		$data['FOTO_PELAMAR'] = $createdPath;
		return $this->insert($data, true, 'pelamar');
		//return id pelamar
	}

	function tambahKeluarga($id)
	{
		$_input = $this->input->post();
		$jumlah = count($_input['kel']);
		$saved = 0;
		for ($i = 0; $i < $jumlah; $i++) {
			$data = array();
			$data['ID_PELAMAR'] = $id;
			$data['NAMAKEL_PELAMAR'] = $_input['kel'][$i];
			$data['ISIKEL_PELAMAR'] = $_input['nmkel'][$i];
			$data['JKKEL_PELAMAR'] = $_input['jkkel'][$i];
			$data['TPTLAHIRKEL_PELAMAR'] = $_input['tptlhrkel'][$i];
			$data['TGLLAHIRKEL_PELAMAR'] = $_input['tgllhrkel'][$i];
			$data['ALAMATKEL_PELAMAR'] = $_input['jobkel'][$i];
			$data['JOBKEL_PELAMAR'] = $_input['alamatkel'][$i];

			if ($this->insert($data, false, 'keluarga_pelamar')) {
				$saved++;
			}
		}
		if ($saved >= $jumlah)
			return true;
	}

	function tambahRelasi($id)
	{
		$_input = $this->input->post();
		$jumlah = count($_input['namarelasi']);
		$saved = 0;
		for ($i = 0; $i < $jumlah; $i++) {
			$data = array();
			$data['ID_PELAMAR'] = $id;
			$data['NAMA_KENALAN'] = $_input['namarelasi'][$i];
			$data['JABATAN_KENALAN'] = $_input['jabatanrelasi'][$i];
			$data['INSTANSI_KENALAN'] = $_input['instansirelasi'][$i];
			$data['HUBUNGAN_KENALAN'] = $_input['hubunganrelasi'][$i];

			if ($this->insert($data, false, 'kenalan_pelamar')) {
				$saved++;
			}
		}
		if ($saved >= $jumlah)
			return true;
	}

	function tambahBahasa($id)
	{
		$_input = $this->input->post();
		$jumlah = count($_input['bahasa']);
		$saved = 0;
		for ($i = 0; $i < $jumlah; $i++) {
			$data = array();
			$data['ID_PELAMAR'] = $id;
			$data['NAMA_BAHASA'] = $_input['bahasa'][$i];
			$data['KECAKAPAN_BAHASA'] = $_input['kecbahasa'][$i];

			if ($this->insert($data, false, 'bahasa_pelamar')) {
				$saved++;
			}
		}
		if ($saved >= $jumlah)
			return true;
	}

	function tambahRiwayat($id)
	{
		$_input = $this->input->post();
		$jumlah = count($_input['namasekolah']);
		$saved = 0;
		for ($i = 0; $i < $jumlah; $i++) {
			$data = array();
			$data['ID_PELAMAR'] = $id;
			$data['SEKOLAHPEND_PELAMAR'] = $_input['namasekolah'][$i];
			$data['KOTAPEND_PELAMAR'] = $_input['kotasekolah'][$i];
			$data['SMPKELAS_PELAMAR'] = $_input['kelassekolah'][$i];
			$data['DARISMPTHPEND_PELAMAR'] = $_input['tahunsekolah'][$i];
			$data['JURUSANPEND_PELAMAR'] = $_input['jurusansekolah'][$i];
			$data['THIJAZAHPEND_PELAMAR'] = $_input['tahunijazah'][$i];

			if ($this->insert($data, false, 'pendidikan_pelamar')) {
				$saved++;
			}
		}
		if ($saved >= $jumlah)
			return true;
	}

	function tambahPengalaman($id)
	{
		$_input = $this->input->post();
		$jumlah = count($_input['namainstansi']);
		$saved = 0;
		for ($i = 0; $i < $jumlah; $i++) {
			$data = array();
			$data['ID_PELAMAR'] = $id;
			$data['INSTANSI_PENGALAMAN'] = $_input['namainstansi'][$i];
			$data['JABATAN_PENGALAMAN'] = $_input['jabataninstansi'][$i];
			$data['DRSMPTH_PENGALAMAN'] = $_input['darismpthinstansi'][$i];
			$data['GAJI_PENGALAMAN'] = $_input['gajiinstansi'][$i];
			$data['TUNJANGAN_PENGALAMAN'] = $_input['tunjanganinstansi'][$i];
			$data['NEGARA_PENGALAMAN'] = $_input['negarainstansi'][$i];
			$data['ALASAN_PENGALAMAN'] = $_input['ketinstansi'][$i];

			if ($this->insert($data, false, 'pengalaman_pelamar')) {
				$saved++;
			}
		}
		if ($saved >= $jumlah)
			return true;
	}

	function tambahReferensi($id)
	{
		$_input = $this->input->post();
		$jumlah = count($_input['namareferensi']);
		$saved = 0;

		for ($i = 0; $i < $jumlah; $i++) {
			$data = array();
			$data['ID_PELAMAR'] = $id;
			$data['NAMA_REFERENSI'] = $_input['namareferensi'][$i];
			$data['ALAMAT_REFERENSI'] = $_input['alamatreferensi'][$i];
			$data['TELP_REFERENSI'] = $_input['telpreferensi'][$i];
			$data['JABATAN_REFERENSI'] = $_input['jabatanreferensi'][$i];
			$data['HUBUNGAN_REFERENSI'] = $_input['hubunganreferensi'][$i];

			if ($this->insert($data, false, 'referensi_pelamar')) {
				$saved++;
			}
		}
		if ($saved >= $jumlah)
			return true;
	}

	function tambahOrganisasi($id)
	{
		$_input = $this->input->post();
		$jumlah = count($_input['namaorganisasi']);
		$saved = 0;

		for ($i = 0; $i < $jumlah; $i++) {
			$data = array();
			$data['ID_PELAMAR'] = $id;
			$data['NAMA_ORGANISASI'] = $_input['namaorganisasi'][$i];
			$data['LOKASI_ORGANISASI'] = $_input['lokasiorganisasi'][$i];
			$data['SEBAGAI_ORGANISASI'] = $_input['sebagaiorganisasi'][$i];

			if ($this->insert($data, false, 'organisasi_pelamar')) {
				$saved++;
			}
		}
		if ($saved >= $jumlah)
			return true;
	}

	function TambahLain($id)
	{
		$_input = $this->input->post();

		$data = array();
		$data['ID_PELAMAR'] = $id;
		$data['TUNJANGAN_KETERANGAN'] = $_input['tunjangan'];
		$data['SUMBER_KETERANGAN'] = $_input['sumbertunjangan'];
		$data['GAJIHARAPAN_KETERANGAN'] = $_input['gaji'];
		$data['FASILITASHARAPAN_KETERANGAN'] = $_input['fasilitas'];
		$data['BERSEDIATEMPAT_KETERANGAN'] = $_input['alllokasi'];
		$data['BERSEDIAATURAN_KETERANGAN'] = $_input['allrule'];
		$data['KET_KETERANGAN'] = $_input['keterangan'];

		return $this->insert($data, false, 'keterangan_pelamar');
	}

	function insert($data, $reqId = false, $table = 'keluarga_peg')
	{
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		if ($this->db->affected_rows() > 0) {
			if ($reqId)
				return $insert_id;
			else
				return true;
		} else
			return false;
	}
}
