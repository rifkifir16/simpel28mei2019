<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

    function __construct() {
        
    }

    function userCheck($where){
		
		$this->db->select("*");
        $this->db->from('user');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row_array();
		
    }
	
	function homeDosen($where,$table){
		$this->db->select("*");
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->num_rows();
	}
	
	function homeAdmin(){
		$this->db->select("NIP");
        $this->db->from('dosen');
        $query = $this->db->get();
		$rows=$query->num_rows();
		
		$this->db->select("USERNAME_USER");
		$this->db->from('user');
		$this->db->where('USERNAME_USER NOT IN (SELECT NIP FROM dosen)', NULL, FALSE);
		$sub=$this->db->get();
		$subs=$sub->num_rows();
		
        return array($rows,$subs);
	}
	
    
    

}