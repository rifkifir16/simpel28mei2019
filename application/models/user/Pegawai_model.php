<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{

	private $table = 'pegawai';
	var $column_order = array('NIP_PEG', 'NAMA_PEG', null, null);
	var $column_search = array('NIP_PEG', 'NAMA_PEG');
	var $order = array('NIP_PEG' => 'ASC');

	private function _get_datatables_query($where = null)
	{
		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column_search as $item) {
			if (isset($_POST['search']['value'])) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getData($id = null, $where = array(), $start = 0, $limit = 10)
	{
		$this->_get_datatables_query();
		if (isset($_POST['length']))
			if ($_POST['length'] != -1)
				$this->db->limit($_POST['length'], $_POST['start']);
			else
				$this->db->limit(10, 0);
		else
			$this->db->limit(10, 0);

		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($where = array())
	{
		$this->_get_datatables_query();
		$this->db->where($where);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($where = array())
	{
		$this->db->from($this->table)->where($where);
		return $this->db->count_all_results();
	}

	function ajaxData($q)
	{
		$this->db->select('NIP_PEG as id,NAMA_PEG as text');
		$this->db->limit(10);
		$this->db->from('pegawai');
		$this->db->where(array('KERRES_PEG' => 'Bekerja'));
		$this->db->like('NAMA_PEG', $q);
		$query = $this->db->get();
		return $query->result_array();
	}

	function check($where, $table = 'pegawai')
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row_array();
	}

	function singleData($where)
	{
		$this->db->select("p.*,k.*,kx.ID_KOTA kidkota,kx.NAMA_KOTA knkota,a.ID_AGAMA,a.NAMA_AGAMA,ku.ID_KUALIFIKASI,ku.NAMA_KUALIFIKASI,j.ID_JABATAN,j.NAMA_JABATAN,t.ID_TARIF,t.UPAH_TARIF,t.TUMUM_TARIF,t.TMAKAN_TARIF,t.TKOMUNIKASI_TARIF,t.JUMLAH_TARIF,t.HSATUAN_TARIF");
		$this->db->from('pegawai p');
		$this->db->join('kota k', 'p.ID_KOTA=k.ID_KOTA');
		$this->db->join('kota kx', 'p.TPT_LAHIR_PEG=kx.ID_KOTA');
		$this->db->join('agama a', 'p.ID_AGAMA=a.ID_AGAMA');
		$this->db->join('kualifikasi ku', 'p.ID_KUALIFIKASI=ku.ID_KUALIFIKASI');
		$this->db->join('jabatan j', 'p.ID_JABATAN=j.ID_JABATAN');
		$this->db->join('tarif t', 'p.NIP_PEG=t.NIP_PEG', 'LEFT');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row_array();
	}
	function tarif($where)
	{
		$this->db->from('tarif');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row_array();
	}

	function insert($data, $reqId = false, $table = 'pegawai')
	{
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		if ($this->db->affected_rows() > 0) {
			if ($reqId)
				return $insert_id;
			else
				return true;
		} else
			return false;
	}

	function update($data, $where, $table = 'pegawai')
	{

		$this->db->update($table, $data, $where);
		return $this->db->last_query();
		// if($this->db->affected_rows()>0)
		//     return true;
		// else
		//     return false;
	}
}
