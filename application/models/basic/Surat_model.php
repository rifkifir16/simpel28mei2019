<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Surat_model extends CI_Model
{

	private $table = 'surat';
	var $column_order = array(null, 'NO_SURAT', 'TANGGAL', 'PERIHAL', null);
	var $column_search = array('NO_SURAT', 'TANGGAL', 'PERIHAL');
	var $order = array('TANGGAL' => 'ASC');

	private function _get_datatables_query($where = null)
	{
		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column_search as $item) {
			if (isset($_POST['search']['value'])) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getData($id = null, $where = array(), $start = 0, $limit = 10)
	{
		$this->_get_datatables_query();
		if (isset($_POST['length']))
			if ($_POST['length'] != -1)
				$this->db->limit($_POST['length'], $_POST['start']);
			else
				$this->db->limit(10, 0);
		else
			$this->db->limit(10, 0);

		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($where = array())
	{
		$this->_get_datatables_query();
		$this->db->where($where);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($where = array())
	{
		$this->db->from($this->table)->where($where);
		return $this->db->count_all_results();
	}

	function ajaxData($q)
	{
		$this->db->select('ID_KUALIFIKASI as id,NAMA_KUALIFIKASI as text');
		$this->db->limit(10);
		$this->db->from('kualifikasi');
		$this->db->where(array('STATUS_KUALIFIKASI >' => '0'));
		$this->db->like('NAMA_KUALIFIKASI', $q);
		$query = $this->db->get();
		return $query->result_array();
	}

	function insert($data, $reqId = false, $table)
	{
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		if ($this->db->affected_rows() > 0) {
			if ($reqId)
				return $insert_id;
			else
				return true;
		} else
			return false;
	}

	function update($data, $where, $table = 'kualifikasi')
	{
		$this->db->where($where);
		$this->db->update($table, $data);

		if ($this->db->affected_rows() > 0)
			return true;
		else
			return false;
	}
	function detail($where)
	{
		$this->db->where($where);
		$this->db->from('surat');
		return $this->db->get()->result_array();
	}
	function detailtenagaahli($where)
	{
		$this->db->select('tenagaahli.*,jabatan.NAMA_JABATAN');
		$this->db->where($where);
		$this->db->from('tenagaahli');
		$this->db->join('jabatan', 'jabatan.ID_JABATAN=tenagaahli.ID_JABATAN');
		// $this->db->join('kualifikasi', 'kualifikasi.ID_KUALIFIKASI=tenagaahli.ID_KUALIFIKASI');
		return $this->db->get()->result_array();
	}
	function updatetenagaahli($where, $data)
	{
		$this->db->where($where);
		$this->db->update('tenagaahli', $data);

		if ($this->db->affected_rows() > 0)
			return true;
		else
			return false;
	}
}
