<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penggajian_model extends CI_Model
{

	private $table = 'gaji';
	var $column_order = array(null, null, null, 'NAMA_PEG', 'BULAN_GAJI', 'TAHUN_GAJI');
	var $column_search = array('NAMA_PEG', 'BULAN_GAJI', 'TAHUN_GAJI');
	var $order = array('BULAN_GAJI' => 'ASC');

	private function _get_datatables_query($where = null)
	{
		$this->db->select("ID_GAJI
		,NAMA_LOKASI
		,gaji.ID_LOKASI as ID_LOKASI
		,gaji.NIP_PEG
		,NAMA_PEG
		,BULAN_GAJI
		,TAHUN_GAJI
		,(JUMHADIR_GAJI/JUMHARI_GAJI) AS MM
		,INDEX_GAJI
		,PPH_GAJI
		,JAMSOSTEK_GAJI
		,THP_GAJI
		,TGLAP_GAJI
		,KODE_LOKASI
		,JUMHARI_GAJI
		,JUMHADIR_GAJI
		,COR_GAJI
		,KODE_LOKASI
		,NAMA_PROP
		,NAMA_JABATAN
		,NAMA_KUALIFIKASI
		,HARGA_KUALIFIKASI
		,KUALIFIKASI_GAJI
		,NKUALIFIKASI_GAJI
		,NKUALIFIKASI_GAJI * (JUMHADIR_GAJI/JUMHARI_GAJI) AS PEMBAYARAN");
		$this->db->from($this->table);
		$this->db->join('pegawai', 'pegawai.NIP_PEG=gaji.NIP_PEG');
		$this->db->join('lokasi', 'gaji.ID_LOKASI=lokasi.ID_LOKASI');
		$this->db->join('propinsi', 'propinsi.ID_PROP=lokasi.ID_PROP');
		$this->db->join('jabatan', 'jabatan.ID_JABATAN=pegawai.ID_JABATAN');
		$this->db->join('kualifikasi', 'kualifikasi.ID_KUALIFIKASI=pegawai.ID_KUALIFIKASI');
		$i = 0;

		foreach ($this->column_search as $item) {
			if (isset($_POST['search']['value'])) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getData($id = null, $where = array(), $start = 0, $limit = 10)
	{
		$this->_get_datatables_query();
		if (isset($_POST['length']))
			if ($_POST['length'] != -1)
				$this->db->limit($_POST['length'], $_POST['start']);
			else
				$this->db->limit(10, 0);
		else
			$this->db->limit(10, 0);

		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($where = array())
	{
		$this->_get_datatables_query();
		$this->db->where($where);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($where = array())
	{
		$this->db->from($this->table)->where($where);
		return $this->db->count_all_results();
	}

	function data($where)
	{
		$this->db->select("ID_GAJI,NAMA_PEG,BULAN_GAJI,TAHUN_GAJI,JUMHADIR_GAJI/JUMHARI_GAJI AS MM,INDEX_GAJI,PPH_GAJI,JAMSOSTEK_GAJI,THP_GAJI,TGLAP_GAJI,KODE_LOKASI");
		$this->db->from('gaji');
		$this->db->join('pegawai', 'pegawai.NIP_PEG=gaji.NIP_PEG');
		$this->db->join('lokasi', 'gaji.ID_LOKASI=lokasi.ID_LOKASI');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}

	function update($data, $where, $table = 'gaji')
	{
		$this->db->where($where);
		$this->db->update($table, $data);

		if ($this->db->affected_rows() > 0)
			return true;
		else
			return false;
	}
	function lokasi($where)
	{
		$this->db->select('propinsi.INDEX_PROP');
		$this->db->from('lokasi');
		$this->db->where($where);
		$this->db->join('propinsi', 'lokasi.ID_PROP=propinsi.ID_PROP');
		$query = $this->db->get();
		return $query->result_array();
	}
	function kualifikasi($where)
	{
		$this->db->select('HARGA_KUALIFIKASI,NAMA_KUALIFIKASI');
		$this->db->from('kualifikasi');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	function pegawai($where)
	{
		$this->db->select('tarif.*, kualifikasi.NAMA_KUALIFIKASI, kualifikasi.HARGA_KUALIFIKASI');
		$this->db->from('pegawai');
		$this->db->where($where);
		$this->db->join('tarif', 'pegawai.NIP_PEG=tarif.NIP_PEG');
		$this->db->join('kualifikasi', 'pegawai.ID_KUALIFIKASI=kualifikasi.ID_KUALIFIKASI');
		$query = $this->db->get();
		return $query->result_array();
	}
	function insert($data, $table)
	{
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		if ($insert_id) {
			return true;
		} else {
			return false;
		}
	}
	function check($NIP_PEG, $BULAN_GAJI, $TAHUN_GAJI)
	{
		$this->db->from('gaji');
		$where = array(
			'NIP_PEG' => $NIP_PEG,
			'BULAN_GAJI' => $BULAN_GAJI,
			'TAHUN_GAJI' => $TAHUN_GAJI
		);
		$this->db->where($where);

		return $this->db->get()->result_array();
	}
	function datagaji($where)
	{
		$this->db->select('*');
		$this->db->from('gaji');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	function ajaxedit($where)
	{
		$this->db->select('gaji.*,pegawai.NAMA_PEG,lokasi.KODE_LOKASI');
		$this->db->from('gaji');
		$this->db->join('pegawai', 'pegawai.NIP_PEG=gaji.NIP_PEG');
		$this->db->join('lokasi', 'gaji.ID_LOKASI=lokasi.ID_LOKASI');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}
	function delete($where)
	{
		$this->db->where($where);
		$delete = $this->db->delete('gaji');
		return $delete;
	}
	function dataexcel($where)
	{
		$this->db->select("ID_GAJI
		,NAMA_LOKASI
		,gaji.NIP_PEG
		,NAMA_PEG
		,BULAN_GAJI
		,TAHUN_GAJI
		,(JUMHADIR_GAJI/JUMHARI_GAJI) AS MM
		,INDEX_GAJI
		,PPH_GAJI
		,JAMSOSTEK_GAJI
		,THP_GAJI
		,TGLAP_GAJI
		,KODE_LOKASI
		,JUMHARI_GAJI
		,JUMHADIR_GAJI
		,COR_GAJI
		,KODE_LOKASI
		,NAMA_PROP
		,NAMA_JABATAN
		,NAMA_KUALIFIKASI
		,HARGA_KUALIFIKASI
		,UPAH_TARIF
		,TUMUM_TARIF
		,TMAKAN_TARIF
		,TKOMUNIKASI_TARIF
		,KUALIFIKASI_GAJI
		,NKUALIFIKASI_GAJI
		,NKUALIFIKASI_GAJI* (JUMHADIR_GAJI/JUMHARI_GAJI) AS PEMBAYARAN");
		$this->db->from($this->table);
		$this->db->where($where);
		$this->db->join('pegawai', 'pegawai.NIP_PEG=gaji.NIP_PEG');
		$this->db->join('tarif', 'tarif.NIP_PEG=gaji.NIP_PEG');
		$this->db->join('lokasi', 'gaji.ID_LOKASI=lokasi.ID_LOKASI');
		$this->db->join('propinsi', 'propinsi.ID_PROP=lokasi.ID_PROP');
		$this->db->join('jabatan', 'jabatan.ID_JABATAN=pegawai.ID_JABATAN');
		$this->db->join('kualifikasi', 'kualifikasi.ID_KUALIFIKASI=pegawai.ID_KUALIFIKASI');
		$this->db->order_by('NAMA_LOKASI', 'ASC');
		return $this->db->get()->result_array();
	}

	function modelChart($daerah, $tahun)
	{

		$query = $this->db->query("SELECT SUM((JUMHADIR_GAJI/JUMHARI_GAJI)) as MM, KUALIFIKASI_GAJI, BULAN_GAJI, TAHUN_GAJI, NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP WHERE TAHUN_GAJI = " . $tahun . " AND NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY  BULAN_GAJI");

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}

	function modelData($daerah, $tahun)
	{

		$query = $this->db->query("SELECT SUM((JUMHADIR_GAJI/JUMHARI_GAJI)) as MM, KUALIFIKASI_GAJI, BULAN_GAJI, TAHUN_GAJI, NAMA_PROP, gaji.ID_LOKASI, propinsi.NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP WHERE TAHUN_GAJI = " . $tahun . " AND NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY  kualifikasi_GAJI, BULAN_GAJI");


		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}
	function modelDataexcel($daerah, $tahun)
	{

		$query = $this->db->query("SELECT SUM(ROUND((JUMHADIR_GAJI/JUMHARI_GAJI),2)) as MM, KUALIFIKASI_GAJI, BULAN_GAJI, TAHUN_GAJI, NAMA_PROP, gaji.ID_LOKASI, propinsi.NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP WHERE TAHUN_GAJI = " . $tahun . " AND NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY  kualifikasi_GAJI, BULAN_GAJI");

		return $query->result_array();
	}

	function modelChart2($daerah, $tahun)
	{

		// $query = $this->db->query("SELECT SUM((HARGA_KUALIFIKASI*(JUMHADIR_GAJI/JUMHARI_GAJI))) as gaji, KUALIFIKASI_GAJI, BULAN_GAJI, TAHUN_GAJI, NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP WHERE TAHUN_GAJI = " . $tahun . " AND NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY  BULAN_GAJI");
		$query = $this->db->query("SELECT SUM((kualifikasi.HARGA_KUALIFIKASI*(gaji.JUMHADIR_GAJI/gaji.JUMHARI_GAJI))) as gaji, gaji.KUALIFIKASI_GAJI, gaji.BULAN_GAJI, gaji.TAHUN_GAJI, propinsi.NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP JOIN pegawai ON pegawai.NIP_PEG = gaji.NIP_PEG JOIN kualifikasi ON kualifikasi.ID_KUALIFIKASI = pegawai.ID_KUALIFIKASI WHERE gaji.TAHUN_GAJI = " . $tahun . " AND propinsi.NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY gaji.BULAN_GAJI");

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}

	function modelData2($daerah, $tahun)
	{
		$query = $this->db->query("SELECT SUM((kualifikasi.HARGA_KUALIFIKASI*(gaji.JUMHADIR_GAJI/gaji.JUMHARI_GAJI))) as gaji, gaji.KUALIFIKASI_GAJI, gaji.BULAN_GAJI, gaji.TAHUN_GAJI, propinsi.NAMA_PROP, gaji.ID_LOKASI FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP JOIN pegawai ON pegawai.NIP_PEG = gaji.NIP_PEG JOIN kualifikasi ON kualifikasi.ID_KUALIFIKASI = pegawai.ID_KUALIFIKASI WHERE gaji.TAHUN_GAJI = " . $tahun . " AND propinsi.NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY gaji.BULAN_GAJI, gaji.KUALIFIKASI_GAJI");


		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}
	function modelData2excel($daerah, $tahun)
	{
		$query = $this->db->query("SELECT SUM((kualifikasi.HARGA_KUALIFIKASI*(gaji.JUMHADIR_GAJI/gaji.JUMHARI_GAJI))) as gaji, gaji.KUALIFIKASI_GAJI, gaji.BULAN_GAJI, gaji.TAHUN_GAJI, propinsi.NAMA_PROP, gaji.ID_LOKASI FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP JOIN pegawai ON pegawai.NIP_PEG = gaji.NIP_PEG JOIN kualifikasi ON kualifikasi.ID_KUALIFIKASI = pegawai.ID_KUALIFIKASI WHERE gaji.TAHUN_GAJI = " . $tahun . " AND propinsi.NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY gaji.BULAN_GAJI, gaji.KUALIFIKASI_GAJI");


		return $query->result_array();
	}
	function getkualifikasi()
	{
		return $this->db->get('kualifikasi')->result();
	}

	function getlokasi()
	{
		return $this->db->get('propinsi')->result();
	}

	function modelChart3($daerah, $tahun)
	{

		$query = $this->db->query("SELECT KUALIFIKASI_GAJI, COUNT(KUALIFIKASI_GAJI) as pegawai, KUALIFIKASI_GAJI, BULAN_GAJI, TAHUN_GAJI, NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP WHERE TAHUN_GAJI = " . $tahun . " AND NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY  BULAN_GAJI");

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}

	function modelData3($daerah, $tahun)
	{
		$query = $this->db->query("SELECT KUALIFIKASI_GAJI, COUNT(KUALIFIKASI_GAJI) as pegawai, KUALIFIKASI_GAJI, BULAN_GAJI, TAHUN_GAJI, NAMA_PROP, gaji.ID_LOKASI, propinsi.NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP WHERE TAHUN_GAJI = " . $tahun . " AND NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY  kualifikasi_GAJI, BULAN_GAJI");


		if ($query->num_rows() > 0) {
			foreach ($query->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}
	function modelData3excel($daerah, $tahun)
	{
		$query = $this->db->query("SELECT KUALIFIKASI_GAJI, COUNT(KUALIFIKASI_GAJI) as pegawai, KUALIFIKASI_GAJI, BULAN_GAJI, TAHUN_GAJI, NAMA_PROP, gaji.ID_LOKASI, propinsi.NAMA_PROP FROM gaji JOIN lokasi ON lokasi.ID_LOKASI = gaji.ID_LOKASI JOIN propinsi ON lokasi.ID_PROP = propinsi.ID_PROP WHERE TAHUN_GAJI = " . $tahun . " AND NAMA_PROP LIKE '%" . $daerah . "%' GROUP BY  kualifikasi_GAJI, BULAN_GAJI");
		return $query->result_array();
	}
}
