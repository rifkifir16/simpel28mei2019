<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sppd extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('penugasan/sppd_model','main',true);
	}

	public function index()
	{
		$param=$this->uri->segment('5');
		$this->data['nip']=$param;
		$this->data['_page']='pages/admin/penugasan/sppd/data';
		$this->data['_scripts']=$this->data['_page'].'/scripts';
		$this->data['_head']=$this->data['_page'].'/head';
		$this->data['_ajaxSource']=base_url('admin/penugasan/sppd/data/'.$param);

		$this->parser->parse('home/index',$this->data);
	}

	public function tambah()
	{
		$this->data['_page']='pages/admin/penugasan/sppd/tambah';
		$this->data['_scripts']=$this->data['_page'].'/scripts';
		$this->data['_head']=$this->data['_page'].'/head';
		$this->data['_ajaxSource']=base_url('admin/penugasan/sppd/data');
		
		$this->parser->parse('home/index',$this->data);
	}

	public function edit()
	{
		$param=$this->uri->segment('5');
		$where=array('ID_SPPD'=>$param);
		$this->data['result']=$this->dbCommand('singleData',$where);
		//print_r($this->data['result']);
		//exit();
		$this->data['_id']=$param;
		$this->data['_page']='pages/admin/penugasan/sppd/edit';
		$this->data['_scripts']=$this->data['_page'].'/scripts';
		$this->data['_head']=$this->data['_page'].'/head';
		$this->data['_ajaxSource']=base_url('admin/penugasan/sppd/data');
		
		$this->parser->parse('home/index',$this->data);
	}

	public function detail()
	{
		$param=$this->uri->segment('5');
		$where=array('ID_SPPD'=>$param);
		$this->data['result']=$this->dbCommand('detail',$where);
		$this->data['data']=$this->dbCommand('rincian',$where);
		//print_r($this->data['data']);
		//exit();
		$this->data['_id']=$param;
		$this->data['_page']='pages/admin/penugasan/sppd/detail';
		//$this->data['_scripts']=$this->data['_page'].'/scripts';
		//$this->data['_head']=$this->data['_page'].'/head';
		//$this->data['_ajaxSource']=base_url('admin/penugasan/sppd/data');
		
		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='sppd';

		if($command=='getData')
			$res=$this->main->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->main->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->main->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->main->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->main->count_filtered($param);
		elseif($command=='ajaxData')
			$res=$this->main->ajaxData($param);
		elseif($command=='singleData')
			$res=$this->main->singleData($param);
		elseif($command=='detail')
			$res=$this->main->detail($param);
		elseif($command=='rincian')
			$res=$this->main->rincian($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function data(){
		/* Menentukan status data yang ingin diambil */
		
		$param=$this->uri->segment('6');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_KEL_PEG']=1;
			elseif($param==2)
				$where['STATUS_KEL_PEG']=0;
		}
		
		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_SPPD;
			$row[] = $field->NO_SPPD;
			$row[] = $field->TGL_SPPD;
			$row[] = $field->NAMA_PEG;
			$row[] = $field->FOTO_SPPD;
			$row[] = $field->STATUS_SPPD;
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function ajax(){
		$q=$this->input->get('q');
		$res=$this->dbCommand('ajaxData',$q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'Anggota keluarga tidak ditemukan');
		
		echo json_encode($r);
	}

	public function save(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		$pegawai=$_input['pegawai'];
		if(is_array($pegawai))
			$filtered = array_unique($pegawai);
		else
			$filtered=array($pegawai);

		$saved=0;$total=count($filtered);
		$updated=0;
		foreach ($filtered as $filt)
		{
			$airtax=$_input['airtax']==''? 0 : str_replace(".","",$_input['airtax']);
			$upesawat=$_input['upesawat']==''?0:str_replace(".","",$_input['upesawat']);
			$bpesawat=$_input['bpesawat']==''?0:str_replace(".","",$_input['bpesawat']);
			$ubus=$_input['ubus']==''?0:str_replace(".","",$_input['ubus']);
			$bbus=$_input['bbus']==''?0:str_replace(".","",$_input['bbus']);
			$ulokal=$_input['ulokal']==''?0:str_replace(".","",$_input['ulokal']);
			$blokal=$_input['blokal']==''?0:str_replace(".","",$_input['blokal']);
			$tbharian=$_input['tbharian']==''?0:str_replace(".","",$_input['tbharian']);
			$tbiaya=$_input['tbiaya']==''?0:str_replace(".","",$_input['tbiaya']);

			$airtax=$airtax==''? 0 : str_replace(",","",$airtax);
			$upesawat=$upesawat==''?0:str_replace(",","",$upesawat);
			$bpesawat=$bpesawat==''?0:str_replace(",","",$bpesawat);
			$ubus=$ubus==''?0:str_replace(",","",$ubus);
			$bbus=$bbus==''?0:str_replace(",","",$bbus);
			$ulokal=$ulokal==''?0:str_replace(",","",$ulokal);
			$blokal=$blokal==''?0:str_replace(",","",$blokal);
			$tbharian=$tbharian==''?0:str_replace(",","",$tbharian);
			$tbiaya=$tbiaya==''?0:str_replace(",","",$tbiaya);

			$dataSave=array();
			$dataSave['NIP_PEG']=$filt;
			$dataSave['TGL_NOTA_SPPD']=$_input['tglnota'];
			$dataSave['NO_JMK_SPPD']=$_input['nojmk'];
			$dataSave['JENIS_SPPD']=$_input['jsppd'];
			$dataSave['TGL_SPPD']=$_input['tglsppd'];
			$dataSave['URAIAN_SPPD']=$_input['uraian'];
			$dataSave['TRANSPORT_SPPD']=$_input['transportasi'];
			$dataSave['TGL_BRKT_SPPD']=$_input['tglberangkat'];
			$dataSave['TGL_KMBL_SPPD']=$_input['tglkembali'];
			$dataSave['ASAL_SPPD']=$_input['asal'];
			$dataSave['TUJUAN_SPPD']=$_input['tujuan'];
			$dataSave['UNIT_PESAWAT_SPPD']=$upesawat;
			$dataSave['BIAYA_PESAWAT_SPPD']=$bpesawat;
			$dataSave['UNIT_BUS_SPPD']=$ubus;
			$dataSave['BIAYA_BUS_SPPD']=$bbus;
			$dataSave['UNIT_TLOKAL_SPPD']=$ulokal;
			$dataSave['BIAYA_TLOKAL_SPPD']=$blokal;
			$dataSave['TAX_BANDARA_SPPD']=$airtax;
			$dataSave['BIAYA_HARIAN_SPPD']=$tbharian;
			$dataSave['TOTAL_BIAYA_SPPD']=$tbiaya;
			$dataSave['BIAYA_OLEH_SPPD']=$_input['bdari'];
			$dataSave['KET_SPPD']=$_input['keterangan'];
			$dataSave['STATUS_SPPD']=$_input['status'];
			$dataSave['OTORISASI_SPPD']=$_input['osppd'];
			$dataSave['TGL_JMK_SPPD']=$_input['tgljmk'];
			$dataSave['PEMK_SPPD']=$_input['pemk'];
			$dataSave['NO_SP_SPPD']=$_input['nosp'];
			$t=0;
			$date1=date_create($_input['tglberangkat']);
			$date2=date_create($_input['tglkembali']);
			$diff=date_diff($date1,$date2);
			$usak= ($diff->days+1) * $tbharian;
			$tot=$tbiaya+$usak+$t;

			$dataSave['TOTAL_BIAYA_SPPD']=$tot;

			$idsppd=isset($_input['id'])?$_input['id']:0;
			if($idsppd==0) //jika addnew
			{
				
				$param=array($dataSave,true);
				$res=$this->dbCommand('insert',$param);
				$not=str_pad($res, 3, '0', STR_PAD_LEFT);

				$where=array();
				$where['ID_SPPD']=$res;

				$dataSave=array();
				$dataSave['NO_SPPD']=$not."/SPPD/SYM/".date('m/Y');
				$param=array($dataSave,$where);
				$res=$this->dbCommand('update',$param);

				$pesan='menyimpan';
			}
			else //jika update
			{
				$where=array();
				$where['ID_SPPD']=$_input['id'];

				$param=array($dataSave,$where);
				$res=$this->dbCommand('update',$param);
				if($res) $saved++;
				$pesan='memperbaharui';
			}

		}

		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data sppd';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_KEL_PEG']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_KEL_PEG']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_KEL_PEG']=1;
			$pesan="mengaktifkan";
		}
		
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data keluarga';	
		}
		
		echo json_encode($data);
	}
}
