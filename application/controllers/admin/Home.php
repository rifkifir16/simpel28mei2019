<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Admin {

	public function index()
	{
		$this->data['_title']=$this->_title;
		$this->data['_page']='pages/admin/home';

		
		

		$this->parser->parse('home/index',$this->data);
	}
}
