<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelamar extends MY_Admin
{
	private $replacer = '';

	function __construct()
	{
		parent::__construct();
		$this->replacer = str_replace('\\', '/', FCPATH);  //WAJIB DIISI, UNTUK RELATIVE PATH UPLOAD LINK
		$this->data['_title'] = $this->_title;
		$this->load->model('user/pelamar_model', 'main', true);
	}

	public function index()
	{
		$this->data['_page'] = 'pages/admin/user/pelamar/data';
		$this->data['_scripts'] = $this->data['_page'] . '/scripts';
		$this->data['_head'] = $this->data['_page'] . '/head';
		$this->data['_ajaxSource'] = base_url('admin/user/pelamar/data');

		$this->parser->parse('home/index', $this->data);
	}

	public function tambah()
	{
		$this->data['_page'] = 'pages/admin/user/pelamar/tambah';
		$this->data['_scripts'] = $this->data['_page'] . '/scripts';
		$this->data['_head'] = $this->data['_page'] . '/head';
		$this->data['_ajaxSource'] = base_url('admin/user/pelamar/data');

		$this->parser->parse('home/index', $this->data);
	}

	public function detail()
	{
		$param = $this->uri->segment('5');
		$where = array(
			'pelamar.ID_PELAMAR' => $param
		);
		$this->data['result'] = $this->dbCommand('singleData', $where);
		$this->data['relasi'] = $this->dbCommand('relasi', $where);
		$this->data['keluarga'] = $this->dbCommand('keluarga', $where);
		$this->data['kecakapan'] = $this->dbCommand('kecakapan', $where);
		$this->data['sekolah'] = $this->dbCommand('sekolah', $where);
		$this->data['kursus'] = $this->dbCommand('kursus', $where);
		$this->data['pengalaman'] = $this->dbCommand('pengalaman', $where);
		$this->data['referensi'] = $this->dbCommand('referensi', $where);
		$this->data['organisasi'] = $this->dbCommand('organisasi', $where);
		$this->data['keterangan'] = $this->dbCommand('keterangan', $where);


		$this->data['_page'] = 'pages/admin/user/pelamar/detail';
		$this->data['_scripts'] = $this->data['_page'] . '/scripts';
		$this->data['_head'] = $this->data['_page'] . '/head';
		$this->data['_ajaxSource'] = base_url('admin/user/pelamar/data');

		$this->parser->parse('home/index', $this->data);
	}

	/*Additional function */
	public function dbCommand($command = 'getData', $param, $table = 'pegawai')
	{

		if ($command == 'getData')
			$res = $this->main->getData($param[0], $param[1]);
		elseif ($command == 'insert')
			$res = $this->main->insert($param[0], $param[1], $table);
		elseif ($command == 'update')
			$res = $this->main->update($param[0], $param[1], $table);
		elseif ($command == 'count_all')
			$res = $this->main->count_all($param);
		elseif ($command == 'count_filtered')
			$res = $this->main->count_filtered($param);
		elseif ($command == 'ajaxData')
			$res = $this->main->ajaxData($param);
		elseif ($command == 'check')
			$res = $this->main->check($param);
		elseif ($command == 'singleData')
			$res = $this->main->singleData($param);
		elseif ($command == 'relasi')
			$res = $this->main->relasi($param);
		elseif ($command == 'keluarga')
			$res = $this->main->keluarga($param);
		elseif ($command == 'kecakapan')
			$res = $this->main->kecakapan($param);
		elseif ($command == 'sekolah')
			$res = $this->main->sekolah($param);
		elseif ($command == 'kursus')
			$res = $this->main->kursus($param);
		elseif ($command == 'pengalaman')
			$res = $this->main->pengalaman($param);
		elseif ($command == 'referensi')
			$res = $this->main->referensi($param);
		elseif ($command == 'organisasi')
			$res = $this->main->organisasi($param);
		elseif ($command == 'keterangan')
			$res = $this->main->keterangan($param);
		else
			$res = false;

		return $res;
	}

	/*All ajax below here*/
	public function check()
	{
		$_input = $this->input->post();

		$where = array('LTRIM(RTRIM(NIP_PEG))' => $_input['id']);
		$refData = $this->dbCommand('check', $where);
		if (count($refData) > 0)
			echo '<a style="color: red;">NIP sudah digunakan</a>';
		else
			echo '<a style="color: green;">NIP dapat digunakan</a>';
	}

	public function data()
	{
		/* Menentukan status data yang ingin diambil */
		$param = $this->uri->segment('5');
		$where = array();
		if (isset($param)) {
			if ($param == 1)
				$where['KERRES_PEG'] = 'Bekerja';
			elseif ($param == 2)
				$where['KERRES_PEG'] = 'Resign';
		}

		$param = array(null, $where);
		$list = $this->dbCommand('getData', $param);
		$data = array();
		$no = isset($_POST['start']) ? $_POST['start'] : 0;
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->ID_PELAMAR;
			$row[] = $field->NAMA_PELAMAR;
			$row[] = $field->TKTPENDTERAKHIR_PELAMAR;
			$row[] = $field->SPESIALISASI_PELAMAR;
			$row[] = $field->STATUSLM_PELAMAR;
			$row[] = $field->STATUSLM_PELAMAR;

			$data[] = $row;
		}

		$draw = isset($_POST['draw']) ? $_POST['draw'] : 1;
		$output = array(
			"draw" => $draw,
			"recordsTotal" => $this->dbCommand('count_all', $where),
			"recordsFiltered" => $this->dbCommand('count_filtered', $where),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function ajax()
	{
		$q = $this->input->get('q');
		$res = $this->dbCommand('ajaxData', $q);
		$i = 0;
		if (count($res) > 0) {
			$r = $res;
		} else
			$r[] = array('id' => '', 'text' => 'Pegawai tidak ditemukan');

		echo json_encode($r);
	}
	/*Edit later */
	public function save()
	{
		$_input = $this->input->post();
		$nip = rtrim(ltrim($_input['nip']));
		$savedKontak = 0;
		$savedKeluarga = 0;
		$_err = false;
		$upload = false;
		$new_name = '';
		$hasil = '';
		$id = 0;
		$userId = rand(1, 25);
		$curTime = date('Y-m-d H:i:s');

		//cek upload
		if (!empty($_FILES['foto']['name'])) {
			$upload = true;
			$date = date('Y-m-d');
			$time = date('His');
			$path = realpath(FCPATH . 'assets/images/karyawan') . '/' . $date;
			$search = $this->replacer;

			if (!is_dir($path)) {
				mkdir($path, 0777, TRUE);
			}

			$config['upload_path']          = $path;
			$config['allowed_types']        = 'png|PNG|jpg|JPG';
			$config['max_size']             = 1024 * 5;
			$config['file_ext_tolower']		= true;
			$new_name = str_replace('-', '', $date) . '_' . $userId . '_' . $time;
			$config['file_name'] = strtolower($new_name);

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('foto')) {
				$error = array('error' => $this->upload->display_errors());
				$hasil = $this->upload->display_errors();
				$_err = true;
			} else {
				$picPath = $this->upload->data(); //full_path
				$_err = false;
			}
		}

		$created_path = '';
		if (isset($picPath['full_path'])) {
			$created_path = str_replace($search, '', $picPath['full_path']);
		}

		if ($_err == false) {

			//insert ke user dulu
			$dataSave = array();
			$dataSave['USERNAME_USER'] = $nip;
			$dataSave['PASSWORD_USER'] = md5($nip);
			$dataSave['NAMA_USER'] = $_input['nama'];
			$dataSave['INSTANSI_USER'] = 'pegawai';
			$dataSave['HAK_USER'] = 'pegawai';
			$this->load->model('user/user_model', 'user', true);
			$myId = $this->user->insert($dataSave, false);
			if ($myId > 0) { //jika user berhasil disimpan
				$_input['id_user'] = $myId;
				$fields = array("NIP_PEG", "ID_AGAMA", "ID_JABATAN", "ID_KOTA", "ID_USER", "NAMA_PEG", "ALAMAT_PEG", "TGL_LAHIR_PEG", "TPT_LAHIR_PEG", "JK_PEG", "STATUS_NIKAH_PEG", "PENDIDIKAN_PEG", "PT_PEG", "TGLMASUK_PEG", "ID_KUALIFIKASI", "REK_PEG", "NAMA_REK_PEG", "BANK_PEG", "NPWP_PEG", "KTP_PEG", "KK_PEG", "BPJS_KERJA_PEG", "BPJS_SEHAT_PEG", "STATUS_PEG", "KERRES_PEG", "NAMANPWP_PEG");

				$val = array("nip", "agama", "jabatan", "domisili", "id_user", "nama", "alamat", "tgllhr", "kotalahir", "jk", "sn", "pendidikan", "pt", "tglmasuk", "kualifikasi", "norek", "namarek", "bank", "npwp", "ktp", "kk", "bpjsk", "bpjss", "status", "kerres", "namanpwp");

				//generate data
				$dataSave = array();
				for ($i = 0; $i < count($val); $i++) {
					$dataSave[$fields[$i]] = ltrim(rtrim($_input[$val[$i]]));
				}
				if ($created_path != '') {
					$dataSave['FOTO_PEG'] = $created_path;
				}

				$param = array($dataSave, false);
				if ($this->dbCommand('insert', $param)) { //jika data pegawai berhasil disimpan

					//simpan kontak
					$jenis = $_input['jenis'];
					$kontak = $_input['kontak'];
					for ($i = 0; $i < count($jenis); $i++) {
						if ($jenis[$i] != '') {
							$dataSave = array();
							$dataSave['NIP_PEG'] = $nip;
							$dataSave['ID_KONTAK'] = $jenis[$i];
							$dataSave['ISI_KONTAK_PEG'] = $kontak[$i];
							$dataSave['STATUS_KONTAK_PEG'] = 1;
							if ($this->user->insert($dataSave, false, 'kontak_pegawai'))
								$savedKontak++;
						}
					}

					//simpan data keluarga

					$kel = $_input['kel'];
					$nmkel = $_input['nmkel'];
					$bpjs = $_input['bpjs'];

					for ($i = 0; $i < count($jenis); $i++) {
						if ($kel[$i] != '') {
							$dataSave = array();
							$dataSave['NIP_PEG'] = $nip;
							$dataSave['NAMA_KEL_PEG'] = $kel[$i];
							$dataSave['ISI_KEL_PEG'] = $nmkel[$i];
							$dataSave['BPJS_KEL_PEG'] = $bpjs[$i];
							$dataSave['STATUS_KEL_PEG'] = 1;
							if ($this->user->insert($dataSave, false, 'keluarga_peg'))
								$savedKeluarga++;
						}
					}
					$vals['upah'] = str_replace('.', '', $_input['upah']);
					$vals['tumum'] = str_replace('.', '', $_input['tumum']);
					$vals['tmakan'] = str_replace('.', '', $_input['tmakan']);
					$vals['tkomunikasi'] = str_replace('.', '', $_input['tkomunikasi']);
					$vals['ttotal'] = str_replace('.', '', $_input['ttotal']);
					//simpan ke tarif
					$dataSave = array();
					$dataSave['NIP_PEG'] = $nip;
					$dataSave['UPAH_TARIF'] = $vals['upah'];
					$dataSave['TUMUM_TARIF'] = $vals['tumum'];
					$dataSave['TMAKAN_TARIF'] = $vals['tmakan'];
					$dataSave['TKOMUNIKASI_TARIF'] = $vals['tkomunikasi'];
					$dataSave['JUMLAH_TARIF'] = $vals['ttotal'];
					$this->user->insert($dataSave, false, 'tarif');
				}
			} else //Gagal menyimpan data, hapus gambar
			{
				if ($upload == true)
					unlink($picPath['full_path']);
			}

			if ($upload == true)
				@unlink($_FILES['foto']);
		}


		echo json_encode($_err);
	}

	public function update()
	{
		$_input = $this->input->post();
		$nip = rtrim(ltrim($_input['nip']));
		$where = array('NIP_PEG' => $nip);
		$_err = false;
		$upload = false;
		$new_name = '';
		$hasil = '';
		$id = 0;
		$userId = rand(1, 25);
		$this->load->model('user/user_model', 'user', true);

		//cek upload
		if (!empty($_FILES['foto']['name'])) {
			$upload = true;
			$date = date('Y-m-d');
			$time = date('His');
			$path = realpath(FCPATH . 'assets/images/karyawan') . '/' . $date;
			$search = $this->replacer;

			if (!is_dir($path)) {
				mkdir($path, 0777, TRUE);
			}

			$config['upload_path']          = $path;
			$config['allowed_types']        = 'png|PNG|jpg|JPG';
			$config['max_size']             = 1024 * 5;
			$config['file_ext_tolower']		= true;
			$new_name = str_replace('-', '', $date) . '_' . $userId . '_' . $time;
			$config['file_name'] = strtolower($new_name);

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('foto')) {
				$error = array('error' => $this->upload->display_errors());
				$hasil = $this->upload->display_errors();
				$_err = true;
			} else {
				$picPath = $this->upload->data(); //full_path
				$_err = false;
			}
		}

		$created_path = '';
		if (isset($picPath['full_path'])) {
			$created_path = str_replace($search, '', $picPath['full_path']);
		}

		if ($_err == false) {
			//jika user berhasil disimpan

			$fields = array("ID_AGAMA", "ID_JABATAN", "ID_KOTA", "NAMA_PEG", "ALAMAT_PEG", "TGL_LAHIR_PEG", "TPT_LAHIR_PEG", "JK_PEG", "STATUS_NIKAH_PEG", "PENDIDIKAN_PEG", "PT_PEG", "TGLMASUK_PEG", "ID_KUALIFIKASI", "REK_PEG", "NAMA_REK_PEG", "BANK_PEG", "NPWP_PEG", "KTP_PEG", "KK_PEG", "BPJS_KERJA_PEG", "BPJS_SEHAT_PEG", "STATUS_PEG", "KERRES_PEG", "NAMANPWP_PEG");

			$val = array("agama", "jabatan", "domisili", "nama", "alamat", "tgllhr", "kotalahir", "jk", "sn", "pendidikan", "pt", "tglmasuk", "kualifikasi", "norek", "namarek", "bank", "npwp", "ktp", "kk", "bpjsk", "bpjss", "status", "kerres", "namanpwp");

			//generate data
			$dataSave = array();
			for ($i = 0; $i < count($val); $i++) {
				$dataSave[$fields[$i]] = ltrim(rtrim($_input[$val[$i]]));
			}
			if ($created_path != '') {
				$dataSave['FOTO_PEG'] = $created_path;
			}

			$param = array($dataSave, $where);
			if ($this->dbCommand('update', $param)) { //jika data pegawai berhasil disimpan


				$vals['upah'] = str_replace('.', '', $_input['upah']);
				$vals['tumum'] = str_replace('.', '', $_input['tumum']);
				$vals['tmakan'] = str_replace('.', '', $_input['tmakan']);
				$vals['tkomunikasi'] = str_replace('.', '', $_input['tkomunikasi']);
				$vals['ttotal'] = str_replace('.', '', $_input['ttotal']);
				//simpan ke tarif
				$dataSave = array();
				$dataSave['NIP_PEG'] = $nip;
				$dataSave['UPAH_TARIF'] = $vals['upah'];
				$dataSave['TUMUM_TARIF'] = $vals['tumum'];
				$dataSave['TMAKAN_TARIF'] = $vals['tmakan'];
				$dataSave['TKOMUNIKASI_TARIF'] = $vals['tkomunikasi'];
				$dataSave['JUMLAH_TARIF'] = $vals['ttotal'];
				$this->user->update($dataSave, $where, 'tarif');

				//update data user
				$where = array('USERNAME_USER' => $nip);
				$dataSave = array();
				$dataSave['NAMA_USER'] = ltrim(rtrim($_input['nama']));
				$this->user->update($dataSave, $where, 'user');
			} else //Gagal menyimpan data, hapus gambar
			{
				if ($upload == true)
					unlink($picPath['full_path']);
			}

			if ($upload == true)
				@unlink($_FILES['foto']);
		}


		echo json_encode($_err);
	}

	public function resetpass()
	{
		$_input = $this->input->get();
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat mengubah status data';

		$dataSave = array();
		$where = array();
		$where['USERNAME_USER'] = $_input['id'];
		$dataSave['PASSWORD_USER'] = md5($_input['id']);


		$param = array($dataSave, $where);
		$res = $this->dbCommand('update', $param, 'user');

		if ($res > 0) {
			$data['result'] = true;
			$data['msg'] = 'Berhasil mereset password karyawan.';
		}

		echo json_encode($data);
	}

	public function rekap2()
	{
		$this->load->model('user/pelamar_model', 'pelamar_model', true);
		// $this->load->model('fitur/penggajian_model', 'main', true);
		$res = $this->pelamar_model->dataexcel();
		// echo "<pre>";
		// print_r($res);
		// echo "</pre>";
		// exit;
		$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
		$this->load->library("excel");
		$excel = new PHPExcel();
		//insert some data to PHPExcel object
		//Judul
		$excel->setActiveSheetIndex(0)->mergeCells('A1:K1')->setCellValue('A1', 'PT. SURVEYOR INDONESIA');
		$excel->setActiveSheetIndex(0)->mergeCells('A2:K2')->setCellValue('A2', 'Data Pelamar');

		$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(23);
		$excel->getActiveSheet()->getStyle("A2")->getFont()->setSize(10);
		$excel->getActiveSheet()->getStyle("A3")->getFont()->setSize(10);


		//header table
		$excel->setActiveSheetIndex(0)
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Posisi Pelamar')
			->setCellValue('C6', 'Nama Pelamar')
			->setCellValue('D6', 'No KTP')
			->setCellValue('E6', 'Alamat')
			->setCellValue('F6', 'Domisili')
			->setCellValue('G6', 'Tempat tanggal lahir')
			->setCellValue('H6', 'Agama')
			->setCellValue('I6', 'Jenis Kelamin')
			->setCellValue('J6', 'No Telepon')
			->setCellValue('K6', 'Email')
			->setCellValue('L6', 'Pendidikan terakhir')
			->setCellValue('M6', 'Spesialis')
			->setCellValue('N6', 'Keahlian');


		$excel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$i = 1;
		$total = 0;
		$x = 7;
		$subtotal = 0;
		$lastKode = '';

		foreach ($res as $result) {
			$excel->setActiveSheetIndex(0)
				->setCellValue('A' . $x, $i)
				->setCellValue('B' . $x, $result['POSISI_PELAMAR'])
				->setCellValue('C' . $x, $result['NAMA_PELAMAR'])
				->setCellValue('D' . $x, $result['NOKTP_PELAMAR'])
				->setCellValue('E' . $x, $result['ALAMAT_PELAMAR'])
				->setCellValue('F' . $x, $result['NAMA_KOTA'])
				->setCellValue('G' . $x, $result['TPT_LAHIR_PELAMAR'] . ', ' . $result['TGL_LAHIR_PELAMAR'])
				->setCellValue('H' . $x, $result['NAMA_AGAMA'])
				->setCellValue('I' . $x, $result['JK_PELAMAR'])
				->setCellValue('J' . $x, $result['NOTELP_PELAMAR'])
				->setCellValue('K' . $x, $result['EMAIL_PELAMAR'])
				->setCellValue('L' . $x, $result['TKTPENDTERAKHIR_PELAMAR'])
				->setCellValue('M' . $x, $result['SPESIALISASI_PELAMAR'])
				->setCellValue('N' . $x, $result['KEAHLIAN_PELAMAR']);
			$i++;
			$x++;
		}
		//autosize
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

		ob_end_clean();
		$object_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="DATA PELAMAR.xls"');
		$object_writer->save('php://output');
		ob_end_clean();
		$data = $res;
		echo json_encode($data);
	}
}
