<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Surat extends MY_Admin
{
	protected $_title = "";
	function __construct()
	{
		parent::__construct();
		$this->data['_title'] = $this->_title;
		$this->load->model('basic/surat_model', 'main', true);
	}

	public function index()
	{
		$this->data['_page'] = 'pages/admin/fitur/surat';
		$this->data['_scripts'] = $this->data['_page'] . '/scripts';
		$this->data['_head'] = $this->data['_page'] . '/head';
		$this->data['_ajaxSource'] = base_url('admin/fitur/surat/data');
		$this->data['jabatan'] = $this->db->get('jabatan')->result();
		$this->data['kualifikasi'] = $this->db->get('kualifikasi')->result();

		$this->parser->parse('home/index', $this->data);
	}

	/*Additional function */
	public function dbCommand($command = 'getData', $param, $table = null)
	{


		if ($command == 'getData')
			$res = $this->main->getData($param[0], $param[1]);
		elseif ($command == 'insert')
			$res = $this->main->insert($param[0], $param[1], $table);
		elseif ($command == 'update')
			$res = $this->main->update($param[0], $param[1], $table);
		elseif ($command == 'count_all')
			$res = $this->main->count_all($param);
		elseif ($command == 'count_filtered')
			$res = $this->main->count_filtered($param);
		elseif ($command == 'ajaxData')
			$res = $this->main->ajaxData($param);
		else
			$res = false;

		return $res;
	}

	/*All ajax below here*/
	public function data()
	{
		/* Menentukan status data yang ingin diambil */
		// $param = $this->uri->segment('5');
		$where = array();
		// if (isset($param)) {
		// 	if ($param == 1)
		// 		$where['STATUS_KUALIFIKASI'] = 1;
		// 	elseif ($param == 2)
		// 		$where['STATUS_KUALIFIKASI'] = 0;
		// }

		$param = array(null, $where);
		$list = $this->dbCommand('getData', $param);
		$data = array();
		$no = isset($_POST['start']) ? $_POST['start'] : 0;
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->ID_SURAT;
			$row[] = $field->NO_SURAT;
			$row[] = $field->TANGGAL;
			$row[] = $field->PERIHAL;
			$row[] = "<a target='_blank' href='" . $field->SCAN_SURAT . "'>Download</a>";
			$row[] = "<a target='_blank' href='" . site_url('admin/fitur/surat/detail/' . $field->ID_SURAT . '') . "'>Detail</a>";


			$data[] = $row;
		}

		$draw = isset($_POST['draw']) ? $_POST['draw'] : 1;
		$output = array(
			"draw" => $draw,
			"recordsTotal" => $this->dbCommand('count_all', $where),
			"recordsFiltered" => $this->dbCommand('count_filtered', $where),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function ajax()
	{
		$q = $this->input->get('q');
		$res = $this->dbCommand('ajaxData', $q);
		$i = 0;
		if (count($res) > 0) {
			$r = $res;
		} else
			$r[] = array('id' => '', 'text' => 'Kualifikasi tidak ditemukan');

		echo json_encode($r);
	}

	public function save()
	{
		$_input = $this->input->post();
		$this->data['result'] = false;
		$this->data['msg'] = 'Terjadi kesalahan saat menyimpan data';

		// UPLOAD 
		$upload = true;
		$date = date('Y-m-d');
		$time = date('His');
		$path = FCPATH . 'assets/images/surat' . '/' . $date;
		$search = '';

		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		$config['upload_path']          = $path;
		$config['allowed_types']        = 'png|PNG|jpg|JPG|pdf|PDF|jpeg|JPEG';
		$config['max_size']             = 1024 * 5;
		$config['file_ext_tolower']		= true;
		$new_name = str_replace('-', '', $date) . '_' . $time;
		$config['file_name'] = strtolower($new_name);

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('scansurat')) {
			$error = array('error' => $this->upload->display_errors());
			$pesan = $this->upload->display_errors();
			$_err = true;
		} else {
			$picPath = $this->upload->data(); //full_path
			$_err = false;
		}

		$created_path = base_url() . 'assets/images/surat' . '/' . $date . '/' . $picPath['orig_name'];
		// $created_path = $picPath['full_path'];
		$dataSave = array();
		$dataSave['NO_SURAT'] = $_input['nosurat'];
		$dataSave['SCAN_SURAT'] = $created_path;
		$dataSave['PERIHAL'] = $_input['perihal'];
		$dataSave['TANGGAL'] = $_input['tanggalsurat'];
		$dataSave['TANGGAL_MULAI'] = $_input['tanggalmulai'];
		$dataSave['TANGGAL_AKHIR'] = $_input['tanggalakhir'];
		// $dataSave['SCAN_SURAT'] = $_input['scansurat'];
		// if ($_input['idKualifikasi'] == 0) //jika addnew
		// {
		// $dataSave['STATUS_KUALIFIKASI'] = 1;
		$table = 'surat';
		$param = array($dataSave, true);
		$res = $this->dbCommand('insert', $param, $table);
		$dataSave = array();
		$dataSave['ID_SURAT'] = $this->db->insert_id();
		// echo $this->db->insert_id();
		// echo count($_input['jumlah']);
		$table = 'tenagaahli';
		for ($x = 0; $x < count($_input['jumlah']); $x++) {
			$dataSave['jumlah'] = $_input['jumlah'][$x];
			$dataSave['ID_JABATAN'] = $_input['jabatan'][$x];
			$dataSave['ID_KUALIFIKASI'] = $_input['tenagaahli'][$x];
			$param = array($dataSave, true);
			$res = $this->dbCommand('insert', $param, $table);
		}
		$pesan = 'menyimpan';
		// } else //jika update
		// {
		// 	$where = array();
		// 	$where['ID_KUALIFIKASI'] = $_input['idKualifikasi'];

		// 	$param = array($dataSave, $where);
		// 	$res = $this->dbCommand('update', $param);
		// 	$pesan = 'memperbaharui';
		// }

		if ($res > 0) {
			$this->data['result'] = true;
			$this->data['msg'] = 'Berhasil ' . $pesan . ' data kualifikasi';
		}

		redirect('admin/fitur/surat');
	}

	public function upstatus()
	{
		$_input = $this->input->get();
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat mengubah status data';

		$dataSave = array();
		$where = array();
		$where['ID_KUALIFIKASI'] = $_input['id'];

		if ($_input['status'] > 0) {
			$dataSave['STATUS_KUALIFIKASI'] = 0;
			$pesan = "menonaktifkan";
		} else {
			$dataSave['STATUS_KUALIFIKASI'] = 1;
			$pesan = "mengaktifkan";
		}

		$param = array($dataSave, $where);
		$res = $this->dbCommand('update', $param);

		if ($res > 0) {
			$data['result'] = true;
			$data['msg'] = 'Berhasil ' . $pesan . ' data kualifikasi';
		}

		echo json_encode($data);
	}
	function Detail()
	{
		$ID_SURAT = $this->uri->segment('5');
		$where = array('ID_SURAT' => $ID_SURAT);
		$this->data['detailsurat'] = $this->main->detail($where);
		$this->data['detailtenagaahli'] = $this->main->detailtenagaahli($where);
		$this->data['_page'] = 'pages/admin/fitur/surat/detail';
		$this->data['_scripts'] = $this->data['_page'] . '/scripts';
		$this->data['_head'] = $this->data['_page'] . '/head';
		$this->data['_ajaxSource'] = base_url('admin/fitur/surat/data');
		// echo "<pre>";
		if (isset($this->data['detailsurat'][0]['TANGGAL_MULAI']) && isset($this->data['detailsurat'][0]['TANGGAL_AKHIR'])) {
			$awal  = date_create(date("Y-m-d"));
			$akhir = date_create($this->data['detailsurat'][0]['TANGGAL_AKHIR']);
			if (date("Y-m-d") < $this->data['detailsurat'][0]['TANGGAL_AKHIR']) {
				$diff  = date_diff($akhir, $awal);
				$selisih = $diff->d;
			} else {
				$selisih = 0;
				// waktu sekarang
			}
		} else {
			$selisih = 0;
		}

		$this->data['selisih'] = $selisih;
		$this->parser->parse('home/index', $this->data);
	}
	function updatetenagaahli()
	{
		$post = $this->input->post();
		$where = array('ID_TENAGAAHLI' => $post['ID_TENAGAAHLI']);
		$hasil = $post['TERPILIH'] + 1;
		$data = array('TERPILIH' => $hasil);
		$this->main->updatetenagaahli($where, $data);
		redirect('admin/fitur/surat/detail/' . $post['ID_SURAT']);
	}
}
