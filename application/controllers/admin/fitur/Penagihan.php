<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penagihan extends MY_Admin
{
	protected $_title = "";
	function __construct()
	{
		parent::__construct();
		$this->data['_title'] = $this->_title;
		$this->load->model('fitur/penggajian_model', 'main', true);
	}

	public function index()
	{
		$this->data['_page'] = 'pages/admin/fitur/penagihan';
		$this->data['_scripts'] = $this->data['_page'] . '/scripts';
		$this->data['_head'] = $this->data['_page'] . '/head';
		$this->data['_ajaxSource'] = base_url('admin/fitur/penagihan/data');

		$this->parser->parse('home/index', $this->data);
	}

	/*Additional function */
	public function dbCommand($command = 'getData', $param)
	{
		$table = 'gaji';

		if ($command == 'getData')
			$res = $this->main->getData($param[0], $param[1]);
		elseif ($command == 'insert')
			$res = $this->main->insert($param, $table);
		elseif ($command == 'update')
			$res = $this->main->update($param[0], $param[1], $table);
		elseif ($command == 'count_all')
			$res = $this->main->count_all($param);
		elseif ($command == 'count_filtered')
			$res = $this->main->count_filtered($param);
		elseif ($command == 'ajaxData')
			$res = $this->main->ajaxData($param);
		else
			$res = false;

		return $res;
	}
	function test()
	{
		$test = $this->main->test();
		echo "<pre>";
		print_r($test);
		echo "</pre>";
	}

	/*All ajax below here*/
	public function data()
	{
		/* Menentukan status data yang ingin diambil */
		$where = array();
		$test = $this->uri->segment('5');
		$test1 = $this->uri->segment('6');
		$where = array();
		if ($test1 != 0) {
			$where['TAHUN_GAJI'] = $test1;
		}
		if ($test != 0) {
			$where['BULAN_GAJI'] = $test;
		}
		$param = array(null, $where);
		$list = $this->dbCommand('getData', $param);
		$data = array();
		$no = isset($_POST['start']) ? $_POST['start'] : 0;

		$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');

		foreach ($list as $field) {
			$x = isset($bulan[$field->BULAN_GAJI - 1]) ? $bulan[$field->BULAN_GAJI - 1] : ' - ';

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->NAMA_PEG;

			$row[] = $field->NAMA_JABATAN;
			$row[] = $field->KODE_LOKASI;
			$row[] = $x . "  " . $field->TAHUN_GAJI;
			$row[] = $field->NAMA_PROP;
			$row[] = $field->KUALIFIKASI_GAJI;
			$row[] = $field->NKUALIFIKASI_GAJI;
			$row[] = $field->JUMHARI_GAJI;
			$row[] = $field->JUMHADIR_GAJI;
			$row[] = $field->MM;
			$row[] = number_format($field->PEMBAYARAN);
			//$row[] = $field->TGLAP_GAJI;
			// $row[] = $status;
			// $row[] = $status;
			$data[] = $row;
		}

		$draw = isset($_POST['draw']) ? $_POST['draw'] : 1;
		$output = array(
			"draw" => $draw,
			"recordsTotal" => $this->dbCommand('count_all', $where),
			"recordsFiltered" => $this->dbCommand('count_filtered', $where),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function ajax()
	{
		$q = $this->input->get('q');
		$res = $this->dbCommand('ajaxData', $q);
		$i = 0;
		if (count($res) > 0) {
			$r = $res;
		} else
			$r[] = array('id' => '', 'text' => 'Agama tidak ditemukan');

		echo json_encode($r);
	}

	public function save()
	{
		$_input = $this->input->post();
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat menyimpan data';

		$dataSave = array();
		$dataSave['ID_LOKASI'] = $_input['lokasi1'];
		$dataSave['NIP_PEG'] = $_input['nip1'];
		$dataSave['BULAN_GAJI'] = $_input['bulan'];
		$dataSave['TAHUN_GAJI'] = $_input['tahun'];
		$dataSave['TGLAP_GAJI'] = $_input['tglapgaji'];
		$dataSave['TGL_GAJI'] = $_input['tglgaji'];
		$dataSave['JUMHARI_GAJI'] = $_input['hk1'];
		$dataSave['JUMHADIR_GAJI'] = $_input['kh1'];
		$dataSave['INDEX_GAJI'] = $_input['index2'];
		$dataSave['PPH_GAJI'] = $_input['pph1'];
		$dataSave['JAMSOSTEK_GAJI'] = $_input['jamsostek1'];
		$dataSave['COR_GAJI'] = $_input['koreksi1'];
		$dataSave['TPENGURANGAN_GAJI'] = $_input['pengurangangaji'];
		$dataSave['THP_GAJI'] = $_input['thpgaji2'];
		$dataSave['KUALIFIKASI_GAJI'] = $_input['kualifikasi'];
		$dataSave['NKUALIFIKASI_GAJI'] = $_input['kualifikasigaji'];
		$dataSave['THPEMBULATAN_GAJI'] = $_input['thpgaji2'];
		$dataSave['JPEMBAYARAN_GAJI'] = $_input['kualifikasigaji'] * ($_input['kh1'] / $_input['hk1']);
		$check = $this->main->check($_input['nip1'], $_input['bulan'], $_input['tahun']);
		if (empty($check)) {
			$res = $this->dbCommand('insert', $dataSave);
			$pesan = 'menyimpan';

			if ($res > 0) {
				$data['result'] = true;
				$data['check'] = $check;
				$data['msg'] = 'Berhasil ' . $pesan . ' data gaji';
			}
		} else {
			$data['result'] = false;
			$data['check'] = $check;
			$data['msg'] = 'Maaf data ' . $dataSave['NIP_PEG'] . ' sudah ada';
		}

		// $data = $_input;

		echo json_encode($data);
	}

	public function upstatus()
	{
		$_input = $this->input->get();
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat mengubah status data';

		$dataSave = array();
		$where = array();
		$where['ID_AGAMA'] = $_input['id'];

		if ($_input['status'] > 0) {
			$dataSave['STATUS_AGAMA'] = 0;
			$pesan = "menonaktifkan";
		} else {
			$dataSave['STATUS_AGAMA'] = 1;
			$pesan = "mengaktifkan";
		}

		$param = array($dataSave, $where);
		$res = $this->dbCommand('update', $param);

		if ($res > 0) {
			$data['result'] = true;
			$data['msg'] = 'Berhasil ' . $pesan . ' data agama';
		}

		echo json_encode($data);
	}
	public function lokasi()
	{
		$where = array();
		$where['ID_LOKASI'] = $this->input->post('id');
		$data = $this->main->lokasi($where);
		echo json_encode($data);
	}
	public function pegawai()
	{
		$where = array();
		$where['pegawai.NIP_PEG'] = $this->input->post('id');

		$data = $this->main->pegawai($where);
		if (!empty($data[0])) {
			echo json_encode($data);
		} else {
			$data = null;
			echo json_encode($data);
		}
	}
	public function rekap2()
	{
		// $this->load->model('fitur/penggajian_model', 'main', true);
		$_input = $this->input->post();
		if (!isset($_input['bulan']) && !isset($_input['tahun']))
			$this->index();
		else {
			$where = array('BULAN_GAJI' => $_input['bulan'], 'TAHUN_GAJI' => $_input['tahun']);
			$res = $this->main->dataexcel($where);
			$bulan = $_input['bulan'];
			$tahun = $_input['tahun'];
			$d = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
			$bln = $bulan[$_input['bulan'] - 1];
			$this->load->library("excel");
			$excel = new PHPExcel();
			//insert some data to PHPExcel object
			//Judul
			$excel->setActiveSheetIndex(0)->mergeCells('A1:K1')->setCellValue('A1', 'PT. SURVEYOR INDONESIA');
			$excel->setActiveSheetIndex(0)->mergeCells('A2:K2')->setCellValue('A2', 'LAPORAN PEMERIKSAAN PEKERJAAN BIAYA LANGSUNG PERSONIL');
			$excel->setActiveSheetIndex(0)->mergeCells('A3:K3')->setCellValue('A3', '01 s.d. ' . $d . ' ' . $bln . ' ' . $tahun);

			$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(23);
			$excel->getActiveSheet()->getStyle("A2")->getFont()->setSize(10);
			$excel->getActiveSheet()->getStyle("A3")->getFont()->setSize(10);


			//header table
			$excel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Nama')
				->setCellValue('C5', 'Penugasan')
				->setCellValue('D5', 'Kode Lokasi')
				->setCellValue('E5', 'Provinsi')
				->setCellValue('F5', 'Kualifikasi')
				->setCellValue('G5', 'Harga Satuan (Incl Ppn)')
				->setCellValue('H5', 'Jumlah Hari Kerja')
				->setCellValue('I5', 'Jumlah Hari Kehadiran')
				->setCellValue('J5', 'MM')
				->setCellValue('K5', 'Jumlah Pembayaran incl PPN');

			$excel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('I5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('K5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$i = 1;
			$total = 0;
			$x = 6;
			$subtotal = 0;
			$lastKode = '';

			foreach ($res as $result) {
				$excel->setActiveSheetIndex(0)
					->setCellValue('A' . $x, $i)
					->setCellValue('B' . $x, $result['NAMA_PEG'])
					->setCellValue('C' . $x, $result['NAMA_JABATAN'])
					->setCellValue('D' . $x, $result['KODE_LOKASI'])
					->setCellValue('E' . $x, $result['NAMA_PROP'])
					->setCellValue('F' . $x, $result['KUALIFIKASI_GAJI'])
					->setCellValue('G' . $x, $result['NKUALIFIKASI_GAJI'])
					->setCellValue('H' . $x, $result['JUMHARI_GAJI'])
					->setCellValue('I' . $x, $result['JUMHADIR_GAJI'])
					->setCellValue('J' . $x, $result['MM'])
					->setCellValue('K' . $x, $result['PEMBAYARAN']);

				$excel->getActiveSheet()->getStyle("G" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("K" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$i++;
				$x++;
			}
			//autosize
			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		}
		ob_end_clean();
		$object_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="LAPORAN PEMERIKSAAN PEKERJAAN BIAYA LANGSUNG PERSONIL ' . $bln . ' ' . $tahun . '.xls"');
		$object_writer->save('php://output');
		ob_end_clean();
		$data = $res;
		echo json_encode($data);
	}
}
