<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penggajian extends MY_Admin
{
	protected $_title = "";
	function __construct()
	{
		parent::__construct();
		$this->data['_title'] = $this->_title;
		$this->load->model('fitur/penggajian_model', 'main', true);
	}

	public function index()
	{
		$this->data['_page'] = 'pages/admin/fitur/penggajian';
		$this->data['_scripts'] = $this->data['_page'] . '/scripts';
		$this->data['_head'] = $this->data['_page'] . '/head';
		$this->data['_ajaxSource'] = base_url('admin/fitur/penggajian/data');

		$this->parser->parse('home/index', $this->data);
	}

	/*Additional function */
	public function dbCommand($command = 'getData', $param)
	{
		$table = 'gaji';

		if ($command == 'getData')
			$res = $this->main->getData($param[0], $param[1]);
		elseif ($command == 'insert')
			$res = $this->main->insert($param, $table);
		elseif ($command == 'update')
			$res = $this->main->update($param[0], $param[1], $table);
		elseif ($command == 'count_all')
			$res = $this->main->count_all($param);
		elseif ($command == 'count_filtered')
			$res = $this->main->count_filtered($param);
		elseif ($command == 'ajaxData')
			$res = $this->main->ajaxData($param);
		else
			$res = false;

		return $res;
	}

	/*All ajax below here*/
	public function data()
	{
		/* Menentukan status data yang ingin diambil */
		$test = $this->uri->segment('5');
		$test1 = $this->uri->segment('6');
		$where = array();
		if ($test1 != 0) {
			$where['TAHUN_GAJI'] = $test1;
		}
		if ($test != 0) {
			$where['BULAN_GAJI'] = $test;
		}


		$param = array(null, $where);
		$list = $this->dbCommand('getData', $param);
		$data = array();
		$no = isset($_POST['start']) ? $_POST['start'] : 0;

		$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');

		foreach ($list as $field) {
			$status = $field->TGLAP_GAJI == '1990-12-12' ? 0 : 1;
			$x = isset($bulan[$field->BULAN_GAJI - 1]) ? $bulan[$field->BULAN_GAJI - 1] : ' - ';

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->ID_GAJI;

			$row[] = $field->ID_LOKASI;
			$row[] = $field->KODE_LOKASI;
			$row[] = $field->NAMA_LOKASI;
			$row[] = $field->NIP_PEG;
			$row[] = $field->NAMA_PEG;
			$row[] = $x . "  " . $field->TAHUN_GAJI;
			$row[] = $field->MM;
			$row[] = $field->INDEX_GAJI;
			$row[] = $field->JUMHARI_GAJI;
			$row[] = $field->JUMHADIR_GAJI;
			$row[] = $field->COR_GAJI;
			$row[] = number_format($field->PPH_GAJI);
			$row[] = number_format($field->JAMSOSTEK_GAJI);
			$row[] = number_format($field->THP_GAJI);
			$row[] = number_format($field->HARGA_KUALIFIKASI);
			//$row[] = $field->TGLAP_GAJI;
			$row[] = $status;
			$row[] = $status;
			$data[] = $row;
		}

		$draw = isset($_POST['draw']) ? $_POST['draw'] : 1;
		$output = array(
			"draw" => $draw,
			"recordsTotal" => $this->dbCommand('count_all', $where),
			"recordsFiltered" => $this->dbCommand('count_filtered', $where),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function ajax()
	{
		$q = $this->input->get('q');
		$res = $this->dbCommand('ajaxData', $q);
		$i = 0;
		if (count($res) > 0) {
			$r = $res;
		} else
			$r[] = array('id' => '', 'text' => 'Agama tidak ditemukan');

		echo json_encode($r);
	}

	public function save()
	{
		$_input = $this->input->post();
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat menyimpan data';

		$dataSave = array();
		$dataSave['ID_LOKASI'] = $_input['lokasi1'];
		$dataSave['NIP_PEG'] = $_input['nip1'];
		$dataSave['BULAN_GAJI'] = $_input['bulan'];
		$dataSave['TAHUN_GAJI'] = $_input['tahun'];
		$dataSave['TGLAP_GAJI'] = $_input['tglapgaji'];
		$dataSave['TGL_GAJI'] = $_input['tglgaji'];
		$dataSave['JUMHARI_GAJI'] = $_input['hk1'];
		$dataSave['JUMHADIR_GAJI'] = $_input['kh1'];
		$dataSave['INDEX_GAJI'] = $_input['index2'];
		$dataSave['PPH_GAJI'] = $_input['pph1'];
		$dataSave['JAMSOSTEK_GAJI'] = $_input['jamsostek1'];
		$dataSave['COR_GAJI'] = $_input['koreksi1'];
		$dataSave['TPENGURANGAN_GAJI'] = $_input['pengurangangaji'];
		$dataSave['THP_GAJI'] = $_input['thpgaji2'];
		$dataSave['KUALIFIKASI_GAJI'] = $_input['kualifikasi'];
		$dataSave['NKUALIFIKASI_GAJI'] = $_input['kualifikasigaji'];
		$dataSave['THPEMBULATAN_GAJI'] = $_input['thpgaji2'];
		$dataSave['JPEMBAYARAN_GAJI'] = $_input['kualifikasigaji'] * number_format(($_input['kh1'] / $_input['hk1']), 2);
		// $check = 0;
		// $check = $this->main->check($_input['nip1'], $_input['bulan'], $_input['tahun']);
		// if (empty($check)) {
		$res = $this->dbCommand('insert', $dataSave);
		$pesan = 'menyimpan';

		if ($res > 0) {
			$data['result'] = true;
			// $data['check'] = $check;
			$data['msg'] = 'Berhasil ' . $pesan . ' data gaji';
		}
		// } else {
		// 	$data['result'] = false;
		// 	$data['check'] = $check;
		// 	$data['msg'] = 'Maaf data ' . $dataSave['NIP_PEG'] . ' sudah ada';
		// }

		// $data = $_input;

		echo json_encode($data);
	}

	public function upstatus()
	{
		$where = array();

		$wherepegawai['pegawai.NIP_PEG'] = $this->input->post('nip');
		$datapegawai = $this->main->pegawai($wherepegawai);

		$_input = $this->input->post();
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat mengubah status data';


		$where['ID_GAJI'] = $_input['id'];
		$dataSave = array();
		$dataSave['ID_LOKASI'] = $_input['lokasi1'];
		// $dataSave['NIP_PEG'] = $_input['nip1'];
		// $dataSave['BULAN_GAJI'] = $_input['bulan'];
		// $dataSave['TAHUN_GAJI'] = $_input['tahun'];
		$dataSave['TGLAP_GAJI'] = $_input['tglapgaji'];
		$dataSave['TGL_GAJI'] = $_input['tglgaji'];
		$dataSave['JUMHARI_GAJI'] = $_input['hk1'];
		$dataSave['JUMHADIR_GAJI'] = $_input['kh1'];
		$dataSave['INDEX_GAJI'] = $_input['index2'];
		$dataSave['PPH_GAJI'] = $_input['pph1'];
		$dataSave['JAMSOSTEK_GAJI'] = $_input['jamsostek1'];
		$dataSave['COR_GAJI'] = $_input['koreksi1'];
		$dataSave['TPENGURANGAN_GAJI'] = $_input['pengurangangaji'];
		$dataSave['THP_GAJI'] = $_input['thpgaji2'];
		$dataSave['KUALIFIKASI_GAJI'] = $_input['kualifikasi'];
		$dataSave['NKUALIFIKASI_GAJI'] = $_input['kualifikasigaji'];
		$dataSave['THPEMBULATAN_GAJI'] = $_input['thpgaji2'];
		$dataSave['JPEMBAYARAN_GAJI'] = $_input['kualifikasigaji'] * number_format(($_input['kh1'] / $_input['hk1']), 2);
		$param = array($dataSave, $where);
		$res = $this->dbCommand('update', $param);
		$pesan = 'menyimpan';

		if ($res > 0) {
			$data['result'] = true;
			$data['msg'] = 'Berhasil ' . $pesan . ' data gaji';
		}

		echo json_encode($data);
	}
	public function delete()
	{
		$_input = $this->input->post();
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat menghapus status data';

		$where = array();
		$where['ID_GAJI'] = $_input['id'];
		$res = $this->main->delete($where);
		$pesan = 'menghapus';

		if ($res > 0) {
			$data['result'] = true;
			$data['msg'] = 'Berhasil ' . $pesan . ' data gaji';
		}

		echo json_encode($data);
	}
	public function lokasi()
	{
		$where = array();
		$where['ID_LOKASI'] = $this->input->post('id');
		$data = $this->main->lokasi($where);
		echo json_encode($data);
	}
	public function kualifikasi()
	{
		$where = array();
		$where['ID_KUALIFIKASI'] = $this->input->post('id');
		$data = $this->main->kualifikasi($where);
		echo json_encode($data);
	}
	public function pegawai()
	{
		$where = array();
		$where['pegawai.NIP_PEG'] = $this->input->post('id');

		$data = $this->main->pegawai($where);
		if (!empty($data[0])) {
			echo json_encode($data);
		} else {
			$data = null;
			echo json_encode($data);
		}
	}
	public function ajaxedit()
	{
		$_input = $this->input->post();
		$data['result'] = false;
		$wherepegawai['pegawai.NIP_PEG'] = $this->input->post('nip');
		$datapegawai = $this->main->pegawai($wherepegawai);
		$where = array();
		$where['ID_GAJI'] = $_input['id'];
		$res = $this->main->ajaxedit($where);
		if ($res > 0) {
			$data['result'] = true;
			$data['data'] = $res;
			$data['datapegawai'] = $datapegawai;
		}
		echo json_encode($data);
	}
	public function rekap2()
	{
		// $this->load->model('fitur/penggajian_model', 'main', true);
		$_input = $this->input->post();
		if (!isset($_input['bulan']) && !isset($_input['tahun']))
			$this->index();
		else {
			$where = array('BULAN_GAJI' => $_input['bulan'], 'TAHUN_GAJI' => $_input['tahun']);
			$res = $this->main->dataexcel($where);
			$bulan = $_input['bulan'];
			$tahun = $_input['tahun'];
			$d = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
			$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
			$bln = $bulan[$_input['bulan'] - 1];
			$this->load->library("excel");
			$excel = new PHPExcel();
			//insert some data to PHPExcel object
			//Judul
			$excel->setActiveSheetIndex(0)->mergeCells('A1:K1')->setCellValue('A1', 'PT. SURVEYOR INDONESIA');
			$excel->setActiveSheetIndex(0)->mergeCells('A2:K2')->setCellValue('A2', 'RINCIAN PERHITUNGAN THP');
			$excel->setActiveSheetIndex(0)->mergeCells('A3:K3')->setCellValue('A3', '01 s.d. ' . $d . ' ' . $bln . ' ' . $tahun);

			$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(23);
			$excel->getActiveSheet()->getStyle("A2")->getFont()->setSize(10);
			$excel->getActiveSheet()->getStyle("A3")->getFont()->setSize(10);


			//header table
			$excel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Nama Pekerja')
				->setCellValue('C5', 'Kualifikasi')
				->setCellValue('D5', 'Jumlah Man Month')
				->setCellValue('E6', 'UPAH/ NET')
				->setCellValue('F6', 'Tunj. Umum')
				->setCellValue('G6', 'Tunj. Uang Makan')
				->setCellValue('H6', 'Tunj. Komunikasi')
				->setCellValue('I6', 'Jumlah')
				->setCellValue('J6', 'Pajak PPh 21')
				->setCellValue('K6', 'Jamsostek 2 %')
				->setCellValue('L6', 'Correction')
				->setCellValue('M6', 'Jumlah')
				->setCellValue('N5', 'THP');

			$excel->setActiveSheetIndex(0)->mergeCells('E5:I5')->setCellValue('E10', 'RINCIAN UPAH & TUNJANGAN');
			$excel->setActiveSheetIndex(0)->mergeCells('J5:M5')->setCellValue('J10', 'PENGURANGAN');

			$excel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$i = 1;
			$total = 0;
			$x = 7;
			$subtotal = 0;
			$lastKode = '';

			foreach ($res as $result) {
				$excel->setActiveSheetIndex(0)
					->setCellValue('A' . $x, $i)
					->setCellValue('B' . $x, $result['NAMA_PEG'])
					->setCellValue('C' . $x, $result['NAMA_KUALIFIKASI'])
					->setCellValue('D' . $x, $result['MM'])
					->setCellValue('E' . $x, $result['UPAH_TARIF'])
					->setCellValue('F' . $x, $result['TUMUM_TARIF'])
					->setCellValue('G' . $x, $result['TMAKAN_TARIF'])
					->setCellValue('H' . $x, $result['TKOMUNIKASI_TARIF'])
					->setCellValue('I' . $x, ($result['MM'] * ($result['UPAH_TARIF'] + $result['TUMUM_TARIF'] + $result['TMAKAN_TARIF'])) + $result['TKOMUNIKASI_TARIF'])
					->setCellValue('J' . $x, $result['PPH_GAJI'])
					->setCellValue('K' . $x, $result['JAMSOSTEK_GAJI'])
					->setCellValue('L' . $x, $result['COR_GAJI'])
					->setCellValue('M' . $x, ($result['PPH_GAJI'] + $result['JAMSOSTEK_GAJI'] + $result['COR_GAJI']))
					->setCellValue('N' . $x, $result['THP_GAJI']);


				$excel->getActiveSheet()->getStyle("E" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("F" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("G" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("H" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("I" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("J" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("K" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("L" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("M" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("N" . $x)->getNumberFormat()->setFormatCode('#,##0');
				$i++;
				$x++;
			}
			//autosize
			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		}
		ob_end_clean();
		$object_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="RINCIAN PERHITUNGAN THP ' . $bln . ' ' . $tahun . '.xls"');
		$object_writer->save('php://output');
		ob_end_clean();
		$data = $res;
		echo json_encode($data);
	}
}
