<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pass extends MY_Admin {

	public function index()
	{
		$this->data['_title']='Reset Password';
        $this->data['_page']='pages/admin/pass';
		$this->data['_scripts']=$this->data['_page'].'/scripts';
        $this->data['_head']=$this->data['_page'].'/head';
        // echo "<pre>";
        // print_r($sesi);
        // echo "</pre>";
		$this->parser->parse('home/index',$this->data);
    }
    public function resetpass(){
        $this->load->model('user/user_model', 'main', true);
        $sesi = $this->session->userdata();
        $table = 'user';
        $_input = $this->input->post();
		$res = 0;
		$data['result'] = false;
		$data['msg'] = 'Terjadi kesalahan saat menyimpan data. ';
        $result = false;
		$where = array('ID_USER' => $sesi['userId']);
        $refData = $this->main->check($where);
        if (md5($_input['passwordlama']) == $refData['PASSWORD_USER']) {
            if ($_input['passwordbaru'] == $_input['ulangpasswordbaru']){
                $datasave = array();
                $datasave['PASSWORD_USER'] = md5($_input['passwordbaru']);
                $this->main->update($datasave, $where, $table);
                $message = 'Password anda sudah di perbaharui.';
                $result = true;
            }else{
		        $message = 'Password baru tidak cocok.';
            }
        }else{
            $message = 'Password lama anda salah.';
        }
echo "<script type='text/javascript'>alert('$message');</script>";
if($result){
    redirect(site_url('admin/home'),'refresh');
}else{
    redirect(site_url('admin/pass'),'refresh');
}


    }
}
