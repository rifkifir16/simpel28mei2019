<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propinsi extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('propinsi_model','propinsi',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/propinsi/head';
		$this->data['_page']='pages/admin/propinsi';
		$this->data['_scripts']='pages/admin/propinsi/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */

	public function dbCommand($command='getData',$param){
		
		$table='propinsi';

		if($command=='getData')
			$res=$this->propinsi->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->propinsi->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->propinsi->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->propinsi->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->propinsi->count_filtered($param);
		
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function data(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_PROP']=1;
			elseif($param==2)
				$where['STATUS_PROP']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_PROP;
			$row[] = $field->NAMA_PROP;
			$row[] = $field->STATUS_PROP;
			$row[] = $field->STATUS_PROP;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	
	public function saveProp(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		$dataSave=array();
		$dataSave['NAMA_PROP']=htmlspecialchars($_input['namaPropinsi']);
		
		if($_input['idProp']==0) //jika addnew
		{
			$dataSave['STATUS_PROP']=1;
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_PROP']=$_input['idProp'];

			//$res=$this->jenis->update($dataSave,$where,'jenis_penelitian');
			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data propinsi';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();

		$dataSave=array();
		$where=array();
		$where['ID_PROP']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_PROP']=0;	
			$pesan="Berhasil menonaktifkan propinsi";
		}
		else{
			$dataSave['STATUS_PROP']=1;
			$pesan="Berhasil mengaktifkan propinsi";
		}
		//$res=$this->jenis->update($dataSave,$where,'jenis_penelitian');
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		echo $res>0 ? $pesan:'Terjadi kesalahan';
	}

	public function ajaxProp(){
		$q=$this->input->get('q');
		$res=$this->propinsi->ajaxData($q);
		
		if(count($res)>0){
			$r=$res;
		}
		else
			$r[] = array('id' => '', 'text' => 'Propinsi tidak ditemukan');
		
			echo json_encode($r);
	}
}
