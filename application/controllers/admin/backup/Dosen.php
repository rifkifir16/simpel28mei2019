<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('dosen_model','dosen',true);
	}
	
	public function index()
	{
		$this->data['_page']='pages/admin/dosen';
		$this->data['_scripts']='pages/admin/dosen/scripts';
		$this->data['_head']='pages/admin/dosen/head';

		/*$this->load->model('pangkat_model','pangkat',true);
		$this->data['_pangkat']=$this->pangkat->getData();

		$this->load->model('jabatan_model','jabatan',true);
		$this->data['_jabatan']=$this->jabatan->getData();

		$this->load->model('jurusan_model','jurusan',true);
		$this->data['_jurusan']=$this->jurusan->getData();
		*/
		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='dosen';

		if($command=='getData')
			$res=$this->dosen->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->dosen->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->dosen->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->dosen->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->dosen->count_filtered($param);
		
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataDosen(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_DOSEN']=1;
			elseif($param==2)
				$where['STATUS_DOSEN']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_USER;
			$row[] = $field->NIP;
			$row[] = $field->NIDN;
			$row[] = $field->NAMA_DOSEN;
			$row[] = $field->STATUS_DOSEN;
			$row[] = $field->STATUS_DOSEN;
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
	}

	public function ajaxDosen(){
		$this->load->model('dosen_model','dosen',true);
		$q=$this->input->get('q');
		//$this->data['sesi']['nip']
		$res=$this->dosen->ajaxData($this->data['sesi']['nip'],$q);
		$i=0;
		if(count($res)>0){
			foreach($res as $result){
				$r[$i]=array('id'=>$result['NIP'],'text'=>$result['NAMA_DOSEN']);
				$i++;
			}
		}
		else
			$r[] = array('id' => '', 'text' => 'Dosen tidak ditemukan');
		
			echo json_encode($r);
	}

	public function reqEdit(){
		$_input=$this->input->post();
		//$_input['id']=$this->uri->segment('4');;
		
		$result=$this->dosen->getEdit($_input['id']);
		$r='';
		
		$status=array('Nonaktif','Aktif');
		//foreach($res as $result)
		//{
			$r.='["'.$result['NIP'].'","'.$result['NIDN'].'","'.$result['NAMA_DOSEN'].'","'.$result['ALAMAT_KANTOR'].'","'.$result['EMAIL'].'","'.$result['NO_TLP'].'","'.$result['ALAMAT_RUMAH'].'","'.$result['ID_PANGKAT'].'","'.$result['ID_JABATAN'].'","'.$result['ID_JURUSAN'].'","'.$result['OTORISASI_USER'].'","'.$result['NIP'].'","'.$result['PANGKAT'].' - '.$result['NAMA_GOLONGAN'].'","'.$result['NAMA_JABATAN'].'","'.$result['NAMA_JURUSAN'].'"]';

		//}
		//echo '{"data":['.$r.']}';
		echo $r;
	}

	public function saveDosen(){
		
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	
		if($_input['otorisasi']<=$this->data['sesi']['hakakses']){

			$curTime=date('Y-m-d H:i:s');
			$dataSave=array();
			$dataSave['ID_PANGKAT']=$_input['pangkat'];
			$dataSave['ID_JABATAN']=$_input['jabatan'];
			$dataSave['ID_JURUSAN']=$_input['jurusan'];
			$dataSave['NIP']=$_input['nip'];
			$dataSave['NIDN']=$_input['nidn'];
			$dataSave['NAMA_DOSEN']=htmlspecialchars($_input['nama']);
			$dataSave['ALAMAT_KANTOR']=$_input['alamatKantor'];
			$dataSave['ALAMAT_RUMAH']=$_input['alamatRumah'];
			$dataSave['NO_TLP']=$_input['telp'];
			$dataSave['EMAIL']=$_input['email'];
			$dataSave['DOSEN_UPDATED_AT']=$curTime;
			
			

			$msg="Terjadi kesalahan.";
			$res=-1;
			if($_input['isedit']==0) //jika addnew
			{
				if($this->dosen->check(array('NIP'=>$dataSave['NIP']))>0){
					$data['msg']="NIP telah digunakan. Silahkan pilih NIP lain.";
					$res=0;
				}elseif($dataSave['NIDN']!=""){
					if($this->dosen->check(array('NIDN'=>$dataSave['NIDN']))>0){
						$data['msg']="NIDN telah digunakan. Silahkan pilih NIDN lain.";
						$res=0;
					}		
				}

				if($res==-1){
					
					$dataUser=array();
				
					$dataUser['NAMA_USER']=$dataSave['NAMA_DOSEN'];
					$dataUser['USERNAME_USER']=$_input['nip'];
					$dataUser['PASSWORD_USER']=md5($_input['nip']);
					$dataUser['OTORISASI_USER']=$_input['otorisasi'];
					$dataUser['STATUS_USER']=1;

					$dataSave['ID_USER']=$this->dosen->insert($dataUser,true,'user');
					$dataSave['DOSEN_CREATED_AT']=$curTime;
					$dataSave['STATUS_DOSEN']=1;

					$res=$this->dosen->insert($dataSave,false,'dosen');
					
					$pesan='menyimpan';
				}
			}
			else //jika update
			{
				$result=$this->dosen->getEdit($_input['isedit']);
				$condition=array();
				$condition['ID_USER']=$result['ID_USER'];
				$dataUser=array();
				$dataUser['USERNAME_USER']=$_input['nip'];
				$dataUser['NAMA_USER']=$dataSave['NAMA_DOSEN'];
				$dataUser['PASSWORD_USER']=md5($_input['nip']);
				$dataUser['OTORISASI_USER']=$_input['otorisasi'];
				//print_r($dataUser);
				$this->dosen->update($dataUser,$condition,'user');

				$where=array();
				$where['NIP']=$_input['isedit'];
				//update user, nama
				$res=$this->dosen->update($dataSave,$where,'dosen');
				
				$pesan='memperbaharui';
			}
		}
		else{
			$data['msg']="Anda tidak berhak menambahkan user dengan otorisasi tersebut.";
			$res=0;
		}
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data dosen';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';

		$dataSave=array();
		$where=array();
		$where['NIP']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_DOSEN']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_DOSEN']=1;
			$pesan="mengaktifkan";
		}
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data dosen';	
		}
		
		echo json_encode($data);
	}
}
