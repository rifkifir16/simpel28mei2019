<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MY_Admin {
	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('setting_model','setting',true);
	}

	public function index()
	{
		$this->data['_page']='pages/admin/setting';
		$this->data['_scripts']='pages/admin/setting/scripts';
		$res=$this->setting->getData();
		$setting=array();
		
		foreach($res as $r){
			$setting[$r['KEY_SETTINGS']]=$r['VALUE_SETTINGS'];
		}

		$this->data['_data']=$setting;
		/*echo '<pre>';
		print_r($this->data['_data']);
		echo '</pre>';*/
		$this->parser->parse('home/index',$this->data);
	}


	/* All ajax goes here */
	public function update(){
		$_input=$this->input->post();
		$res=true;
		$hasil="Terjadi kesalahan saat memperbaharui setting";

		$fields=array('pdekan','npdekan','ndekan','plppm','nplppm','nlppm');
		$curTime=date('Y-m-d H:i:s');
		$count=0;
		$counter=0;
		for($i=0;$i<count($fields);$i++){
			$dataSave=array();
			$dataSave['VALUE_SETTINGS']=htmlspecialchars($_input[$fields[$i]]);
			
			$where=array();
			$where['KEY_SETTINGS']=$fields[$i];
			//update user, nama
			if($this->setting->update($dataSave,$where,'settings'))
				$count++;
			$counter++;
		
		}
		if($count>0)
		{
			$res=false;
			$hasil="Berhasil memperbaharui setting. ".$count." perubahan telah disimpan.";
		}
		
		$response=array($res,$hasil);
		echo json_encode($response);
	}
}
