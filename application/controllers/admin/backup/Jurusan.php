<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('jurusan_model','jurusan',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/jurusan/head';
		$this->data['_page']='pages/admin/jurusan';
		$this->data['_scripts']='pages/admin/jurusan/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */

	public function dbCommand($command='getData',$param){
		
		$table='jurusan';

		if($command=='getData')
			$res=$this->jurusan->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->jurusan->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->jurusan->update($param[0],$param[1],$table);
		elseif($command=='delete')
			$res=$this->jurusan->delete($param,$table);
		elseif($command=='count_all')
			$res=$this->jurusan->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->jurusan->count_filtered($param);
		elseif($command=='ajax')
			$res=$this->jurusan->ajaxData($param[0],$param[1]);
		
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataJurusan(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_JURUSAN']=1;
			elseif($param==2)
				$where['STATUS_JURUSAN']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
			$row[] = $field->ID_JURUSAN;
			$row[] = $field->ID_PRODI;
			$row[] = $field->NAMA_PRODI;
			$row[] = $field->NAMA_JURUSAN;
			$row[] = $field->STATUS_JURUSAN;
			$row[] = $field->STATUS_JURUSAN;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function saveJurusan(){
		$_input=$this->input->post();
		$data['result']=true;
		$data['msg']='Terjadi kesalahan saat menyimpan data';

		$dataSave=array();
		$dataSave['ID_PRODI']=$_input['idProdi'];
		$dataSave['NAMA_JURUSAN']=htmlspecialchars($_input['namaJurusan']);
		
		if($_input['idJurusan']==0) //jika addnew
		{

			$dataSave['STATUS_JURUSAN']=1;
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_JURUSAN']=$_input['idJurusan'];

			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data jurusan';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_JURUSAN']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_JURUSAN']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_JURUSAN']=1;
			$pesan="mengaktifkan";
		}
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data kota';	
		}
		
		echo json_encode($data);
	}

	public function ajaxJurusan(){
		$where=array('STATUS_JURUSAN',1);
		$q=$this->input->get('q');
		$param=array($q,$where);
		$res=$this->dbCommand('ajax',$param);
		//$res=$this->propinsi->ajaxData($q);
		
		if(count($res)>0){
			$r=$res;
		}
		else
			$r[] = array('id' => '', 'text' => 'Jurusan tidak ditemukan');
		
			echo json_encode($r);
	}
}
