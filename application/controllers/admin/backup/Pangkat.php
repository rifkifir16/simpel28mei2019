<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pangkat extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('pangkat_model','pangkat',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/pangkat/head';
		$this->data['_page']='pages/admin/pangkat';
		$this->data['_scripts']='pages/admin/pangkat/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='pangkat';

		if($command=='getData')
			$res=$this->pangkat->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->pangkat->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->pangkat->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->pangkat->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->pangkat->count_filtered($param);
		elseif($command=='ajax')
			$res=$this->pangkat->ajaxData($param[0]);
		
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataPangkat(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_PANGKAT']=1;
			elseif($param==2)
				$where['STATUS_PANGKAT']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_PANGKAT;
			$row[] = $field->PANGKAT;
			$row[] = $field->NAMA_GOLONGAN;
			$row[] = $field->STATUS_PANGKAT;
			$row[] = $field->STATUS_PANGKAT;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function savePangkat(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';

		$dataSave=array();
		$dataSave['PANGKAT']=htmlspecialchars($_input['pangkat']);
		$dataSave['NAMA_GOLONGAN']=htmlspecialchars($_input['golongan']);
		
		if($_input['idPangkat']==0) //jika addnew
		{
			$dataSave['STATUS_PANGKAT']=1;
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_PANGKAT']=$_input['idPangkat'];

			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data pangkat';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';

		$dataSave=array();
		$where=array();
		$where['ID_PANGKAT']=$_input['id'];
		
		if($_input['status']>0){
			$dataSave['STATUS_PANGKAT']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_PANGKAT']=1;
			$pesan="mengaktifkan";
		}
		
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data pangkat';	
		}
		
		echo json_encode($data);
	}

	public function ajaxPangkat(){
		$q=$this->input->get('q');
		$param=array($q);
		$res=$this->dbCommand('ajax',$param);
		//$res=$this->propinsi->ajaxData($q);
		
		if(count($res)>0){
			$r=$res;
		}
		else
			$r[] = array('id' => '', 'text' => 'Pangkat tidak ditemukan');
		
			echo json_encode($r);
	}
}
