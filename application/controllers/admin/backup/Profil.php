<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends MY_Admin {
	private $_userId;

	public function index()
	{
		$this->data['_title']=$this->_title;
		$this->data['_page']='pages/admin/profil';
		$this->data['_scripts']='pages/admin/profil/scripts';
		$this->load->model('dosen_model','dosen',true);
		$this->load->model('user_model','user',true);
		$this->_userId=$this->data['sesi']['userid'];
		$this->data['_nip']=$this->_userId;
		$this->data['_data']=$this->user->getData($this->_userId);
		
		//print_r($this->data['_data']);
		$this->parser->parse('home/index',$this->data);
	}


	/* All ajax goes here */
	public function chpass(){
		$_input=$this->input->post();
		$this->load->model('user_model','user',true);
		$hasil='Terjadi kesalahan. ';

		$dataSave=array();
		$dataSave['PASSWORD_USER']=md5(htmlspecialchars($_input['pass']));
		
		if($this->_userId==0) //jika addnew
		{
			$res=0;
			$hasil.='Silahkan keluar dari aplikasi dan masuk kembali.';
		}
		else //jika update
		{
			//$this->load->model('dosen_model','dosen',true);
			//$result=$this->dosen->getEdit($_input['idUser']);

			$where=array();
			$where['ID_USER']=$this->_userId;//$_input['idUser'];

			$res=$this->user->update($dataSave,$where,'user');
			if($res>0) $hasil="Berhasil memperbaharui password.";
			
		}
		$response=array(($res<=0),$hasil);
		echo json_encode($response);
	}

	public function chprofil(){
		$_input=$this->input->post();
		$this->load->model('dosen_model','dosen',true);

		$curTime=date('Y-m-d H:i:s');
		$dataSave=array();
		$dataSave['NAMA_DOSEN']=htmlspecialchars($_input['nama']);
		$dataSave['ALAMAT_KANTOR']=$_input['alamatKantor'];
		$dataSave['ALAMAT_RUMAH']=$_input['alamatRumah'];
		$dataSave['NO_TLP']=$_input['telp'];
		$dataSave['EMAIL']=$_input['email'];
		$dataSave['DOSEN_UPDATED_AT']=$curTime;
		
		


		if($_input['idUser']==0) //jika addnew
		{
			//input dulu ke tabel user
			$res=0;
			$hasil.='Silahkan keluar dari aplikasi dan masuk kembali.';
		}
		else //jika update
		{
			$where=array();
			$where['NIP']=$_input['idUser'];
			//update user, nama
			$res=$this->dosen->update($dataSave,$where,'dosen');
			if($res>0) $hasil="Berhasil memperbaharui data dosen.";
		}
		
		//echo $res>0 ? true:$hasil;
		$response=array(($res<=0),$hasil);
		echo json_encode($response);
	}
}
