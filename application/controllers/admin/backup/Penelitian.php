<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penelitian extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
	}

	public function index()
	{
		$this->data['_page']='pages/admin/penelitian';
		$this->data['_scripts']='pages/admin/penelitian/scripts';
		$this->data['_head']='pages/admin/penelitian/head';
		$this->data['_scripts']='pages/admin/penelitian/scripts';
		//$this->load->model('user_model','user',true);
		//$this->data['_user']=$this->user->getData();

		//$this->load->model('bidang_model','bidang',true);
		//$this->data['_bidang']=$this->bidang->getData();
		$where=array('STATUS_JENIS',1);
		$this->load->model('jenis_model','jenis',true);
		$this->data['_jenis']=$this->jenis->getData($where);

		$this->parser->parse('home/index',$this->data);
	}

	public function pkm()
	{
		$this->data['_page']='pages/admin/pkm';
		$this->data['_scripts']='pages/admin/pkm/scripts';
		$this->data['_head']='pages/admin/pkm/head';
		$this->data['_scripts']='pages/admin/pkm/scripts';
		
		$where=array('STATUS_JENIS',1);
		$this->load->model('jenis_model','jenis',true);
		$this->data['_jenis']=$this->jenis->getData($where);

		$this->parser->parse('home/index',$this->data);
	}

	
	/*All ajax below here*/
	public function dataPenelitian(){
		$where=array('STATUS_PENELITIAN >'=>'0');
		
		$this->load->model('penelitian_model','penelitian',true);
		
		$list=$this->penelitian->getDatas($where);

		$data = array();
		$no = isset($_POST['start'])?$_POST['start']:0;
		$status=array(
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Pending</a>',
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Belum disetujui</a>',
			' class="btn btn-xs bg-yellow"><i class="fa fa-warning"></i> Perlu Revisi</a>',
			' class="btn btn-xs bg-aqua"><i class="fa fa-round"></i> Sudah Direvisi</a>',
			' class="btn btn-xs btn-success"><i class="fa fa-check"></i> Disetujui</a>'
		);
		$jenis=array('-','Swadana Fakultas','Swadana Jurusan','-');
        foreach ($list as $field) {
			$proposal=" - ";
			if($field->PROPOSAL_PENELITIAN!='')
				$proposal='<a href="'.$field->PROPOSAL_PENELITIAN.'">Download</a>';
			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $field->ID_PENELITIAN;
			$row[] = $field->TANGGAL;
			$row[] = $field->JUDUL_PENELITIAN;
			$row[] = $field->NAMA_JENIS;
			$row[] = $field->NAMA_DOSEN;
			$row[] = $field->ANGGOTA;
			$row[] = $proposal;
			$row[] = $status[$field->STATUS_PENELITIAN];
			$row[] = $field->ID_PENELITIAN;
			
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->penelitian->count_all($where),
            "recordsFiltered" => $this->penelitian->count_filtered($where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$this->load->model('penelitian_model','penelitian',true);
		
		$dataSave=array();
		$where=array();
		$where['ID_PENELITIAN']=$_input['id'];

		$status=array(2=>'meminta revisi',4=>'menyetujui');
		$pesan="Terjadi kesalahan. Tidak berhasil ".$status[$_input['status']]." penelitian";

		$dataSave['STATUS_PENELITIAN']=$_input['status'];	
		
		$res=$this->penelitian->update($dataSave,$where,'penelitian');

		if($res>0)
			$pesan="Berhasil ".$status[$_input['status']]." penelitian";
		
		$response=array($res<=0,$pesan);
		echo json_encode($response);
		//echo $res>0 ? $pesan:'Terjadi kesalahan';
	}
}
