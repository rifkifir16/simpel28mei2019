<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkm extends MY_Dosen {
	private $replacer='';
	private $_userId;
	private $_nip;

	function __construct(){
		parent::__construct();
		$this->replacer=str_replace('\\','/',FCPATH);  //WAJIB DIISI, UNTUK RELATIVE PATH UPLOAD LINK
		$this->_title="Pkm";
		$this->data['_title']=$this->_title;
		$this->_userId=$this->data['sesi']['userid'];
		$this->_nip=$this->data['sesi']['nip'];
	}

	public function index()
	{	
		
		$this->data['_page']='pages/dosen/pkm';
		$this->data['_scripts']='pages/dosen/pkm/scripts';
		$this->data['_head']='pages/dosen/pkm/head';
		

		$this->parser->parse('home/index',$this->data);
	}
	
	/*All ajax below here*/
	public function reqEdit(){
		$_input=$this->input->post();

		$this->load->model('pkm_model','pkm',true);
		$result=$this->pkm->getEdit($_input['id']);
		$status=array('Nonaktif','Aktif');		
		//$r='["'.$result['ID_PKM'].'","'.$result['JUDUL_PKM'].'","'.$result['ID_BIDANG'].'","'.$result['JENIS_BIDANG'].'","'.$result['BIAYA_PKM'].'","'.$result['ID_JENIS'].'","'.$result['ANGGOTA'].'","'.$result['NAMA_JENIS'].'"]';
		//echo $r;
		echo json_encode(array_values($result));
	}

	public function savePkm(){
		$_input=$this->input->post();
		$_err=false;$upload=false;
		$new_name='';$hasil='';
		$id=0;
		$userId=$this->_nip;
		$curTime=date('Y-m-d H:i:s');

		$this->load->model('pkm_model','pkm',true);
		$this->load->model('anggota_model','anggota',true);
		$penCnt=$this->pkm->allowReg($userId,date('Y'));
		if($penCnt>0){
			$_err=true;
			$hasil='Anda telah mengajukan penelitian/pkm pada tahun ini sebagai ketua.';
		}
		else {
			//cek upload
			if (!empty($_FILES['proposal']['name'])) {
				$upload=true;
				$date = date('Y-m-d');
				$time=date('His');
				$path = realpath(FCPATH . 'assets/docs').'/'.$date;
				$search=$this->replacer;
				
				if (!is_dir($path)) {
					mkdir($path, 0777, TRUE);
				}
				
				$config['upload_path']          = $path;
				$config['allowed_types']        = 'doc|docx|odt|pdf';
				$config['max_size']             = 1024*5;
				$config['file_ext_tolower']		= true;
				$new_name = str_replace('-','',$date).'_'.$userId.'_'.$time;
				$config['file_name'] = strtolower($new_name);

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('proposal'))
				{
						$error = array('error' => $this->upload->display_errors());
						$hasil=$this->upload->display_errors();
						$_err=true;
				}
				else
				{
						$picPath=$this->upload->data();//full_path
						$_err=false;
				}
			}

			if($_err==false){
				$biaya=$_input['biaya']==''?0:str_replace(".","",$_input['biaya']);
				$jarak=$_input['jarak']==''?0:str_replace(".","",$_input['jarak']);
				$dataSave=array();
				$dataSave['ID_JENIS']=$_input['jenis'];
				$dataSave['ID_BIDANG']=$_input['bidangk'];
				$dataSave['ID_KOTA']=$_input['kota'];
				$dataSave['NIP']=$userId;
				$dataSave['UPDATED_AT_PKM']=$curTime;
				
				
				$dataSave['JUDUL_PKM']=htmlspecialchars($_input['judul']);
				$dataSave['BIAYA_PKM']=$biaya;
				$dataSave['SUMBER_BIAYA_PKM']=$_input['sbBiaya'];
				$dataSave['MITRA_PKM']=htmlspecialchars($_input['mitra']);
				$dataSave['WIL_MITRA_PKM']=htmlspecialchars($_input['desa']);
				$dataSave['JARAK_PKM']=$jarak;

				//proses input mahasiswa
				$mhs=$_input['mahasiswa'];
				$inputs=explode("\n",$mhs);
				$str="";
				for($i=0;$i<count($inputs)-1;$i++){
					$str.=$inputs[$i].'#';
				}
				$str.=$inputs[count($inputs)-1];
				
				$dataSave['MAHASISWA_PKM']=htmlspecialchars($str);
				//end proses input mahasiswa
				$dataSave['OUTPUT_PKM']=htmlspecialchars($_input['output']);
				$dataSave['PELAKSANAAN_PKM']=$_input['waktu'];
				$created_path='';
				if(isset($picPath['full_path'])){
					$created_path=str_replace($search,'',$picPath['full_path']);
				}

				//$dataSave['PROPOSAL_PENELITIAN']=$created_path;
				
				if($_input['isedit']==0) //jika addnew
				{
					
					$dataSave['CREATED_AT_PKM']=$curTime;
					$dataSave['RESPONDER_PKM']=0;
					$dataSave['STATUS_PKM']=1;
					
					$id=$this->pkm->insert($dataSave,true);
					$hasil.="Data PKM berhasil ditambahkan.<br/>";
				}
				else //jika update
				{
					//delete anggota
					$id=$_input['isedit'];
					$where=array();
					$where['ID_PKM']=$id;
					if($_input['status']==2)
					$dataSave['STATUS_PKM']=3;
					$this->pkm->delAnggota($where,'anggota_pkm');
					$res=$this->pkm->update($dataSave,$where,'pkm');				
				}

				if($id>0){
					//save anggota
					$members=0;$dupmembers=0;
					$ident=array();
					$anggota = $_input['anggota'];//array_unique(
					$bidang = $_input['bidang'];
					for($i=0;$i<count($anggota);$i++){
						if(in_array($anggota[$i],$ident)){
							$dupmembers++;
						}
						else{
							$ident[$i]=$anggota[$i];
							$dataLink=array();
							$dataLink['ID_PKM']=$id;
							$dataLink['NIP']=$anggota[$i];
							$dataLink['ID_BIDANG']=$bidang[$i];
							$dataLink['APKM_CREATED_AT']=$curTime;
							$dataLink['APKM_UPDATED_AT']=$curTime;
							$xy=$this->anggota->insert($dataLink,false,'anggota_pkm');
							if($xy>0) $members+=1;
						}
					}
					$hasil.=' '.$members.' anggota berhasil ditambahkan.';  
					if($dupmembers>0)
						$hasil.=' '.$dupmembers.' duplikat hanya disimpan sebagai satu orang.';
					if($created_path!=''){
						//save proposal penelitian
						$dataProposal=array();
						$dataProposal['CREATED_AT_PPKM']=$curTime;
						$dataProposal['ID_PKM']=$id;
						$dataProposal['LOCATION_PPKM']=$created_path;
						$result=$this->pkm->insert($dataProposal,true,'proposal_pkm');
						$hasil.='<br/>Dokumen telah tersimpan.';

						//save id proposal ke penelitian
						$updatePenelitian=array();
						$updatePenelitian['ID_PPKM']=$result;

						$where=array();
						$where['ID_PKM']=$id;

						$res=$this->pkm->update($updatePenelitian,$where,'pkm');				
					}
				}
				else //Gagal menyimpan data, hapus gambar
				{
					if($upload==true)
						unlink($picPath['full_path']);
				}
				if($upload==true)
					@unlink($_FILES['proposal']);
			}
		}
		echo json_encode(array($_err,$hasil));
	}

	public function hapus(){
		$_input=$this->input->get();
		$this->load->model('pkm_model','pkm',true);

		$dataSave=array();
		$where=array();
		$where['ID_PKM']=$_input['id'];

		$dataSave['STATUS_PKM']=0;
		$res=$this->pkm->update($dataSave,$where,'pkm');
		
		
		echo $res>0 ? 'Berhasil menghapus data':'Terjadi kesalahan';
	}

	public function data(){
		$param=$this->uri->segment('4');
		$where=array('STATUS_PKM >'=>'0','p.NIP'=>$this->_nip);
		
		$this->load->model('pkm_model','pkm',true);
		
		$list=$this->pkm->getDatas($where);

		$data = array();
		$no = isset($_POST['start'])?$_POST['start']:0;
		$status=array(
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Pending</a>',
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Belum disetujui</a>',
			' class="btn btn-xs bg-yellow"><i class="fa fa-warning"></i> Perlu Revisi</a>',
			' class="btn btn-xs bg-aqua"><i class="fa fa-round"></i> Sudah Direvisi</a>',
			' class="btn btn-xs btn-success"><i class="fa fa-check"></i> Disetujui</a>'
		);
		$jenis=array('-','Swadana Fakultas','Swadana Jurusan','-');
        foreach ($list as $field) {
			$proposal=" - ";
			if($field->PROPOSAL_PKM!='')
				$proposal='<a href="'.base_url($field->PROPOSAL_PKM).'" class="btn btn-success btn-xs">Download</a><br/><a href="'.site_url('cetak/pkm/'.$field->ID_PKM).'" target="_blank" class="btn btn-primary btn-xs">Cetak Lembar Pengesahan</a>';
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $field->ID_PKM;
			$row[] = $field->TANGGAL;
			$row[] = $field->JUDUL_PKM;
			$row[] = $field->NAMA_JENIS;
			$row[] = $field->NAMA_DOSEN;
			$row[] = $field->ANGGOTA;
			$row[] = $proposal;
			$row[] = $status[$field->STATUS_PKM];
			$row[] = $field->STATUS_PKM;
			
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->pkm->count_all($where),
            "recordsFiltered" => $this->pkm->count_filtered($where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
}
