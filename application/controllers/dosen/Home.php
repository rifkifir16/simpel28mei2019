<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Dosen {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('home_model','home',true);
	}

	public function index()
	{
		$this->data['_page']='pages/dosen/home';

		$this->data['penelitian']=$this->home->homeDosen(array('NIP'=>$this->data['sesi']['nip'],'STATUS_PENELITIAN >'=>0),'penelitian');
		$this->data['pkm']=$this->home->homeDosen(array('NIP'=>$this->data['sesi']['nip'],'STATUS_PKM >'=>0),'pkm');

		$this->parser->parse('home/index',$this->data);
	}
}
