<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends MY_Dosen {
	

	public function index()
	{
		$this->data['_title']=$this->_title;
		$this->data['_name']='Mas Bambang';
		$this->data['_page']='pages/dosen/profil';
		$this->data['_scripts']='pages/dosen/profil/scripts';
		$this->load->model('dosen_model','dosen',true);
		$var=$this->data['sesi']['nip'];
		$this->data['_nip']=$var;
		$this->data['_data']=$this->dosen->getDosen($var);
		
		//print_r($this->data['_data']);
		$this->parser->parse('home/index',$this->data);
	}


	/* All ajax goes here */
	public function chpass(){
		$_input=$this->input->post();
		$this->load->model('user_model','user',true);
		$hasil='Terjadi kesalahan. ';

		$dataSave=array();
		$dataSave['PASSWORD_USER']=md5(htmlspecialchars($_input['pass']));
		
		if($_input['idUser']==0) //jika addnew
		{
			$res=0;
			$hasil.='Silahkan keluar dari aplikasi dan masuk kembali.';
		}
		else //jika update
		{
			$this->load->model('dosen_model','dosen',true);
			$result=$this->dosen->getEdit($_input['idUser']);

			$where=array();
			$where['ID_USER']=$result['ID_USER'];//$_input['idUser'];

			$res=$this->user->update($dataSave,$where,'user');
			if($res>0) $hasil="Berhasil memperbaharui password.";
			
		}
		$response=array(($res<=0),$hasil);
		echo json_encode($response);
	}

	public function chprofil(){
		$_input=$this->input->post();
		$_input['idUser']=$this->data['sesi']['nip'];
		$this->load->model('dosen_model','dosen',true);

		$curTime=date('Y-m-d H:i:s');
		$dataSave=array();
		$namdos=isset($_input['nama'])?htmlspecialchars($_input['nama']):"";
		$alkan=isset($_input['alamatKantor'])?$_input['alamatKantor']:"";
		$alrum=isset($_input['alamatRumah'])?$_input['alamatRumah']:"";
		$telp=isset($_input['telp'])?$_input['telp']:"";
		$email=isset($_input['email'])?$_input['email']:"";
		$dataSave['NAMA_DOSEN']=$namdos;
		$dataSave['ALAMAT_KANTOR']=$alkan;
		$dataSave['ALAMAT_RUMAH']=$alrum;
		$dataSave['NO_TLP']=$telp;
		$dataSave['EMAIL']=$email;
		$dataSave['DOSEN_UPDATED_AT']=$curTime;
		
		$counter=0;
		$where=array();
		$where['NIP']=$_input['idUser'];
		//update user, nama
		$res=$this->dosen->update($dataSave,$where,'dosen');
		if($res>0) $counter++;
		
		$dataUser=array('NAMA_USER'=>$dataSave['NAMA_DOSEN']);
		$where=array('USERNAME_USER'=>$_input['idUser']);
		$res=$this->dosen->update($dataUser,$where,'user');
		if($res>0) $counter++;
		if($counter==2){
			$hasil="Berhasil memperbaharui data dosen. Silahkan logout untuk melihat hasil.";
			$this->session->set_userdata(array('nama'=>$dataSave['NAMA_DOSEN']));
		}
		else
			$hasil="Terjadi kesalahan.";
		
		
		//echo $res>0 ? true:$hasil;
		$response=array(($res<=0),$hasil);
		echo json_encode($response);
	}
}
