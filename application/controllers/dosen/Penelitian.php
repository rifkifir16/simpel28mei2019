<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penelitian extends MY_Dosen {
	private $replacer='';
	private $_userId;
	private $_nip;

	function __construct(){
		parent::__construct();
		$this->replacer=str_replace('\\','/',FCPATH);  //WAJIB DIISI, UNTUK RELATIVE PATH UPLOAD LINK
		$this->_title="Penelitian";
		$this->data['_title']=$this->_title;
		$this->_userId=$this->data['sesi']['userid'];
		$this->_nip=$this->data['sesi']['nip'];
	}

	public function index()
	{	
		
		$this->data['_page']='pages/dosen/penelitian';
		$this->data['_scripts']='pages/dosen/penelitian/scripts';
		$this->data['_head']='pages/dosen/penelitian/head';
		

		$this->parser->parse('home/index',$this->data);
	}

	/*All ajax below here*/	
	public function reqEdit(){
		$_input=$this->input->post();
		
		$this->load->model('penelitian_model','penelitian',true);
		$result=$this->penelitian->getEdit($_input['id']);
		$status=array('Nonaktif','Aktif');		
		$r='["'.$result['ID_PENELITIAN'].'","'.$result['JUDUL_PENELITIAN'].'","'.$result['ID_BIDANG'].'","'.$result['JENIS_BIDANG'].'","'.$result['BIAYA_PENELITIAN'].'","'.$result['ID_JENIS'].'","'.$result['ANGGOTA'].'","'.$result['NAMA_JENIS'].'","'.$result['SUMBER_BIAYA_PENELITIAN'].'"]';
		echo $r;
	}

	public function savePenelitian(){
		$_input=$this->input->post();
		$_err=false;$upload=false;
		$new_name='';$hasil='';
		$id=0;
		$userId=$this->_nip;
		$curTime=date('Y-m-d H:i:s');

		$this->load->model('penelitian_model','penelitian',true);
		$this->load->model('anggota_model','anggota',true);
		$penCnt=$this->penelitian->allowReg($userId,date('Y'));
		
		if($penCnt>0 || $_input['isedit']!=0){
			$_err=true;
			$hasil='Anda telah mengajukan penelitian/pkm pada tahun ini sebagai ketua.';
		}
		else {
		//cek upload
			if (!empty($_FILES['proposal']['name'])) {
				$upload=true;
				$date = date('Y-m-d');
				$time=date('His');
				$path = realpath(FCPATH . 'assets/docs').'/'.$date;
				$search=$this->replacer;
				
				if (!is_dir($path)) {
					mkdir($path, 0777, TRUE);
				}
				
				$config['upload_path']          = $path;
				$config['allowed_types']        = 'doc|docx|odt|pdf';
				$config['max_size']             = 1024*5;
				$config['file_ext_tolower']		= true;
				$new_name = str_replace('-','',$date).'_'.$userId.'_'.$time;
				$config['file_name'] = strtolower($new_name);

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('proposal'))
				{
						$error = array('error' => $this->upload->display_errors());
						$hasil=$this->upload->display_errors();
						$_err=true;
				}
				else
				{
						$picPath=$this->upload->data();//full_path
						$_err=false;
				}
			}

			if($_err==false){
				$biaya=$_input['biaya']==''?0:str_replace(".","",$_input['biaya']);
				$dataSave=array();
				$dataSave['ID_JENIS']=$_input['jenis'];
				$dataSave['ID_BIDANG']=$_input['bidang'];
				$dataSave['NIP']=$userId;
				$dataSave['UPDATED_AT_PENELITIAN']=$curTime;
				
				
				$dataSave['JUDUL_PENELITIAN']=htmlspecialchars(strtolower($_input['judul']));
				$dataSave['BIAYA_PENELITIAN']=$biaya;
				$dataSave['SUMBER_BIAYA_PENELITIAN']=$_input['sbBiaya'];
				$created_path='';
				if(isset($picPath['full_path'])){
					$created_path=str_replace($search,'',$picPath['full_path']);
				}
				//$hasil.=$created_path;
				//$dataSave['PROPOSAL_PENELITIAN']=$created_path;
				
				if($_input['isedit']==0) //jika addnew
				{
					
					$dataSave['CREATED_AT_PENELITIAN']=$curTime;
					$dataSave['RESPONDER_PENELITIAN']=0;
					$dataSave['STATUS_PENELITIAN']=1;
					
					$id=$this->penelitian->insert($dataSave,true);
					$hasil.="Data berhasil ditambahkan.<br/>";
				}
				else //jika update
				{
					//delete anggota
					$id=$_input['isedit'];
					$where=array();
					$where['ID_PENELITIAN']=$id;
					if($_input['status']==2)
					$dataSave['STATUS_PENELITIAN']=3;
					$this->penelitian->delAnggota($where,'anggota_penelitian');
					$res=$this->penelitian->update($dataSave,$where,'penelitian');				
				}

				if($id>0){
					//save anggota
					$members=0;
					$anggota = array_unique($_input['anggota']);
					for($i=0;$i<count($anggota);$i++){
						$dataLink=array();
						$dataLink['ID_PENELITIAN']=$id;
						$dataLink['NIP']=$anggota[$i];
						$dataLink['APENELITIAN_CREATED_AT']=$curTime;
						$dataLink['APENELITIAN_UPDATED_AT']=$curTime;
						$xy=$this->anggota->insert($dataLink,false,'anggota_penelitian');
						if($xy>0) $members+=1;
					}
					$hasil.=$members.' anggota berhasil ditambahkan.';  

					if($created_path!=''){
						//save proposal penelitian
						$dataProposal=array();
						$dataProposal['CREATED_AT_PPEN']=$curTime;
						$dataProposal['ID_PENELITIAN']=$id;
						$dataProposal['LOCATION_PPEN']=$created_path;
						$result=$this->penelitian->insert($dataProposal,true,'proposal_penelitian');
						$hasil.='<br/>Dokumen telah tersimpan.';

						//save id proposal ke penelitian
						$updatePenelitian=array();
						$updatePenelitian['ID_PPENELITIAN']=$result;

						$where=array();
						$where['ID_PENELITIAN']=$id;

						$res=$this->penelitian->update($updatePenelitian,$where,'penelitian');				
					}
				}
				else //Gagal menyimpan data, hapus gambar
				{
					if($upload==true)
						unlink($picPath['full_path']);
				}
				if($upload==true)
					@unlink($_FILES['proposal']);
			}
		}
		
		echo json_encode(array($_err,$hasil));
	}

	public function hapus(){
		$_input=$this->input->get();
		$this->load->model('penelitian_model','penelitian',true);

		$dataSave=array();
		$where=array();
		$where['ID_PENELITIAN']=$_input['id'];

		$dataSave['STATUS_PENELITIAN']=0;
		$res=$this->penelitian->update($dataSave,$where,'penelitian');
		
		
		echo $res>0 ? 'Berhasil menghapus data':'Terjadi kesalahan';
	}

	public function data(){
		$param=$this->uri->segment('4');
		$where=array('STATUS_PENELITIAN >'=>'0','p.NIP'=>$this->_nip);
		
		$this->load->model('penelitian_model','penelitian',true);
		
		$list=$this->penelitian->getDatas($where);

		$data = array();
		$no = isset($_POST['start'])?$_POST['start']:0;
		$status=array(
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Pending</a>',
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Belum disetujui</a>',
			' class="btn btn-xs bg-yellow"><i class="fa fa-warning"></i> Perlu Revisi</a>',
			' class="btn btn-xs bg-aqua"><i class="fa fa-round"></i> Sudah Direvisi</a>',
			' class="btn btn-xs btn-success"><i class="fa fa-check"></i> Disetujui</a>'
		);
		$jenis=array('-','Swadana Fakultas','Swadana Jurusan','-');
        foreach ($list as $field) {
			$proposal=" - ";
			if($field->PROPOSAL_PENELITIAN!=''){
				$proposal='<a href="'.base_url($field->PROPOSAL_PENELITIAN).'" class="btn btn-success btn-xs">Download</a>';
				$proposal.='<br/><a href="'.site_url('cetak/penelitian/'.$field->ID_PENELITIAN).'" target="_blank" class="btn btn-primary btn-xs">Cetak Lembar Pengesahan</a>';
			}
			$no++;
			$jumlah=$field->jumlah;
			/*if($jumlah>1){
				$proposal.='<br/> Revisi ke-'.($jumlah-1);
			}*/
			

            $row = array();
			$row[] = $no;
			$row[] = $field->ID_PENELITIAN;
			$row[] = $field->TANGGAL;
			$row[] = $field->JUDUL_PENELITIAN;
			$row[] = $field->NAMA_JENIS;
			$row[] = $field->NAMA_DOSEN;
			$row[] = $field->ANGGOTA;
			$row[] = $proposal;
			$row[] = $status[$field->STATUS_PENELITIAN];
			$row[] = $field->STATUS_PENELITIAN;
			
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->penelitian->count_all($where),
            "recordsFiltered" => $this->penelitian->count_filtered($where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
}
