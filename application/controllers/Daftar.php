<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Daftar extends CI_Controller
{

	protected $_title = "PT. Surveyor Indonesia";
	protected $data = array();
	private $replacer = '';

	function __construct()
	{
		parent::__construct();
		$this->replacer = str_replace('\\', '/', FCPATH);  //WAJIB DIISI, UNTUK RELATIVE PATH UPLOAD LINK
		//redirect('front');

	}

	public function index($msg = null)
	{
		$_cont = array();
		$_cont['_title'] = $this->_title;
		if (isset($msg))
			$_cont['msg'] = $msg;
		$this->parser->parse('daftar/top-nav', $_cont);
	}

	public function save()
	{
		$msg = array();
		$pesan = '';
		$msg['success'] = 0;
		$msg['msg'] = "Terdapat kesalahan saat menyimpan data. Silahkan coba beberapa saat lagi.";
		$isSuccess = 0;
		$_input = $this->input->post();
		$this->load->model('daftar_model', 'daftar', true);
		$where = array(
			"NOKTP_PELAMAR" => $_input['ktp'],
			"POSISI_PELAMAR" => $_input['posisi'],
		);
		$check = $this->daftar->check($where);
		if ($check < 1) {
			//upload foto
			if (!empty($_FILES['foto']['name'])) {
				$upload = true;
				$date = date('Y-m-d');
				$time = date('His');
				$path = realpath(FCPATH . 'assets/images/pelamar') . '/' . $date;
				$search = $this->replacer;

				if (!is_dir($path)) {
					mkdir($path, 0777, TRUE);
				}

				$config['upload_path']          = $path;
				$config['allowed_types']        = 'png|PNG|jpg|JPG';
				$config['max_size']             = 1024 * 5;
				$config['file_ext_tolower']		= true;
				$new_name = str_replace('-', '', $date) . '_' . $time;
				$config['file_name'] = strtolower($new_name);

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('foto')) {
					$error = array('error' => $this->upload->display_errors());
					$pesan = $this->upload->display_errors();
					$_err = true;
				} else {
					$picPath = $this->upload->data(); //full_path
					$_err = false;
				}
			}

			$created_path = '';
			if (isset($picPath['full_path'])) {
				$created_path = str_replace($search, '', $picPath['full_path']);
			}

			$id = $this->daftar->tambahPelamar($created_path);
			if ($id > 0) {
				if ($this->daftar->tambahKeluarga($id)) {
					$isSuccess++;
				}
				if ($this->daftar->tambahRelasi($id)) {
					$isSuccess++;
				}
				if ($this->daftar->tambahBahasa($id)) {
					$isSuccess++;
				}
				if ($this->daftar->tambahRiwayat($id)) {
					$isSuccess++;
				}
				if ($this->daftar->tambahPengalaman($id)) {
					$isSuccess++;
				}
				if ($this->daftar->tambahReferensi($id)) {
					$isSuccess++;
				}
				if ($this->daftar->tambahOrganisasi($id)) {
					$isSuccess++;
				}
				if ($this->daftar->tambahLain($id)) {
					$isSuccess++;
				}
			}

			if ($isSuccess >= 8) {
				$msg['success'] = 1;
				$msg['msg'] = "Berhasil menyimpan data. Kami akan menghubungi anda jika anda memenuhi kriteria kami.";
			}
		} else {
			$msg['success'] = 1;
			$msg['msg'] = "Maaf anda sudah terdaftar sebelumnya";
		}


		// $msg['msg'].=$pesan;

		echo json_encode($msg);
	}

	public function login()
	{
		$input = $this->input->post();
		if (count($input) > 0) {
			$this->load->model('home_model', 'home', true);

			$where = array('USERNAME_USER' => $input['uname']);

			$res = $this->home->userCheck($where);
			if (count($res) <= 0) //jika kosong
				$this->index('Username tidak ditemukan');
			else {
				if (md5($input['passw']) == $res['PASSWORD_USER']) {
					$data = array(
						'userId'  => $res['ID_USER'],
						'nip'     => $res['USERNAME_USER'],
						'nama'     => $res['NAMA_USER'],
						'hakAkses' => $res['HAK_USER'],
					);

					$this->session->set_userdata($data);
					if ($res['HAK_USER'] == 1)
						redirect('dosen/home');
					else //if($res['HAK_USER']==2 || $res['HAK_USER']==3)
						redirect('admin/home');
				} else
					$this->index('Password yang anda masukkan salah');
			}
		}
		//print_r(count($this->input->post()));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->index('Anda berhasil logout.');
	}
}
