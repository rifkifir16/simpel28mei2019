<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends MY_Controller {

	function __construct(){
		parent::__construct();
		//$this->_title="Penelitian";
		$this->data['_title']=$this->_title;
		$this->_userId=$this->data['sesi']['userid'];
		$this->_nip=$this->data['sesi']['nip'];
		$this->data['anggota']=array();

		$this->load->model('setting_model','setting',true);
		$res=$this->setting->getData();
		$setting=array();
		
		foreach($res as $r){
			$setting[$r['KEY_SETTINGS']]=$r['VALUE_SETTINGS'];
		}
		$this->data['_data']=$setting;
	}

	public function penelitian()
	{
		$param=$this->uri->segment('3');

		$this->load->model('cetak_model','cetak',true);
		$result=$this->cetak->getPenelitian($param);	
		$anggota=$this->cetak->anggotaPenelitian($param);
		
		$this->data['datas']=$result;
		$tanggal=date("d");
		$bulan=(int)date("m")-1;
		$tahun=date("Y");
		$bln=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

		$this->data['datas']['tanggal']='Surabaya, '.$tanggal.' '.$bln[$bulan].' '.$tahun;
		$this->data['anggota']=$anggota;
		$this->parser->parse('cetak_penelitian',$this->data);
	}

	public function pkm()
	{
		$param=$this->uri->segment('3');

		$this->load->model('cetak_model','cetak',true);
		$result=$this->cetak->getPKM($param);	
		$anggota=$this->cetak->anggotaPKM($param);
		
		$this->data['datas']=$result;
		$tanggal=date("d");
		$bulan=(int)date("m")-1;
		$tahun=date("Y");
		$bln=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		
		$this->data['datas']['tanggal']='Surabaya, '.$tanggal.' '.$bln[$bulan].' '.$tahun;
		$this->data['anggota']=$anggota;

		$this->data['mahasiswa']=explode('#',$result['MAHASISWA_PKM']);
		$this->data['jumlah']=count($anggota)+count($this->data['mahasiswa']);
		
		$this->parser->parse('cetak_pkm',$this->data);
	}
}

