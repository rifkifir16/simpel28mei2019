<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends MY_Admin {

	public function index()
	{
		$this->data['_title']=$this->_title;
		$this->data['_page']='pages/admin/rekap';
		$this->data['_scripts']='pages/admin/rekap/scripts';

		$this->parser->parse('home/index',$this->data);
	}
	
	public function generate(){
		$this->load->model('rekap_model','rekap',true);
		
		$input=$this->input->post();
		//$input['kategori']
		/*echo '<pre>';
		print_r($res);
		echo '</pre>';
		exit();
		*/
		$kategori=$input['kategori'];
		$numKat=0;

		$styleArray = array(
			'font'  => array(
				'bold'  => false,
				'size'  => 11,
				'name'  => 'Times New Roman'
			));

		$this->load->library("excel");
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$object->getDefaultStyle()->applyFromArray($styleArray);
		$proc=1;
		$excel_row = -1;
		$start=1;
		$lfcr = chr(10) . chr(13);
		if($kategori!='pkm'){
			$numKat++;
			$res=$this->rekap->getPenelitian();
			foreach(range('A','H') as $columnID) {
				$object->getActiveSheet()->getStyle($columnID)->getAlignment()->setWrapText(true);
				$object->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}
			foreach($res as $row)
			{
				if($proc==1){
					$excel_row+=2;
					
					$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $numKat.'. Penelitian Kebijakan Fakultas dan Jurusan.');
					$object->getActiveSheet()->mergeCells('A'.$excel_row.':G'.$excel_row);
					$excel_row+=1;	
					$table_columns = array("No", "Jurusan", "Judul", "Nama", "NIDN","DANA","SKIM");
					$column = 0;
					foreach($table_columns as $field)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, $field);
						$column++;
					}
					$excel_row+=1;	
					$start=1;
					$proc++;
				}
				$tempNama=$row->NAMA_DOSEN.'\n'.$row->ANGGOTA;
				$nama=stripcslashes($tempNama);//
				$nama=str_replace("\n",PHP_EOL,$nama);
				$tempNIDN=$row->NIDA."\n".$row->NIDN;
				$nidn=stripcslashes($tempNIDN);//str_replace("'",'"',$tempNIDN);
				$nidn=str_replace("\n",PHP_EOL,$nidn);
				$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $start);
				$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->NAMA_JURUSAN);
				$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->JUDUL_PENELITIAN);
				$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $nama);
				$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $nidn);
				$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, 'Rp. '.number_format($row->BIAYA_PENELITIAN).' ('.$row->SUMBER_BIAYA_PENELITIAN.')');
				$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->NAMA_JENIS);
				$excel_row++;
				$start++;
			}
		}
		$proc=1;
		if($kategori!='penelitian'){
			$numKat++;
			$res=$this->rekap->getPkm();
			foreach(range('A','H') as $columnID) {
				$object->getActiveSheet()->getStyle($columnID)->getAlignment()->setWrapText(true);
				$object->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}
			foreach($res as $row)
			{
				if($proc==1){
					$excel_row+=2;
					$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $numKat.'. PKM Kebijakan Fakultas dan Jurusan.');
					$object->getActiveSheet()->mergeCells('A'.$excel_row.':G'.$excel_row);
					$excel_row+=1;	
					$table_columns = array("No", "Jurusan", "Judul", "Nama", "NIDN","DANA","SKIM");
					$column = 0;
					foreach($table_columns as $field)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, $field);
						$column++;
					}
					$excel_row+=1;	
					$start=1;
					$proc++;
				}
				$tempNama=$row->NAMA_DOSEN.'\n'.$row->ANGGOTA;
				$nama=stripcslashes($tempNama);//
				$nama=str_replace("\n",PHP_EOL,$nama);
				$tempNIDN=$row->NIDA."\n".$row->NIDN;
				$nidn=stripcslashes($tempNIDN);//str_replace("'",'"',$tempNIDN);
				$nidn=str_replace("\n",PHP_EOL,$nidn);
				$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $start);
				$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->NAMA_JURUSAN);
				$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->JUDUL_PKM);
				$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $nama);
				$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $nidn);
				$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, 'Rp. '.number_format($row->BIAYA_PKM).' ('.$row->SUMBER_BIAYA_PKM.')');
				$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->NAMA_JENIS);
				$excel_row++;
				$start++;
			}

		}
		//$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
		

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Rekapitulasi Penelitian.xls"');
		$object_writer->save('php://output');
	}
	
}
