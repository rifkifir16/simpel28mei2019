<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkm extends MY_Admin {

	public function index()
	{
		$this->data['_title']=$this->_title;
		$this->data['_name']='Mas Bambang';
		$this->data['_page']='pages/admin/pkm';
		$this->data['_scripts']='pages/admin/pkm/scripts';
		$this->data['_head']='pages/admin/pkm/head';
		$this->data['_scripts']='pages/admin/pkm/scripts';
		
		$where=array('STATUS_JENIS',1);
		$this->load->model('jenis_model','jenis',true);
		$this->data['_jenis']=$this->jenis->getData($where);

		$this->parser->parse('home/index',$this->data);
	}

	
	/*All ajax below here*/
	public function dataPenelitian(){
		$where=array('STATUS_PKM >'=>'0');
		
		
		$this->load->model('pkm_model','penelitian',true);
		
		$list=$this->penelitian->getDatas($where);

		$data = array();
		$no = isset($_POST['start'])?$_POST['start']:0;
		$status=array(
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Pending</a>',
			' class="btn btn-xs bg-orange"><i class="fa fa-ban"></i> Belum disetujui</a>',
			' class="btn btn-xs bg-yellow"><i class="fa fa-warning"></i> Perlu Revisi</a>',
			' class="btn btn-xs bg-aqua"><i class="fa fa-round"></i> Sudah Direvisi</a>',
			' class="btn btn-xs btn-success"><i class="fa fa-check"></i> Disetujui</a>'
		);
		$jenis=array('-','Swadana Fakultas','Swadana Jurusan','-');
        foreach ($list as $field) {
			$proposal=" - ";
			if($field->PROPOSAL_PKM!='')
				$proposal='<a href="'.$field->PROPOSAL_PKM.'">Download</a>';
			
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $field->ID_PKM;
			$row[] = $field->TANGGAL;
			$row[] = $field->JUDUL_PKM;
			$row[] = $field->NAMA_JENIS;
			$row[] = $field->NAMA_DOSEN;
			$row[] = $field->ANGGOTA;
			$row[] = $proposal;
			$row[] = $status[$field->STATUS_PKM];
			$row[] = $field->ID_PKM;
			
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->penelitian->count_all($where),
            "recordsFiltered" => $this->penelitian->count_filtered($where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$this->load->model('pkm_model','penelitian',true);
		
		$dataSave=array();
		$where=array();
		$where['ID_PKM']=$_input['id'];

		$status=array(2=>'meminta revisi',4=>'menyetujui');
		$pesan="Terjadi kesalahan. Tidak berhasil ".$status[$_input['status']]." PKM";

		$dataSave['STATUS_PKM']=$_input['status'];	
		
		$res=$this->penelitian->update($dataSave,$where,'pkm');

		if($res>0)
			$pesan="Berhasil ".$status[$_input['status']]." PKM";
		
		$response=array($res<=0,$pesan);
		echo json_encode($response);
		//echo $res>0 ? $pesan:'Terjadi kesalahan';
	}
}
