<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('jenis_model','jenis',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/jenis/head';
		$this->data['_page']='pages/admin/jenis';
		$this->data['_scripts']='pages/admin/jenis/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='jenis_penelitian';

		if($command=='getData')
			$res=$this->jenis->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->jenis->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->jenis->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->jenis->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->jenis->count_filtered($param);
		elseif($command=='ajaxData')
			$res=$this->jenis->ajaxData($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataJenis(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_JENIS']=1;
			elseif($param==2)
				$where['STATUS_JENIS']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_JENIS;
			$row[] = $field->NAMA_JENIS;
			$row[] = $field->STATUS_JENIS;
			$row[] = $field->STATUS_JENIS;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function ajaxJenis(){
		$q=$this->input->get('q');
		$res=$this->dbCommand('ajaxData',$q);
		$i=0;
		if(count($res)>0){
			$r=$res;
		}
		else
			$r[] = array('id' => '', 'text' => 'Jenis penelitian tidak ditemukan');
		
			echo json_encode($r);
	}

	public function saveJenis(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data.';

		$dataSave=array();
		$dataSave['NAMA_JENIS']=htmlspecialchars($_input['namaJenis']);
		
		if($_input['idJenis']==0) //jika addnew
		{
			$dataSave['STATUS_JENIS']=1;
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_JENIS']=$_input['idJenis'];

			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data jenis';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_JENIS']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_JENIS']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_JENIS']=1;
			$pesan="mengaktifkan";
		}
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data jenis penelitian';	
		}
		
		echo json_encode($data);
	}
}
