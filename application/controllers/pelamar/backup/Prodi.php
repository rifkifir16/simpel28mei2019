<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('prodi_model','prodi',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/prodi/head';
		$this->data['_page']='pages/admin/prodi';
		$this->data['_scripts']='pages/admin/prodi/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='prodi';

		if($command=='getData')
			$res=$this->prodi->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->prodi->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->prodi->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->prodi->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->prodi->count_filtered($param);
		
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataProdi(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_PRODI']=1;
			elseif($param==2)
				$where['STATUS_PRODI']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_PRODI;
			$row[] = $field->NAMA_PRODI;
			$row[] = $field->STATUS_PRODI;
			$row[] = $field->STATUS_PRODI;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function ajaxProdi(){
		$q=$this->input->get('q');
		$res=$this->prodi->ajaxData($q);
		
		if(count($res)>0){
			$r=$res;
		}
		else
			$r[] = array('id' => '', 'text' => 'Dosen tidak ditemukan');
		
			echo json_encode($r);
	}

	public function saveProdi(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		$dataSave=array();
		$dataSave['NAMA_PRODI']=htmlspecialchars($_input['namaProdi']);
		
		if($_input['idProdi']==0) //jika addnew
		{
			$dataSave['STATUS_PRODI']=1;
			
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_PRODI']=$_input['idProdi'];

			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data bidang';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';

		$dataSave=array();
		$where=array();
		$where['ID_PRODI']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_PRODI']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_PRODI']=1;
			$pesan="mengaktifkan";
		}
		
		
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data prodi';	
		}
		
		echo json_encode($data);
	}
}
