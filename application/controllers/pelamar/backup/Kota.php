<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('kota_model','kota',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/kota/head';
		$this->data['_page']='pages/admin/kota';
		$this->data['_scripts']='pages/admin/kota/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */

	public function dbCommand($command='getData',$param){
		
		$table='kota';

		if($command=='getData')
			$res=$this->kota->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->kota->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->kota->update($param[0],$param[1],$table);
		elseif($command=='delete')
			$res=$this->kota->delete($param,$table);
		elseif($command=='count_all')
			$res=$this->kota->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->kota->count_filtered($param);
		elseif($command=='ajaxData')
			$res=$this->kota->ajaxData($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function data(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_KOTA']=1;
			elseif($param==2)
				$where['STATUS_KOTA']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
			$row[] = $field->ID_KOTA;
			$row[] = $field->ID_PROP;
			$row[] = $field->NAMA_KOTA;
			$row[] = $field->NAMA_PROP;
			$row[] = $field->STATUS_KOTA;
			$row[] = $field->STATUS_KOTA;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	
	public function saveKota(){
		$_input=$this->input->post();
		$data['result']=true;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		$dataSave=array();
		$dataSave['NAMA_KOTA']=htmlspecialchars($_input['namaKota']);
		$dataSave['ID_PROP']=$_input['prop'];
		
		if($_input['idKota']==0) //jika addnew
		{
			$dataSave['STATUS_KOTA']=1;
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_KOTA']=$_input['idKota'];

			//$res=$this->jenis->update($dataSave,$where,'jenis_penelitian');
			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data kota';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_KOTA']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_KOTA']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_KOTA']=1;
			$pesan="mengaktifkan";
		}
		//$res=$this->jenis->update($dataSave,$where,'jenis_penelitian');
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data kota';	
		}
		
		echo json_encode($data);
	}

	public function ajaxKota(){
		$q=$this->input->get('q');
		$res=$this->dbCommand('ajaxData',$q);
		$i=0;
		if(count($res)>0){
			$r=$res;
		}
		else
			$r[] = array('id' => '', 'text' => 'Kota tidak ditemukan');
		
			echo json_encode($r);
	}
}
