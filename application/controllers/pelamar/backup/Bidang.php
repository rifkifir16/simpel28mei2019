<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidang extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('bidang_model','bidang',true);
	}

	public function index()
	{
		$this->data['_page']='pages/admin/bidang';
		$this->data['_scripts']='pages/admin/bidang/scripts';
		$this->data['_head']='pages/admin/bidang/head';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='bidang';

		if($command=='getData')
			$res=$this->bidang->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->bidang->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->bidang->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->bidang->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->bidang->count_filtered($param);
		elseif($command=='ajaxData')
			$res=$this->bidang->ajaxData($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataBidang(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_BIDANG']=1;
			elseif($param==2)
				$where['STATUS_BIDANG']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_BIDANG;
			$row[] = $field->JENIS_BIDANG;
			$row[] = $field->STATUS_BIDANG;
			$row[] = $field->STATUS_BIDANG;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function ajaxBidang(){
		$q=$this->input->get('q');
		//$where=array('STATUS_JENIS',1);
		//$res=$this->bidang->getData($where);
		//$param=array(null,$where);
		$res=$this->dbCommand('ajaxData',$q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'Bidang tidak ditemukan');
		
		echo json_encode($r);
	}

	public function saveBidang(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		$dataSave=array();
		$dataSave['JENIS_BIDANG']=htmlspecialchars($_input['jenisBidang']);
		
		if($_input['idBidang']==0) //jika addnew
		{
			$dataSave['STATUS_BIDANG']=1;
			//$res=$this->bidang->insert($dataSave,true,'bidang');
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_BIDANG']=$_input['idBidang'];

			//$res=$this->bidang->update($dataSave,$where,'bidang');
			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data bidang';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_BIDANG']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_BIDANG']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_BIDANG']=1;
			$pesan="mengaktifkan";
		}
		//$res=$this->bidang->update($dataSave,$where,'bidang');
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data bidang';	
		}
		
		echo json_encode($data);
	}
}
