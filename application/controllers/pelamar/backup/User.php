<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('user_model','user',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/user/head';
		$this->data['_page']='pages/admin/user';
		$this->data['_scripts']='pages/admin/user/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='user';

		if($command=='getData')
			$res=$this->user->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->user->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->user->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->user->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->user->count_filtered($param);
		elseif($command=='selectData')
			$res=$this->user->selectData($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataUser(){
		
		$status=array(0=>'Nonaktif',1=>'Aktif',null=>'Undefined');
		$akses=array('-','User','Admin','Super Admin');
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_USER']=1;
			elseif($param==2)
				$where['STATUS_USER']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_USER;
			$row[] = $field->NAMA_USER;
			$row[] = $field->USERNAME_USER;
			$row[] = $akses[$field->OTORISASI_USER];
			$row[] = $field->STATUS_USER;
			$row[] = $field->STATUS_USER;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function saveUser(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		if($_input['otorisasi']<=$this->data['sesi']['hakakses']){
			$this->load->model('user_model','user',true);

			$dataSave=array();
			$dataSave['NAMA_USER']=htmlspecialchars($_input['nama']);
			$dataSave['USERNAME_USER']=htmlspecialchars($_input['username']);
			$dataSave['PASSWORD_USER']=md5(htmlspecialchars($_input['username']));
			$dataSave['OTORISASI_USER']=$_input['otorisasi'];
		

			if($_input['idUser']==0) //jika addnew
			{
				$dataSave['STATUS_USER']=1;
				if($this->user->check(array('USERNAME_USER'=>$dataSave['USERNAME_USER']))>0){
					$data['msg']="Username telah digunakan. Silahkan pilih username lain.";
					$res=0;
				}
				else{
					$param=array($dataSave,true);
					$res=$this->dbCommand('insert',$param);
					$pesan='menyimpan';
				}
			}
			else //jika update
			{
				$where=array();
				$where['ID_USER']=$_input['idUser'];

				$param=array($dataSave,$where);
				$res=$this->dbCommand('update',$param);
				$pesan='memperbaharui';
				
			}
		}
		else{
			$data['msg']="Anda tidak berhak menambahkan user dengan otorisasi tersebut.";
			$res=0;
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data user';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';

		$dataSave=array();
		$where=array();
		$where['ID_USER']=$_input['id'];
		
		if($_input['status']>0){
			$dataSave['STATUS_USER']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_USER']=1;
			$pesan="mengaktifkan";
		}
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data user';	
		}
		
		echo json_encode($data);
	}

	public function resetpass(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';

		//get username
		//$res=$this->user->getDatax($_input['id']);
		$where=array('ID_USER',$_input['id']);
		//$param=array(null,$where);
		$res=$this->dbCommand('selectData',$where);

		$username=$res[0]['USERNAME_USER'];
		$pass=$res[0]['PASSWORD_USER'];
		$pesan='Terjadi kesalahan saat mereset password.';

		if($pass==md5($username)){
			$res=1;
			
		}
		else{
			$dataSave=array();
			$where=array();
			$where['ID_USER']=$_input['id'];
			
			
			$dataSave['PASSWORD_USER']=md5($username);
			
			//$res=$this->user->update($dataSave,$where,'user');
			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil mereset password user';	
		}
		
		echo json_encode($data);
	}
}
