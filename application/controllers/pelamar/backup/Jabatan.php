<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends MY_Admin {
	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('jabatan_model','jabatan',true);
	}

	public function index()
	{
		$this->data['_head']='pages/admin/jabatan/head';
		$this->data['_page']='pages/admin/jabatan';
		$this->data['_scripts']='pages/admin/jabatan/scripts';

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='jabatan';

		if($command=='getData')
			$res=$this->jabatan->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->jabatan->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->jabatan->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->jabatan->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->jabatan->count_filtered($param);
		elseif($command=='ajax')
			$res=$this->jabatan->ajaxData($param[0]);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function dataJabatan(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('4');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_JABATAN']=1;
			elseif($param==2)
				$where['STATUS_JABATAN']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_JABATAN;
			$row[] = $field->NAMA_JABATAN;
			$row[] = $field->STATUS_JABATAN;
			$row[] = $field->STATUS_JABATAN;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function saveJabatan(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';

		$dataSave=array();
		$dataSave['NAMA_JABATAN']=htmlspecialchars($_input['jenisJabatan']);
		
		if($_input['idJabatan']==0) //jika addnew
		{
			$dataSave['STATUS_JABATAN']=1;
			
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_JABATAN']=$_input['idJabatan'];

			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data jabatan';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_JABATAN']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_JABATAN']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_JABATAN']=1;
			$pesan="mengaktifkan";
		}
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data jabatan';	
		}
		
		echo json_encode($data);
	}

	public function ajaxJabatan(){
		$q=$this->input->get('q');
		$param=array($q);
		$res=$this->dbCommand('ajax',$q);
		//$res=$this->propinsi->ajaxData($q);
		
		if(count($res)>0){
			$r=$res;
		}
		else
			$r[] = array('id' => '', 'text' => 'Jabatan tidak ditemukan');
		
			echo json_encode($r);
	}
}
