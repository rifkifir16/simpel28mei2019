<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends MY_Admin {
	protected $_title="";
	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('basic/kontak_model','main',true);
	}

	public function index()
	{
		$this->data['_page']='pages/admin/basic/kontak';
		$this->data['_scripts']=$this->data['_page'].'/scripts';
		$this->data['_head']=$this->data['_page'].'/head';
		$this->data['_ajaxSource']=base_url('admin/basic/kontak/data');

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='nama_kontak';

		if($command=='getData')
			$res=$this->main->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->main->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->main->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->main->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->main->count_filtered($param);
		elseif($command=='ajaxData')
			$res=$this->main->ajaxData($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function data(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('5');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['STATUS_KONTAK']=1;
			elseif($param==2)
				$where['STATUS_KONTAK']=0;
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ID_KONTAK;
			$row[] = $field->NAMA_KONTAK;
			$row[] = $field->STATUS_KONTAK;
			$row[] = $field->STATUS_KONTAK;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function ajax(){
		$q=$this->input->get('q');
		$res=$this->dbCommand('ajaxData',$q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'Kontak tidak ditemukan');
		
		echo json_encode($r);
	}

	public function save(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		$dataSave=array();
		$dataSave['NAMA_KONTAK']=htmlspecialchars($_input['kontak']);
		
		if($_input['idKontak']==0) //jika addnew
		{
			$dataSave['STATUS_KONTAK']=1;
			
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_KONTAK']=$_input['idKontak'];

			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data kontak';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_KONTAK']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_KONTAK']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_KONTAK']=1;
			$pesan="mengaktifkan";
		}
		
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data kontak';	
		}
		
		echo json_encode($data);
	}
}
