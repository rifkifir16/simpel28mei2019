<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Admin {
	/*
	Belum dibuatkan : 
	- Hak akses untuk site mana saja
	*/
	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('user/user_model','main',true);
	}

	public function index()
	{
		$this->data['_page']='pages/admin/user/user';
		$this->data['_scripts']=$this->data['_page'].'/scripts';
		$this->data['_head']=$this->data['_page'].'/head';
		$this->data['_ajaxSource']=base_url('admin/user/user/data');

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='user';

		if($command=='getData')
			$res=$this->main->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->main->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->main->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->main->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->main->count_filtered($param);
		elseif($command=='ajaxData')
			$res=$this->main->ajaxData($param);
		elseif($command=='check')
			$res=$this->main->check($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function data(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('5');
		$where=array('HAK_USER !=' => 'pegawai');
		/*if(isset($param)){
			if($param==1)
				$where['STATUS_AGAMA']=1;
			elseif($param==2)
				$where['STATUS_AGAMA']=0;
		}*/

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
        $no = isset($_POST['start'])?$_POST['start']:0;
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
			$row[] = $field->ID_USER;
			$row[] = $field->USERNAME_USER;
			$row[] = $field->NAMA_USER;
			$row[] = $field->INSTANSI_USER;
			$row[] = $field->HAK_USER;
 
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function ajax(){
		$q=$this->input->get('q');
		$res=$this->dbCommand('ajaxData',$q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'User tidak ditemukan');
		
		echo json_encode($r);
	}

	public function save(){
		$_input=$this->input->post();
		$res=0;
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data. ';	

		$where=array('USERNAME_USER'=>htmlspecialchars($_input['uname']));
		$refData=$this->dbCommand('check',$where);

		$dataSave=array();
		$dataSave['USERNAME_USER']=strtolower(htmlspecialchars($_input['uname']));
		$dataSave['PASSWORD_USER']=md5($dataSave['USERNAME_USER']);
		$dataSave['NAMA_USER']=htmlspecialchars($_input['nama']);
		$dataSave['INSTANSI_USER']=htmlspecialchars($_input['instansi']);
		$dataSave['HAK_USER']=htmlspecialchars($_input['akses']);
		
		if($_input['idUser']==0) //jika addnew
		{
			$data['msg'].='Username sudah digunakan, silahkan pilih username lainnya.';	
			if(count($refData)<1)
			{
				//$dataSave['STATUS_USER']=1;
				
				$param=array($dataSave,true);
				$res=$this->dbCommand('insert',$param);
				$pesan='menyimpan';
			}
		}
		else //jika update
		{
			$execute=false;
			$data['msg'].='Username sudah digunakan, silahkan pilih username lainnya.';	
			if(count($refData)>0)
			{
				if($refData['ID_USER']==$_input['idUser']){
					$execute=true;
				}
			}
			else
				$execute=true;

			if($execute==true){
				$where=array();
				$where['ID_USER']=$_input['idUser'];

				$param=array($dataSave,$where);
				$res=$this->dbCommand('update',$param);
				$pesan='memperbaharui';
			}
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data user';	
		}

		echo json_encode($data);
	}

	public function resetpass(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_USER']=$_input['id'];
		$dataSave['PASSWORD_USER']=md5($_input['username']);
		
		
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil mereset password user.';	
		}
		
		echo json_encode($data);
	}
}
