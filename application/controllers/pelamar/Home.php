<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Pelamar
{

	public function index()
	{
		$this->data['_title'] = $this->_title;
		$this->data['_page'] = 'pages/pelamar/home';




		$this->parser->parse('home/index', $this->data);
	}
}
