<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Admin {

	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
	}

	public function index()
	{
		$this->data['_page']='pages/admin/fitur/laporan/main';
		//$this->data['_scripts']=$this->data['_page'].'/scripts';
		$this->data['_head']=$this->data['_page'].'/head';
	
		$this->parser->parse('home/index',$this->data);
	}

	public function rekap(){
		$_input=$this->input->post();
		if(!isset($_input['bulan']) && !isset($_input['tahun']))
			$this->index();
		else{
			$where=array('MONTH(TGL_NOTA_SPPD)'=>$_input['bulan'],'YEAR(TGL_NOTA_SPPD)'=>$_input['tahun']);
			
			$this->load->model('penugasan/rekap_model','main',true);
			$res=$this->main->data($where);
//			print_r($result);
			$bulan=$_input['bulan'];
			$tahun=$_input['tahun'];

			$nojmk=str_replace("'","",$_input['nojmk']);
			$tgljmk='';
			$d=cal_days_in_month(CAL_GREGORIAN,$bulan,$tahun);

			$bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
			$bln=$bulan[$_input['bulan']-1];
			$this->load->library("excel");
			$excel = new PHPExcel();

			//insert some data to PHPExcel object
			//Judul
			$excel->setActiveSheetIndex(0)->mergeCells('A1:K1')->setCellValue('A1','PT. SURVEYOR INDONESIA');
			$excel->setActiveSheetIndex(0)->mergeCells('A2:K2')->setCellValue('A2','REKAPITULASI PERJALANAN DINAS PENUGASAN KHUSUS , MOB-DEMOBILISASI, PERJALANAN CUTI');
			$excel->setActiveSheetIndex(0)->mergeCells('A3:K3')->setCellValue('A3','PEMBORONGAN PEKERJAAN JASA KONSULTASI SUPERVISI KONSTRUKSI PEMBANGKIT DAN JARINGAN PAKET II');

			$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(23);
			$excel->getActiveSheet()->getStyle("A2")->getFont()->setSize(10);
			$excel->getActiveSheet()->getStyle("A3")->getFont()->setSize(10);


			//HEAD
			$excel->setActiveSheetIndex(0)->mergeCells('A4:C4')->setCellValue('A4','Surat Perjanjian Nomor');
			$excel->setActiveSheetIndex(0)->mergeCells('A5:C5')->setCellValue('A5','Tanggal Surat Perjanjian');
			$excel->setActiveSheetIndex(0)->mergeCells('A6:C6')->setCellValue('A6','Pelaksana');
			$excel->setActiveSheetIndex(0)->mergeCells('A7:C7')->setCellValue('A7','Surat Permohonan Pemeriksaan No.');
			$excel->setActiveSheetIndex(0)->mergeCells('A8:C8')->setCellValue('A8','Unit Manajemen Konstruksi');
			$excel->setActiveSheetIndex(0)->mergeCells('A9:C9')->setCellValue('A9','Periode Pelaksanaan Pekerjaan');

			//isi head
			$excel->setActiveSheetIndex(0)->mergeCells('D4:E4')->setCellValue('D4',$nojmk);
			$excel->setActiveSheetIndex(0)->mergeCells('D5:K5')->setCellValue('D5',$tgljmk);
			$excel->setActiveSheetIndex(0)->mergeCells('D6:K6')->setCellValue('D6','PT. Surveyor Indonesia (Persero) Surabaya');
			$excel->setActiveSheetIndex(0)->mergeCells('D7:K7')->setCellValue('D7',$_POST['spp'].', Tanggal : '.date_format(date_create($_POST['tg']),'d-m-Y'));
			$excel->setActiveSheetIndex(0)->mergeCells('D8:K8')->setCellValue('D8','UMK II');
			$excel->setActiveSheetIndex(0)->mergeCells('D9:K9')->setCellValue('D9','01 s.d. '.$d.' '.$bln.' '.$tahun);

			//header table
			$excel->setActiveSheetIndex(0)
				->setCellValue('A10','No')
				->setCellValue('B10','Nama')
				->setCellValue('C11','No Nota Dinas')
				->setCellValue('D11','Tanggal')
				->setCellValue('E11','Nomor')
				->setCellValue('F11','Tanggal')
				->setCellValue('G11','Tanggal Berangkat')
				->setCellValue('H11','Tanggal Kembali')
				->setCellValue('I11','Durasi Hari')
				->setCellValue('J11','Dari')
				->setCellValue('K11','Tujuan')
				->setCellValue('L11','Bentuk Penugasan')
				->setCellValue('M11','Pemberi Kerja')
				->setCellValue('N11','Kode Lokasi')
				->setCellValue('O12','Kode')
				->setCellValue('P12','Biaya')
				->setCellValue('Q11','Transportasi Lokasi')
				->setCellValue('R11','Airport Tax')
				->setCellValue('S11','Per Hari')
				->setCellValue('T11','Jumlah')
				->setCellValue('U10','Total SPPD')
				->setCellValue('V10','Kelengkapan Dokumen Tagihan');
				
			$excel->setActiveSheetIndex(0)->mergeCells('C10:D10')->setCellValue('C10','SURAT JMK');
			$excel->setActiveSheetIndex(0)->mergeCells('E10:F10')->setCellValue('E10','SPK SI');
			$excel->setActiveSheetIndex(0)->mergeCells('G10:I10')->setCellValue('G10','Pelaksanaan SPK SI');
			$excel->setActiveSheetIndex(0)->mergeCells('J10:L10')->setCellValue('J10','Penugasan khusus/Mob-Demob/Cuti');
			$excel->setActiveSheetIndex(0)->mergeCells('M10:N10')->setCellValue('M10','PROYEK');
			$excel->setActiveSheetIndex(0)->mergeCells('O10:R10')->setCellValue('O10','Biaya Transportasi');
			$excel->setActiveSheetIndex(0)->mergeCells('S10:T10')->setCellValue('S10','Uang Saku');
			$excel->setActiveSheetIndex(0)->mergeCells('O11:P11')->setCellValue('O11','Angkutan');

			$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('B10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('C10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('E10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('G10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('J10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('M10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('O10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('S10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('U10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('V10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('C11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('D11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('E11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('F11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('G11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('H11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('I11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('J11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('K11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('L11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('M11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('N11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('O12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle('P12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$i=1;
			$total=0;$x=13;$subtotal=0;
			$lastKode='';

			foreach ($res as $result){

				
				$param=array('ID_LOKASI'=>$result['TUJUAN_SPPD']);
				$hasil=$this->main->lokasi($param);
				$tujuan=$hasil['NAMA_LOKASI'];

				$param=array('ID_LOKASI'=>$result['PEMK_SPPD']);
				$hasil=$this->main->lokasi($param);
				$kdpemk=$hasil['KODE_LOKASI'];
				$pemk=$hasil['NAMA_LOKASI'];

				$us=$result['BIAYA_HARIAN_SPPD']*$result['LAMA_SPPD'];
				$tx=$result['BIAYA_HARIAN_SPPD']*$result['LAMA_SPPD']+$result['BIAYA_SPPD']+$result['TLOKAL_SPPD']+$result['TAX_BANDARA_SPPD'];
				$excel->setActiveSheetIndex(0)
				->setCellValue('A'.$x,$i)
				->setCellValue('B'.$x,$result['NAMA_PEG'])
				->setCellValue('C'.$x,$result['NO_SP_SPPD'])
				->setCellValue('D'.$x,$result['TGL_NOTA_SPPD'])
				->setCellValue('E'.$x,$result['NO_SPPD'])
				->setCellValue('F'.$x,$result['TGL_SPPD'])
				->setCellValue('G'.$x,$result['TGL_BRKT_SPPD'])
				->setCellValue('H'.$x,$result['TGL_KMBL_SPPD'])
				->setCellValue('I'.$x,$result['LAMA_SPPD'])
				->setCellValue('J'.$x,"'".$result['NAMA_LOKASI'])
				->setCellValue('K'.$x,"'".$tujuan)
				->setCellValue('L'.$x,$result['JENIS_SPPD'])
				->setCellValue('M'.$x,$pemk)
				->setCellValue('N'.$x,$kdpemk)
				->setCellValue('O'.$x,$result['TRANSPORT_SPPD'])
				->setCellValue('P'.$x,$result['BIAYA_SPPD'])
				->setCellValue('Q'.$x,$result['TLOKAL_SPPD'])
				->setCellValue('R'.$x,$result['TAX_BANDARA_SPPD'])
				->setCellValue('S'.$x,$result['BIAYA_HARIAN_SPPD'])
				->setCellValue('T'.$x,$us)
				->setCellValue('U'.$x,$tx)
				->setCellValue('V'.$x,'Ada');
				$subtotal+=$tx;
				
				$excel->getActiveSheet()->getStyle("P".$x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("Q".$x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("R".$x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("S".$x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("T".$x)->getNumberFormat()->setFormatCode('#,##0');
				$excel->getActiveSheet()->getStyle("U".$x)->getNumberFormat()->setFormatCode('#,##0');
				
				$total+=$tx;
				$i++;
				$x++;
			}
			$excel->setActiveSheetIndex(0)
			->mergeCells('A'.$x.':I'.$x)
			->setCellValue('A'.$x,'TOTAL')
			->setCellValue('U'.$x,$total);
			
			$excel->getActiveSheet()->getStyle('A'.$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$excel->getActiveSheet()->getStyle("U".$x)->getNumberFormat()->setFormatCode('#,##0');
			//autosize
			$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
			$excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		}
		ob_end_clean();
		$object_writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Rekap SPPD '.$bln.' '.$tahun.'.xls"');
		$object_writer->save('php://output');
		ob_end_clean();
	}
	
}
