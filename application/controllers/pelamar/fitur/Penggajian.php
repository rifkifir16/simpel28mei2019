<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggajian extends MY_Admin {
	protected $_title="";
	function __construct(){
		parent::__construct();
		$this->data['_title']=$this->_title;
		$this->load->model('fitur/penggajian_model','main',true);
	}

	public function index()
	{
		$this->data['_page']='pages/admin/fitur/penggajian';
		$this->data['_scripts']=$this->data['_page'].'/scripts';
		$this->data['_head']=$this->data['_page'].'/head';
		$this->data['_ajaxSource']=base_url('admin/fitur/penggajian/data');

		$this->parser->parse('home/index',$this->data);
	}

	/*Additional function */
	public function dbCommand($command='getData',$param){
		$table='gaji';

		if($command=='getData')
			$res=$this->main->getData($param[0],$param[1]);
		elseif($command=='insert')
			$res=$this->main->insert($param[0],$param[1],$table);
		elseif($command=='update')
			$res=$this->main->update($param[0],$param[1],$table);
		elseif($command=='count_all')
			$res=$this->main->count_all($param);
		elseif($command=='count_filtered')
			$res=$this->main->count_filtered($param);
		elseif($command=='ajaxData')
			$res=$this->main->ajaxData($param);
		else
			$res=false;

		return $res;
	}
	
	/*All ajax below here*/
	public function data(){
		/* Menentukan status data yang ingin diambil */
		$param=$this->uri->segment('5');
		$where=array();
		if(isset($param)){
			if($param==1)
				$where['TGLAP_GAJI <>']='1990-12-12';
			elseif($param==2)
				$where['TGLAP_GAJI']='1990-12-12';
		}

		$param=array(null,$where);
		$list=$this->dbCommand('getData',$param);
		$data = array();
		$no = isset($_POST['start'])?$_POST['start']:0;
		
		$bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');

        foreach ($list as $field) {
			$status=$field->TGLAP_GAJI=='1990-12-12'? 0 : 1;
			$x=isset($bulan[$field->BULAN_GAJI-1])? $bulan[$field->BULAN_GAJI-1] : ' - ';

            $no++;
            $row = array();
            $row[] = $no;
			$row[] = $field->ID_GAJI;
			
			$row[] = $field->KODE_LOKASI;
			$row[] = $field->NAMA_LOKASI;
			$row[] = $field->NIP_PEG;
			$row[] = $field->NAMA_PEG;
			$row[] = $x.' '.$field->TAHUN_GAJI;
			$row[] = $field->MM;
			$row[] = $field->INDEX_GAJI;
			$row[] = $field->JUMHARI_GAJI;
			$row[] = $field->JUMHADIR_GAJI;
			$row[] = $field->COR_GAJI;
			$row[] = number_format($field->PPH_GAJI);
			$row[] = number_format($field->JAMSOSTEK_GAJI);
			$row[] = number_format($field->THP_GAJI);
			//$row[] = $field->TGLAP_GAJI;
			$row[] = $status;
			$row[] = $status;
            $data[] = $row;
        }
 
		$draw=isset($_POST['draw'])?$_POST['draw']:1;
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->dbCommand('count_all',$where),
            "recordsFiltered" => $this->dbCommand('count_filtered',$where),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	public function ajax(){
		$q=$this->input->get('q');
		$res=$this->dbCommand('ajaxData',$q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'Agama tidak ditemukan');
		
		echo json_encode($r);
	}

	public function save(){
		$_input=$this->input->post();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat menyimpan data';	

		$dataSave=array();
		$dataSave['NAMA_AGAMA']=htmlspecialchars($_input['agama']);
		
		if($_input['idAgama']==0) //jika addnew
		{
			$dataSave['STATUS_AGAMA']=1;
			
			$param=array($dataSave,true);
			$res=$this->dbCommand('insert',$param);
			$pesan='menyimpan';
		}
		else //jika update
		{
			$where=array();
			$where['ID_AGAMA']=$_input['idAgama'];

			$param=array($dataSave,$where);
			$res=$this->dbCommand('update',$param);
			$pesan='memperbaharui';
		}
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data agama';	
		}

		echo json_encode($data);
	}

	public function upstatus(){
		$_input=$this->input->get();
		$data['result']=false;
		$data['msg']='Terjadi kesalahan saat mengubah status data';	

		$dataSave=array();
		$where=array();
		$where['ID_AGAMA']=$_input['id'];

		if($_input['status']>0){
			$dataSave['STATUS_AGAMA']=0;	
			$pesan="menonaktifkan";
		}
		else{
			$dataSave['STATUS_AGAMA']=1;
			$pesan="mengaktifkan";
		}
		
		$param=array($dataSave,$where);
		$res=$this->dbCommand('update',$param);
		
		if($res>0){
			$data['result']=true;
			$data['msg']='Berhasil '.$pesan.' data agama';	
		}
		
		echo json_encode($data);
	}
}
