<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	protected $_title="PT. Surveyor Indonesia";
	protected $data=array();

	public function index($msg=null)
	{
		redirect('front');
	}

	public function ajaxPropinsi()
	{
		$this->load->model('basic/propinsi_model','main',true);
		$q=$this->input->get('q');
		$res=$this->main->ajaxData($q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'propinsi tidak ditemukan');
		
		echo json_encode($r);
	}
	
	public function ajaxKota(){
		$this->load->model('basic/kota_model','main',true);
		$q=$this->input->get('q');
		$res=$this->main->ajaxData($q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'kota tidak ditemukan');
		
		echo json_encode($r);
	}
	
	public function ajaxAgama(){
		$this->load->model('basic/agama_model','main',true);
		$q=$this->input->get('q');
		$res=$this->main->ajaxData($q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'agama tidak ditemukan');
		
		echo json_encode($r);
	}
	
	public function ajaxJabatan(){
		$this->load->model('basic/jabatan_model','main',true);
		$q=$this->input->get('q');
		$res=$this->main->ajaxData($q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'jabatan tidak ditemukan');
		
		echo json_encode($r);
	}
	
	public function ajaxKualifikasi(){
		$this->load->model('basic/kualifikasi_model','main',true);
		$q=$this->input->get('q');
		$res=$this->main->ajaxData($q);
		$i=0;
		if(count($res)>0){
			$r=$res;
			
		}
		else
			$r[] = array('id' => '', 'text' => 'kualifikasi tidak ditemukan');
		
		echo json_encode($r);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->index('Anda berhasil logout.');
	}
}
