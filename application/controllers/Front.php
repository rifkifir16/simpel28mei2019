<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Front extends CI_Controller
{

	protected $_title = "PT. Surveyor Indonesia";
	protected $data = array();

	function __construct()
	{
		parent::__construct();
		// if ($this->uri->segment(2) != 'logout') {
		// 	if ($this->session->userdata('userId') !== null && $this->session->userdata('nip') !== null && $this->session->userdata('hakAkses') !== null) {
		// 		if ($this->session->userdata('hakAkses') == 'admin')
		// 			redirect('dosen/home');
		// 		else //if($this->session->userdata('hakAkses')==2 || $this->session->userdata('hakAkses')==3)
		// 			redirect('admin/home');
		// 	}
		// }
		// redirect('front');
	}

	public function index($msg = null)
	{
		$_cont = array();
		$_cont['_title'] = $this->_title;
		if (isset($msg))
			$_cont['msg'] = $msg;
		$this->parser->parse('login/top-nav', $_cont);
	}

	public function login()
	{
		$input = $this->input->post();
		if (count($input) > 0) {
			$this->load->model('home_model', 'home', true);

			$where = array('USERNAME_USER' => $input['uname']);

			$res = $this->home->userCheck($where);
			if (count($res) <= 0) //jika kosong
				$this->index('Username tidak ditemukan');
			else {
				if (md5($input['passw']) == $res['PASSWORD_USER']) {
					$data = array(
						'userId'  => $res['ID_USER'],
						'nip'     => $res['USERNAME_USER'],
						'nama'     => $res['NAMA_USER'],
						'hakAkses' => $res['HAK_USER'],
					);

					$this->session->set_userdata($data);
					if ($res['HAK_USER'] == 'admin')
						redirect('admin/home');
					elseif ($res['HAK_USER'] == 'pegawai') {
						redirect('surveyor/home');
					} elseif ($res['HAK_USER'] == 'pln') {
						redirect('admin/home');
					} elseif ($res['HAK_USER'] == 'pelamar') {
						redirect('pelamar/home');
					} else {
						redirect('front');
					}
				} else
					$this->index('Password yang anda masukkan salah');
			}
		}
		//print_r(count($this->input->post()));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->index('Anda berhasil logout.');
	}
}
