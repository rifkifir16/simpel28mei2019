

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Send_email extends CI_Controller
{

	/**
	 * Kirim email dengan SMTP SendGrid.
	 *
	 */
	public function kirimemail()
	{
		// Load library email
		$this->load->library('email');

		// Inisiasi library email
		$this->email->initialize(array(
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_user' => 'manusiakerupuk',  // Ganti dengan username akun SendGrid kamu
			'smtp_pass' => 'manusiakerupuk123',  // Ganti dengan password akun SendGrid kamu
			'smtp_port' => 587,
			'crlf'      => "\r\n",
			'newline'   => "\r\n"
		));

		// Email dan nama pengirim
		$this->email->from('no-reply@masrud.com', 'MasRud.com | M. Rudianto');
		$email = $this->post->input('ids');
		// Email penerima
		$this->email->to($email); // Ganti dengan email tujuan kamu
		// $this->email->to('rifkifir16@gmail.com'); // Ganti dengan email tujuan kamu

		// Lampiran email, isi dengan url/path file
		$this->email->attach('https://masrud.com/content/images/20181011115143-send-email-codeigniter-sendgrid-smtp.png');

		// Subject email
		$this->email->subject('Kirim Email dengan SMTP SendGrid | MasRud.com');
		$this->email->message("Ini adalah contoh email CodeIgniter yang dikirim menggunakan SMTP SendGrid.<br><br> Klik <strong><a href='https://masrud.com/post/kirim-email-dengan-smtp-sendgrid' target='_blank' rel='noopener'>disini</a></strong> untuk melihat tutorialnya.");

		// Tampilkan pesan sukses atau error
		if ($this->email->send()) {
			$msg = 'Sukses! email berhasil dikirim.';
		} else {
			$msg = 'Error! email tidak dapat dikirim.';
		}

		// Debugging error
		echo $this->email->print_debugger();
		echo json_encode($msg);
	}
	public function email()
	{
		$this->load->model('user/pelamar_model', 'main', true);
		$mail = $this->input->post('ids');
		$ID_PELAMAR = $this->input->post('ID_PELAMAR');
		$param[0] = array("KONFIRMASI_EMAIL" => "1");
		$param[1] = array("ID_PELAMAR" => $ID_PELAMAR);

		$this->load->library('email');
		$this->email->initialize(array(
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_user' => 'manusiakerupuk',  // Ganti dengan username akun SendGrid kamu
			'smtp_pass' => 'manusiakerupuk123',  // Ganti dengan password akun SendGrid kamu
			'smtp_port' => 587,
			'crlf'      => "\r\n",
			'newline'   => "\r\n"
		));
		$this->email->from('no-reply@SurveyorIndonesia.com', 'PT. Surveyor Indonesia');
		$this->email->to($mail); // Ganti dengan email tujuan kamu
		// $this->email->to('rifkifir16@gmail.com'); // Ganti dengan email tujuan kamu

		// Lampiran email, isi dengan url/path file
		// Subject email
		$this->email->subject('Pemanggilan wawancara PT. Surveyor Indonesia');
		$this->email->message("Ini adalah contoh email CodeIgniter yang dikirim menggunakan SMTP SendGrid.");

		// Tampilkan pesan sukses atau error
		if ($this->email->send()) {
			$msg = true;
			$this->main->update($param[0], $param[1]);
		} else {
			$msg = false;
		}
		echo json_encode($msg);
	}
}
