<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

require_once APPPATH.'vendor/autoload.php';
use PhpOffice\PhpWord\PhpWord;

class Cetak extends CI_Controller {
    function __construct() {
        parent::__construct();
        //$this->load->library('word');
        
    }
    function sppd() {
        $param=$this->uri->segment('3');
        $where=array('ID_SPPD'=>$param);

        //exit();

        $this->load->model('penugasan/sppd_model','main',true);
        $result=$this->main->cetak($param);
        $data=$this->main->rincian($param);
        $rin=count($data);
        $template="";
        if($result['OTORISASI_SPPD']=='1')
            $template='assets/templates/samsudin.docx';
        elseif($result['OTORISASI_SPPD']=='2')
            $template='assets/templates/kholidun.docx';
        else
            $template='assets/templates/eko.docx';

        $lokasi=isset($result['KODE_LOKASI'])?$result['KODE_LOKASI']:'-';

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template);

        //$templateProcessor = $PHPWord->loadTemplate($template);
        
        $templateProcessor->setValue('nosppd', $result['NO_SPPD']);            
        $templateProcessor->setValue('namapeg', $result['NAMA_PEG']);        
        $templateProcessor->setValue('kualifikasipeg', ucfirst($result['NAMA_KUALIFIKASI'])); 
        $templateProcessor->setValue('jenis', ucfirst($result['JENIS_SPPD']));
        $templateProcessor->setValue('uraian', $result['URAIAN_SPPD']);
        $templateProcessor->setValue('transportasi', ucfirst($result['TRANSPORT_SPPD']));
        $templateProcessor->setValue('asal', $result['ASAL_SPPD']);
        $templateProcessor->setValue('tujuan', $result['TUJUAN_SPPD']);
        $templateProcessor->setValue('kodelokasi', $lokasi);
        $templateProcessor->setValue('lama', $result['LAMA_SPPD']+1);
        $templateProcessor->setValue('tglbrkt', date_format(date_create($result['TGL_BRKT_SPPD']),'d-m-Y'));
        $templateProcessor->setValue('tglkmbl', date_format(date_create($result['TGL_KMBL_SPPD']),'d-m-Y'));
        $templateProcessor->setValue('asalbiaya', ucfirst($result['BIAYA_OLEH_SPPD']));
        $templateProcessor->setValue('tglnota', date_format(date_create($result['TGL_NOTA_SPPD']),'d-m-Y'));
        $templateProcessor->setValue('jenjangpeg', $result['NAMA_JABATAN']);
        $templateProcessor->setValue('statuspeg', '-' );

        $templateProcessor->cloneRow('ntrans1', 4);
        $templateProcessor->setValue('ntrans1#1', 'Pesawat Udara');
        $templateProcessor->setValue('ntrans1#2', 'Bus');
        $templateProcessor->setValue('ntrans1#3', 'Transportasi Lokal');
        $templateProcessor->setValue('ntrans1#4', 'Airport Tax');
        $templateProcessor->setValue('utrans1#1', number_format($result['UNIT_PESAWAT_SPPD']));
        $templateProcessor->setValue('utrans1#2', number_format($result['UNIT_BUS_SPPD']));
        $templateProcessor->setValue('utrans1#3', number_format($result['UNIT_TLOKAL_SPPD']));
        $templateProcessor->setValue('utrans1#4', number_format($result['TAX_BANDARA_SPPD']));
        $templateProcessor->setValue('btrans1#1', number_format($result['BIAYA_PESAWAT_SPPD']));
        $templateProcessor->setValue('btrans1#2', number_format($result['BIAYA_BUS_SPPD']));
        $templateProcessor->setValue('btrans1#3', number_format($result['BIAYA_TLOKAL_SPPD']));
        $templateProcessor->setValue('btrans1#4', number_format($result['TAX_BANDARA_SPPD']));
        $templateProcessor->setValue('ttrans1#1', number_format($v=$result['UNIT_PESAWAT_SPPD']*$result['BIAYA_PESAWAT_SPPD']));
        $templateProcessor->setValue('ttrans1#2', number_format($w=$result['UNIT_BUS_SPPD']*$result['BIAYA_BUS_SPPD']));
        $templateProcessor->setValue('ttrans1#3', number_format($x=$result['UNIT_TLOKAL_SPPD']*$result['BIAYA_TLOKAL_SPPD']));
        $templateProcessor->setValue('ttrans1#4', number_format($y=$result['TAX_BANDARA_SPPD']));
        $templateProcessor->setValue('sttrans1', number_format($v+$x+$y+$w));
        $tot=0;
        $templateProcessor->cloneRow('ntrans2', $rin+1);
        $templateProcessor->setValue('ntrans2#1', 'Uang Saku');
        $templateProcessor->setValue('utrans2#1', number_format($result['LAMA_SPPD']+1));
        $templateProcessor->setValue('btrans2#1', number_format($result['BIAYA_HARIAN_SPPD']));
        $templateProcessor->setValue('ttrans2#1', number_format($tot+=($result['LAMA_SPPD']+1)*$result['BIAYA_HARIAN_SPPD']));
        $i=2;
        foreach($data as $r){
            $templateProcessor->setValue('ntrans2#'.$i, $r['NAMA_RINSPPD']);
            $templateProcessor->setValue('utrans2#'.$i, number_format($r['UNIT_RINSPPD']));
            $templateProcessor->setValue('btrans2#'.$i, number_format($r['BIAYA_RINSPPD']));
            $templateProcessor->setValue('ttrans2#'.$i, number_format($tot+=$r['UNIT_RINSPPD']*$r['BIAYA_RINSPPD']));
            $i++;
        }
        $ttot=$tot+$v+$x+$y+$w;
        $terbilang=$this->terbilang($ttot, $style=4);
        $templateProcessor->setValue('sttrans2', number_format($tot));
        $templateProcessor->setValue('tttrans', number_format($ttot));
        $templateProcessor->setValue('terbilang', $terbilang.' rupiah');

        $templateProcessor->saveAs('SPPD.docx');

        //echo getEndingNotes(array('Word2007' => 'docx'));
        header("Location: ".base_url("SPPD.docx"));
		
        /*$filename='SPPD.docx'; //save our document as this file name
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        $objWriter->save('php://output');*/
    }

    private function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }     
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }     
        return $hasil;
    }

    private function kekata($x) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = kekata($x - 10). " belas";
        } else if ($x <100) {
            $temp = kekata($x/10)." puluh". kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x <1000) {
            $temp = kekata($x/100) . " ratus" . kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
        }     
            return $temp;
    }
}
?>
