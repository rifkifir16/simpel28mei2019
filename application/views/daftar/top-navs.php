<!DOCTYPE html>
<html>
<?php include 'inc/head.php';?>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?=site_url()?>" class="navbar-brand"><b>SIMPP</b>FIO UNESA</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
      </div>
    </nav>
  </header>
  <div class="content-wrapper">
    <div class="container">
      <section class="content">
        
        <?php include 'inc/loginbox.php'; ?>
        <?php include 'inc/descbox.php'; ?>
      </section>
    </div>
  </div>
  <?php include 'inc/footer.php'; ?>
</div>

<?php include 'inc/scripts.php';?>
</body>
</html>
