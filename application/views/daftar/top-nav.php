<!DOCTYPE html>
<html>
<?php include 'inc/head.php';?>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav" >
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?=site_url()?>" class="navbar-brand"><b>Surveyor</b> Indonesia</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
      </div>
    </nav>
  </header>
  <div class="content-wrapper" style="background-image:url('<?=base_url()?>assets/images/b2.png');">
  
    <div class="container">
      <section class="content">
        <div class="row">
          <div class="col-md-2 col-md-push-5 col-xs-4 col-xs-push-4">
            <img class="img-responsive" src="<?=base_url()?>assets/images/logo.png" />
          </div>
        </div><br/>
        <div class="row">
          <?php //include 'inc/loginbox.php'; ?>
          <?php include 'inc/descbox.php'; ?>
        </div>
      </section>
    </div>
  </div>
  <?php include 'inc/footer.php'; ?>
</div>

<?php include 'inc/scripts.php';?>
</body>
</html>
