<div class="col-md-4 col-md-push-8 col-sm-12">
  <div class="box  box-solid box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Masuk</h3>
    </div>
    <div class="login-box-body">
      <!--p class="login-box-msg"></p-->

      <form action="<?=site_url()?>" method="post">
        <div class="form-group has-feedback">
          <input type="email" class="form-control" placeholder="Email" autofocus="true">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-6">
            <button type="submit" class="btn btn-info btn-block btn-flat">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="#">Lupa password</a><br>
      <a href="<?=site_url('register')?>" class="text-center">Daftar baru</a>

    </div>
    <!-- /.login-box-body -->
  </div>
</div>