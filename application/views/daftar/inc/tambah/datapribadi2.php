
	<div class="form-group">
		<label>Tinggi Badan (Cm)</label>
		<input class="form-control" type="number" name="tb" placeholder="Tinggi Badan"/>
	</div>
	<div class="form-group">
		<label>Berat Badan</label>
		<input class="form-control" type="number" name="bb" placeholder="Berat Badan"/>
	</div>
	<div class="form-group">
		<label>Hobi</label>
		<input class="form-control" type="text" name="hobi" placeholder="Hobi" required="true" />
	</div>
	<div class="form-group">
		<label>Olahraga yang dikuasai</label>
		<input class="form-control" type="text" name="or" placeholder="Olahraga yang dikuasai" required="true" />
	</div>
	<div class="form-group">
		<label>Pernah sakit keras</label>
		<select class="form-control" name="sakit" required>
			<option value="1">Ya</option>
			<option value="0">Tidak</option>
		</select>
	</div>
	<div class="form-group">
		<label>Tanggal Sakit</label>
		<input class="form-control" type="date" name="tglsakit" placeholder="Tanggal Sakit" />
	</div>
	<div class="form-group">
		<label>Sakit apa</label>
		<input class="form-control" type="text" name="sakitapa" placeholder="Sakit apa"/>
	</div>
	<div class="form-group">
		<label>Pernah kecelakaan</label>
		<select class="form-control" name="kecelakaan" required>
			<option value="1">Ya</option>
			<option value="0">Tidak</option>
		</select>
	</div>
	<div class="form-group">
		<label>Tanggal Kecelakaan</label>
		<input class="form-control" type="date" name="tglkecelakaan" placeholder="Tanggal Kecelakaan" />
	</div>
	<div class="form-group">
		<label>Kecelakaan apa</label>
		<input class="form-control" type="text" name="kecelakaanapa" placeholder="Kecelakaan apa"/>
	</div>
	<div class="form-group">
		<label>Akibat Kecelakaan</label>
		<input class="form-control" type="text" name="akibatkecelakaan" placeholder="Akibat Kecelakaan"/>
	</div>
	<div class="form-group">
		<label>Pernah terlibat kegiatan terlarang</label>
		<select class="form-control" name="terlarang" required>
			<option value="1">Ya</option>
			<option value="0">Tidak</option>
		</select>
	</div>
	<div class="form-group">
		<label>Pernah terlibat urusan dengan pihak kepolisian</label>
		<select class="form-control" name="kepolisian" required>
			<option value="1">Ya</option>
			<option value="0">Tidak</option>
		</select>
	</div>
	<div class="form-group">
		<label>jelaskan</label>
		<input class="form-control" type="text" name="jelaskepol" placeholder="Jelaskan"/>
	</div>

	<div class="form-group">
		<label>Pernah terdaftar sebagai peserta/anggota program ASTEK</label>
		<select class="form-control" name="astek" required>
			<option value="1">Ya</option>
			<option value="0">Tidak</option>
		</select>
	</div>
	<div class="form-group">
		<label>No Peserta/KPA</label>
		<input class="form-control" type="text" name="noastek" placeholder="No ASTEK"/>
	</div>
    