<div id="formRelasi">
	<div class="form-group relasi-group">
		<div class="row">
			<div class="col-md-6" style="padding-right:0;">
				<label>Nama</label>
				<input class="form-control" type="text" name="namarelasi[]" />
			</div>
			<div class="col-md-6">
				<label>Jabatan</label>
				<input class="form-control" type="text" name="jabatanrelasi[]" />
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Instansi</label>
				<input class="form-control" type="text" name="instansirelasi[]" />
			</div>
			<div class="col-md-6">
				<label>Hubungan</label>
				<input class="form-control" type="text" name="hubunganrelasi[]" />
			</div>
		</div>
		<hr />
	</div>
</div>

<div class="form-group text-center">
	<a href="#" class="btn btn-info" onclick="duprelasi();">Tambah Relasi</a>
</div>
