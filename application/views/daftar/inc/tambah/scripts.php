<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script>
	$(document).ready(function() {
		$(".ajaxkota").select2({
          ajax: {
            url: "<?php echo site_url('admin/basic/kota/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
        
        $(".ajaxagama").select2({
          ajax: {
            url: "<?php echo site_url('admin/basic/agama/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
        
        $(".ajaxjabatan").select2({
          ajax: {
            url: "<?php echo site_url('admin/basic/jabatan/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text};
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
		
		$(".ajaxkualifikasi").select2({
          ajax: {
            url: "<?php echo site_url('admin/basic/kualifikasi/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text};
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
		$(".ajaxlokasi").select2({
          ajax: {
            url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text};
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
        
        ajaxkontak();
	} );
	
	function dupkontak(){
	    $( ".kontak-group" ).children('select').select2('destroy').end();
        $( ".kontak-group" ).clone().removeClass('kontak-group').appendTo( "#formkontak" );
        
       //$( ".form-group" ).children('select').select2();
       
        
        ajaxkontak();
    }

    function duprelasi(){
        $( ".relasi-group" ).clone().removeClass('relasi-group').appendTo( "#formRelasi" );
    }

    function dupkecakapan(){
        $( ".kecakapan-group" ).clone().removeClass('kecakapan-group').appendTo( "#formKecakapan" );
    }

    function dupriwayat(){
        $( ".riwayat-group" ).clone().removeClass('riwayat-group').appendTo( "#formRiwayat" );
    }

	function dupkeluarga(){
        $( ".keluarga-group" ).clone().removeClass('keluarga-group').appendTo( "#formkeluarga" );
    }
    
    function ajaxkontak(){
        $(".ajaxkontak").select2({
          ajax: {
            url: "<?php echo site_url('admin/basic/kontak/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text};
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
    }
    
    function check(id){
        $.ajax({
			  url: '<?php echo site_url('admin/user/pegawai/check'); ?>',
            data : {id: id,},
            type : 'POST',
			      success: function(data){
	          var $peringatan = $('#peringatan');
              $peringatan.empty();
              $peringatan.append(data);
              myData='<a style="color: red;">NIP sudah digunakan</a>';
			  
              if(myData.localeCompare(data)==0)
              {
                $('#btnSubmit').hide();
              //$('.inmdl').prop('disabled',true);
              }
              else
              {
                $('#btnSubmit').show();
              //$('.inmdl').prop('disabled',false);
              }
			}
		  });
    }

    $('#fillForm').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#fillForm')[0]);
    
    $.ajax({
        url: "<?php echo site_url('daftar/save'); ?>",
        type: 'POST',
        data: form,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
          if(data==false){
            window.location.href = "<?=site_url('')?>";
          }
        }
    });
  });
	
	function calTot()
    {
		upah=$('.upah').val();
		tumum=$('.tumum').val();
		tmakan=$('.tmakan').val();
		tkom=$('.tkom').val();
		var upah = upah.replace(/[\D\s\._\-]+/g, "");
		var tumum = tumum.replace(/[\D\s\._\-]+/g, "");
		var tmakan = tmakan.replace(/[\D\s\._\-]+/g, "");
		var tkom = tkom.replace(/[\D\s\._\-]+/g, "");
        var tot=upah*1+tmakan*1+tumum*1+tkom*1;
		
		tot = tot ? parseInt( tot, 10 ) : 0;
		 
		// 4
		$('.ttotal').val( function() {
			return ( tot === 0 ) ? "" : tot.toLocaleString( "id-ID" );
		} );
		//$('.ttotal').val(tot);
    }
	
	var iupah = $('#iupah');
	iupah.on( "keyup", function( event ){
		// 1.
		var selection = window.getSelection().toString();
		if ( selection !== '' ) {
			return;
		}
	 
		// 2.
		if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
			return;
		}
		
		// 1
		var $this = $( this );
		var input = $this.val();
		 
		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");
		 
		// 3
		input = input ? parseInt( input, 10 ) : 0;
		 
		// 4
		$this.val( function() {
			return ( input === 0 ) ? "" : input.toLocaleString( "id-ID" );
		} );
	 
	} );
	
	var itumum = $('#itumum');
	itumum.on( "keyup", function( event ){
		// 1.
		var selection = window.getSelection().toString();
		if ( selection !== '' ) {
			return;
		}
	 
		// 2.
		if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
			return;
		}
		
		// 1
		var $this = $( this );
		var input = $this.val();
		 
		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");
		 
		// 3
		input = input ? parseInt( input, 10 ) : 0;
		 
		// 4
		$this.val( function() {
			return ( input === 0 ) ? "" : input.toLocaleString( "id-ID" );
		} );
	 
	} );
	
	var itmakan = $('#itmakan');
	itmakan.on( "keyup", function( event ){
		// 1.
		var selection = window.getSelection().toString();
		if ( selection !== '' ) {
			return;
		}
	 
		// 2.
		if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
			return;
		}
		
		// 1
		var $this = $( this );
		var input = $this.val();
		 
		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");
		 
		// 3
		input = input ? parseInt( input, 10 ) : 0;
		 
		// 4
		$this.val( function() {
			return ( input === 0 ) ? "" : input.toLocaleString( "id-ID" );
		} );
	 
	} );
	
	var itkom = $('#itkom');
	itkom.on( "keyup", function( event ){
		// 1.
		var selection = window.getSelection().toString();
		if ( selection !== '' ) {
			return;
		}
	 
		// 2.
		if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
			return;
		}
		
		// 1
		var $this = $( this );
		var input = $this.val();
		 
		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");
		 
		// 3
		input = input ? parseInt( input, 10 ) : 0;
		 
		// 4
		$this.val( function() {
			return ( input === 0 ) ? "" : input.toLocaleString( "id-ID" );
		} );
	 
	} );
</script>