<div id="formkeluarga">
	<div class="form-group keluarga-group">
		<div class="row">
			<div class="col-md-6" style="padding-right:0;">
				<label>Hubungan Keluarga</label>
				<select class="form-control" name="kel[]">
					<option value="">--Pilih Anggota--</option>
					<option value="Ayah">Ayah</option>
					<option value="Ibu">Ibu</option>
					<option value="pasangan">Suami/Istri</option>
					<option value="anak">Anak</option>
					<option value="s">Saudara</option>
				</select>
			</div>
			<div class="col-md-6">
				<label>Nama</label>
				<input class="form-control" type="text" name="nmkel[]" placeholder="Nama Keluarga" />
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Jenis Kelamin</label>
				<select class="form-control" name="jkkel[]" required>
					<option value="L">Laki-Laki</option>
					<option value="P">Perempuan</option>
				</select>
			</div>
			<div class="col-md-6">
				<label>Tempat Lahir</label>
				<input class="form-control" type="text" name="tptlhrkel[]" placeholder="Tempat Lahir" />
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Tanggal Lahir</label>
				<input class="form-control" type="date" name="tgllhrkel[]" placeholder="Tanggal Lahir" />
			</div>
			<div class="col-md-6">
				<label>Pendidikan/pekerjaan</label>
				<input class="form-control" type="text" name="jobkel[]" placeholder="Pendidikan/Pekerjaan" />
			</div>
			<div class="col-md-12">
				<label>Alamat</label>
				<input class="form-control" type="text" name="alamatkel[]" placeholder="Alamat" />
			</div>
		</div>
		<hr />
	</div>
</div>
<div class="form-group text-center">
	<a href="#" class="btn btn-info" onclick="dupkeluarga();">Tambah Anggota Keluarga</a>
</div>
