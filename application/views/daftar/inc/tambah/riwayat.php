<div id="formRiwayat">
    	<div class="form-group riwayat-group">
			<div class="row">
			<div class="col-md-6" style="padding-right:0;">
    		<label>Nama Sekolah</label>
    		<input class="form-control" type="text" name="namasekolah[]" required="true" />
			</div>
			<div class="col-md-6">
				<label>Kota</label>
    		<input class="form-control" type="text" name="kotasekolah[]" required="true" />
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Sampai kelas</label>
    		<input class="form-control" type="text" name="kelassekolah[]" required="true" />
			</div>
			<div class="col-md-6">
				<label>Dari/sampai tahun</label>
    		<input class="form-control" type="text" name="tahunsekolah[]" required="true" />
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Jurusan</label>
    		<input class="form-control" type="text" name="jurusansekolah[]" required="true" />
			</div>
			<div class="col-md-6">
				<label>Tahun Ijazah</label>
    		<input class="form-control" type="text" name="tahunijazah[]" required="true" />
			</div>
    	</div>
		<hr/>
		</div>
		
	</div>
	<br/>
	<div class="form-group text-center">
		<a href="javascript:void(0)" class="btn btn-info" onclick="dupriwayat();">Tambah Sekolah</a>
	</div>

	<hr/>

	<div id="formKursus">
    	<div class="form-group kursus-group">
			<div class="row">
			<div class="col-md-6" style="padding-right:0;">
    		<label>Nama Kursus</label>
    		<input class="form-control" type="text" name="namakursus[]"/>
			</div>
			<div class="col-md-6">
				<label>Lama Kursus</label>
    		<input class="form-control" type="text" name="lamakursus[]"/>
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Dari - Sampai (Tanggal,Bulan,Tahun)</label>
    		<input class="form-control" type="text" name="drsmpthkursus[]"/>
			</div>
			<div class="col-md-6">
				<label>Sertifikat/Keterangan</label>
    		<input class="form-control" type="text" name="keterangankursus[]"/>
			</div>
    	</div>
		<hr/>
		</div>
		
	</div>
	<br/>
	<div class="form-group text-center">
		<a href="javascript:void(0)" class="btn btn-info" onclick="dupkursus();">Tambah Kursus</a>
	</div>