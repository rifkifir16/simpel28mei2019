<div class="form-group">
	<label>Foto</label>
	<input class="form-control" type="file" name="foto" placeholder="Foto Pegawai" />
</div>
<div class="form-group">
	<label>Posisi yang dilamar</label>
	<input class="form-control" type="text" name="posisi" placeholder="Posisi yang dilamar" />
</div>
<div class="form-group">
	<label>No. KTP</label>
	<input class="form-control" type="text" name="ktp" placeholder="No. KTP" />
</div>
<div class="form-group">
	<label>Jenis SIM (A/C)</label>
	<input class="form-control" type="text" name="sim" placeholder="Jenis Sim (A/C)" />
</div>
<div class="form-group">
	<label>No SIM</label>
	<input class="form-control" type="text" name="nosim" placeholder="No SIM" />
</div>
<div class="form-group">
	<label>Nama</label>
	<input class="form-control" type="text" name="nama" placeholder="Nama" required="true" />
</div>
<!-- <div class="form-group">
	<label>Email</label>
	<input class="form-control" type="text" name="email" placeholder="Email" />
</div> -->
<div class="form-group">
	<label>Alamat</label>
	<textarea class="form-control" name="alamat"></textarea>
</div>
<div class="form-group">
	<label>Kota Domisili</label>
	<select class="form-control ajaxkota" name="domisili" style="width: 100%;" required>
		<option value="">--Pilih Kota--</option>
	</select>
</div>
<div class="form-group">
	<label>Tempat Lahir</label>
	<input class="form-control" type="text" name="kotalahir" placeholder="Tempat Lahir" required="true" />
</div>
<div class="form-group">
	<label>Tanggal Lahir</label>
	<input class="form-control" type="date" name="tgllhr" required="true" />
</div>
<div class="form-group">
	<label>Agama</label>
	<select class="form-control ajaxagama" name="agama" style="width: 100%;" required>
		<option value="">--Pilih Agama--</option>
	</select>
</div>
<div class="form-group">
	<label>Jenis Kelamin</label>
	<select class="form-control" name="jk" required>
		<option value="L">Laki-Laki</option>
		<option value="P">Perempuan</option>
	</select>
</div>
<div class="form-group">
	<label>Status Pernikahan</label>
	<select class="form-control" name="sn" required>
		<option>Tidak Kawin</option>
		<option>Kawin</option>
		<option>Duda/Janda</option>
	</select>
</div>
<div class="form-group">
	<label>No Telp</label>
	<input class="form-control" type="text" name="telp" placeholder="No Telp" required="true" />
</div>
<div class="form-group">
	<label>Alamat Email</label>
	<input class="form-control" type="text" name="email" placeholder="Email" required="true" />
</div>
<div class="form-group">
	<label>Pendidikan Terakhir (tingkat/bidang/jurusan)</label>
	<input class="form-control" type="text" name="pendidikan" placeholder="Pendidikan Terakhir" required="true" />
</div>
<div class="form-group">
	<label>Spesialisasi</label>
	<input class="form-control" type="text" name="spesialisasi" placeholder="Spesialisasi" />
</div>
<div class="form-group">
	<label>Keahlian yang paling dikuasai</label>
	<input class="form-control" type="text" name="keahlian" placeholder="" />
</div>
<div class="form-group">
	<label>Bidang studi yang paling dikuasai</label>
	<input class="form-control" type="text" name="bidstud" placeholder="" />
</div>
<div class="form-group">
	<label>Judul skripsi/proyek akhir</label>
	<input class="form-control" type="text" name="judul" placeholder="" />
</div>
