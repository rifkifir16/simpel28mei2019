<div id="formPengalaman">
    	<div class="form-group pengalaman-group">
			<div class="row">
			<div class="col-md-6" style="padding-right:0;">
    		<label>Nama Instansi</label>
    		<input class="form-control" type="text" name="namainstansi[]"  />
			</div>
			<div class="col-md-6">
				<label>Jabatan</label>
    		<input class="form-control" type="text" name="jabataninstansi[]"  />
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Dari-Sampai (Tanggal Bulan Tahun)</label>
    		<input class="form-control" type="text" name="darismpthinstansi[]"  />
			</div>
			<div class="col-md-6">
				<label>Gaji</label>
    		<input class="form-control" type="text" name="gajiinstansi[]"  />
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Tunjangan</label>
    		<input class="form-control" type="text" name="tunjanganinstansi[]"  />
			</div>
			<div class="col-md-6">
				<label>Negara</label>
    		<input class="form-control" type="text" name="negarainstansi[]"  />
			</div>
			<div class="col-md-12">
				<label>Alasan Berhenti/Tujuan/Keterangan</label>
    		<input class="form-control" type="text" name="ketinstansi[]"  />
			</div>
    	</div>
		<hr/>
		</div>
		
	</div>
	<br/>
	<div class="form-group text-center">
		<a href="javascript:void(0)" class="btn btn-info" onclick="duppengalaman();">Tambah Pengalaman</a>
	</div>

	<hr/>

	<div id="formReferensi">
    	<div class="form-group referensi-group">
			<div class="row">
			<div class="col-md-6" style="padding-right:0;">
    		<label>Nama Referensi</label>
    		<input class="form-control" type="text" name="namareferensi[]"/>
			</div>
			<div class="col-md-6">
				<label>Alamat & Telp</label>
    		<input class="form-control" type="text" name="alamatreferensi[]"/>
			</div>
			<div class="col-md-6" style="padding-right:0;">
				<label>Jabatan</label>
    		<input class="form-control" type="text" name="jabatanreferensi[]"/>
			</div>
			<div class="col-md-6">
				<label>Telp Referensi</label>
    		<input class="form-control" type="text" name="telpreferensi[]"/>
			</div>
			<div class="col-md-12">
				<label>Hubungan</label>
    		<input class="form-control" type="text" name="hubunganreferensi[]"/>
			</div>
    	</div>
		<hr/>
		</div>
		
	</div>
	<br/>
	<div class="form-group text-center">
		<a href="javascript:void(0)" class="btn btn-info" onclick="dupreferensi();">Tambah Referensi</a>
	</div>

	<hr/>

	<div id="formOrganisasi">
    	<div class="form-group organisasi-group">
			<div class="row">
			<div class="col-md-6" style="padding-right:0;">
    		<label>Nama Organisasi</label>
    		<input class="form-control" type="text" name="namaorganisasi[]"/>
			</div>
			<div class="col-md-6">
				<label>Lokasi</label>
    		<input class="form-control" type="text" name="lokasiorganisasi[]"/>
			</div>
			<div class="col-md-12">
				<label>Sebagai</label>
    		<input class="form-control" type="text" name="sebagaiorganisasi[]"/>
			</div>
    	</div>
		<hr/>
		</div>
		
	</div>
	<br/>
	<div class="form-group text-center">
		<a href="javascript:void(0)" class="btn btn-info" onclick="duporganisasi();">Tambah Organisasi</a>
	</div>