<div id="formKecakapan">
	<div class="form-group kecakapan-group">
		<div class="row">
			<div class="col-md-6" style="padding-right:0;">
				<label>Bahasa</label>
				<input class="form-control" type="text" name="bahasa[]" required="true" />
			</div>
			<div class="col-md-6">
				<label>Kecakapan</label>
				<select class="form-control" name="kecbahasa[]" required>
					<option>Aktif</option>
					<option>Pasif</option>
				</select>
			</div>
		</div>
		<hr/>
	</div>
</div>

<div class="form-group text-center">
	<a href="#" class="btn btn-info" onclick="dupkecakapan();">Tambah Kecakapan</a>
</div>