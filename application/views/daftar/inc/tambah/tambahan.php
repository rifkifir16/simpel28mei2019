<div class="form-group">
	<label>Tunjangan saat ini</label>
	<input class="form-control" type="text" name="tunjangan" />
</div>
<div class="form-group">
	<label>Sumber Tunjangan</label>
	<input class="form-control" type="text" name="sumbertunjangan" />
</div>
<div class="form-group">
	<label>Gaji yang diharapkan</label>
	<input class="form-control" type="text" name="gaji" required="true" />
</div>
<div class="form-group">
	<label>Fasilitas yang diharapkan</label>
	<input class="form-control" type="text" name="fasilitas" required="true" />
</div>
<div class="form-group">
	<label>Bersedia ditempatkan dimana saja di seluruh wilayah operasi perusahaan</label>
	<select class="form-control" name="alllokasi" required>
		<option value="1">Ya</option>
		<option value="0">Tidak</option>
	</select>
</div>
<div class="form-group">
	<label>Bersedia mematuhi peraturan perusahaan yang berlaku maupun akan dikeluarkan perusahaan</label>
	<select class="form-control" name="allrule" required>
		<option value="1">Ya</option>
		<option value="0">Tidak</option>
	</select>
</div>
<div class="form-group">
	<label>Keterangan</label>
	<textarea class="form-control" name="keterangan"></textarea>
</div>

<div class="form-group text-center">
	<input class="btn btn-primary" id="btnSubmit" type="submit" name="submit" value="submit" />
</div>
