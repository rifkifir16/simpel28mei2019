
<div class="col-md-4 col-md-push-8  col-sm-12">
  <div class="box box-solid box-primary">
    <div class="box-header">
    <i class="fa fa-user"></i>
      Login
    </div>
    <div class="box-body">
    <?php
      if(isset($msg)){
        echo '<div class="col-xs-12 text-center"><a href="#" style="color:red;">'.$msg.'</a></div><br/>';
      }

    ?>
    <form action="<?=site_url('front/login')?>" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="uname" class="form-control" placeholder="Username" autofocus="true" required="true">
        <span class="glyphicon glyphicon-magnet form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="passw" class="form-control" placeholder="Password" required="true">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <div class="col-md-6 col-xs-12">
        <a href="<?=site_url('daftar')?>" class="btn btn-primary btn-block btn-flat">Daftar</a>
        </div>
        <div class="col-md-6 col-xs-12">
          <button type="submit" class="btn btn-block btn-flat">Masuk</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

      
    </div>
    <!--eeee-->
  </div>
</div>
