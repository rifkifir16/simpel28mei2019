<div class="col-md-4 col-md-push-8">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Petunjuk</h3>
			</div>
			<div class="box-body">
				Petunjuk :
				<ol>
					<li>Anda harus mengisi semua data yang dibutuhkan, karena tombol <b>submit</b> tidak akan berfungsi sebelum anda melengkapi data dengan valid.</li><br />
				</ol>
			</div>
		</div>
		</div>
		<div class="col-md-8 col-md-pull-4">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Data Pegawai</h3>
			  <div class="pull-right">
				<a href="<?=site_url()?>" class="btn btn-warning">Batal</a>
			  </div>
			</div>
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#tab_1" data-toggle="tab">Data Pribadi</a></li>
					  <li><a href="#tab_2" data-toggle="tab">Data Pribadi(2)</a></li>
					  <li><a href="#tab_3" data-toggle="tab">Relasi</a></li>
					  <li><a href="#tab_4" data-toggle="tab">Data Keluarga</a></li>
					  <li><a href="#tab_5" data-toggle="tab">Kecakapan</a></li>
					  <li><a href="#tab_6" data-toggle="tab">Riwayat</a></li>
					  <li><a href="#tab_7" data-toggle="tab">Pengalaman</a></li>
					  <li><a href="#tab_8" data-toggle="tab">Tambahan</a></li>
					</ul>
					<form method="post" id="fillForm" action="<?=site_url('daftar/save')?>" enctype="multipart/form-data">
						<div class="tab-content">
						  <div class="tab-pane active" id="tab_1">
							<?php include 'tambah/datapribadi.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_2">
							<?php include 'tambah/datapribadi2.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_3">
							<?php include 'tambah/datarelasi.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_4">
						  <?php include 'tambah/datakeluarga.php'; ?>
							
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_5">
							<?php include 'tambah/datakecakapan.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_6">
							<?php include 'tambah/riwayat.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_7">
							<?php include 'tambah/pengalaman.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_8">
							<?php include 'tambah/tambahan.php'; ?>
						  </div><!-- /.tab-pane -->
						</div><!-- /.tab-content -->
					</form>
				  </div><!-- nav-tabs-custom -->
			</div>
		</div>
		</div>