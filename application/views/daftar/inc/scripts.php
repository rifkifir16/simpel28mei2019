<!-- jQuery 3 -->
<script src="<?= base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/dist/js/adminlte.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
<script>
	$(document).ready(function() {
		$(".ajaxkota").select2({
			ajax: {
				url: "<?php echo site_url('ajax/ajaxkota'); ?>",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						q: params.term
					};
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(obj) {
							return {
								id: obj.id,
								text: obj.text
							};
						})
					};
				},
				cache: true
			},
			"language": "id"
		});

		$(".ajaxagama").select2({
			ajax: {
				url: "<?php echo site_url('ajax/ajaxagama'); ?>",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						q: params.term
					};
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(obj) {
							return {
								id: obj.id,
								text: obj.text
							};
						})
					};
				},
				cache: true
			},
			"language": "id"
		});

		$(".ajaxjabatan").select2({
			ajax: {
				url: "<?php echo site_url('ajax/ajaxjabatan'); ?>",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						q: params.term
					};
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(obj) {
							return {
								id: obj.id,
								text: obj.text
							};
						})
					};
				},
				cache: true
			},
			"language": "id"
		});

		$(".ajaxkualifikasi").select2({
			ajax: {
				url: "<?php echo site_url('ajax/ajaxkualifikasi'); ?>",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						q: params.term
					};
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(obj) {
							return {
								id: obj.id,
								text: obj.text
							};
						})
					};
				},
				cache: true
			},
			"language": "id"
		});
		$(".ajaxlokasi").select2({
			ajax: {
				url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						q: params.term
					};
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(obj) {
							return {
								id: obj.id,
								text: obj.text
							};
						})
					};
				},
				cache: true
			},
			"language": "id"
		});

		ajaxkontak();
	});

	function duprelasi() {
		$(".relasi-group").clone().removeClass('relasi-group').appendTo("#formRelasi");
	}

	function duporganisasi() {
		$(".organisasi-group").clone().removeClass('organisasi-group').appendTo("#formOrganisasi");
	}

	function dupkursus() {
		$(".kursus-group").clone().removeClass('kursus-group').appendTo("#formKursus");
	}

	function dupreferensi() {
		$(".referensi-group").clone().removeClass('referensi-group').appendTo("#formReferensi");
	}

	function duppengalaman() {
		$(".pengalaman-group").clone().removeClass('pengalaman-group').appendTo("#formPengalaman");
	}

	function dupkecakapan() {
		$(".kecakapan-group").clone().removeClass('kecakapan-group').appendTo("#formKecakapan");
	}

	function dupriwayat() {
		$(".riwayat-group").clone().removeClass('riwayat-group').appendTo("#formRiwayat");
	}

	function dupkontak() {
		$(".kontak-group .row .col-md-6").children('select').select2('destroy').end();
		$(".kontak-group").clone().removeClass('kontak-group').appendTo("#formkontak");

		//$( ".form-group" ).children('select').select2();


		ajaxkontak();
	}

	function dupkeluarga() {
		$(".keluarga-group").clone().removeClass('keluarga-group').appendTo("#formkeluarga");
	}

	function ajaxkontak() {
		$(".ajaxkontak").select2({
			ajax: {
				url: "<?php echo site_url('admin/basic/kontak/ajax'); ?>",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						q: params.term
					};
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(obj) {
							return {
								id: obj.id,
								text: obj.text
							};
						})
					};
				},
				cache: true
			},
			"language": "id"
		});
	}



	$('#fillForm').submit(function(e) {
		e.preventDefault();
		var form = new FormData($('#fillForm')[0]);

		$.ajax({
			url: "<?php echo site_url('daftar/save'); ?>",
			type: 'POST',
			data: form,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				alert(data.msg);
				if (data.success == true) {
					window.location.href = "<?= site_url('') ?>";
				}
			}
		});
	});

	function calTot() {
		upah = $('.upah').val();
		tumum = $('.tumum').val();
		tmakan = $('.tmakan').val();
		tkom = $('.tkom').val();
		var upah = upah.replace(/[\D\s\._\-]+/g, "");
		var tumum = tumum.replace(/[\D\s\._\-]+/g, "");
		var tmakan = tmakan.replace(/[\D\s\._\-]+/g, "");
		var tkom = tkom.replace(/[\D\s\._\-]+/g, "");
		var tot = upah * 1 + tmakan * 1 + tumum * 1 + tkom * 1;

		tot = tot ? parseInt(tot, 10) : 0;

		// 4
		$('.ttotal').val(function() {
			return (tot === 0) ? "" : tot.toLocaleString("id-ID");
		});
		//$('.ttotal').val(tot);
	}

	var iupah = $('#iupah');
	iupah.on("keyup", function(event) {
		// 1.
		var selection = window.getSelection().toString();
		if (selection !== '') {
			return;
		}

		// 2.
		if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
			return;
		}

		// 1
		var $this = $(this);
		var input = $this.val();

		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");

		// 3
		input = input ? parseInt(input, 10) : 0;

		// 4
		$this.val(function() {
			return (input === 0) ? "" : input.toLocaleString("id-ID");
		});

	});

	var itumum = $('#itumum');
	itumum.on("keyup", function(event) {
		// 1.
		var selection = window.getSelection().toString();
		if (selection !== '') {
			return;
		}

		// 2.
		if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
			return;
		}

		// 1
		var $this = $(this);
		var input = $this.val();

		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");

		// 3
		input = input ? parseInt(input, 10) : 0;

		// 4
		$this.val(function() {
			return (input === 0) ? "" : input.toLocaleString("id-ID");
		});

	});

	var itmakan = $('#itmakan');
	itmakan.on("keyup", function(event) {
		// 1.
		var selection = window.getSelection().toString();
		if (selection !== '') {
			return;
		}

		// 2.
		if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
			return;
		}

		// 1
		var $this = $(this);
		var input = $this.val();

		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");

		// 3
		input = input ? parseInt(input, 10) : 0;

		// 4
		$this.val(function() {
			return (input === 0) ? "" : input.toLocaleString("id-ID");
		});

	});

	var itkom = $('#itkom');
	itkom.on("keyup", function(event) {
		// 1.
		var selection = window.getSelection().toString();
		if (selection !== '') {
			return;
		}

		// 2.
		if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
			return;
		}

		// 1
		var $this = $(this);
		var input = $this.val();

		// 2
		var input = input.replace(/[\D\s\._\-]+/g, "");

		// 3
		input = input ? parseInt(input, 10) : 0;

		// 4
		$this.val(function() {
			return (input === 0) ? "" : input.toLocaleString("id-ID");
		});

	});
</script>
<script>
	$(function() {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});
</script>
