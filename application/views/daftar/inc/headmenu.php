<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
  <ul class="nav navbar-nav">
    <li><a href="#">Feedback</a></li>
    <li><a href="#">Tentang Cathut</a></li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi <span class="caret"></span></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="#">Kebijakan Layanan</a></li>
        <li><a href="#">Privasi Pengguna</a></li>
        <li class="divider"></li>
        <li><a href="#">Dukungan pihak ketiga</a></li>
      </ul>
    </li>
  </ul>
  <form class="navbar-form navbar-left" role="search">
    <div class="form-group">
      <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
    </div>
  </form>
</div>