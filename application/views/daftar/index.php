<!DOCTYPE html>
<html>
<?php include 'inc/head.php';?>
<body class="hold-transition">
  <div class="login-logo bg-blue">
    <a href="<?=site_url()?>" style="color:white;"><b>SIMPP</a> <small>versi alpha</small>
  </div>
  <?php include 'inc/loginbox.php'; ?>
  <?php include 'inc/descbox.php'; ?>
  <?php include 'inc/footer.php'; ?>
  

  <!-- /.login-box -->
  <?php include 'inc/scripts.php';?>

</body>
</html>
