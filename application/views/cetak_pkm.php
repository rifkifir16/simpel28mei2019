<?php
function numberToRomanRepresentation($number) {
  $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
  $returnValue = '';
  while ($number > 0) {
      foreach ($map as $roman => $int) {
          if($number >= $int) {
              $number -= $int;
              $returnValue .= $roman;
              break;
          }
      }
  }
  return $returnValue;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Lembar Pengesahan Penelitian</title>
<style type="text/css">
  .style1 {font-family: Arial, Helvetica, sans-serif}
  .style3 {font-size: 14px}
  .style4 {font-size: 14}
  .style7 {font-family: "Times New Roman", Times, serif; font-size: 14; }
</style>
</head>

<body>
<div align="center">
  <p>&nbsp;</p>
  <p><strong><a href="javascript:window.print()" style="text-decoration:none; color:#000000">HALAMAN PENGESAHAN USULAN PKM</a></strong></p>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
  	<tr>
      <td class="style3">&nbsp;</td>
      <td colspan="4" class="style3">&nbsp;</td>
      <td class="style3">&nbsp;</td>
      <td colspan="2" class="style3">&nbsp;</td>
    </tr>
  <tr>
    <td width="6%" class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">JUDUL</span></td>
    <td width="1%" class="style3"><span class="style7">:</span></td>
    <td width="71%" colspan="2" class="style3"><span class="style7"><?=$datas['JUDUL_PKM']?>

</span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <tr>
    <td width="6%" class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">NAMA MITRA</span></td>
    <td width="1%" class="style3"><span class="style7">:</span></td>
    <td width="71%" colspan="2" class="style3"><span class="style7"><?=$datas['MITRA_PKM']?>

</span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">KETUA TIM PENGUSUL </span></td>
    <td class="style3">&nbsp;</td>
    <td class="style3" colspan="2"><span class="style4"></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td width="2%" class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">a. Nama Lengkap </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"> <?=$datas['NAMA_DOSEN']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">b. NIDN </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"> <?=$datas['NIDN']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">c. Jabatan Fungsional / Pangkat </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['PANGKAT']?> / <?=$datas['NAMA_GOLONGAN']?>
          </span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">d. Program Studi </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['NAMA_JURUSAN']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">e. Bidang Keahlian </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['JENIS_BIDANG']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">f. Nomor Hp </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['NO_TLP']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">g. Alamat surel <i>(e-mail)</i> </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['EMAIL']?></span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <!--start anggota-->
  
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">ANGGOTA TIM PENGUSUL </span></td>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td width="1%" class="style3"><span class="style7">a.</span></td>
    <td colspan="2" class="style3"><span class="style7">Jumlah Anggota </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"> <?=$jumlah?> Orang</span></td>
  </tr>
  <?php
  $num='b';
  $c=1;
  $ke=numberToRomanRepresentation($c);
  foreach($anggota as $ag){ ?>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td width="1%" class="style3"><span class="style7"><?=$num?>.</span></td>
    <td colspan="2" class="style3"><span class="style7">Nama Anggota <?=$ke?>/Bidang Keahlian </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$ag['NIDN'].' - '.$ag['NAMA_DOSEN'].' / '.$ag['JENIS_BIDANG'];?></span></td>
  </tr>
  <?php 
    $num++;
    $c++;
  }
  ?>
  <tr>
    <td height="23" class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td class="style3"><span class="style7"><?=$num?>.</span></td>
    <td colspan="2" class="style3"><span class="style7">Mahasiswa yang terlibaat </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7">
    <?php 
      $cnt=1;
      foreach($mahasiswa as $mh)
      {
        echo $cnt.'. '.$mh.'<br/>';
        $cnt++;
      }
    ?>
    </span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  
  <!--end anggota-->
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">LOKASI KEGIATAN </span></td>
    <td class="style3">&nbsp;</td>
    <td class="style3" colspan="2"><span class="style4"></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td width="2%" class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">a. Wilayah Mitra (Desa/Kec) </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"> <?=$datas['WIL_MITRA_PKM']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">b. Kabupaten/Kota </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"> <?=$datas['NAMA_KOTA']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">c. Propinsi </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['NAMA_PROP']?> / <?=$datas['NAMA_GOLONGAN']?>
          </span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">d. Jarak PT ke Lokasi </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=number_format($datas['JARAK_PKM'])?> Km</span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">LUARAN YANG DIHASILKAN</span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['OUTPUT_PKM']?> </span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">JANGKA WAKTU PELAKSANAAN</span></td>
    <td class="style3"><span class="style7">:</span></td>
	
    <td class="style3"><span class="style7"><?=$datas['PELAKSANAAN_PKM']?> Bulan</span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">BIAYA PENGABDIAN</span></td>
    <td class="style3"><span class="style7">:</span></td>
	
    <td class="style3"><span class="style7"><?=$datas['SUMBER_BIAYA_PKM']?> </span></td>
	<td class="style3"><span class="style7">Rp <?=number_format($datas['BIAYA_PKM'])?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
    <td width="15%" class="style3">&nbsp;</td>
    <td width="4%" class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
  </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4"><?=$datas['tanggal'];?></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3">Mengetahui,</span></td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4"></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3"><?=$_data['pdekan']?></span></td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4">Ketua Pelaksana</span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3"></span></td>
      <td>&nbsp;</td>
      <td class="style3"><p class="style4">&nbsp;</p>
      <p class="style4">&nbsp;</p>                    </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3"><?=$_data['ndekan']?></span></td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4"> <?=$datas['NAMA_DOSEN']?></span></td>
    </tr>
    <tr>
      <td width="5%">&nbsp;</td>
      <td width="45%"><span class="style3">NIP.<?=$_data['npdekan']?></span></td>
      <td width="14%">&nbsp;</td>
      <td width="36%" class="style3"><span class="style4">NIP.<?=$datas['NIP']?></span></td>
    </tr>
</table>
<table width="50%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="85%">&nbsp;</td>
      <td width="15%">&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3">Menyetujui,</div></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3"><?=$_data['plppm']?></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>                    </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3"><?=$_data['nlppm']?></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3">NIP.<?=$_data['nplppm']?></div></td>
      <td>&nbsp;</td>
    </tr>
  </table>
  
  
</body>
<script src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script src="<?=base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<script>
	$(document).ready(function() {
		window.print();
		
	});
	</script>
</html>

