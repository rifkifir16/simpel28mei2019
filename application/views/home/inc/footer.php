<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0.0
    </div>
    <strong>Copyright © 2019 <a href="http://imfar.id" target="_blank">PT. Surveyor Indonesia</a></strong> All rights reserved.

  </footer>