<aside class="main-sidebar">
    <section class="sidebar">
    
    <ul class="sidebar-menu" data-widget="tree">
    <?php if($sesi['hakakses']==3){?>
    <li><a href="<?=site_url('admin/home');?>"><i class="fa fa-refresh"></i> <span>User Admin</span></a></li>
    <?php }?>
      <li <?php if($_page=='pages/home') echo 'class="active"';?>>
        <a href="<?=site_url('dosen/home');?>">
          <i class="fa fa-home"></i> <span>Home</span>
        </a>
      </li>
      <li class="treeview <?php if($_page=='pages/dosen/penelitian' || $_page=='pages/dosen/pkm') echo 'active';?>">
        <a href="#">
          <i class="fa fa-file-o"></i> <span>Penelitian dan PKM</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          <x/span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?=site_url('dosen/penelitian');?>"><i class="fa fa-circle-o"></i> Penelitian</a></li>
          <li><a href="<?=site_url('dosen/pkm');?>"><i class="fa fa-circle-o"></i> PKM</a></li>
        </ul>
      </li>
      <li <?php if($_page=='pages/profil') echo 'class="active"';?>>
        <a href="<?=site_url('dosen/profil');?>">
          <i class="fa fa-user-o"></i> <span>Profile</span>
        </a>
      </li>
      <li>
        <a href="<?=site_url('front/logout');?>"><i class="fa fa-lock"></i> <span>Keluar</span>
        </a>
      </li>
  </ul>
</section>
<!-- /.sidebar -->
</aside>