<aside class="main-sidebar">
    <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
    <?php if($sesi['hakakses']==3){?>
    <li><a href="<?=site_url('dosen/home');?>"><i class="fa fa-refresh"></i> <span>User Dosen</span></a></li>
    <?php }?>
    <li <?php if($_page=='pages/admin/home') echo 'class="active"';?>>
        <a href="<?=site_url('admin/home');?>">
          <i class="fa fa-home"></i> <span>Home</span>
        </a>
      </li>
    <li class="treeview">
        <a href="#">
          <i class="fa fa-file"></i> <span>Data Basic</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?=site_url('admin/bidang');?>"><i class="fa fa-circle-o"></i> Bidang</a></li>
          <li><a href="<?=site_url('admin/jabatan');?>"><i class="fa fa-circle-o"></i> Jabatan</a></li>
          <li><a href="<?=site_url('admin/jenis');?>"><i class="fa fa-circle-o"></i> Jenis Penelitian</a></li>
          <li><a href="<?=site_url('admin/pangkat');?>"><i class="fa fa-circle-o"></i> Pangkat</a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i> Fakultas
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?=site_url('admin/jurusan');?>"><i class="fa fa-circle-o"></i> Jurusan</a></li>
              <li><a href="<?=site_url('admin/prodi');?>"><i class="fa fa-circle-o"></i> Prodi</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i> Data Pendukung
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?=site_url('admin/kota');?>"><i class="fa fa-circle-o"></i> Kota</a></li>
              <li><a href="<?=site_url('admin/propinsi');?>"><i class="fa fa-circle-o"></i> Propinsi</a></li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="treeview <?php if($_page=='pages/admin/dosen' || $_page=='pages/admin/user') echo 'active';?>">
        <a href="#">
          <i class="fa fa-users"></i> <span>Pengguna</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?=site_url('admin/dosen');?>"><i class="fa fa-circle-o"></i> Data Dosen</a></li>
          <li><a href="<?=site_url('admin/user');?>"><i class="fa fa-circle-o"></i> Data Pengguna</a></li>
        </ul>
      </li>
      <li class="treeview <?php if($_page=='pages/admin/penelitian' || $_page=='pages/admin/pkm') echo 'active';?>">
        <a href="#">
          <i class="fa fa-file-o"></i> <span>Penelitian & PKM</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?=site_url('admin/penelitian');?>"><i class="fa fa-circle-o"></i> Penelitian</a></li>
          <li><a href="<?=site_url('admin/pkm');?>"><i class="fa fa-circle-o"></i> PKM</a></li>
          <li><a href="<?=site_url('admin/rekap');?>"><i class="fa fa-circle-o"></i> Download Rekap</a></li>
        </ul>
      </li>
      <li <?php if($_page=='pages/admin/setting') echo 'class="active"';?>>
        <a href="<?=site_url('admin/setting');?>">
          <i class="fa fa-gear"></i> <span>Setting</span>
        </a>
      </li>
      <li <?php if($_page=='pages/admin/profil') echo 'class="active"';?>>
        <a href="<?=site_url('admin/profil');?>">
          <i class="fa fa-user-o"></i> <span>Profile</span>
        </a>
      </li>
      <li>
        <a href="<?=site_url('front/logout');?>"><i class="fa fa-lock"></i> <span>Keluar</span>
        </a>
      </li>
  </ul>
</section>
<!-- /.sidebar -->
</aside>