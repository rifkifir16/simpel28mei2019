<aside class="main-sidebar">
<section class="sidebar">
  <ul class="sidebar-menu">
    <li class="<?php if($_page=='admin/pass') echo 'active';?>"><a href="index.php?page=pass"><i class="fa fa-unlock"></i> <span>Ubah Password</span></a></li>
   
	<li class="treeview <?php if($_page=='admin/my/my' || $_page=='admin/my/kontak' || $_page=='admin/my/keluarga') echo 'active';?>">
	  <a href="#">
		<i class="fa fa-user"></i> <span>Data Pribadi</span> <i class="fa fa-angle-left pull-right"></i>
	  </a>
	  <ul class="treeview-menu">
		<li><a href="index.php?page=my/my"><i class="fa fa-circle-o"></i> Data Saya</a></li>
		<li><a href="index.php?page=my/keluarga"><i class="fa fa-circle-o"></i> Data Keluarga</a></li>
		<li><a href="index.php?page=my/kontak"><i class="fa fa-circle-o"></i> Kontak </a></li>
	  </ul>
	</li>
    <li class="<?php if($_page=='admin/my/sppd') echo 'active';?>"><a href="index.php?page=my/sppd"><i class="fa fa-tasks"></i> <span>SPPD</span></a></li>
    <li class="<?php if($_page=='admin/pegawai/absen') echo 'active';?>"><a href="index.php?page=pegawai/absen"><i class="fa fa-edit"></i> <span>Absensi</span></a></li>
    <li class="<?php if($_page=='admin/my/slip') echo 'active';?>"><a href="index.php?page=my/slip"><i class="fa fa-money"></i> <span>Slip Gaji </span></a></li>
    <li><a href="<?=site_url('front/logout');?>"><i class="fa fa-lock"></i> <span>Logout</span></a></li>
  </ul>
</section>

</aside>