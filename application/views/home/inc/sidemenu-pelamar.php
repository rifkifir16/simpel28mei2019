<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel" style="background-color:white;">
			<div class="pull-left image">
				<img src="<?= base_url("assets/images/logo.png") ?>">
			</div>
			<div class="pull-left info">
				<p style="color:black;">PT. Surveyor Indonesia</p>
				<a href="#"></a>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="<?php if ($_page == 'admin/pass') echo 'active'; ?>"><a href="<?= site_url('admin/pass'); ?>"><i class="fa fa-unlock"></i> <span>Ubah Password</span></a></li>

			<li class="treeview ">
				<a href="#">
					<i class="fa fa-info"></i> <span>Pelamar</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= site_url(); ?>"><i class="fa fa-circle-o"></i> Data Pribadi</a></li>
					<li><a href="<?= site_url(); ?>"><i class="fa fa-circle-o"></i> Data Relasi</a></li>
					<li><a href="<?= site_url(); ?>"><i class="fa fa-circle-o"></i> Data Keluarga </a></li>
					<li><a href="<?= site_url(); ?>"><i class="fa fa-circle-o"></i> Data Kecakapan </a></li>
					<li><a href="<?= site_url(); ?>"><i class="fa fa-circle-o"></i> Data Riwayat </a></li>
					<li><a href="<?= site_url(); ?>"><i class="fa fa-circle-o"></i> Data Keterangan </a></li>
				</ul>
			</li>

			<li><a href="<?= site_url('front/logout'); ?>"><i class="fa fa-lock"></i> <span>Logout</span></a></li>
		</ul>
	</section>

</aside>
