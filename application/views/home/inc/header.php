<header class="main-header">

  <!-- Logo -->
  <a href="<?=site_url()?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>S</b>P</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Sim</b>Pel</span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <?php 
        //   include 'messages.php';
        //include 'friendreq.php';
        //include 'notifications.php';
        
        ?>
        
        
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            
            <span class="hidden-xs"><?=$sesi['nama']?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              

              <p>
                <?=$sesi['nama']?>
                <small><?=$sesi['nip']?></small>
              </p>
            </li>
            
            <li class="user-footer">
              
              <div class="col-xs-12">
                <a href="<?=site_url('front/logout')?>" class="btn btn-default btn-flat col-xs-12">Logout</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
          <!--li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li-->
        </ul>
      </div>

    </nav>
  </header>