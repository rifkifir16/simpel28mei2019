<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel" style="background-color:white;">
			<div class="pull-left image">
				<img src="<?= base_url("assets/images/logo.png") ?>">
			</div>
			<div class="pull-left info">
				<p style="color:black;">PT. Surveyor Indonesia</p>
				<a href="#"></a>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="<?php if ($_page == 'pages/admin/pass') echo 'active'; ?>"><a href="<?= site_url('admin/pass'); ?>"><i class="fa fa-unlock"></i> <span>Ubah Password</span></a></li>
			<li class="<?php if ($_page == 'pages/admin/user/pegawai/data') echo 'active'; ?>">
				<a href="<?= site_url('admin/user/pegawai'); ?>"><i class="fa fa-circle-o"></i> Data Pegawai</a>
			</li>
			<li class="<?php if ($_page == 'pages/admin/penugasan/sppd/data') echo 'active'; ?>">
				<a href="<?= site_url('admin/penugasan/sppd'); ?>"><i class="fa fa-circle-o"></i> SPPD</a>
			</li>
			
			<li class="<?php if ($_page == 'pages/admin/serapan/main') echo 'active'; ?>"><a href="<?= site_url('admin/fitur/serapan'); ?>"><i class="fa fa-area-chart"></i> <span>Serapan</span></a></li>

			<li><a href="<?= site_url('front/logout'); ?>"><i class="fa fa-lock"></i> <span>Logout</span></a></li>
		</ul>
	</section>

</aside>
