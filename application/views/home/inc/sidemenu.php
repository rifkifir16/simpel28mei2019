<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel" style="background-color:white;">
			<div class="pull-left image">
				<img src="<?= base_url("assets/images/logo.png") ?>">
			</div>
			<div class="pull-left info">
				<p style="color:black;">PT. Surveyor Indonesia</p>
				<a href="#"></a>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="<?php if ($_page == 'admin/pass') echo 'active'; ?>"><a href="<?= site_url('admin/pass'); ?>"><i class="fa fa-unlock"></i> <span>Ubah Password</span></a></li>

			<li class="treeview <?php if ($_page == 'admin/basic/agama' || $_page == 'admin/basic/jabatan' || $_page == 'admin/basic/kualifikasi' || $_page == 'admin/basic/lokasi' || $_page == 'admin/basic/kota' || $_page == 'admin/basic/propinsi' || $_page == 'admin/basic/kontak') echo 'active'; ?>">
				<a href="#">
					<i class="fa fa-info"></i> <span>Data Basic</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= site_url('admin/basic/agama'); ?>"><i class="fa fa-circle-o"></i> Data Agama</a></li>
					<li><a href="<?= site_url('admin/basic/jabatan'); ?>"><i class="fa fa-circle-o"></i> Data Jabatan</a></li>
					<li><a href="<?= site_url('admin/basic/kualifikasi'); ?>"><i class="fa fa-circle-o"></i> Data Kualifikasi</a></li>
					<li><a href="<?= site_url('admin/basic/kota'); ?>"><i class="fa fa-circle-o"></i> Data Kota</a></li>
					<li><a href="<?= site_url('admin/basic/lokasi'); ?>"><i class="fa fa-circle-o"></i> Data Lokasi</a></li>
					<li><a href="<?= site_url('admin/basic/propinsi'); ?>"><i class="fa fa-circle-o"></i> Data Propinsi</a></li>
					<li><a href="<?= site_url('admin/basic/kontak'); ?>"><i class="fa fa-circle-o"></i> Jenis Kontak</a></li>
				</ul>
			</li>
			<li class="<?php if ($_page == 'admin/penggajian/gaji') echo 'active'; ?>"><a href="<?= site_url('admin/user/pelamar'); ?>"><i class="fa fa-graduation-cap"></i> <span>Pelamar</span></a></li>
			<li class="treeview <?php if ($_page == 'admin/pegawai/pegawai' || $_page == 'admin/user/user') echo 'active'; ?>">
				<a href="#">
					<i class="fa fa-users"></i> <span>Data User</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= site_url('admin/user/pegawai'); ?>"><i class="fa fa-circle-o"></i> Data Pegawai</a></li>
					<li><a href="<?= site_url('admin/user/user'); ?>"><i class="fa fa-circle-o"></i> Data User</a></li>
				</ul>
			</li>
			<li class="treeview <?php if ($_page == 'admin/sppd/sppd' || $_page == 'admin/penempatan/penempatan') echo 'active'; ?>">
				<a href="#">
					<i class="fa fa-tasks"></i> <span>Penugasan</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= site_url('admin/penugasan/penempatan'); ?>"><i class="fa fa-circle-o"></i> Penempatan</a></li>
					<li><a href="<?= site_url('admin/penugasan/sppd'); ?>"><i class="fa fa-circle-o"></i> SPPD</a></li>
					<li><a href="<?= site_url('admin/penugasan/rekap'); ?>"><i class="fa fa-circle-o"></i> Rekap SPPD</a></li>
				</ul>
			</li>

			<li class="<?php if ($_page == 'admin/penggajian/gaji') echo 'active'; ?>"><a href="<?= site_url('admin/fitur/penggajian'); ?>"><i class="fa fa-money"></i> <span>Penggajian</span></a></li>
			<li class="<?php if ($_page == 'pages/admin/fitur/penagihan') echo 'active'; ?>"><a href="<?= site_url('admin/fitur/penagihan'); ?>"><i class="fa fa-money"></i> <span>Penagihan</span></a></li>


			<li class="treeview <?php if ($_page == 'admin/sppd/sppd' || $_page == 'admin/penempatan/penempatan') echo 'active'; ?>">
				<a href="#">
					<i class="fa fa-area-chart"></i> <span>Serapan</span> <i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= site_url('admin/fitur/serapan'); ?>"><i class="fa fa-circle-o"></i> <span>Serapan</span></a></li>
					<li><a href="<?= site_url('admin/fitur/Serapan_penggajian'); ?>"><i class="fa fa-circle-o"></i> <span>Penggajian</span></a></li>
					<li><a href="<?= site_url('admin/fitur/serapan_pegawai'); ?>"><i class="fa fa-circle-o"></i> <span>Pegawai</span></a></li>
				</ul>
			</li>
			<li><a href="<?= site_url('admin/fitur/surat'); ?>"><i class="fa glyphicon glyphicon-envelope"></i> Surat</a></li>

			<li><a href="<?= site_url('front/logout'); ?>"><i class="fa fa-lock"></i> <span>Logout</span></a></li>
		</ul>
	</section>

</aside>
