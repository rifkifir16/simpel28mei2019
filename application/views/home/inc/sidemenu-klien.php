<aside class="main-sidebar">
<section class="sidebar">
  <ul class="sidebar-menu">
    <li class="<?php if($_page=='admin/pass') echo 'active';?>"><a href="index.php?page=pass"><i class="fa fa-unlock"></i> <span>Ubah Password</span></a></li>
	<li class="<?php if($_page=='admin/user/absen') echo 'active';?>"><a href="index.php?page=user/absen"><i class="fa fa-edit"></i> <span>Absensi</span></a></li>
    <li><a href="<?=site_url('front/logout');?>"><i class="fa fa-lock"></i> <span>Logout</span></a></li>
  </ul>
</section>

</aside>