<!DOCTYPE html>
<html>
<?php include 'inc/head.php';?>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <?php 
    if(!isset($_page))  $_page='';
    include 'inc/header.php';
    include $_sidebar;?>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> <!--start-->
    <?php
      if($_page!='') 
        $this->view($_page);
    ?>
  </div>
  <!-- /.content-wrapper -->

  

  <?php include 'inc/footer.php';?>
  <!--php include 'inc/sidecon.php';?-->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
<?php include 'inc/scripts.php';?>


</body>
</html>
