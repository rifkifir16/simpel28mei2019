<div class="col-md-8 col-md-pull-4 col-sm-12">
  <div class="box box-solid box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">PT. Surveyor Indonesia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    <p>Aplikasi ini merupakan sistem informasi yang dikembangkan oleh PT. Surveyor Indonesia untuk keperluan :</p>
      <p>
        <ol>
          <li>Rekrutmen karyawan</li>
          <li>Manajemen kepegawaian</li>
          <li>Penugasan karyawan</li>
          <li>Pelaporan serapan karyawan</li>
        </ol>
      </p>
      <p>Jika anda mengalami permasalahan saat menggunakan aplikasi ini, silahkan menghubungi admin.</p>
      <p>Terima Kasih</p>
    </div>
    <!-- /.box-body -->
  </div>
</div>