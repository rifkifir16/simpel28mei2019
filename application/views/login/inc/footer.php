<footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Versi</b> 2.0.0
      </div>
      <strong>Copyright © 2019 <a href="http://imfar.id" target="_blank">PT. Surveyor Indonesia</a></strong> All rights reserved.
    </div>
    <!-- /.container -->
  </footer>