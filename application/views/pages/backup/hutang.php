<section class="content">
  <?php 
    include 'hutang/widget.php';
    include 'hutang/modal.php';
  ?>

  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Catatan Hutang</h3>
      <div class="box-tools">

        <div class="input-group input-group-sm pull-right" style="width: 150px;">
          <input type="text" name="table_search" class="form-control" placeholder="Search">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table id="tblHutang" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Jumlah Pinjam</th>
              <th>Status</th>
              <th>Pilihan</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              <td>Rp 15.000.000</td>
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
            <tr>
              <td>Mas Bambang</td>
              
              <td>Rp 15.000.000</td>
              
              <td><span class="label label-success">Disetujui</span></td>
              <td><button data-toggle="modal" data-target="#modal-bayar" class="btn btn-primary btn-xs">Bayar</button></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>