<?php
$jabatan="";
if($sesi['hakakses']==1) $jabatan='Dosen';
elseif($sesi['hakakses']==2) $jabatan='Administrator';
elseif($sesi['hakakses']==3) $jabatan='Super Admin';
?>

  <div class="col-md-6">
	  <!-- Widget: user widget style 1 -->
	  <div class="box box-widget widget-user">
		<!-- Add the bg color to the header using any of the bg-* classes -->
		<div class="widget-user-header bg-aqua-active">
		  <h3 class="widget-user-username"><?=$sesi['nama']?></h3>
		  <h5 class="widget-user-desc"><?=$jabatan?></h5>
		</div>

		<div class="box-footer">
		  <div class="row">
			<div class="col-sm-6 border-right">
			  <div class="description-block">
				<h5 class="description-header"><?=$penelitian?></h5>
				<span class="description-text">Penelitian</span>
			  </div>
			  <!-- /.description-block -->
			</div>
			<!-- /.col -->
			<div class="col-sm-6 border-right">
			  <div class="description-block">
				<h5 class="description-header"><?=$pkm?></h5>
				<span class="description-text">PKM</span>
			  </div>
			  <!-- /.description-block -->
			</div>
		  </div>
		  <!-- /.row -->
		</div>
	  </div>
	  <!-- /.widget-user -->
	</div>
  