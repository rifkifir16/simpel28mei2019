<div class="col-md-6">
  <div class="box box-widget">
	<div class="box-header with-border">
	  <div class="user-block">
		<span class="username" style="margin-left:0;"><a href="#">Administrator.</a></span>
		<span class="description" style="margin-left:0;"></span>
	  </div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
	  <!-- post text -->
	  <p>Selamat datang di halaman utama Dosen.</p>

	  <ul>
		<li>Periksa data anda pada halaman profil, hubungi admin jika ada kesalahan data mengenai diri anda.</li>
		<li>Ubahlah password anda secara berkala.</li>
	  </ul>

	</div>
  </div>
  <!-- /.box -->
</div>