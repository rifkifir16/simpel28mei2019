<section class="content">
  <div id="msgAlert" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
    <p id="msgContent"></p>
  </div>
  <div class="row">
  <div class="col-sm-6">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Profil Dosen</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form role="form" id="profil" method="post">
        <!-- text input -->
        <div class="form-group col-sm-12" style="padding:0;">
          <label class="col-sm-2">Nama</label>
          <div class="col-sm-10">
          <input type="text" class="form-control" placeholder="Nama" name="nama" value="<?=isset($_data['NAMA_DOSEN'])?$_data['NAMA_DOSEN']:''?>" autofocus="true" id="inputNama">
          </div>
        </div>
        <div class="form-group col-sm-12" style="padding:0;">
          <label class="col-sm-2">Email</label>
          <div class="col-sm-10">
          <input type="email" class="form-control" placeholder="Email" name="email" value="<?=isset($_data['EMAIL'])?$_data['EMAIL']:''?>">
          </div>
        </div>
        <div class="form-group col-sm-12" style="padding:0;">
          <label class="col-sm-2">Telepon</label>
          <div class="col-sm-10">
          <input type="text" class="form-control" placeholder="No. Telepon" name="telp" value="<?=isset($_data['NO_TLP'])?$_data['NO_TLP']:''?>">
          </div>
        </div>
        <!-- textarea -->
        <div class="form-group col-sm-12" style="padding:0;">
          <label class="col-sm-2">Alamat Rumah</label>
          <div class="col-sm-10">
          <textarea class="form-control" rows="3" name="alamatRumah" placeholder="Alamat Rumah"><?=isset($_data['ALAMAT_RUMAH'])?$_data['ALAMAT_RUMAH']:''?></textarea>
          </div>
        </div>
        <div class="form-group col-sm-12" style="padding:0;">
          <label class="col-sm-2">Alamat Kantor</label>
          <div class="col-sm-10">
          <textarea class="form-control" rows="3" name="alamatKantor" placeholder="Alamat Kantor"><?=isset($_data['ALAMAT_KANTOR'])?$_data['ALAMAT_KANTOR']:''?></textarea>
          </div>
        </div>
        <div class="form-group col-sm-12 text-center">
          <input type="hidden" name="idUser" value="<?=$_nip?>" />
          <input type="submit" name="submit" value="simpan"  class="btn btn-primary"/>

        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
  </div>

  <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-user"></i>

          <h3 class="box-title">Informasi anda</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt>NIP</dt>
            <dd><?=isset($_data['NIP'])?$_data['NIP']:'-'?></dd>
            <dt>NIDN</dt>
            <dd><?=isset($_data['NIDN'])?$_data['NIDN']:''?></dd>
            <dt>Pangkat</dt>
            <dd><?=isset($_data['NAMA_PANGKAT'])?$_data['NAMA_PANGKAT']:''?></dd>
            <dt>Jabatan</dt>
            <dd><?=isset($_data['NAMA_JABATAN'])?$_data['NAMA_JABATAN']:''?>
            </dd>
            <dt>Jurusan</dt>
            <dd><?=isset($_data['NAMA_JURUSAN'])?$_data['NAMA_JURUSAN']:''?>
            </dd>
          </dl>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>

    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-lock"></i>

          <h3 class="box-title">Ubah Password</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="password" method="post">
          <!-- text input -->
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Password</label>
              <div class="col-sm-10">
              <input type="password" class="form-control" id="pass" name="pass" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Verifikasi password</label>
              <div class="col-sm-10">
              <input type="password" class="form-control" id="vpass" name="vpass" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12 text-center">
              <input type="hidden" name="idUser" value="<?=$_nip?>" />
              <input type="submit" name="submit" value="simpan" class="btn btn-primary"/>
            
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>