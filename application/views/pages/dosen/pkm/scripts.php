<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/jquery.backstretch.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/retina-1.1.0.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/scripts.js')?>"></script>
<script>
  
  var table;
  var kategori="pkm";
  var status=0;
  $(function () {
    table=$('#tblPenelitian').DataTable({
      "processing": true,
      "serverSide":true,
      
      "ajax": {
            "url": "<?php echo site_url('dosen/pkm/data'); ?>",
            "type": "POST"
        },
			
      "columnDefs": [ {
          "targets": -2,
          "render":function ( data, type, full, meta ) {
                return '<a href="#"'+data+'</a>';
            }
          },
          {
            "targets": -1,
            "defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
            "render":function ( data, type, full, meta ) {
              var pesan='Hapus';
              //if(data==1) pesan='Nonaktifkan';
              return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkedit">Edit</a></li><li><a href="#" class="linkdelete">'+pesan+'</a></li></ul></div>';
            }

          },
          {
            "targets": [1],
            "visible": false,
            "searchable": false
          }
          
            ],
      "language": {
          "lengthMenu": "Menampilkan _MENU_ data per halaman",
          "zeroRecords": "Data masih kosong atau tidak ditemukan",
          "info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
          "infoEmpty": "Data masih kosong"
      }
    })

    $('#tblPenelitian tbody').on( 'click', '.linkedit', function (e) {
            var data = table.row( $(this).parents('tr') ).data();
            if(data[9]==4)
              errmsg('Tidak dapat mengubah data karena PKM telah disetujui. Silahkan hubungi administrator.');
            else{
              status=data[9];
              edit(data[1]);
              $('#judul').focus();
            }
        });
    $('#tblPenelitian tbody').on( 'click', '.linkdelete', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        if(data[9]==4)
          errmsg('Tidak dapat menghapus data karena PKM telah disetujui. Silahkan hubungi administrator.');
        else
          hapus(data[1],data[3]);
    });
  })

  //$('#modal-bayar').modal({backdrop: 'static', keyboard: false,show:false}) ;
  $('.modal').modal({backdrop: 'static', keyboard: false,show:false}) ;

  $('#formPenelitian').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#formPenelitian')[0]);
    form.append('kategori',kategori);
    form.append('status',status);
    $.ajax({
        url: "<?php echo site_url('dosen/pkm/savePkm'); ?>",
        type: 'POST',
        data: form,
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
            var resp=JSON.parse(data);
            
            if(resp[0]==true){
              errmsg(resp[1]);
              
            }
            else{
              status=0;
              batal();
              sucmsg(resp[1]);
              setModal();
              table.ajax.reload();
            }
        },
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                // For handling the progress of the upload
                conModal();
                myXhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      $('#progress').html( e.loaded+"/"+e.total);
                        if(e.loaded>=e.total)
                        $('#modalPenelitian').modal('hide');
                    }
                } , false);
            }
            return myXhr;
        }
    });
  });

  function hapus(id,judul){
    if(confirm('Apakah anda yakin menghapus PKM dengan judul : '+judul+' ?')){
      $.ajax({
          url: "<?php echo site_url('dosen/pkm/hapus'); ?>",
          type: "GET",
          data : {id: id},
          success: function (data) {
            alert(data);
            table.ajax.reload();
          }
      });
    }else
        {
            alert('Batal menghapus data.');
        }
  }
 
  $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });
  function conModal(){
    $('#forms').toggle();
    $('#tef').toggle();
  }

  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
  $('#modalPenelitian').on('shown.bs.modal', function () {
        $('#judul').focus();
        $('#forms').show();
        $('#tef').hide();
  }) 



    function setModal(cont=false){
      var isi=['','','','','','','','','','','','','','','','',''];
      if(cont!=false)
        isi=cont;
      $('#isedit').val(isi[0]);
      $('#judul').val(isi[1]);
      $('#mitra').val(isi[2]);
      
      if(cont==false)
      $('#bidangk').prepend('<option value="" selected>--Pilih Bidang--</option>');
      else
        $('#bidangk').prepend('<option value="'+isi[3]+'" selected>'+isi[4]+'</option>');
      
      var mhs=isi[6].replace(/#/g,"");
      $('#mahasiswa').val(mhs);
      if(cont==false)
        $('#jenis').prepend('<option value="" selected>--Pilih Jenis PKM--</option>');
      else
        $('#jenis').prepend('<option value="'+isi[7]+'" selected>'+isi[8]+'</option>');

      $('#biaya').val(isi[9]);
      $('#biaya').keyup();
      $('#sbBiaya').val(isi[10]);
      $('#output').val(isi[11]);
      if(cont==false)
        $('#kota').prepend('<option value="" selected>--Pilih Lokasi Kegiatan--</option>');
      else
        $('#kota').prepend('<option value="'+isi[12]+'" selected>'+isi[13]+'</option>');
      
      $('#desa').val(isi[14]);
      $('#jarak').val(isi[15]);
      $('#waktu').val(isi[16]);
      $('.cloned').remove();
      if(isi[5]!=''){
        var anggota=isi[5].split('#');
        
        if(anggota.length>0){
              i=0;
              l=anggota[i].split(';');
              $('#anggota').prepend('<option value="'+l[0]+'" selected>'+l[1]+'</option>');
              $('#bidang').prepend('<option value="'+l[2]+'" selected>'+l[3]+'</option>');
              for(i=1;i<anggota.length;i++){
                  l=anggota[i].split(';');
                  $( ".links" ).children('select').select2('destroy').end();
                  var nForm=$( ".links" ).clone();
                  nForm.find(".ajaxDosen").prop('selectedIndex',parseInt(l[0]));
                  nForm.find(".ajaxDosen").prepend('<option value="'+l[0]+'" selected>'+l[1]+'</option>').end().addClass('cloned').removeClass('links').appendTo( "#linksWrap" );
                  nForm.find(".ajaxBidang").prop('selectedIndex',parseInt(l[2]));
                  nForm.find(".ajaxBidang").prepend('<option value="'+l[2]+'" selected>'+l[3]+'</option>').end().addClass('cloned').removeClass('links').appendTo( "#linksWrap" );
                  nForm.css('margin-top', '5px' );
              }
              ajaxDosen();
              ajaxBidang();
          }
          else{
          
          $('#anggota').prepend('<option value="" selected>--Pilih Anggota--</option>');
          $('#bidang').prepend('<option value="" selected>--Pilih Bidang Keahlian Anggota--</option>');
        }
      }
      else{
        $('#anggota').prepend('<option value="" selected>--Pilih Anggota--</option>');
        $('#bidang').prepend('<option value="" selected>--Pilih Bidang Keahlian Anggota--</option>');
      }
      
    }   
		
    function edit(id){
      $.ajax({
          url: "<?php echo site_url('dosen/pkm/reqEdit'); ?>",
          dataType: 'json', // jQuery will parse the response as JSON
          type: "POST",
          data : {id: id},
          success: function (data) {
            setModal(data);
            tambah();
            $('#modalPenelitian').modal('show');
          }
      });
    }
	
  function duplinks(){
      $( ".links" ).children('select').select2('destroy').end();
      var clone=$( ".links" ).clone().addClass('cloned').removeClass('links').appendTo( "#linksWrap" );
      clone.css('margin-top', '5px' );
      ajaxDosen();
      ajaxBidang();
  }

  $(document).ready(function() {
      $('#formView').hide();
      $(".ajaxJenis").select2({
        ajax: {
          url: "<?php echo site_url('admin/jenis/ajaxJenis'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });

       $(".ajaxJurusan").select2({
        ajax: {
          url: "<?php echo site_url('admin/jurusan/ajaxJurusan'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });
      $(".ajaxKota").select2({
        ajax: {
          url: "<?php echo site_url('admin/kota/ajaxKota'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });
      ajaxDosen();
      ajaxBidang();
        
	} );

  function ajaxBidang(){
    $(".ajaxBidang").select2({
        ajax: {
          url: "<?php echo site_url('admin/bidang/ajaxBidang'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });
  }

  function ajaxDosen(){
    $(".ajaxDosen").select2({
        ajax: {
          url: "<?php echo site_url('admin/dosen/ajaxDosen'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });
  }

  function tambah(){
    $('#formView').show();
    $('#tableView').hide();
    $('#judul').focus();
  }

  function batal(){
    $('#formView').hide();
    $('#tableView').show();
    setModal();
  }

  var ftpl=$(".ftpl");
  ftpl.on("keyup",function(i){if(""===window.getSelection().toString()&&-1===$.inArray(i.keyCode,[38,40,37,39])){var n=$(this),a=(a=n.val()).replace(/[\D\s\._\-]+/g,"");a=a?parseInt(a,10):0,n.val(function(){return 0===a?"":a.toLocaleString("id-ID")})}});
</script>