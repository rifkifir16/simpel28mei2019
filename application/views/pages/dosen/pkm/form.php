<!--modal bayar hutang-->
<div class="row" id="formView">
<form method="post" id="formPenelitian" enctype="multipart/form-data">
<div class="col-md-12">
  <div class="box">
    <div class="box-header">
      <div class="modal-header">
        <h3 class="box-title">Form PKM</h3>
      </div>
      <div class="box-body">
        <div class="col-md-6">
          <div class="form-group">
            <label>Judul PKM</label>
            <input type="text" class="form-control" id="judul" placeholder="Judul PKM" required="true" name="judul">
          </div>

          <div class="form-group">
            <label>Nama Mitra</label>
            <input type="text" class="form-control" id="mitra" placeholder="Nama Mitra" name="mitra">
          </div>

          <div class="form-group">
            <label>Bidang Ketua PKM</label>
            <select name="bidangk" class="form-control ajaxBidang" id="bidangk" style="width:100%;" required>
              <option value="">--Pilih Bidang--</option>
            </select>
          </div>
		  
          <div class="form-group col-sm-12" id="linksWrap" style="padding:0;">
              <div class="col-sm-12" style="padding:0;">
                <label>Anggota  (ketik untuk mencari)</label><a href="#" onclick="duplinks()" class="pull-right">Tambah Anggota</a>
              </div>
              <div class="links">
                <select name="anggota[]" class="form-control ajaxDosen" id="anggota" style="width:100%;">
                  <option value="">--Pilih Anggota--</option>
                </select>
                <select name="bidang[]" class="form-control ajaxBidang" id="bidang" style="width:100%;">
                  <option value="">--Pilih Bidang Keahlian Anggota--</option>
                </select>
              </div>
          </div>

          <div class="form-group">
            <label>Mahasiswa terlibat (1 Mahasiswa per baris)</label>
            <textarea name="mahasiswa" id="mahasiswa" rows="5" class="form-control"></textarea>
          </div>

          <div class="form-group">
              <label>File Proposal</label>
              <input type="file" name="proposal" id="proposal" class="form-control" placeholder="Proposal"/>
          </div>

          <div class="form-group">
            <label>Jenis PKM  (ketik untuk mencari)</label>
            <select name="jenis" class="form-control ajaxJenis" style="width:100%;" id="jenis" required>
              <option value="">--Pilih Jenis PKM--</option>
            </select>
          </div>
        </div> 
        <div class="col-md-6">
          <div class="form-group">
            <label>Usulan Biaya</label>
            <input type="text" class="form-control ftpl text-right" id="biaya" required="true" name="biaya" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Sumber Biaya</label>
            <input type="text" class="form-control" id="sbBiaya" placeholder="Sumber Biaya" required="true" name="sbBiaya">
          </div>
          <div class="form-group">
            <label>Luaran yang dihasilkan</label>
            <input type="text" class="form-control" id="output" placeholder="Luaran yang dihasilkan" required="true" name="output">
          </div>
          <div class="form-group">
            <label>Kota Lokasi Kegiatan (ketik untuk mencari)</label>
            <select name="kota" class="form-control ajaxKota" style="width:100%;" id="kota" required>
              <option value="">--Pilih Lokasi Kegiatan--</option>
            </select>
          </div>
          <div class="form-group">
            <label>Wilayah Mitra (Desa/Kecamatan)</label>
            <input type="text" class="form-control" id="desa" name="desa" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Jarak Perguruan Tinggi ke Lokasi (dalam km)</label>
            <input type="text" class="form-control ftpl text-right" id="jarak" name="jarak" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Jangka Waktu Pelaksanaan (dalam bulan)</label>
            <input type="text" class="form-control ftpl text-right" id="waktu" name="waktu" autocomplete="off">
          </div>
        </div> 
        <div class="col-xs-6 col-xs-offset-3">
          <a href="#" onClick="batal()" class="btn btn-default pull-left">Batal</a>
          <input type="hidden" name="isedit" id="isedit" value="0"/>
          <input type="submit" value="Submit" class="btn btn-primary pull-right"/>
        </div>
      </div>
        
    </div>
   </div>
  </div>
 </form>
</div>
  