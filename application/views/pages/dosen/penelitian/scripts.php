<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/jquery.backstretch.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/retina-1.1.0.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/scripts.js')?>"></script>
<script>
  
  var table;
  var kategori="penelitian";
  var status=0;
  $(function () {
    table=$('#tblPenelitian').DataTable({
      "processing": true,
      "serverSide":true,
      "ajax": {
          "url": "<?php echo site_url('dosen/penelitian/data/penelitian'); ?>",
          "type": "POST"
      },
      "columnDefs": [ {
          "targets": -2,
          "render":function ( data, type, full, meta ) {
                return '<a href="#"'+data+'</a>';
            }
          },
          {
            "targets": -1,
            "defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
            "render":function ( data, type, full, meta ) {
              var pesan='Hapus';
              //if(data==1) pesan='Nonaktifkan';
              return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkedit">Edit</a></li><li><a href="#" class="linkdelete">'+pesan+'</a></li></ul></div>';
            }

          },
          {
            "targets": [1],
            "visible": false,
            "searchable": false
          }
          
            ],
      "language": {
          "lengthMenu": "Menampilkan _MENU_ data per halaman",
          "zeroRecords": "Data masih kosong atau tidak ditemukan",
          "info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
          "infoEmpty": "Data masih kosong"
      }
    })

    $('#tblPenelitian tbody').on( 'click', '.linkedit', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        if(data[9]==4)
          errmsg('Tidak dapat mengubah data karena penelitian telah disetujui. Silahkan hubungi administrator.');
        else{
          edit(data[1]);
          status=data[9];
          $('#judul').focus();
        }
    });
    $('#tblPenelitian tbody').on( 'click', '.linkdelete', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        if(data[9]==4)
          errmsg('Tidak dapat menghapus data karena penelitian telah disetujui. Silahkan hubungi administrator.');
        else
          hapus(data[1],data[2]);
    });
  })

  //$('#modal-bayar').modal({backdrop: 'static', keyboard: false,show:false}) ;
  $('.modal').modal({backdrop: 'static', keyboard: false,show:false}) ;

  $('#formPenelitian').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#formPenelitian')[0]);
    form.append('kategori',kategori);
    form.append('status',status);
    $.ajax({
        url: "<?php echo site_url('dosen/penelitian/savePenelitian'); ?>",
        type: 'POST',
        data: form,
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
            var resp=JSON.parse(data);
            
            if(resp[0]==true){
              errmsg(resp[1]);
              
            }
            else{
              status=0;
              sucmsg(resp[1]);
              setModal();
              table.ajax.reload();
            }
        },
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                // For handling the progress of the upload
                conModal();
                myXhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      $('#progress').html( e.loaded+"/"+e.total);
                        if(e.loaded>=e.total)
                        $('#modalPenelitian').modal('hide');
                    }
                } , false);
            }
            return myXhr;
        }
    });
  });

  function hapus(id,judul){
    if(confirm('Apakah anda yakin menghapus penelitian dengan judul : '+judul+' ?')){
      $.ajax({
          url: "<?php echo site_url('dosen/penelitian/hapus'); ?>",
          type: "GET",
          data : {id: id},
          success: function (data) {
            console.log(data);
            table.ajax.reload();
          }
      });
    }else
        {
            alert('Batal menghapus data.');
        }
  }
 
  $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });
  function conModal(){
    $('#forms').toggle();
    $('#tef').toggle();
  }

  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
  $('#modalPenelitian').on('shown.bs.modal', function () {
        $('#judul').focus();
        $('#forms').show();
        $('#tef').hide();
  }) 


    $('#modalPenelitian').on('hidden.bs.modal', function (e) {
      
      setModal();
      
    });


    function setModal(cont=false){
      var isi=['','','','','','0','',''];
      if(cont!=false)
        isi=cont;
      $('#judul').val(isi[1]);
      if(cont==false)
        $('#bidang').prepend('<option value="" selected>--Pilih Bidang--</option>');
      else
        $('#bidang').prepend('<option value="'+isi[2]+'" selected>'+isi[3]+'</option>');
      $('#biaya').val(isi[4]);
      //$('#jenis').val(isi[5]);
      if(cont==false)
        $('#jenis').prepend('<option value="" selected>--Pilih Jenis Penelitian--</option>');
      else
        $('#jenis').prepend('<option value="'+isi[5]+'" selected>'+isi[7]+'</option>');
      $('#isedit').val(isi[0]);
      $('#sbBiaya').val(isi[8]);
      $('#proposal').val('');
      $('.cloned').remove();
      if(isi[6]!=''){
        var anggota=isi[6].split('#');
        
        if(anggota.length>0){
            i=0;
            l=anggota[i].split(';');
            
            $('#anggota').prepend('<option value="'+l[0]+'" selected>'+l[1]+'</option>');
            for(i=1;i<anggota.length;i++){
                l=anggota[i].split(';');
                $( ".links" ).children('select').select2('destroy').end();
                var nForm=$( ".links" ).clone();
                nForm.find("select").prop('selectedIndex',parseInt(l[0]));
                nForm.find("select").prepend('<option value="'+l[0]+'" selected>'+l[1]+'</option>').end().addClass('cloned').removeClass('links').appendTo( "#linksWrap" );
                nForm.css('margin-top', '5px' );
                ajaxDosen();
            
          }  
        }
        else{
          
          $('#anggota').prepend('<option value="" selected>--Pilih Anggota--</option>');
        }
      }
      else{
        $('#anggota').prepend('<option value="" selected>--Pilih Anggota--</option>');
      }

      
    }   
		
    function edit(id){
      $.ajax({
          url: "<?php echo site_url('dosen/penelitian/reqEdit'); ?>",
          dataType: 'json', // jQuery will parse the response as JSON
          type: "POST",
          data : {id: id},
          success: function (data) {
            
            setModal(data);
            $('#modalPenelitian').modal('show');
          }
      });
    }
	
  function duplinks(){
      $( ".links" ).children('select').select2('destroy').end();
      var clone=$( ".links" ).clone().addClass('cloned').removeClass('links').appendTo( "#linksWrap" );
      clone.css('margin-top', '5px' );
      ajaxDosen();
  }

  $(document).ready(function() {
		$(".ajaxBidang").select2({
        ajax: {
          url: "<?php echo site_url('admin/bidang/ajaxBidang'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });
      
      $(".ajaxJenis").select2({
        ajax: {
          url: "<?php echo site_url('admin/jenis/ajaxJenis'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });

      


      ajaxDosen();
      
        
	} );

  function ajaxDosen(){
    $(".ajaxDosen").select2({
        ajax: {
          url: "<?php echo site_url('admin/dosen/ajaxDosen'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text };
              })
            };
          },
          cache: true
        },
        "language": "id"
      });
  }

  function tambah(){
    $('#modalPenelitian').modal('show');
  }

  var ftpl=$(".ftpl");ftpl.on("keyup",function(i){if(""===window.getSelection().toString()&&-1===$.inArray(i.keyCode,[38,40,37,39])){var n=$(this),a=(a=n.val()).replace(/[\D\s\._\-]+/g,"");a=a?parseInt(a,10):0,n.val(function(){return 0===a?"":a.toLocaleString("id-ID")})}});
</script>