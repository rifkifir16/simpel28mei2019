<section class="content">
<?php 
    include 'penelitian/modal.php';
  ?>
  <div id="msgAlert" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
    <p id="msgContent"></p>
  </div>
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Penelitian</h3>
      <div class="pull-right">
      <a href="#" onclick="tambah('penelitian')" class="btn btn-primary">Tambah Penelitian</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tblPenelitian" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>idp</th>
              <th>Tanggal</th>
              <th>Judul Penelitian</th>
              <th>Jenis Penelitian</th>
              <th>Ketua</th>
              <th>Anggota </th>
              <th>Proposal</th>
              <th>Status</th>
              <th>Pilihan</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
    </div>
    <!-- /.box-body -->
  </div>
  
</section>