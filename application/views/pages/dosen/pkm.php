<section class="content">
<?php 
    //include 'pkm/modal.php';
  ?>
  <div id="msgAlert" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
    <p id="msgContent"></p>
  </div>
  <div class="box" id="tableView">
    <div class="box-header">
      <h3 class="box-title">PKM</h3>
      <div class="pull-right">
      <a href="#" onclick="tambah()" class="btn btn-success">Tambah PKM</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tblPenelitian" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>idp</th>
              <th>Tanggal</th>
              <th>Judul PKM</th>
              <th>Jenis PKM</th>
              <th>Ketua</th>
              <th>Anggota </th>
              <th>Proposal</th>
              <th>Status</th>
              <th>Pilihan</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
    </div>
    <!-- /.box-body -->
  </div>
  <?php 
    include 'pkm/form.php';
  ?>
</section>