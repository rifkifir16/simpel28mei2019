
	<div class="form-group">
		<label>Total Biaya (Belum termasuk biaya harian)</label>
		<input class="form-control totalbiaya text-right" type="text" name="tbiaya" value="<?=number_format($result['TOTAL_BIAYA_SPPD'],0,'','.')?>" readonly="true" />
	</div>
    <div class="form-group">
		<label>Status SPPD</label>
		<input class="form-control" type="text" name="status" placeholder="Status SPPD" value="<?=$result['STATUS_SPPD']?>" />
	</div>
	<div class="form-group">
		<label>Biaya dari</label>
		<input class="form-control" type="text" name="bdari" placeholder="Biaya dari" value="<?=$result['BIAYA_OLEH_SPPD']?>"/>
	</div>
	<div class="form-group">
		<label>Pemberi Kerja</label>
		<select class="form-control ajaxlokasi" name="pemk" style="width: 100%;" required>
			<option value="<?=$result['PEMK_SPPD']?>"><?=$result['PEMK_LOKASI']?></option>
		</select>
	</div>
	<div class="form-group">
		<label>Keterangan SPPD</label>
		<textarea class="form-control" name="keterangan"><?=$result['KET_SPPD']?></textarea>
	</div>
	<div class="form-group">
		<label>Otorisasi SPPD</label>
		<select class="form-control" name="osppd" style="width: 100%;" required>
			<option value="">--Pilih Otorisasi--</option>
            <option value="1" <?php if($result['OTORISASI_SPPD']=='1') echo 'selected';?>>Samsudin Hidayat</option>
			<option value="2" <?php if($result['OTORISASI_SPPD']=='2') echo 'selected';?>>Kholidun</option>
            <option value="3"> <?php if($result['OTORISASI_SPPD']=='3') echo 'selected';?>x</option>
		</select>
	</div>
    <div class="form-group text-center">
        <input type="hidden" name="id" value="<?=$_id?>"/>
		<input class="btn btn-primary" id="btnSubmit" type="submit" name="submit" value="submit"/>
	</div>
