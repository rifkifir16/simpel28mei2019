
	<div class="form-group">
		<label>Jumlah Unit Pesawat (Kosongkan jika tidak ada)</label>
		<input class="form-control upes text-right ftpl" type="text" name="upesawat" onkeyup="calTot()" value="<?=number_format($result['UNIT_PESAWAT_SPPD'],0,'','.')?>" placeholder="Jumlah Unit Pesawat" />
	</div>
    <div class="form-group">
		<label>Biaya Satuan Pesawat (Kosongkan jika tidak ada)</label>
		<input class="form-control bpes text-right ftpl" type="text" name="bpesawat" onkeyup="calTot()" value="<?=number_format($result['BIAYA_PESAWAT_SPPD'],0,'','.')?>" placeholder="Biaya Satuan Pesawat" />
	</div>
	<div class="form-group">
		<label>Jumlah Unit Bus (Kosongkan jika tidak ada)</label>
		<input class="form-control ubus text-right ftpl" type="text" name="ubus" onkeyup="calTot()" value="<?=number_format($result['UNIT_BUS_SPPD'],0,'','.')?>" placeholder="Jumlah Unit Bus"/>
	</div>
	<div class="form-group">
		<label>Biaya Satuan Bus (Kosongkan jika tidak ada)</label>
		<input class="form-control bbus text-right ftpl" type="text" name="bbus" onkeyup="calTot()" value="<?=number_format($result['BIAYA_BUS_SPPD'],0,'','.')?>" placeholder="Biaya Satuan Bus"/>
	</div>
	<div class="form-group">
		<label>Jumlah Unit Transportasi Lokal (Kosongkan jika tidak ada)</label>
		<input class="form-control ulok text-right ftpl" type="text" name="ulokal" onkeyup="calTot()" value="<?=number_format($result['UNIT_TLOKAL_SPPD'],0,'','.')?>" placeholder="Jumlah Unit Transportasi Lokal"/>
	</div>
	<div class="form-group">
		<label>Biaya Satuan Transportasi Lokal (Kosongkan jika tidak ada)</label>
		<input class="form-control blok text-right ftpl" type="text" name="blokal" onkeyup="calTot()" value="<?=number_format($result['BIAYA_TLOKAL_SPPD'],0,'','.')?>" placeholder="Biaya Satuan Transportasi Lokal"/>
	</div>
	<div class="form-group">
		<label>Airport Tax (Kosongkan jika tidak ada)</label>
		<input class="form-control tax text-right ftpl" type="text" name="airtax" onkeyup="calTot()" value="<?=number_format($result['TAX_BANDARA_SPPD'],0,'','.')?>" placeholder="Airport Tax"/>
	</div>
	<div class="form-group">
		<label>Biaya Harian Perjalanan Dinas (Kosongkan jika tidak ada)</label>
		<input class="form-control biaya text-right ftpl" type="text" name="tbharian" value="<?=number_format($result['BIAYA_HARIAN_SPPD'],0,'','.')?>" placeholder="Biaya Harian Perjalanan Dinas"/>
	</div>