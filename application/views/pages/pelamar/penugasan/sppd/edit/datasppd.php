    
    <div id="formpegawai">
        <div class="form-group pegawaigrup">
    		<label>Nama Pegawai</label>
    		<select class="form-control ajaxpegawai" name="pegawai" style="width: 100%;" required>
    			<option value="<?=$result['NIP_PEG']?>"><?=$result['NAMA_PEG']?></option>
    		</select>
    	</div>
    </div>
    
	<div class="form-group">
		<label>Tanggal Nota Dinas</label>
		<input class="form-control" type="date" name="tglnota" value="<?=$result['TGL_NOTA_SPPD']?>" required="true" />
	</div>
	<div class="form-group">
		<label>Nomor Nota Dinas</label>
		<input class="form-control" type="text" name="nosp" value="<?=$result['NO_SP_SPPD']?>" required="true" />
	</div>
    <div class="form-group">
		<label>Nomor JMK</label>
		<input class="form-control" type="text" name="nojmk" placeholder="Nomor JMK" value="<?=$result['NO_JMK_SPPD']?>" required="true" />
	</div>
	<div class="form-group">
		<label>Tanggal JMK</label>
		<input class="form-control" type="date" name="tgljmk" value="<?=$result['TGL_JMK_SPPD']?>" required="true" />
	</div>
    <div class="form-group">
		<label>Jenis SPPD</label>
		<select class="form-control" name="jsppd" style="width: 100%;" required>
			<option value="">--Pilih SPPD--</option>
            <option value="khusus" <?php if($result['JENIS_SPPD']=='khusus') echo 'selected';?>>Penugasan Khusus</option>
			<option value="mobilisasi" <?php if($result['JENIS_SPPD']=='mobilisasi') echo 'selected';?>>Mobilisasi</option>
            <option value="cuti" <?php if($result['JENIS_SPPD']=='cuit') echo 'selected';?>>Perjalanan cuti</option>
		</select>
	</div>
    <div class="form-group">
		<label>Tanggal SPPD</label>
		<input class="form-control" type="date" name="tglsppd" value="<?=$result['TGL_SPPD']?>" required="true" />
	</div>
	<div class="form-group">
		<label>Uraian</label>
		<textarea class="form-control" name="uraian"><?=$result['URAIAN_SPPD']?></textarea>
	</div>
    <div class="form-group">
		<label>Lokasi asal</label>
		<select class="form-control ajaxlokasi" name="asal" style="width: 100%;" required>
			<option value="<?=$result['ASAL_SPPD']?>"><?=$result['NAMA_LOKASI']?></option>
		</select>
	</div>
    <div class="form-group">
		<label>Lokasi tujuan</label>
		<select class="form-control ajaxlokasi" name="tujuan" style="width: 100%;" required>
			<option value="<?=$result['TUJUAN_SPPD']?>"><?=$result['TUJUAN_LOKASI']?></option>
		</select>
	</div>
	<div class="form-group">
		<label>Transportasi</label>
		<select class="form-control" name="transportasi" required>
			<option value="">--Pilih Transportasi--</option>
            <option value="dinas" <?php if($result['TRANSPORT_SPPD']=='dinas') echo 'selected';?>>Kendaraan Dinas</option>
			<option value="umum" <?php if($result['TRANSPORT_SPPD']=='umum') echo 'selected';?>>Kendaraan Umum</option>
		</select>
	</div>
	<div class="form-group">
		<label>Tanggal Berangkat</label>
		<input class="form-control" type="date" name="tglberangkat" value="<?=$result['TGL_BRKT_SPPD']?>" required="true" />
	</div>
    <div class="form-group">
		<label>Tanggal Kembali</label>
		<input class="form-control" type="date" name="tglkembali" value="<?=$result['TGL_KMBL_SPPD']?>" required="true" />
	</div>
	
    