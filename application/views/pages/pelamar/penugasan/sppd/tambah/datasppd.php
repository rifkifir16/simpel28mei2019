	<div class="form-group">
		<label>Tanggal Nota Dinas</label>
		<input class="form-control" type="date" name="tglnota" value="<?=date('Y-m-d');?>" required="true" />
	</div>
	<div class="form-group">
		<label>Nomor Nota Dinas</label>
		<input class="form-control" type="text" name="nosp" placeholder="Nomor surat perjanjian" required="true" />
	</div>
    <div class="form-group">
		<label>Nomor JMK</label>
		<input class="form-control" type="text" name="nojmk" placeholder="Nomor JMK" required="true" />
	</div>
	<div class="form-group">
		<label>Tanggal JMK</label>
		<input class="form-control" type="date" name="tgljmk" value="<?=date('Y-m-d');?>" required="true" />
	</div>
    <div class="form-group">
		<label>Jenis SPPD</label>
		<select class="form-control" name="jsppd" style="width: 100%;" required>
			<option value="">--Pilih SPPD--</option>
            <option value="khusus">Penugasan Khusus</option>
			<option value="mobilisasi">Mobilisasi</option>
            <option value="cuti">Perjalanan cuti</option>
		</select>
	</div>
    <div class="form-group">
		<label>Tanggal SPPD</label>
		<input class="form-control" type="date" name="tglsppd" value="<?=date('Y-m-d');?>" required="true" />
	</div>
	<div class="form-group">
		<label>Uraian</label>
		<textarea class="form-control" name="uraian"></textarea>
	</div>
    <div class="form-group">
		<label>Lokasi asal</label>
		<select class="form-control ajaxlokasi" name="asal" style="width: 100%;" required>
			<option value="">--Pilih Lokasi--</option>
		</select>
	</div>
    <div class="form-group">
		<label>Lokasi tujuan</label>
		<select class="form-control ajaxlokasi" name="tujuan" style="width: 100%;" required>
			<option value="">--Pilih Lokasi--</option>
		</select>
	</div>
	<div class="form-group">
		<label>Transportasi</label>
		<select class="form-control" name="transportasi" required>
			<option value="">--Pilih Transportasi--</option>
            <option value="dinas">Kendaraan Dinas</option>
			<option value="umum">Kendaraan Umum</option>
		</select>
	</div>
	<div class="form-group">
		<label>Tanggal Berangkat</label>
		<input class="form-control" type="date" name="tglberangkat" value="<?=date('Y-m-d');?>" required="true" />
	</div>
    <div class="form-group">
		<label>Tanggal Kembali</label>
		<input class="form-control" type="date" name="tglkembali" value="<?=date('Y-m-d');?>" required="true" />
	</div>
	
    