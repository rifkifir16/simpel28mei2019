<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script>
	$(document).ready(function() {
		
		$(".ajaxlokasi").select2({
          ajax: {
            url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text};
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
        ajaxpegawai();
	} );
	

    $('#fillForm').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#fillForm')[0]);
    
    $.ajax({
        url: "<?php echo site_url('admin/penugasan/sppd/save'); ?>",
        type: 'POST',
        data: form,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
          if(data.result==true){
            alert(data.msg);
            window.location.href = "<?=site_url('admin/penugasan/sppd')?>";
          }
        }
    });
  });
	
	function duppegawai(){
	    $( ".pegawaigrup" ).children('select').select2('destroy').end();
        $( ".pegawaigrup" ).clone().addClass('cloned').removeClass('pegawaigrup').appendTo( "#formpegawai" );
        ajaxpegawai();
    }
	
	function rempegawai(){
		$( ".pegawaigrup" ).children('select').select2('destroy').end();
        $( ".cloned" ).last().remove();
        ajaxpegawai();
	}
	
	function duprincian(){
        $( ".rinciangrup" ).clone().find("input").val("").end().removeClass('rinciangrup').appendTo( "#formrincian" );
    }
    
    function ajaxpegawai(){
        $(".ajaxpegawai").select2({
          ajax: {
            url: "<?php echo site_url('admin/user/pegawai/ajax'); ?>",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term
              };
            },
            processResults: function (data) {
              return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text};
                })
              };
            },
            cache: true
          },
          "language": "id"
        });
    }
    
    function calTot()
    {
		var upes=$('.upes').val();
		var bpes=$('.bpes').val();
		var ubus=$('.ubus').val();
		var bbus=$('.bbus').val();
		var ulok=$('.ulok').val();
		var blok=$('.blok').val();
		var tax=$('.tax').val();
		upes = upes.replace(/[\D\s\._\-]+/g, "");
		bpes = bpes.replace(/[\D\s\._\-]+/g, "");
		ubus = ubus.replace(/[\D\s\._\-]+/g, "");
		bbus = bbus.replace(/[\D\s\._\-]+/g, "");
		ulok = ulok.replace(/[\D\s\._\-]+/g, "");
		blok = blok.replace(/[\D\s\._\-]+/g, "");
		tax = tax.replace(/[\D\s\._\-]+/g, "");
		
		
        var pes=upes*bpes;
        var bus=ubus*bbus;
        var lok=ulok*blok;
        var biaya=tax*1;
		var tot=pes+bus+lok+biaya;
        
		tot = tot ? parseInt( tot, 10 ) : 0;
		 
		// 4
		$('.totalbiaya').val( function() {
			return ( tot === 0 ) ? "" : tot.toLocaleString( "id-ID" );
		} );
    }
	
	
	var ftpl=$(".ftpl");ftpl.on("keyup",function(i){if(""===window.getSelection().toString()&&-1===$.inArray(i.keyCode,[38,40,37,39])){var n=$(this),a=(a=n.val()).replace(/[\D\s\._\-]+/g,"");a=a?parseInt(a,10):0,n.val(function(){return 0===a?"":a.toLocaleString("id-ID")})}});
</script>