<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>
<script>
var table;
$(document).ready(function() {
  $(".ajaxProp").select2({
    ajax: {
      url: "<?php echo site_url('admin/propinsi/ajaxProp'); ?>",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term
        };
      },
      processResults: function (data) {
        return {
          results: $.map(data, function(obj) {
              return { id: obj.id, text: obj.text };
          })
        };
      },
      cache: true
    },
    "language": "id"
  });
});

  $(function () {
    table=$('#tblKota').DataTable({
    "processing": true,
    "serverSide":true,  
    "ajax": {
          "url": "<?php echo site_url('admin/kota/data'); ?>",
          "type": "POST"
      },
		"columnDefs": [ {
			"targets": -2,
			"render":function ( data, type, full, meta ) {
				if(data==null) 
				  return '<a href="#">Terdapat kesalahan pada data ini</a>';
				else if(data==0)
				  return '<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i> Tidak Aktif</a>';
				else 
				  return '<a href="#" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Aktif</a>';
			  },
			  "orderable": false
			  },
		  {
			"targets": -1,
			"defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
			"render":function ( data, type, full, meta ) {
				if(data==null) return '<a href="#">Terdapat kesalahan pada data ini</a>';
				var pesan='Aktifkan';
				if(data==1) pesan='Nonaktifkan';
				return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkedit">Edit</a></li><li><a href="#" class="linkdelete">'+pesan+'</a></li></ul></div>';
			  },
			"orderable": false

			},
			{
			  "targets": [1,2],
			  "visible": false,
			  "searchable": false
			},
			{
			"targets": 0,
			"orderable": false
			}],
		"language": {
			"lengthMenu": "Menampilkan _MENU_ data per halaman",
			"zeroRecords": "Data masih kosong atau tidak ditemukan",
			"info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
			"infoEmpty": "Data masih kosong"
		}
    })
	
    $('#tblKota tbody').on( 'click', '.linkedit', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        $('#idKota').val(data[1]);
        $('#namaKota').val(data[3]);
        
        $('#prop').html("<option value='"+data[2]+"' selected ='selected'>" + data[4]+ " </option>");
        $('#namaKota').focus();
    });
    $('#tblKota tbody').on( 'click', '.linkdelete', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        upStatus(data[1],data[5])
    });
  })

  function reset(){
    $('#idKota').val(0);
    $('#namaKota').val('');
    $('#prop').html("<option value=''> --Ketik untuk mencari--</option>");
    $('#namaKota').focus();
  }

  function changeData(index=0){
    table.ajax.url('<?=base_url('admin/kota/data/'); ?>'+index);
    table.ajax.reload();
  }

  $('#formKota').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#formKota')[0]);
    
    $.ajax({
        url: "<?php echo site_url('admin/kota/saveKota'); ?>",
        type: 'POST',
        data: form,
        cache: false,
        dataType: 'json',
        contentType: false,
        processData: false,
        success : function(data) {
            if(data.result==false){
              errmsg(data.msg);
            }
            else{
              sucmsg(data.msg);
              reset();
              table.ajax.reload();
            }
        }
    });
  });

  function upStatus(id,status){
      
    $.ajax({
      url: "<?php echo site_url('admin/kota/upstatus'); ?>",
      type: "GET",
      dataType: 'json',
      data : {id: id,status:status},
      success: function (data){
        if(data.result==false){
          errmsg(data.msg);
        }
        else{
          sucmsg(data.msg);
          reset();
          table.ajax.reload(null,false);
        }
      }
        
    });
  }
  
  $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });

  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
</script>