<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>
<script>
var table;
  
  $('.modal').modal({backdrop: 'static', keyboard: false,show:false}) ;
  $('.select2').select2();

  $('#formBidang').submit(function(e) {
    sucmsg('Memproses permintaan');
  });
  
    $('#msgAlert').hide();
    $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });

  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
</script>