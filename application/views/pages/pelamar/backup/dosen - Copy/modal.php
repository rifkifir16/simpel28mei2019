<!--modal bayar hutang-->
<div class="modal fade" id="modalDosen">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <form role="form" action="" id="formDosen" method="post" class="f1">
          
            <div class="f1-steps text-center">
                <div class="f1-progress">
                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                </div>
                <div class="f1-step active col-xs-4" id="mainstep">
                <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                <p>Data Dosen</p>
                </div>
                <div class="f1-step col-xs-4">
                <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                <p>Data Pribadi</p>
                </div>
                <div class="f1-step col-xs-4">
                <div class="f1-step-icon"><i class="fa fa-graduation-cap"></i></div>
                <p>Jabatan</p>
                </div>
            </div>

            <fieldset id="firstfieldset">
                <h4>Data Dosen :</h4>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2">NIP</label>
                    <div class="col-sm-10">
                    <input type="text" name="nip" id="nip" placeholder="NIP" class="form-control" required="true">
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2">NIDN</label>
                    <div class="col-sm-10">
                    <input type="text" name="nidn" id="nidn" placeholder="NIDN" class="form-control">
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2">Nama Dosen</label>
                    <div class="col-sm-10">
                    <input type="text" name="nama" id="nama" placeholder="Nama Dosen" class="form-control">
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2">Alamat Kantor</label>
                    <div class="col-sm-10">
                    <textarea name="alamatKantor" id="alamatKantor" placeholder="Alamat Kantor" class="form-control"></textarea>
                    </div>
                </div>
                <div class="f1-buttons col-sm-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-next bg-blue">Next</button>
                </div>
            </fieldset>

            <fieldset>
                <h4>Data Pribadi :</h4>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2">Email</label>
                    <div class="col-sm-10">
                    <input type="email" name="email" id="email" placeholder="Alamat email" class="form-control">
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2">No Telepon/HP</label>
                    <div class="col-sm-10">
                    <input type="text" name="telp" id="telp" placeholder="No Telepon/HP" class="form-control" >
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2">Alamat Rumah</label>
                    <div class="col-sm-10">
                    <textarea name="alamatRumah" id="alamatRumah" placeholder="Alamat Rumah" class="form-control"></textarea>
                    </div>
                </div>
                <div class="f1-buttons col-sm-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-previous bg-orange" id="prevbtn">Previous</button>
                    <button type="button" class="btn btn-next bg-blue">Next</button>
                </div>
            </fieldset>

            <fieldset>
                <h4>Data Jabatan :</h4>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2" for="f1-facebook">Pangkat</label>
                    <div class="col-sm-10">
                    <select name="pangkat" id="pangkat" class="form-control" required>
                        <option value="">--Pilih Pangkat--</option>
                        <?php
                        foreach($_pangkat as $pangkat){
                            echo '<option value="'.$pangkat['ID_PANGKAT'].'">'.$pangkat['PANGKAT'].' - '.$pangkat['NAMA_GOLONGAN'].'</option>';
                        }
                        ?>
                    </select>
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2" for="f1-twitter">Jabatan</label>
                    <div class="col-sm-10">
                    <select name="jabatan" id="jabatan" class="form-control">
                        <option value="">--Pilih Jabatan--</option>
                        <?php
                        foreach($_jabatan as $jabatan){
                            echo '<option value="'.$jabatan['ID_JABATAN'].'">'.$jabatan['NAMA_JABATAN'].'</option>';
                        }
                        ?>
                    </select>
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2" for="f1-about-yourself">Jurusan</label>
                    <div class="col-sm-10">
                    <select name="jurusan" id="jurusan" class="form-control">
                        <option value="">--Pilih Jurusan--</option>
                        <?php
                        foreach($_jurusan as $jurusan){
                            echo '<option value="'.$jurusan['ID_JURUSAN'].'">'.$jurusan['NAMA_JURUSAN'].'</option>';
                        }
                        ?>
                    </select>
                    </div>
                </div>
                <div class="form-group col-sm-12" style="padding:0;">
                    <label class="col-sm-2" for="f1-about-yourself">Hak Akses</label>
                    <div class="col-sm-10">
                    <select name="otorisasi" id="otorisasi" class="form-control">
                        <option value="">--Pilih Otorisasi--</option>
                        <option value="1">User</option>
                        <option value="2">Admin</option>
                    </select>
                    </div>
                </div>
                <div class="f1-buttons col-sm-12">
                    <input type="hidden" id="isedit" name="isedit" value="0"/>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-previous bg-orange">Previous</button>
                    <button type="submit" class="btn btn-submit bg-blue">Submit</button>
                </div>
            </fieldset>

          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>