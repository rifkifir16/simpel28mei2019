<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script>
  
  var table;
  var kategori="";
  $(function () {
    table=$('#tblPenelitian').DataTable({
      "processing": true,
      "serverSide":true,
      
      "ajax": {
            "url": "<?php echo site_url('admin/pkm/dataPenelitian'); ?>",
            "type": "POST"
        },
      "columnDefs": [ {
          "targets": -2,
          "render":function ( data, type, full, meta ) {
                return '<a href="#"'+data+'</a>';
            }
            },
        {
          "targets": -1,
          "defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
          "render":function ( data, type, full, meta ) {
              return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="revisi">Perlu Revisi</a></li><li><a href="#" class="setujui">Setujui</a></li></ul></div>';
            }

          },
          {
            "targets": [1],
            "visible": false,
            "searchable": false
          }
          
            ],
      "language": {
          "lengthMenu": "Menampilkan _MENU_ data per halaman",
          "zeroRecords": "Data masih kosong atau tidak ditemukan",
          "info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
          "infoEmpty": "Data masih kosong"
      }
    })

    $('#tblPenelitian tbody').on( 'click', '.revisi', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        upstatus(data[1],2);
        
    });

    $('#tblPenelitian tbody').on( 'click', '.setujui', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        upstatus(data[1],4);
        
    });
  })
 
  $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });
  
  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
  
  function upstatus(id,status){
    $.ajax({
      url: "<?php echo site_url('admin/pkm/upstatus'); ?>",
      type: "GET",
      data : {id: id,status:status},
      success: function (data){
        var resp=JSON.parse(data);
        console.log(data);
        
        if(resp[0]==true){
          errmsg(resp[1]);
          
        }
        else{
          sucmsg(resp[1]);
          table.ajax.reload();
        }
      }
        
    });
  }



    

</script>