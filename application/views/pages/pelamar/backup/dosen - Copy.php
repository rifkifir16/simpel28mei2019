<section class="content">
<?php 
    include 'dosen/modal.php';
  ?>
  <div id="msgAlert" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
    <p id="msgContent"></p>
  </div>
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Dosen</h3>
      <div class="pull-right">
      <a href="#" data-toggle="modal" data-target="#modalDosen" class="btn btn-primary">Coba</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tblDosen" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>idu</th>
              <th>NIP</th>
              <th>Nama Dosen</th>
              <th>Status</th>
              <th>Pilihan</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
    </div>
    <!-- /.box-body -->
  </div>
  
</section>