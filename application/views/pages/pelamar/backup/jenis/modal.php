<!--modal bayar hutang-->
<div class="modal fade" id="modal-bayar">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Form Bayar</h4>
        </div>
        <div class="modal-body">
          <form method="post">
            <div class="form-group has-feedback" id="jum">
              <input type="number" class="form-control" placeholder="Jumlah ">
              <span class="glyphicon glyphicon-money form-control-feedback"></span>
            </div>
            
              
                <div class="checkbox icheck">
                  <label>
                    <input type="checkbox"> Lunas <small>(pilih jika anda tidak mencicil).</small>
                  </label>
                </div>
            
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary">Bayar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>