<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/jquery.backstretch.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/retina-1.1.0.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/scripts.js')?>"></script>

<script>
  

  $('#setting').submit(function(e) {
    e.preventDefault();
    
    var form = new FormData($('#setting')[0]);
  
    $.ajax({
        url: "<?php echo site_url('admin/setting/update'); ?>",
        type: 'POST',
        data: form,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
          
          var resp=data;
          if(resp[0]==true){
            errmsg(resp[1]);
          }
          else{
            sucmsg(resp[1]);
            
          }
        }
    });

    
      
  });

  $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });
  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
  
</script>