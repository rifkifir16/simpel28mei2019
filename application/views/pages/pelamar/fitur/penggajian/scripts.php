<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>
<script>
var table;
$("#boxform").hide();
  $(function () {

    $(document).ready(function() {
      $(".ajaxlokasi").select2({
        ajax: {
          url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data) {
            return {
              results: $.map(data, function(obj) {
                  return { id: obj.id, text: obj.text};
              })
            };
          },
          cache: true
        },
        "language": "id"
      });

    });

    table=$('#tblUtama').DataTable({
      "processing": true,
      "serverSide":true,  
      "ajax": {
            "url": "<?=$_ajaxSource?>",
            "type": "POST"
        },
      "columnDefs": [ {
          "targets": -2,
          "render":function ( data, type, full, meta ) {
              if(data==null) 
                return '<a href="#">Terdapat kesalahan pada data ini</a>';
              else if(data==0)
                return '<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i> Pending</a>';
              else 
                return '<a href="#" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Disetujui</a>';
            }
            },
        {
          "targets": -1,
          "defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
          "render":function ( data, type, full, meta ) {
              if(data==null) return '<a href="#">Terdapat kesalahan pada data ini</a>';
              var pesan='Approve';
              if(data==1) pesan='Nonapprove';
              return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkedit">Edit</a></li><li><a href="#" class="linkdelete">'+pesan+'</a></li></ul></div>';
            }

          },
          {
            "targets": [1,2,3,4,9,10],
            "visible": false,
            "searchable": false
          }
          
            ],
      "language": {
          "lengthMenu": "Menampilkan _MENU_ data per halaman",
          "zeroRecords": "Data masih kosong atau tidak ditemukan",
          "info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
          "infoEmpty": "Data masih kosong"
      }
    })

    $('#tblUtama tbody').on( 'click', '.linkedit', function (e) {
            var data = table.row( $(this).parents('tr') ).data();
            $('#id').val(data[1]);
            $('#nip').val(data[4]);
            $('#lokasi').html("<option value='"+data[2]+"' selected ='selected'>" + data[3]+ " </option>");
            $('#hk').val(data[9]);
            $('#kh').val(data[10]);
            $('#koreksi').val(data[11].replace(",",""));
            $('#index').val(data[8]);
            $('#pph').val(data[12].replace(",",""));
            $('#jamsostek').val(data[13].replace(",",""));
            $("#boxdata").attr('class', 'col-md-9');
            $("#boxform").show();
        });
    $('#tblUtama tbody').on( 'click', '.linkdelete', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        alert("Status on going");
        //upStatus(data[1],data[4])
    });
  })

  function changeData(index=0){
    table.ajax.url('<?=$_ajaxSource.'/'?>'+index);
    table.ajax.reload();
  }

  function reset(){
    $('#id').val(0);
    $('#nip').val(0);
    $('#lokasi').html("<option value=''> --Ketik untuk mencari--</option>");
    $('#hk').val(0);
    $('#kh').val(0);
    $('#koreksi').val(0);
    $('#index').val(0);
    $('#pph').val(0);
    $('#jamsostek').val(0);
    $("#boxdata").attr('class', 'col-md-12');
    $("#boxform").hide();
  }

  $('#fillForm').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#fillForm')[0]);
    
    $.ajax({
        url: "<?php echo site_url('admin/fitur/penggajian/save'); ?>",
        type: 'POST',
        data: form,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
          if(data.result==false){
            errmsg(data.msg);
          }
          else{
            sucmsg(data.msg);
            reset();
            table.ajax.reload(null,false);
          }
        }
    });
  });

  function upStatus(id,status){
      
    $.ajax({
      url: "<?php echo site_url('admin/fitur/penggajian/upstatus'); ?>",
      type: "GET",
      dataType: 'json',
      data : {id: id,status:status},
      success: function (data){
        if(data.result==false){
          errmsg(data.msg);
        }
        else{
          sucmsg(data.msg);
          reset();
          table.ajax.reload(null,false);
        }
        
      }
        
    });
  }
  
   $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });

  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
</script>