<section class="content">
<div class="row">
  <div class="col-md-3" id="boxform">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Update Data</h3>
        <div class="pull-right">
          <a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Batal</a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="fillForm">
        <div class="form-group">
            <label>Lokasi</label>
            <select class="form-control ajaxlokasi" id="lokasi" name="lokasi" style="width: 100%;" required>
              <option value="">--Pilih Lokasi--</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="hk">Jumlah Hari Kerja</label>
            <input type="hidden" name="id" id="id"  class="form-control" value="0" />
            <input type="hidden" name="nip" id="nip"  class="form-control" value="0" />
            <input id="hk" type="number" name="hk" step="0.01" class="inmdl form-control" placeholder="Jumlah Hari Kerja" value="0" required="true"/>
          </div>
            
          <div class="form-group" style="padding-bottom: 20px;">
            <label for="kh">Jumlah Kehadiran</label>
            <input id="kh" type="number" name="kh" step="0.01" class="inmdl form-control" placeholder="Jumlah Kehadiran" value="0" required="true"/>
          </div>
          
          <div class="form-group" style="padding-bottom: 20px;">
            <label for="index">Index</label>
            <input id="index" type="number" name="index" step="0.001" class="inmdl form-control" placeholder="Index" value="0" required="true"/>
          </div>
          <div class="form-group" style="padding-bottom: 20px;">
            <label for="pph">PPh 21</label>
            <input id="pph" type="number" name="pph" class="inmdl form-control" placeholder="PPh 21" value="0" required="true"/>
          </div>
          
          <div class="form-group" style="padding-bottom: 20px;">
            <label for="jamsostek">Jamsostek</label>
            <input id="jamsostek" type="number" name="jamsostek" class="inmdl form-control" placeholder="Jamsostek" value="0" required="true"/>
          </div>
          
          <div class="form-group" style="padding-bottom: 20px;">
            <label for="koreksi">Koreksi</label>
            <input id="koreksi" type="number" name="koreksi" class="inmdl form-control" placeholder="Koreksi" value="0" required="true"/>
          </div>
          <div class="form-group text-center">
            <input type="submit" value="Submit" class="btn btn-primary"/>
            <a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Batal</a>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-12" id="boxdata">
	  <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Gaji</h3>
        <div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Data</option>
                  <option value="1">Sudah di Approve</option>
                  <option value="2">Belum di Approve</option>
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUtama" class="table table-bordered table-striped">
            <thead>
              <tr>
              <th>No</th>
              <th>ID Gaji</th>
              <th>ID Lokasi</th>
              <th>Nama Lokasi</th>
              <th>NIP</th>
              <th>Nama</th>
              <th>Bulan</th>
              <th>MM</th>
              <th>Index</th>
              <th>Hari Kerja</th>
              <th>Kehadiran</th>
              <th>Koreksi</th>
              <th>PPh 21</th>
              <th>Jamsostek</th>
              <th>THP</th>
              <th>Status</th>
              <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>