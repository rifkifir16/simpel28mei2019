<section class="content">
	<div class="row">
		<div class="col-md-4">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Filter</h3>
				</div>
				<div class="box-body">
							<form method="post">
					<div class="form-group">
						<label>Lokasi</label>
						<select class="form-control ajaxlokasi" name="lokasi" style="width: 100%;" required>
							<option value="">--Ketik untuk mencari--</option>
						</select>
					</div>
					
					<div class="form-group">
						<label>Tahun</label>
						<input class="form-control" type="number" min="2016" value="0" name="tahun" required="true"/>
					</div>
					
					<div class="form-group text-center">
						<input class="btn btn-primary" id="btnSubmit" type="submit" name="submit" value="submit"/>
					</div>

				</form>
				</div>
			</div>
		</div>
	
		<div class="col-md-8">
    <div class="box">
    	<div class="box-header with-border">
    	  <h3 class="box-title">Data Serapan</h3>
		  <?php if(isset($_POST['submit'])) { ?>
			<a href="serapan.php?lokasi=<?=$val?>&tahun=<?=$tahun?>" target="_blank" class="btn btn-success pull-right" >Download Data</a>
		  <?php }?>
    	</div>
    	<div class="box-body">
			<?php if(isset($_POST['submit'])) { ?>
			<table id="tbl" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th>Bulan</th>
						<th class="text-center">Dana</th>
						<th class="text-center">Tenaga</th>
					</tr>
				</thead>
			</table>
			<?php } else { ?>
			<div class="attachment-block clearfix">
			
				<h4>Data akan ditampilkan setelah anda menentukan filter data yang akan ditampilkan.</h4>
		  </div><!-- /.attachment-block -->
			<?php }?>
    	</div>
    </div>
</div>
  </div>
    
</section>