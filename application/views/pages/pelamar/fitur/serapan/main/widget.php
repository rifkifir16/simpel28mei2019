<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="#" style="color:black;">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-checkmark"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Lunas</span>
          <span class="info-box-number">90<small>%</small></span>
          <span class="info-box-text">Detail <i class="fa fa-arrow-circle-right"></i></span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </a>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="#" style="color:black;">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-exclamation"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Sedang dicicil</span>
          <span class="info-box-number">2,000</span>
          <span class="info-box-text">Detail <i class="fa fa-arrow-circle-right"></i></span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </a>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="#" style="color:black;">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-close"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Belum dibayar</span>
          <span class="info-box-number">41,410</span>
          <span class="info-box-text">Detail <i class="fa fa-arrow-circle-right"></i></span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </a>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <!-- fix for small devices only -->


  <div class="col-md-3 col-sm-6 col-xs-12">
    <a href="#" style="color:black;" data-toggle="modal" data-target="#modal-tambah">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-plus"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Tambah</span>
          <span class="info-box-number">Hutang baru</span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </a>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

</div>
<!-- /.row -->
<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Hutang</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="#">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama teman</label>
              <select class="form-control select2" style="width: 100%;">
                <option selected="selected">Alabama</option>
                <option>Alaska</option>
                <option>California</option>
                <option>Delaware</option>
                <option>Tennessee</option>
                <option>Texas</option>
                <option>Washington</option>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-group">
              <label for="exampleInputFile">File input</label>
              <input type="file" id="exampleInputFile">

              <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox"> Check me out
              </label>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary">Simpan</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
        <!-- /.modal -->