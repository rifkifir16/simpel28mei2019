

	<div class="form-group">
		<label>NIP </label>
		<input class="form-control" type="text" name="nip" placeholder="NIP" autofocus="true" onkeyup="check(this.value)" required="true"/>
        <a id="peringatan" style="color: red;"></a>
	</div>
	<div class="form-group">
		<label>Foto</label>
		<input class="form-control" type="file" name="foto" placeholder="Foto Pegawai" />
	</div>
	<div class="form-group">
		<label>Nama Pegawai</label>
		<input class="form-control" type="text" name="nama" placeholder="Nama Pegawai" required="true" />
	</div>
	<div class="form-group">
		<label>Alamat Pegawai</label>
		<textarea class="form-control" name="alamat"></textarea>
	</div>
	<div class="form-group">
		<label>Kota Domisili</label>
		<select class="form-control ajaxkota" name="domisili" style="width: 100%;" required>
			<option value="">--Pilih Kota--</option>
		</select>
	</div>
    <div class="form-group">
		<label>Tempat Lahir</label>
		<select class="form-control ajaxkota" name="kotalahir" style="width: 100%;" required>
			<option value="">--Pilih Kota--</option>
		</select>
	</div>
	<div class="form-group">
		<label>Tanggal Lahir</label>
		<input class="form-control" type="date" name="tgllhr" required="true"/>
	</div>
	<div class="form-group">
		<label>Agama</label>
		<select class="form-control ajaxagama" name="agama" style="width: 100%;" required>
			<option value="">--Pilih Agama--</option>
		</select>
	</div>
	<div class="form-group">
		<label>Jenis Kelamin</label>
		<select class="form-control" name="jk" required>
			<option value="L">Laki-Laki</option>
			<option value="P">Perempuan</option>
		</select>
	</div>
	<div class="form-group">
		<label>Status Pernikahan</label>
		<select class="form-control" name="sn" required>
			<option>Belum Menikah</option>
			<option>Sudah Menikah</option>
            <option>Duda/Janda</option>
		</select>
	</div>
	<div class="form-group">
		<label>Pendidikan</label>
		<input class="form-control" type="text" name="pendidikan" placeholder="Pendidikan Terakhir" required="true" />
	</div>
    <div class="form-group">
		<label>Perguruan Tinggi</label>
		<input class="form-control" type="text" name="pt" placeholder="Perguruan Tinggi" />
	</div>
	<div class="form-group">
		<label>Tanggal Masuk</label>
		<input class="form-control" type="date" name="tglmasuk" required="true"/>
	</div>
	<div class="form-group">
		<label>Kualifikasi</label>
		<select class="form-control ajaxkualifikasi" name="kualifikasi" style="width: 100%;" required>
			<option value="">--Pilih Kualifikasi--</option>
		</select>
	</div>
	<div class="form-group">
		<label>Jabatan</label>
		<select class="form-control ajaxjabatan" name="jabatan" style="width: 100%;" required>
			<option value="">--Pilih Jabatan--</option>
		</select>
	</div>
	<div class="form-group">
		<label>Status Karyawan</label>
		<select class="form-control" name="status" required>
			<option value="">--Pilih Status--</option>
			<option value="OS">OS</option>
			<option value="TKWT">TKWT</option>
		</select>
	</div>
	<div class="form-group">
		<label>Kerja/Resign</label>
		<select class="form-control" name="kerres" required>
			<option>Bekerja</option>
			<option>Resign</option>
		</select>
	</div>
	
    