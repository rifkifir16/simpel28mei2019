
<div class="row">
    <div class="col-md-4 col-md-push-8">
    <div class="box">
    	<div class="box-header with-border">
    	  <h3 class="box-title">Petunjuk</h3>
    	</div>
    	<div class="box-body">
            Petunjuk :
            <ol>
                <li>Anda harus mengisi semua data yang dibutuhkan, karena tombol <b>submit</b> tidak akan berfungsi sebelum anda melengkapi data dengan valid.</li><br />
            </ol>
			<br/>
			<img src="<?=$result['FOTO_PEG']?>" class="img-responsive"/>
    	</div>
    </div>
    </div>
    <div class="col-md-8 col-md-pull-4">
    <div class="box">
    	<div class="box-header with-border">
    	  <h3 class="box-title">Data Pegawai</h3>
    	  <div class="pull-right">
    		<a href="index.php?page=pegawai/pegawai" class="btn btn-warning">Batal</a>
    	  </div>
    	</div>
    	<div class="box-body">
    		<div class="nav-tabs-custom">
    			<ul class="nav nav-tabs">
    			  <li class="active"><a href="#tab_1" data-toggle="tab">Data Pribadi</a></li>
    			  <li><a href="#tab_2" data-toggle="tab">Data Pegawai</a></li>
    			  <li><a href="#tab_3" data-toggle="tab">Gaji</a></li>
    			</ul>
                <form method="post" action="<?=site_url('admin/user/pegawai/update')?>"  enctype="multipart/form-data">
        			<div class="tab-content">
        			  <div class="tab-pane active" id="tab_1">
        				<?php include 'edit/datapegawai.php'; ?>
        			  </div><!-- /.tab-pane -->
        			  <div class="tab-pane" id="tab_2">
        				<?php include 'edit/addpegawai.php'; ?>
        			  </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_3">
        				<?php include 'edit/tarifpegawai.php'; ?>
        			  </div><!-- /.tab-pane -->
        			</div><!-- /.tab-content -->
                </form>
    		  </div><!-- nav-tabs-custom -->
    	</div>
    </div>
    </div>
    
</div>
