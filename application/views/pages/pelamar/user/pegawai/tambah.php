<section class="content">
	<div class="row">
		<div class="col-md-4 col-md-push-8">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Petunjuk</h3>
			</div>
			<div class="box-body">
				Petunjuk :
				<ol>
					<li>Anda harus mengisi semua data yang dibutuhkan, karena tombol <b>submit</b> tidak akan berfungsi sebelum anda melengkapi data dengan valid.</li><br />
					<li>NIP bersifat unik, yang berarti tidak bisa ada NIP yang sama. Jika ada NIP yang sama maka tombol <b>submit</b> tidak akan muncul</li><br />
					<li>Username dan password default adalah NIP pegawai. Password bisa diganti oleh pegawai yang bersangkutan.</li>
				</ol>
			</div>
		</div>
		</div>
		<div class="col-md-8 col-md-pull-4">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Data Pegawai</h3>
			  <div class="pull-right">
				<a href="<?=site_url('admin/user/pegawai')?>" class="btn btn-warning">Batal</a>
			  </div>
			</div>
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#tab_1" data-toggle="tab">Data Pribadi</a></li>
					  <li><a href="#tab_2" data-toggle="tab">Data Pegawai</a></li>
					  <li><a href="#tab_3" data-toggle="tab">Kontak Pegawai</a></li>
					  <li><a href="#tab_4" data-toggle="tab">Keluarga Pegawai</a></li>
					  <li><a href="#tab_5" data-toggle="tab">Gaji</a></li>
					  <li><a href="#tab_6" data-toggle="tab">Penempatan</a></li>
					</ul>
					<form method="post" id="fillForm" action="<?=site_url('admin/user/pegawai/save')?>" enctype="multipart/form-data">
						<div class="tab-content">
						  <div class="tab-pane active" id="tab_1">
							<?php include 'tambah/datapegawai.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_2">
							<?php include 'tambah/addpegawai.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_3">
							<?php include 'tambah/kontakpegawai.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_4">
							<?php include 'tambah/keluargapegawai.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_5">
							<?php include 'tambah/tarifpegawai.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_6">
							<?php include 'tambah/penempatanpegawai.php'; ?>
						  </div><!-- /.tab-pane -->
						</div><!-- /.tab-content -->
					</form>
				  </div><!-- nav-tabs-custom -->
			</div>
		</div>
		</div>
		
	</div>
</section>