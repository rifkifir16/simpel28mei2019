<section class="content">
<div class="row">
  <div class="col-md-12">
	  <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Pelamar</h3>
        <div class="pull-right">
        </div>
        <div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9" style="padding:0;">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Pelamar</option>
                  <option value="1">Pelamar Aktif</option>
                  <option value="2">Pelamar Nonaktif</option>
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUtama" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama Pelamar</th>
                <th>Pendidikan Pelamar</th>
                <th>Spesialisasi Pelamar</th>
                <th>Status</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>