<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>
<script>
var table;
  $(function () {
    table=$('#tblUtama').DataTable({
      "processing": true,
      "serverSide":true,  
      "ajax": {
            "url": "<?=$_ajaxSource?>",
            "type": "POST"
        },
      "columnDefs": [{
          "targets": -2,
          "render":function ( data, type, full, meta ) {
              if(data==null) 
                return '<a href="#">Terdapat kesalahan pada data ini</a>';
              else if(data==0)
                return '<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i> Tidak Aktif</a>';
              else 
                return '<a href="#" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Aktif</a>';
            }
            },
        {
          "targets": -1,
          "defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
          "render":function ( data, type, full, meta ) {
              //if(data==null) return '<a href="#">Terdapat kesalahan pada data ini</a>';
              var pesan='Aktifkan';
              //if(data==1) pesan='Nonaktifkan';
              return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkedit">Penilaian</a></li><li class="divider"></li><li><a href="#" class="linkreset">Reset Password</a></li></ul></div>';
            }

          },
          {
            "targets": 1,
            "visible": false,
            "searchable": false
          }
          
            ],
      "language": {
          "lengthMenu": "Menampilkan _MENU_ data per halaman",
          "zeroRecords": "Data masih kosong atau tidak ditemukan",
          "info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
          "infoEmpty": "Data masih kosong"
      }
    })

    $('#tblUtama tbody').on( 'click', '.linkedit', function (e) {
            var data = table.row( $(this).parents('tr') ).data();
            //window.location.href = "<?=base_url('admin/user/pegawai/edit')?>/"+data[1]+".html";
			alert('Fitur belum tersedia');
        });

    $('#tblUtama tbody').on( 'click', '.linkreset', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        reset(data[1],data[2])
    });
  })

  function changeData(index=0){
    table.ajax.url('<?=$_ajaxSource.'/'?>'+index);
    table.ajax.reload();
  }

  $('#fillForm').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#fillForm')[0]);
    
    $.ajax({
        url: "<?php echo site_url('admin/user/pegawai/save'); ?>",
        type: 'POST',
        data: form,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
          if(data.result==false){
            errmsg(data.msg);
          }
          else{
            sucmsg(data.msg);
            reset();
            table.ajax.reload(null,false);
          }
        }
    });
  });

  function reset(id,username){
      
      $.ajax({
        url: "<?php echo site_url('admin/user/pegawai/resetpass'); ?>",
        type: "GET",
        dataType: 'json',
        data : {id: id,username:username},
        success: function (data){
          if(data.result==false){
            errmsg(data.msg);
          }
          else{
            sucmsg(data.msg);
            reset();
            table.ajax.reload(null,false);
          }
          
        }
          
      });
    }
  
   $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });

  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
</script>