<section class="content">
<div class="row">
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tambah User</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="fillForm">
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" autocomplete="off" name="uname" id="uname" placeholder="Username" autofocus="true">
          </div>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" autocomplete="off" name="nama" id="nama" placeholder="Nama">
          </div>
          <div class="form-group">
            <label>Instansi</label>
            <input type="text" class="form-control" autocomplete="off" name="instansi" id="instansi" placeholder="Nama Instansi">
          </div>
          <div class="form-group">
					   <label for="akses">Hak Akses</label>
					   <select name="akses" class="inmdl form-control" id="akses" required>
						<option value="">--Pilih Hak Akses--</option>
						<option value="admin">Admin</option>
						<option value="klien">Klien</option>
					   </select>
					</div>
          <div class="form-group text-center">
            <input type="hidden" value="0" name="idUser" id="idUser"/>
            <input type="submit" value="Submit" class="btn btn-primary"/>
            <a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Reset</a>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-9">
	  <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">User</h3>
        <div class="col-xs-12 left-text" style="padding:0;">
          <!--form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Agama</option>
                  <option value="1">Agama Aktif</option>
                  <option value="2">Agama Nonaktif</option>
                </select>
              </div>
            </div>
          </form-->
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUtama" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>idb</th>
                <th>Username</th>
                <th>Nama</th>
                <th>Instansi</th>
                <th>Hak Akses</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>