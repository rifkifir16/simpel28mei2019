<section class="content">
<div class="row">
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tambah Lokasi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="fillForm">
        <div class="form-group">
            <label>Kode Lokasi</label>
            <input type="text" class="form-control" autocomplete="off" name="kode" id="kode" placeholder="Kode Lokasi" maxlength="10" autofocus="true">
          </div>

          <div class="form-group">
            <label>Nama Propinsi</label>
            <select name="idProp" id="idProp" class="inmdl form-control ajaxProp" style="width:100%;" required>
            <option value="">--Ketik untuk mencari--</option>
            
            </select>
          </div>
          
          <div class="form-group">
            <label>Nama Lokasi</label>
            <textarea class="inmdl form-control" autocomplete="off" name="nama" id="nama" placeholder="Nama Lokasi" rows="3"></textarea>
          </div>
          <div class="form-group text-center">
            <input type="hidden" value="0" name="idLokasi" id="idLokasi"/>
            <input type="submit" value="Submit" class="inmdl btn btn-primary"/>
            <a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Reset</a>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-9">
	  <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Lokasi</h3>
        <div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Lokasi</option>
                  <option value="1">Lokasi Aktif</option>
                  <option value="2">Lokasi Nonaktif</option>
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUtama" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>idb</th>
                <th>idp</th>
                <th>Kode Lokasi</th>
                <th>Nama Propinsi</th>
                <th>Nama Lokasi</th>
                
                <th>Status</th>
                <th style="min-width:80px">Pilihan</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>