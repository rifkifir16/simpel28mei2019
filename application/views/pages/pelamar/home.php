<section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Selamat Datang Pelamar</h3>
				</div>
				<div class="box-body">
					Anda berada pada halaman Plamar. Silahkan klik menu di sebelah kiri untuk beralih halaman .<br />
					Petunjuk :
					<ol>
						<li><b>Pelamar </b> adalah data pendukung untuk melihat atau mengubah data Pelamar.</li>
						<li>Anda dapat melihat atau mengubah data Pribadi pada submenu <b>Pelamar</b>.</li>
						<li>Anda dapat melihat atau mengubah data Relasi pada submenu <b>Pelamar</b>.</li>
						<li>Anda dapat melihat atau mengubah data Keluarga pada submenu <b>Pelamar</b>.</li>
						<li>Anda dapat melihat atau mengubah data Kecakapan pada submenu <b>Pelamar</b>.</li>
						<li>Anda dapat melihat atau mengubah data Riwayat pada submenu <b>Pelamar</b>.</li>
						<li>Anda dapat melihat atau mengubah data Keterangan pada submenu <b>Pelamar</b>.</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

</section>
