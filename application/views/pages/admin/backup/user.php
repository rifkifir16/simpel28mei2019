<section class="content">
<div class="row">
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tambah User</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="formUser" action="<?php echo site_url('admin/user/saveUser'); ?>" method="post">
          <div class="form-group">
            <label>Nama User</label>
            <input type="text" required="true" class="form-control" autocomplete="off" name="nama" id="nama" placeholder="Nama User" autofocus="true">
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" required="true" class="form-control" autocomplete="off" name="username" id="username" placeholder="Nama username">
          </div>
          <div class="form-group"> 
            <label>Hak Akses</label>
            <select name="otorisasi" id="otorisasi" class="form-control" required>
                <option value="">--Pilih Otorisasi--</option>
                <option value="1">User</option>
                <option value="2">Admin</option>
                <?php
                  if($sesi['hakakses']==3)
                    echo '<option value="3">Super Admin</option>';
                ?>
            </select>
          </div>
          <div class="form-group text-center">
            <input type="hidden" value="0" name="idUser" id="idUser"/>
            <input type="submit" value="Submit" class="btn btn-primary"/>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-9">
	<div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data User</h3>
        <div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua User</option>
                  <option value="1">User Aktif</option>
                  <option value="2">User Nonaktif</option>
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUser" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>idp</th>
                <th>Nama User</th>
                <th>Username</th>
                <th>Hak Akses</th>
                <th>Status</th>
                <th>Pilihan</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>