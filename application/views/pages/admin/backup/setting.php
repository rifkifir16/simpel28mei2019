<section class="content">
  <div id="msgAlert" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
    <p id="msgContent"></p>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-gear"></i>

          <h3 class="box-title">Setting</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="setting" method="post">
          <!-- text input -->
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Posisi Dekan</label>
              <div class="col-sm-10">
              <input type="text" class="form-control" id="pdekan" name="pdekan" value="<?=$_data['pdekan']?>" required="true" autofocus="true">
              </div>
            </div>
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">NIP Dekan</label>
              <div class="col-sm-10">
              <input type="text" class="form-control" id="npdekan" name="npdekan" value="<?=$_data['npdekan']?>" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Nama Dekan</label>
              <div class="col-sm-10">
              <input type="text" class="form-control" id="ndekan" name="ndekan" value="<?=$_data['ndekan']?>" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Posisi LPPM</label>
              <div class="col-sm-10">
              <input type="text" class="form-control" id="plppm" name="plppm" value="<?=$_data['plppm']?>" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">NIP LPPM</label>
              <div class="col-sm-10">
              <input type="text" class="form-control" id="nplppm" name="nplppm" value="<?=$_data['nplppm']?>" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Nama LPPM</label>
              <div class="col-sm-10">
              <input type="text" class="form-control" id="nlppm" name="nlppm" value="<?=$_data['nlppm']?>" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12 text-center">
              <input type="submit" name="submit" value="Simpan" class="btn btn-primary"/>
            
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</section>