<script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/bower_components/select2/dist/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/jquery.backstretch.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/retina-1.1.0.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/wizard/js/scripts.js')?>"></script>
<script>
  
  var table;
  $(document).ready(function() {
    $(".ajaxPangkat").select2({
      ajax: {
        url: "<?php echo site_url('admin/pangkat/ajaxPangkat'); ?>",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term
          };
        },
        processResults: function (data) {
          return {
            results: $.map(data, function(obj) {
                return { id: obj.id, text: obj.text };
            })
          };
        },
        cache: true
      },
      "language": "id"
    });
    $(".ajaxJabatan").select2({
      ajax: {
        url: "<?php echo site_url('admin/jabatan/ajaxJabatan'); ?>",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term
          };
        },
        processResults: function (data) {
          return {
            results: $.map(data, function(obj) {
                return { id: obj.id, text: obj.text };
            })
          };
        },
        cache: true
      },
      "language": "id"
    });
    $(".ajaxJurusan").select2({
      ajax: {
        url: "<?php echo site_url('admin/jurusan/ajaxJurusan'); ?>",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term
          };
        },
        processResults: function (data) {
          return {
            results: $.map(data, function(obj) {
                return { id: obj.id, text: obj.text };
            })
          };
        },
        cache: true
      },
      "language": "id"
    });
  });

  $(function () {
    table=$('#tblDosen').DataTable({
      "processing": true,
      "serverSide":true,  
      "ajax": {
          "url": "<?php echo site_url('admin/dosen/dataDosen'); ?>",
          "type": "POST"
      },
      "columnDefs": [ {
          "targets": -2,
          "render":function ( data, type, full, meta ) {
              if(data==null) 
                return '<a href="#">Terdapat kesalahan pada data ini</a>';
              else if(data==0)
                return '<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i> Tidak Aktif</a>';
              else 
                return '<a href="#" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Aktif</a>';
            }
            },
        {
          "targets": -1,
          "defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
          "render":function ( data, type, full, meta ) {
              if(data==null) return '<a href="#">Terdapat kesalahan pada data ini</a>';
              var pesan='Aktifkan';
              if(data==1) pesan='Nonaktifkan';
              return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkedit">Edit</a></li><li><a href="#" class="linkdelete">'+pesan+'</a></li></ul></div>';
            }

          },
          {
            "targets": [1],
            "visible": false,
            "searchable": false
          }
          
            ],
      "language": {
          "lengthMenu": "Menampilkan _MENU_ data per halaman",
          "zeroRecords": "Data masih kosong atau tidak ditemukan",
          "info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
          "infoEmpty": "Data masih kosong"
      }
    })

    $('#tblDosen tbody').on( 'click', '.linkedit', function (e) {
            var data = table.row( $(this).parents('tr') ).data();
            edit(data[2]);
            $('#nip').focus();
        });
    $('#tblDosen tbody').on( 'click', '.linkdelete', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        upStatus(data[2],data[6])
    });
  });

  function changeData(index=0){
    table.ajax.url('<?=base_url('admin/dosen/dataDosen/'); ?>'+index);
    table.ajax.reload();
  }

  //$('#modal-bayar').modal({backdrop: 'static', keyboard: false,show:false}) ;
  $('.modal').modal({backdrop: 'static', keyboard: false,show:false}) ;
  $('.select2').select2()

  $('#formDosen').submit(function(e) {
    e.preventDefault();
    var form = new FormData($('#formDosen')[0]);
    
    $.ajax({
        url: "<?php echo site_url('admin/dosen/saveDosen'); ?>",
        type: 'POST',
        data: form,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success : function(data) {
          if(data.result==false){
            errmsg(data.msg);
          }
          else{
            sucmsg(data.msg);
            setModal();
            table.ajax.reload(null,false);
          }
        }
    });
  });

  function upStatus(id,status){
      
    $.ajax({
      url: "<?php echo site_url('admin/dosen/upstatus'); ?>",
      type: "GET",
      dataType: 'json',
      data : {id: id,status:status},
      success: function (data){
        if(data.result==false){
          errmsg(data.msg);
        }
        else{
          sucmsg(data.msg);
          table.ajax.reload(null,false);
        }
      }
        
    });
  }
 
  $('#msgAlert').hide();
  $('#msgAlert').on("close.bs.alert", function () {
    $('#msgAlert').hide();
        return false;
  });

  function sucmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-danger');
    $('#msgAlert').addClass('alert-success');

    $('#msgIcon').removeClass('fa-ban');
    $('#msgIcon').addClass('fa-check');

    $('#msgContent').html(msg);
  }

  function errmsg(msg){
    $('#msgAlert').show();
    $('#msgAlert').removeClass('alert-success');
    $('#msgAlert').addClass('alert-danger');

    $('#msgIcon').addClass('fa-ban');
    $('#msgIcon').removeClass('fa-check');

    $('#msgContent').html(msg);
  }
  $('#modalDosen').on('shown.bs.modal', function () {
        $('#nip').focus();
  }) 

  $('.f1').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"], input[type="email"], textarea').each(function() {
        if(!$(this).prop('required')){
							}
        else{
          if( $(this).val() == "" ) {
            e.preventDefault();
            $(this).addClass('input-error');
          }
          else {
            $(this).removeClass('input-error');
          }
        }
    	});
      e.preventDefault();
      $('#modalDosen').modal('hide');
    });

    function clearModal(){
      var form=$('.f1');
      form.find('input[type="text"], input[type="password"], input[type="email"], textarea').each(function() {
        $(this).val("");
      });
    }

    $('#modalDosen').on('hidden.bs.modal', function (e) {
      if($('#otorisasi').val()!=""){
        $('#prevbtn').trigger('click');
      }
      if($('#email').val()!=""){
        $('#prevbtn').trigger('click');
      }
      $('fieldset').hide();
      $('#firstfieldset').show();
      setModal();
      
    });


    function setModal(cont=false){
      var isi=['','','','','','','','','','','','0','','',''];
      if(cont!=false)
        isi=cont;
      $('#nip').val(isi[0]);
      $('#nidn').val(isi[1]);
      $('#nama').val(isi[2]);
      $('#alamatKantor').val(isi[3]);
      $('#email').val(isi[4]);
      $('#telp').val(isi[5]);
      $('#alamatRumah').val(isi[6]);
     
      if(cont==false)
        $('#pangkat').prepend('<option value="" selected>--Pilih pangkat--</option>');
      else
        $('#pangkat').prepend('<option value="'+isi[7]+'" selected>'+isi[12]+'</option>');
        //$('#pangkat').val(isi[7]);
      
      if(cont==false)
        $('#jabatan').prepend('<option value="" selected>--Pilih Jabatan--</option>');
      else
        $('#jabatan').prepend('<option value="'+isi[8]+'" selected>'+isi[13]+'</option>');
      
      if(cont==false)
        $('#jurusan').prepend('<option value="" selected>--Pilih Jurusan--</option>');
      else
        $('#jurusan').prepend('<option value="'+isi[9]+'" selected>'+isi[14]+'</option>');
      
      
      $('#otorisasi').val(isi[10]);
      $('#isedit').val(isi[11]);
    }   
		
    function edit(id){
      $.ajax({
          url: "<?php echo site_url('admin/dosen/reqEdit'); ?>",
          dataType: 'json', // jQuery will parse the response as JSON
          type: "POST",
          data : {id: id},
          success: function (data) {
            console.log(data);
            setModal(data);
            $('#modalDosen').modal('show');
          }
      });
    }
	
</script>