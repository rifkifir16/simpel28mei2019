<section class="content">
  <div id="msgAlert" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
    <p id="msgContent"></p>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-lock"></i>

          <h3 class="box-title">Ubah Password</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="password" method="post">
          <!-- text input -->
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Password</label>
              <div class="col-sm-10">
              <input type="password" class="form-control" id="pass" name="pass" required="true" autofocus="true">
              </div>
            </div>
            <div class="form-group col-sm-12" style="padding:0;">
              <label class="col-sm-2">Verifikasi password</label>
              <div class="col-sm-10">
              <input type="password" class="form-control" id="vpass" name="vpass" required="true">
              </div>
            </div>
            <div class="form-group col-sm-12 text-center">
              <input type="submit" name="submit" value="simpan" class="btn btn-primary"/>
            
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    

  <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-user"></i>

          <h3 class="box-title">Informasi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          Sebagai administrator, anda hanya bisa mengubah password pada halaman ini. Jika anda bermaksud untuk mengubah nama atau username, silahkan kunjungi halaman data pengguna.
          <hr/>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>