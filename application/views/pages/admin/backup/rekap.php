<section class="content">
<div class="row">
  <div class="col-md-12">
    <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Download Rekapitulasi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="formBidang" method="post" action="<?=site_url('admin/rekap/generate')?>">
          <div class="col-xs-12">
            <div class="form-group">
              <label>Kategori</label>
              <select name="kategori" class="form-control" required>
                <option value="">-- Pilih Kategori--</option>
                <option value="penelitian">Penelitian</option>
                <option value="pkm">PKM</option>
                <option value="semua">Semua</option>
              </select>
            </div>
          </div>
          <?php $bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');?>
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>Bulan</label>
              <select name="bulan" class="form-control" required>
                <option value="0">Semua Bulan</option>
                <?php
                for($i=0;$i<12;$i++){
                  echo '<option value="'.($i+1).'">'.$bulan[$i].'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>Tahun</label>
              <input type="number" min="2018" value="2018" class="form-control" name="tahun"/>
            </div>
          </div>
          <div class="form-group text-center">
            <input type="submit" value="Submit" class="btn btn-primary"/>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>