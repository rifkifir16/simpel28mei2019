<section class="content">

<?php 
    include 'dosen/modal.php';
  ?>
  <div id="msgAlert" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
    <p id="msgContent"></p>
  </div>
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Dosen</h3>
      <div class="pull-right">
      <a href="#" data-toggle="modal" data-target="#modalDosen" class="btn btn-primary">Tambah Dosen</a>
      </div>
      <div class="col-xs-12 left-text" style="padding:0;">
        <form>
          <div class="form-group">
            <div class="col-xs-3" style="padding:0;">
              <label class="control-label">Tampilkan : </label>
            </div>
            <div class="col-xs-9">
              <select class="form-control" onchange="changeData(this.value)">
                <option value="0">Semua Dosen</option>
                <option value="1">Dosen Aktif</option>
                <option value="2">Dosen Nonaktif</option>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tblDosen" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>idu</th>
              <th>NIP</th>
              <th>NIDN</th>
              <th>Nama Dosen</th>
              <th>Status</th>
              <th>Pilihan</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
    </div>
    <!-- /.box-body -->
  </div>
  
  
</section>