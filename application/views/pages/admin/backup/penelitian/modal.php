<!--modal bayar hutang-->
<div class="modal fade" id="modalPenelitian">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Form Penelitian</h4>
        </div>
        <div class="modal-body">
          <div id="forms">
          <form method="post" id="formPenelitian" enctype="multipart/form-data">
            <div class="form-group">
              <label>Judul Penelitian</label>
              <input type="text" class="form-control" id="judul" placeholder="Judul Penelitian" required="true" name="judul">
              
            </div>
            <div class="form-group">
              <label>Bidang Penelitian</label>
              <select name="bidang" class="form-control ajaxBidang" id="bidang" style="width:100%;" required>
                <option value="">--Pilih Bidang--</option>
              </select>
              
            </div>
            <div class="form-group col-sm-12" id="linksWrap" style="padding:0;">
								<div class="col-sm-12" style="padding:0;">
                  <label>Anggota*</label><a href="#" onclick="duplinks()" class="pull-right">Tambah Anggota</a>
								</div>
								<div class="links">
                  <select name="anggota[]" class="form-control ajaxDosen" id="anggota" style="width:100%;">
                    <option value="">--Pilih Anggota--</option>
                  </select>
								</div>
            </div>
            <div class="form-group">
                <label>File Proposal</label>
                <input type="file" name="proposal" id="proposal" class="form-control" placeholder="Proposal"/>
            </div>

            <div class="form-group">
              <label>Usulan Biaya</label>
              <input type="text" class="form-control ftpl text-right" id="biaya" required="true" name="biaya" autocomplete="off">
              
            </div>
            <div class="form-group">
              <label>Jenis Penelitian</label>
              <select name="jenis" class="form-control" id="jenis" required>
                <option value="">--Pilih Jenis Penelitian--</option>
                <option value="1">Swadana Fakultas</option>
                <option value="2">Swadana Jurusan</option>
              </select>
            </div>
            <div class="col-xs-12">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
            <input type="hidden" name="isedit" id="isedit" value="0"/>
            <input type="submit" value="Submit" class="btn btn-primary pull-right"/>
            </div>
            
          </form>
          </div>
          <div id="tef">Sedang mengupload
          <a href="#" id="progress"></a>
          </div>
        </div>
        <div class="modal-footer"></div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>