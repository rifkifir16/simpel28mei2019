<section class="content">
<div class="row">
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tambah Jabatan</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="fillForm">
          <div class="form-group">
            <label>Jabatan</label>
            <input type="text" class="form-control" autocomplete="off" name="jabatan" id="jabatan" placeholder="Jabatan" autofocus="true">
          </div>
          <div class="form-group text-center">
            <input type="hidden" value="0" name="idJabatan" id="idJabatan"/>
            <input type="submit" value="Submit" class="btn btn-primary"/>
            <a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Reset</a>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-9">
	  <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Jabatan</h3>
        <div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Jabatan</option>
                  <option value="1">Jabatan Aktif</option>
                  <option value="2">Jabatan Nonaktif</option>
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUtama" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>idb</th>
                <th>Nama Jabatan</th>
                <th>Status</th>
                <th>Pilihan</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>