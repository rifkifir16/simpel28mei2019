<?php
$jabatan="";
if($sesi['hakakses']==1) $jabatan='Dosen';
elseif($sesi['hakakses']==2) $jabatan='Administrator';
elseif($sesi['hakakses']==3) $jabatan='Super Admin';
?>

  <div class="col-md-6">
	  <!-- Widget: user widget style 1 -->
	  <div class="box box-widget widget-user">
		<!-- Add the bg color to the header using any of the bg-* classes -->
		<div class="widget-user-header bg-aqua-active">
		  <h3 class="widget-user-username"><?=$sesi['nama']?></h3>
		  <h5 class="widget-user-desc"><?=$jabatan?></h5>
		</div>
	  </div>
	  <!-- /.widget-user -->
	</div>
  