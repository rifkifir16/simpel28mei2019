<section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Selamat Datang</h3>
				</div>
				<?php
				if ($this->session->userdata('hakAkses') == 'admin') {
					?>
					<div class="box-body">
						Anda berada pada halaman administrator. Silahkan klik menu di sebelah kiri untuk beralih halaman .<br />
						Petunjuk :
						<ol>
							<li><b>Data Basic</b> adalah data pendukung untuk pengisian data user dan pegawai.</li>
							<li>Anda dapat menambahkan pegawai dan user pada submenu <b>data user</b>.</li>
							<li>Anda dapat menambahkan data SPPD pada menu <b>SPPD</b>.</li>
							<li>Untuk mendapatkan laporan internal, anda dapat memilih bulan dan tahun pada menu <b>penggajian</b> kemudian tekan tombol <b>laporan internal</b>.</li>
							<li>Pegawai akan menginputkan jumlah hari kerja dan jumlah kehadiran pada halaman pegawai. Anda dapat menyetujui input pada halaman <b>penggajian</b></li>
							<li>Menu <b>laporan</b> digunakan untuk mendapatkan laporan eksternal.</li>
							<li>Anda perlu menambahkan kode kota di halaman <b class="btn btn-default"><a href="<?= base_url('admin/basic/kota') ?>">Data Kota</a></b> sebelum menggunakan sistem ini.</li>
						</ol>
					</div>
				<?php } else { ?>
					<div class="box-body">
						Anda berada pada halaman administrator PLN. Silahkan klik menu di sebelah kiri untuk beralih halaman .<br />
						Petunjuk :
						<ol>
							<li><b>Data Pegawai</b> adalah data untuk melihat data pegawai.</li>
							<li><b>Data SPPD</b> adalah data untuk melihat data SPPD.</li>
							<li>Menu <b>laporan</b> digunakan untuk mendapatkan laporan eksternal.</li>
							<li>Menu <b>Serapan</b> digunakan untuk melihat grafik serapan.</li>
						</ol>
					</div>

				<?php } ?>
			</div>
		</div>
	</div>

</section>
