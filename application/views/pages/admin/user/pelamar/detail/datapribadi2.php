<div class="table-responsive">
	<table class="table table-borderless">
		<tr>
			<td><b>Tinggi Badan (Cm)</b></td>
			<td><?= $result[0]['TB_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Berat Badan</b></td>
			<td><?= $result[0]['BB_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Hobi</b></td>
			<td><?= $result[0]['HOBI_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Olahraga yang dikuasai</b></td>
			<td><?= $result[0]['OLAHRAGA_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Pernah sakit keras</b></td>
			<td><?php if ($result[0]['SAKIT_PELAMAR'] == 1) {
					echo "Pernah";
				} else {
					echo "Tidak pernah";
				} ?></td>
		</tr>
		<?php if ($result[0]['SAKIT_PELAMAR'] == 1) {
			?>
			<tr>
				<td><b>Tanggal sakit</b></td>
				<td><?= $result[0]['TGLSAKIT_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Sakit apa ?</b></td>
				<td><?= $result[0]['JNSSAKIT_PELAMAR'] ?></td>
			</tr>
		<?php } ?>
		<tr>
			<td><b>Pernah Kecelakaan</b></td>
			<td><?php if ($result[0]['KECELAKAAN_PELAMAR'] == 1) {
					echo "Pernah";
				} else {
					echo "Tidak pernah";
				} ?></td>
		</tr>
		<?php if ($result[0]['SAKIT_PELAMAR'] == 1) {
			?>
			<tr>
				<td><b>Tanggal Kecelakaan</b></td>
				<td><?= $result[0]['TGLKEC_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Kecelakaan apa</b></td>
				<td><?= $result[0]['JNSKEC_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Akibat Kecelakaan</b></td>
				<td><?= $result[0]['AKIBATKEC_PELAMAR'] ?></td>
			</tr>
		<?php } ?>
		<tr>
			<td><b>Pernah terlibat kegiatan terlarang</b></td>
			<td><?php if ($result[0]['TERLARANG_PELAMAR'] == 1) {
					echo "Pernah";
				} else {
					echo "Tidak pernah";
				} ?></td>
		</tr>
		<tr>
			<td><b>Pernah terlibat urusan dengan pihak kepolisian</b></td>
			<td><?php if ($result[0]['KEPOLISIAN_PELAMAR'] == 1) {
					echo "Pernah";
				} else {
					echo "Tidak pernah";
				} ?></td>
		</tr>
		<?php if ($result[0]['KEPOLISIAN_PELAMAR'] == 1) {
			?>
			<tr>
				<td><b>Jelaskan</b></td>
				<td><?= $result[0]['KETKEPOL_PELAMAR'] ?></td>
			</tr>
		<?php } ?>
		<tr>
			<td><b>Pernah terdaftar sebagai peserta/anggota program ASTEK</b></td>
			<td><?php if ($result[0]['ASTEK_PELAMAR'] == 1) {
					echo "Pernah";
				} else {
					echo "Tidak pernah";
				} ?></td>
		</tr>
		<?php if ($result[0]['ASTEK_PELAMAR'] == 1) {
			?>
			<tr>
				<td><b>No Peserta/KPA</b></td>
				<td><?= $result[0]['NOASTEK_PELAMAR'] ?></td>
			</tr>
		<?php } ?>
	</table>

</div>
