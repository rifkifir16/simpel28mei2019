<?php foreach ($sekolah as $sekolah) {
	?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td><b>Nama Sekolah</b></td>
				<td><?= $sekolah['SEKOLAHPEND_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Kota</b></td>
				<td><?= $sekolah['KOTAPEND_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Sampai kelas</b></td>
				<td><?= $sekolah['SMPKELAS_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Dari/sampai tahun</b></td>
				<td><?= $sekolah['DARISMPTHPEND_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Jurusan</b></td>
				<td><?= $sekolah['JURUSANPEND_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Tahun Ijazah</b></td>
				<td><?= $sekolah['THIJAZAHPEND_PELAMAR'] ?></td>
			</tr>
		</table>
	</div>
	<hr>
<?php } ?>

<?php foreach ($kursus as $kursus) {
	?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td><b>Nama Kursus</b></td>
				<td><?= $kursus['JENISKURSUS_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Lama Kursus</b></td>
				<td><?= $kursus['LAMAKURSUS_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Dari - Sampai (Tanggal,Bulan,Tahun)</b></td>
				<td><?= $kursus['DARISMPTHKURSUS_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Sertifikat/Keterangan</b></td>
				<td><?= $kursus['SERTIFIKATKURSUS_PELAMAR'] ?></td>
			</tr>
		</table>
	</div>
	<hr>
<?php } ?>
