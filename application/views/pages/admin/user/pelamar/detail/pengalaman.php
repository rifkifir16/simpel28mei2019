<?php foreach ($pengalaman as $pengalaman) {
	?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td><b>Nama Instansi</b></td>
				<td><?= $pengalaman['INSTANSI_PENGALAMAN'] ?></td>
			</tr>
			<tr>
				<td><b>Jabatan</b></td>
				<td><?= $pengalaman['JABATAN_PENGALAMAN'] ?></td>
			</tr>
			<tr>
				<td><b>Dari-Sampai (Tanggal Bulan Tahun)</b></td>
				<td><?= $pengalaman['DRSMPTH_PENGALAMAN'] ?></td>
			</tr>
			<tr>
				<td><b>Gaji</b></td>
				<td><?= $pengalaman['GAJI_PENGALAMAN'] ?></td>
			</tr>
			<tr>
				<td><b>Tunjangan</b></td>
				<td><?= $pengalaman['TUNJANGAN_PENGALAMAN'] ?></td>
			</tr>
			<tr>
				<td><b>Negara</b></td>
				<td><?= $pengalaman['NEGARA_PENGALAMAN'] ?></td>
			</tr>
			<tr>
				<td><b>Alasan Berhenti/Tujuan/Keterangan</b></td>
				<td><?= $pengalaman['ALASAN_PENGALAMAN'] ?></td>
			</tr>
		</table>
	</div>
	<hr>
<?php } ?>

<?php foreach ($referensi as $referensi) {
	?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td><b>Nama referensi</b></td>
				<td><?= $referensi['NAMA_REFERENSI'] ?></td>
			</tr>
			<tr>
				<td><b>Alamat & Telp</b></td>
				<td><?= $referensi['ALAMAT_REFERENSI'] ?></td>
			</tr>
			<tr>
				<td><b>Jabatan</b></td>
				<td><?= $referensi['JABATAN_REFERENSI'] ?></td>
			</tr>
			<tr>
				<td><b>Telp referensi</b></td>
				<td><?= $referensi['TELP_REFERENSI'] ?></td>
			</tr>
			<tr>
				<td><b>Hubungan</b></td>
				<td><?= $referensi['HUBUNGAN_REFERENSI'] ?></td>
			</tr>
		</table>
	</div>
	<hr>
<?php } ?>
<?php foreach ($organisasi as $organisasi) {
	?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td><b>Nama organisasi</b></td>
				<td><?= $organisasi['NAMA_ORGANISASI'] ?></td>
			</tr>
			<tr>
				<td><b>Lokasi</b></td>
				<td><?= $organisasi['LOKASI_ORGANISASI'] ?></td>
			</tr>
			<tr>
				<td><b>Sebagai</b></td>
				<td><?= $organisasi['SEBAGAI_ORGANISASI'] ?></td>
			</tr>
		</table>
	</div>
	<hr>
<?php } ?>
