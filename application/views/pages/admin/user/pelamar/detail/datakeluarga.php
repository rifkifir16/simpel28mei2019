<?php foreach ($keluarga as $keluarga) {
	?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td><b>Nama</b></td>
				<td><?= $keluarga['ISIKEL_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Hubungan keluarga</b></td>
				<td><?= $keluarga['NAMAKEL_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Jenis Kelamin</b></td>
				<td><?= $keluarga['JKKEL_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Tempat tanggal lahir</b></td>
				<td><?= $keluarga['TPTLAHIRKEL_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Tanggal lahir</b></td>
				<td><?= $keluarga['TGLLAHIRKEL_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Alamat</b></td>
				<td><?= $keluarga['ALAMATKEL_PELAMAR'] ?></td>
			</tr>
			<tr>
				<td><b>Pendidikan/pekerjaan</b></td>
				<td><?= $keluarga['JOBKEL_PELAMAR'] ?></td>
			</tr>
		</table>
	</div>
	<hr>
<?php } ?>
