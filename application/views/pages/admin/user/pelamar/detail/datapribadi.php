<div class="table-responsive">
	<input class="form-control" type="hidden" id="ID_PELAMAR" name="ID_PELAMAR" value="<?= $result[0]['ID_PELAMAR'] ?>" />
	<input class="form-control" type="hidden" id="email_pelamar" name="email_pelamar" value="<?= $result[0]['EMAIL_PELAMAR'] ?>" />
	<input class="form-control" type="hidden" id="konfirmasi_email" name="konfirmasi_email" value="<?= $result[0]['KONFIRMASI_EMAIL'] ?>" />
	<table class="table table-borderless">
		<tr>
			<td><b>Posisi yang dilamar</b></td>
			<td><?= $result[0]['POSISI_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Nama</b></td>
			<td><?= $result[0]['NAMA_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>No. KTP</b></td>
			<td><?= $result[0]['NOKTP_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Jenis SIM</b></td>
			<td><?= $result[0]['SIM_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>No SIM</b></td>
			<td><?= $result[0]['NOSIM_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Alamat</b></td>
			<td><?= $result[0]['ALAMAT_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Kota Domisili</b></td>
			<td><?= $result[0]['NAMA_KOTA'] ?></td>
		</tr>
		<tr>
			<td><b>Tempat Lahir</b></td>
			<td><?= $result[0]['TPT_LAHIR_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Tanggal Lahir</b></td>
			<td><?= $result[0]['TGL_LAHIR_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Agama</b></td>
			<td><?= $result[0]['NAMA_AGAMA'] ?></td>
		</tr>
		<tr>
			<td><b>Jenis Kelamin</b></td>
			<td><?= $result[0]['JK_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Status Pernikahan</b></td>
			<td><?= $result[0]['STATUS_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>No Telp</b></td>
			<td><?= $result[0]['NOTELP_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Alamat Email</b></td>
			<td><?= $result[0]['EMAIL_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Pendidikan Terakhir (tingkat/bidang/jurusan)</b></td>
			<td><?= $result[0]['TKTPENDTERAKHIR_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Spesialisasi</b></td>
			<td><?= $result[0]['SPESIALISASI_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Keahlian yang paling dikuasai</b></td>
			<td><?= $result[0]['KEAHLIAN_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Bidang studi yang paling dikuasai</b></td>
			<td><?= $result[0]['BIDSTUDFAV_PELAMAR'] ?></td>
		</tr>
		<tr>
			<td><b>Judul skripsi/proyek akhir</b></td>
			<td><?= $result[0]['JDLSKRIPSI_PELAMAR'] ?></td>
		</tr>
	</table>

</div>
