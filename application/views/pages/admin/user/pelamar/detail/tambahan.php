<?php foreach ($keterangan as $keterangan) {
	?>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<td><b>Tunjangan saat ini</b></td>
				<td><?= $keterangan['TUNJANGAN_KETERANGAN'] ?></td>
			</tr>
			<tr>
				<td><b>Sumber Tunjangan</b></td>
				<td><?= $keterangan['SUMBER_KETERANGAN'] ?></td>
			</tr>
			<tr>
				<td><b>Gaji yang diharapkan</b></td>
				<td><?= $keterangan['GAJIHARAPAN_KETERANGAN'] ?></td>
			</tr>
			<tr>
				<td><b>Fasilitas yang diharapkan</b></td>
				<td><?= $keterangan['FASILITASHARAPAN_KETERANGAN'] ?></td>
			</tr>
			<tr>
				<td><b>Bersedia ditempatkan dimana saja di seluruh wilayah operasi perusahaan</b></td>
				<td><?php if ($keterangan['BERSEDIATEMPAT_KETERANGAN'] == 1) {
						echo "Bersedia";
					} else {
						echo "Tidak bersedia";
					}  ?></td>
			</tr>
			<tr>
				<td><b>Bersedia mematuhi peraturan perusahaan yang berlaku maupun akan dikeluarkan perusahaan</b></td>
				<td><?php if ($keterangan['BERSEDIAATURAN_KETERANGAN'] == 1) {
						echo "Bersedia";
					} else {
						echo "Tidak bersedia";
					}  ?></td>
			</tr>
			<tr>
				<td><b>Keterangan</b></td>
				<td><?= $keterangan['KET_KETERANGAN'] ?></td>
			</tr>
		</table>
	</div>
	<hr>
<?php } ?>
