
	<div class="form-group">
		<label>Upah/NET</label>
		<input class="form-control upah text-right" id="iupah" type="text" value="<?=number_format($result['UPAH_TARIF'],0,'','.')?>" onkeyup="calTot()"  name="upah" placeholder="Upah/NET" />
	</div>
    <div class="form-group">
		<label>Tunjangan Umum</label>
		<input class="form-control tumum text-right" id="itumum" type="text" value="<?=number_format($result['TUMUM_TARIF'],0,'','.')?>" onkeyup="calTot()" name="tumum" placeholder="Tunjangan Umum" />
	</div>
	<div class="form-group">
		<label>Tunjangan Uang Makan</label>
		<input class="form-control tmakan text-right" id="itmakan" type="text" value="<?=number_format($result['TMAKAN_TARIF'],0,'','.')?>" onkeyup="calTot()" name="tmakan" placeholder="Tunjangan Uang Makan"/>
	</div>
	<div class="form-group">
		<label>Tunjangan Komunikasi</label>
		<input class="form-control tkom text-right" id="itkom" type="text" value="<?=number_format($result['TKOMUNIKASI_TARIF'],0,'','.')?>" onkeyup="calTot()" name="tkomunikasi" placeholder="Tunjangan Komunikasi"/>
	</div>
	<div class="form-group">
		<label>Jumlah</label>
		<input class="form-control ttotal text-right" type="text" value="<?=number_format($result['JUMLAH_TARIF'],0,'','.')?>" name="ttotal" placeholder="Jumlah" readonly="true"/>
	</div>
	<div class="form-group text-center">
		<input class="btn btn-primary" id="btnSubmit" type="submit" name="submit" value="submit"/>
	</div>