

	<div class="form-group">
		<label>NIP </label>
		<input class="form-control" type="text" placeholder="NIP" value="<?=$result['NIP_PEG']?>"  readonly="true"/>
		<input type="hidden" name="nip" value="<?=$result['NIP_PEG']?>" />
	</div>
	<div class="form-group">
		<label>Foto</label>
		<input class="form-control" type="file" name="foto" placeholder="Foto Pegawai" />
	</div>
	<div class="form-group">
		<label>Nama Pegawai</label>
		<input class="form-control" type="text" name="nama" placeholder="Nama Pegawai" value="<?=$result['NAMA_PEG']?>" required="true" />
	</div>
	<div class="form-group">
		<label>Alamat Pegawai</label>
		<textarea class="form-control" name="alamat"><?=$result['ALAMAT_PEG']?></textarea>
	</div>
	<div class="form-group">
		<label>Kota Domisili</label>
		<select class="form-control ajaxkota" name="domisili" style="width: 100%;" required>
			<option value="<?=$result['ID_KOTA']?>"><?=$result['NAMA_KOTA']?></option>
		</select>
	</div>
    <div class="form-group">
		<label>Tempat Lahir</label>
		<select class="form-control ajaxkota" name="kotalahir" style="width: 100%;" required>
			<option value="<?=$result['kidkota']?>"><?=$result['knkota']?></option>
		</select>
	</div>
	<div class="form-group">
		<label>Tanggal Lahir</label>
		<input class="form-control" type="date" name="tgllhr" value="<?=$result['TGL_LAHIR_PEG']?>" required="true"/>
	</div>
	<div class="form-group">
		<label>Agama</label>
		<select class="form-control ajaxagama" name="agama" style="width: 100%;" required>
			<option value="<?=$result['ID_AGAMA']?>"><?=$result['NAMA_AGAMA']?></option>
		</select>
	</div>
	<div class="form-group">
		<label>Jenis Kelamin</label>
		<select class="form-control" name="jk" required>
			<option value="L" <?php if($result['JK_PEG']=='L') echo 'selected';?>>Laki-Laki</option>
			<option value="P" <?php if($result['JK_PEG']=='P') echo 'selected';?>>Perempuan</option>
		</select>
	</div>
	<div class="form-group">
		<label>Status Pernikahan</label>
		<select class="form-control" name="sn" required>
			<option>Belum Menikah</option>
			<option <?php if($result['STATUS_NIKAH_PEG']=='Sudah Menikah') echo 'selected';?>>Sudah Menikah</option>
            <option <?php if($result['STATUS_NIKAH_PEG']=='Duda/Janda') echo 'selected';?>>Duda/Janda</option>
		</select>
	</div>
	<div class="form-group">
		<label>Pendidikan</label>
		<input class="form-control" type="text" name="pendidikan" value="<?=$result['PENDIDIKAN_PEG']?>" placeholder="Pendidikan Terakhir" required="true" />
	</div>
    <div class="form-group">
		<label>Perguruan Tinggi</label>
		<input class="form-control" type="text" name="pt" value="<?=$result['PT_PEG']?>" placeholder="Perguruan Tinggi" />
	</div>
	<div class="form-group">
		<label>Tanggal Masuk</label>
		<input class="form-control" type="date" name="tglmasuk" value="<?=$result['TGLMASUK_PEG']?>" required="true"/>
	</div>
	<div class="form-group">
		<label>Kualifikasi</label>
		<select class="form-control ajaxkualifikasi" name="kualifikasi" style="width: 100%;" required>
			<option value="<?=$result['ID_KUALIFIKASI']?>"><?=$result['NAMA_KUALIFIKASI']?></option>
		</select>
	</div>
	<div class="form-group">
		<label>Jabatan</label>
		<select class="form-control ajaxjabatan" name="jabatan" style="width: 100%;" required>
			<option value="<?=$result['ID_JABATAN']?>"><?=$result['NAMA_JABATAN']?></option>
		</select>
	</div>
	<div class="form-group">
		<label>Status Karyawan</label>
		<select class="form-control" name="status" required>
			<option value="">--Pilih Status--</option>
			<option value="OS" <?php if($result['STATUS_PEG']=='OS') echo 'selected';?>>OS</option>
			<option value="TKWT" <?php if($result['STATUS_PEG']=='TKWT') echo 'selected';?>>TKWT</option>
		</select>
	</div>
	<div class="form-group">
		<label>Kerja/Resign</label>
		<select class="form-control" name="kerres" required>
			<option <?php if($result['KERRES_PEG']=='Bekerja') echo 'selected';?>>Bekerja</option>
			<option <?php if($result['KERRES_PEG']=='Resign') echo 'selected';?>>Resign</option>
		</select>
	</div>
    