<section class="content">
<div class="row">
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tambah Penempatan</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="fillForm">
          <div class="form-group">
						<label for="Nama">Nama Karyawan</label>
            <select name="nama" id="nama" class="inmdl form-control ajaxpegawai" style="width:100%;" required>
            <option value="">--Ketik untuk mencari--</option>
            </select>
					</div>
		
					<div class="form-group">
						<label for="tanggal">Tanggal</label>
						<input type="date" name="tanggal" id="tanggal" class="form-control" required="true"/>
					</div>
					
					<div class="form-group">
						<label for="lokasi">Lokasi Penempatan</label>
            <select class="form-control ajaxlokasi" id="penempatan" name="penempatan" style="width: 100%;" required>
            <option value="">--Pilih Lokasi--</option>
          </select>
					</div>


          <div class="form-group text-center">
            <input type="hidden" value="0" name="idPenempatan" id="idPenempatan"/>
            <input type="submit" value="Submit" class="btn btn-primary"/>
            <a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Reset</a>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-9">
	  <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Penempatan</h3>
        <!--div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Penempatan</option>
                  <option value="1">Penempatan Aktif</option>
                  <option value="2">Penempatan Nonaktif</option>
                </select>
              </div>
            </div>
          </form>
        </div-->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUtama" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>idpenempatan</th>
                <th>idlokasi</th>
                <th>Tanggal</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Lokasi</th>
                <th style="min-width:80px">Pilihan</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>