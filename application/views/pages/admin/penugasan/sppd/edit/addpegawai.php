
	<div class="form-group">
		<label>No. Rekening</label>
		<input class="form-control" type="text" name="norek" placeholder="No. Rekening" value="<?=$result['REK_PEG']?>" />
	</div>
    <div class="form-group">
		<label>Nama Rekening</label>
		<input class="form-control" type="text" name="namarek" placeholder="Nama Rekening" value="<?=$result['NAMA_REK_PEG']?>" />
	</div>
	<div class="form-group">
		<label>Nama Bank</label>
		<input class="form-control" type="text" name="bank" placeholder="Nama Bank" value="<?=$result['BANK_PEG']?>"/>
	</div>
	<div class="form-group">
		<label>Nama NPWP</label>
		<input class="form-control" type="text" name="namanpwp" placeholder="No. NPWP" value="<?=$result['NAMANPWP_PEG']?>"/>
	</div>
	<div class="form-group">
		<label>No. NPWP</label>
		<input class="form-control" type="text" name="npwp" placeholder="No. NPWP" value="<?=$result['NPWP_PEG']?>"/>
	</div>
	<div class="form-group">
		<label>No. KTP</label>
		<input class="form-control" type="text" name="ktp" placeholder="No. KTP" value="<?=$result['KTP_PEG']?>"/>
	</div>
	<div class="form-group">
		<label>No. KK</label>
		<input class="form-control" type="text" name="kk" placeholder="No. KK" value="<?=$result['KK_PEG']?>"/>
	</div>
	<div class="form-group">
		<label>No. BPJS Tenaga Kerja</label>
		<input class="form-control" type="text" name="bpjsk" placeholder="No. BPJS Tenaga Kerja" value="<?=$result['BPJS_KERJA_PEG']?>"/>
	</div>
	<div class="form-group">
		<label>No. BPJS Kesehatan</label>
		<input class="form-control" type="text" name="bpjss" placeholder="No. BPJS Kesehatan" value="<?=$result['BPJS_SEHAT_PEG']?>"/>
	</div>