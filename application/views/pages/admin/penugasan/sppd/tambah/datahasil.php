
	<div class="form-group">
		<label>Total Biaya (Belum termasuk biaya harian)</label>
		<input class="form-control totalbiaya text-right" type="text" name="tbiaya" value="0" readonly="true" />
	</div>
    <div class="form-group">
		<label>Status SPPD</label>
		<input class="form-control" type="text" name="status" placeholder="Status SPPD" />
	</div>
	<div class="form-group">
		<label>Biaya dari</label>
		<input class="form-control" type="text" name="bdari" placeholder="Biaya dari" />
	</div>
	<div class="form-group">
		<label>Pemberi Kerja</label>
		<select class="form-control ajaxlokasi" name="pemk" style="width: 100%;" required>
			<option value="">--Pilih Lokasi--</option>
		</select>
	</div>
	<div class="form-group">
		<label>Keterangan SPPD</label>
		<textarea class="form-control" name="keterangan"></textarea>
	</div>
	<div class="form-group">
		<label>Otorisasi SPPD</label>
		<select class="form-control" name="osppd" style="width: 100%;" required>
			<option value="">--Pilih Otorisasi--</option>
            <option value="1">Syamsudin Hidayat</option>
			<option value="2">Kholidun</option>
            <option value="3">Eko Nurcahyanto</option>
		</select>
	</div>
    <div class="form-group text-center">
		<input class="btn btn-primary" id="btnSubmit" type="submit" name="submit" value="submit"/>
	</div>
