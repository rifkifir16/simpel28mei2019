
	<div class="form-group">
		<label>Jumlah Unit Pesawat (Kosongkan jika tidak ada)</label>
		<input class="form-control upes text-right ftpl" type="text" name="upesawat" onkeyup="calTot()" placeholder="Jumlah Unit Pesawat" />
	</div>
    <div class="form-group">
		<label>Biaya Satuan Pesawat (Kosongkan jika tidak ada)</label>
		<input class="form-control bpes text-right ftpl" type="text" name="bpesawat" onkeyup="calTot()" placeholder="Biaya Satuan Pesawat" />
	</div>
	<div class="form-group">
		<label>Jumlah Unit Bus (Kosongkan jika tidak ada)</label>
		<input class="form-control ubus text-right ftpl" type="text" name="ubus" onkeyup="calTot()" placeholder="Jumlah Unit Bus"/>
	</div>
	<div class="form-group">
		<label>Biaya Satuan Bus (Kosongkan jika tidak ada)</label>
		<input class="form-control bbus text-right ftpl" type="text" name="bbus" onkeyup="calTot()" placeholder="Biaya Satuan Bus"/>
	</div>
	<div class="form-group">
		<label>Jumlah Unit Transportasi Lokal (Kosongkan jika tidak ada)</label>
		<input class="form-control ulok text-right ftpl" type="text" name="ulokal" onkeyup="calTot()" placeholder="Jumlah Unit Transportasi Lokal"/>
	</div>
	<div class="form-group">
		<label>Biaya Satuan Transportasi Lokal (Kosongkan jika tidak ada)</label>
		<input class="form-control blok text-right ftpl" type="text" name="blokal" onkeyup="calTot()" placeholder="Biaya Satuan Transportasi Lokal"/>
	</div>
	<div class="form-group">
		<label>Airport Tax (Kosongkan jika tidak ada)</label>
		<input class="form-control tax text-right ftpl" type="text" min="0" name="airtax" onkeyup="calTot()" placeholder="Airport Tax"/>
	</div>
	<div class="form-group">
		<label>Biaya Harian Perjalanan Dinas (Kosongkan jika tidak ada)</label>
		<input class="form-control biaya text-right ftpl" type="text" name="tbharian" placeholder="Biaya Harian Perjalanan Dinas"/>
	</div>