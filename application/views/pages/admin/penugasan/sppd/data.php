<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div id="msgAlert" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
				<p id="msgContent"></p>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data SPPD</h3>
					<?php if ($this->session->userdata('hakAkses') == 'admin') { ?>
						<div class="pull-right">
							<a href="<?= site_url('admin/penugasan/sppd/tambah') ?>" class="btn btn-success">Tambah SPPD</a>

						</div>
					<?php } ?>
					<!--div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9" style="padding:0;">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Pegawai</option>
                  <option value="1">Pegawai Aktif</option>
                  <option value="2">Pegawai Resign</option>
                </select>
              </div>
            </div>
          </form>
        </div-->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="tblUtama" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>ids</th>
								<th>No. SPPD</th>
								<th>Tanggal SPPD</th>
								<th>Nama Pegawai</th>
								<th>Bukti</th>
								<th>Opsi</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>
