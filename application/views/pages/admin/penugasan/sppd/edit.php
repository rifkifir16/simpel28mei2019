<div class="content">
	<div class="row">
		<div class="col-md-4 col-md-push-8">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Petunjuk</h3>
			</div>
			<div class="box-body">
				Petunjuk :
				<ol>
					<li>Anda harus mengisi semua data yang dibutuhkan, karena tombol <b>submit</b> tidak akan berfungsi sebelum anda melengkapi data dengan valid.</li><br />
				</ol>
			</div>
		</div>
		</div>
		<div class="col-md-8 col-md-pull-4">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Tambah SPPD</h3>
			  <div class="pull-right">
				<a href="<?php echo site_url('admin/penugasan/sppd'); ?>" class="btn btn-warning">Batal</a>
			  </div>
			</div>
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#tab_1" data-toggle="tab">Detail SPPD</a></li>
					  <li><a href="#tab_2" data-toggle="tab">Rincian Biaya</a></li>
					  <li><a href="#tab_3" data-toggle="tab">Hasil SPPD</a></li>
					</ul>
					<form method="post" id="fillForm" action="<?php echo site_url('admin/penugasan/sppd/save'); ?>">
						<div class="tab-content">
						  <div class="tab-pane active" id="tab_1">
							<?php include 'edit/datasppd.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_2">
							<?php include 'edit/datarincian.php'; ?>
						  </div><!-- /.tab-pane -->
						  <div class="tab-pane" id="tab_3">
							<?php include 'edit/datahasil.php'; ?>
						  </div><!-- /.tab-pane -->
						</div><!-- /.tab-content -->
					</form>
				  </div><!-- nav-tabs-custom -->
			</div>
		</div>
		</div>
		
	</div>
</div>