<section class="content">
<div class="row">
  <div class="col-md-3">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tambah Kontak</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="fillForm">
        <input type="hidden" name="nip"  class="form-control" value="<?=$nip?>" />
          <div class="form-group">
            <label>Nama Kontak</label>
            <select name="idKontak" id="idKontak" class="form-control ajaxKontak" style="width:100%;" required>
            <option value="">--Ketik untuk mencari--</option>
            
            </select>
          </div>
          <div class="form-group">
            <label>Isi Kontak</label>
            <input type="text" class="form-control" autocomplete="off" name="kontak" id="kontak" placeholder="Isi Kontak" autofocus="true">
          </div>
          <div class="form-group text-center">
            <input type="hidden" value="0" name="idKontakpeg" id="idKontakpeg"/>
            <input type="submit" value="Submit" class="btn btn-primary"/>
            <a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Reset</a>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-9">
	  <div id="msgAlert" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
      <p id="msgContent"></p>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Kontak</h3>
        <div class="col-xs-12 left-text" style="padding:0;">
          <form>
            <div class="form-group">
              <div class="col-xs-3" style="padding:0;">
                <label class="control-label">Tampilkan : </label>
              </div>
              <div class="col-xs-9">
                <select class="form-control" onchange="changeData(this.value)">
                  <option value="0">Semua Kontak</option>
                  <option value="1">Kontak Aktif</option>
                  <option value="2">Kontak Nonaktif</option>
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="tblUtama" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>idb</th>
                <th>idp</th>
                <th>Jenis Kontak</th>
                <th>Nama Kontak</th>
                
                <th>Status</th>
                <th>Pilihan</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  </div>
</section>