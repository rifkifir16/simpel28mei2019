<div class="content">
<section class="invoice">
	
	  <!-- title row -->
	  <div class="row">
		<div class="col-xs-12">
		  <h2 class="page-header">
			<i class="fa fa-globe"></i> PT. Surveyor Indonesia
			<small class="pull-right"></small>
			
		  </h2>
		</div><!-- /.col -->
		
	  </div>
	  <!-- info row -->
	  <div class="row invoice-info">
		<div class="col-sm-4 invoice-col">
		  <address>
			<?=$result['NAMA_PEG']?> ( <?=ucfirst($result['NAMA_KUALIFIKASI'])?> ) <br>
			Dari : <strong><?=$result['ASAL_SPPD']?></strong><br>
			Ke : <strong><?=$result['TUJUAN_SPPD']?></strong><br>
		  </address>
		</div><!-- /.col -->
		<div class="col-sm-4 invoice-col">
		  <b>Berangkat:</b> <?=date_format(date_create($result['TGL_BRKT_SPPD']),'d-m-Y')?><br>
		  <b>Kembali:</b> <?=date_format(date_create($result['TGL_KMBL_SPPD']),'d-m-Y')?><br>
		  <b>Lama Perjalanan:</b> <?=$result['LAMA_SPPD']+1?> hari
		</div><!-- /.col -->
		<div class="col-sm-4 invoice-col">
		  <b>Perjalanan :</b> <?=ucfirst($result['JENIS_SPPD'])?><br>
		  <b>Kendaraan :</b> <?=ucfirst($result['TRANSPORT_SPPD'])?><br>
		</div><!-- /.col -->
	  </div><!-- /.row -->

	  <!-- Table row -->
	  <div class="row">
		<div class="col-xs-12 table-responsive">
		  <table class="table table-bordered">
			<thead>
			  <tr>
				<th class="text-center">No </th>
				<th class="text-center">Keperluan</th>
				<th class="text-center">Unit</th>
				<th class="text-center">Harga Satuan</th>
				<th class="text-center">Subtotal</th>
			  </tr>
			</thead>
			<tbody>
			<?php
			$i=1;
			if($result['UNIT_PESAWAT_SPPD']>0){ ?>
				<tr>
					<td><?=$i?></td>
					<td>Pesawat</td>
					<td class="text-right"><?=number_format($result['UNIT_PESAWAT_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['BIAYA_PESAWAT_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['UNIT_PESAWAT_SPPD']*$result['BIAYA_PESAWAT_SPPD'])?></td>
				</tr>
			<?php 
			$i++;
			} 
			if($result['UNIT_BUS_SPPD']>0){ ?>
				<tr>
					<td><?=$i?></td>
					<td>Bus</td>
					<td class="text-right"><?=number_format($result['UNIT_BUS_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['BIAYA_BUS_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['UNIT_BUS_SPPD']*$result['BIAYA_BUS_SPPD'])?></td>
				</tr>
			<?php 
			$i++;
			} 
			if($result['UNIT_TLOKAL_SPPD']>0){ ?>
				<tr>
					<td><?=$i?></td>
					<td>Transportasi Lokal</td>
					<td class="text-right"><?=number_format($result['UNIT_TLOKAL_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['BIAYA_TLOKAL_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['UNIT_TLOKAL_SPPD']*$result['BIAYA_TLOKAL_SPPD'])?></td>
				</tr>
			<?php 
			$i++;
			} 
			if($result['TAX_BANDARA_SPPD']>0){ ?>
				<tr>
					<td><?=$i?></td>
					<td>Airport Tax</td>
					<td class="text-right">-</td>
					<td class="text-right">Rp <?=number_format($result['TAX_BANDARA_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['TAX_BANDARA_SPPD'])?></td>
				</tr>
			<?php 
			$i++;
			} ?>
				<tr>
					<td><?=$i?></td>
					<td>Uang Saku</td>
					<td class="text-right"><?=number_format($result['LAMA_SPPD']+1)?> hari</td>
					<td class="text-right">Rp <?=number_format($result['BIAYA_HARIAN_SPPD'])?></td>
					<td class="text-right">Rp <?=number_format($result['BIAYA_HARIAN_SPPD']*($result['LAMA_SPPD']+1))?></td>
				</tr>
			<?php
			$i++;
			foreach ($data as $r)
			{
				echo '<tr>';
				echo '<td>'.$i.'</td>';
				echo '<td>'.ucfirst($r['NAMA_RINSPPD']).'</td>';
				echo '<td class="text-right">'.number_format($r['UNIT_RINSPPD']).'</td>';
				echo '<td class="text-right">Rp '.number_format($r['BIAYA_RINSPPD']).'</td>';
				echo '<td class="text-right">Rp '.number_format($r['UNIT_RINSPPD']*$r['BIAYA_RINSPPD']).'</td>';
				echo '</tr>';
				$i++;
			}
			?>
			</tbody>
		  </table>
		</div><!-- /.col -->
	  </div><!-- /.row -->

	  <div class="row">
		<!-- accepted payments column -->
		<div class="col-xs-6">
		  <p class="lead">Uraian:</p>
		  
		  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
			<?=$result['URAIAN_SPPD']?>
		  </p>
		</div><!-- /.col -->
		<div class="col-xs-6">
		  <p class="lead">Total</p>
		  <div class="col-xs-6">
		  <div class="table-responsive">
			<table class="table">
			  <tr>
				<th>Total:</th>
				<td>Rp <?=number_format($result['TOTAL_BIAYA_SPPD'])?></td>
			  </tr>
			</table>
		  </div>
		  </div>
		  <?php
		  $op="admin/penugasan/sppd";
		  if(isset($_GET['opsi'])) $op='sppd/sppd';?>
		  <div class="col-xs-6"><a href="<?=site_url($op)?>" class="btn btn-warning pull-right" >Kembali</a>
		  </div>
		</div><!-- /.col -->
	  </div><!-- /.row -->

	</section><!-- /.content -->
		</div>