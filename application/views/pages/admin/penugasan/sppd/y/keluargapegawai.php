
	<div id="formkeluarga">
    	<div class="form-group keluarga-group">
    		<label>Keluarga</label>
    		<select class="form-control" name="kel[]">
    			<option value="">--Pilih Anggota--</option>
    			<option value="pasangan">Suami/Istri</option>
    			<option value="anak">Anak</option>
    		</select>
    		<input class="form-control" type="text" name="nmkel[]" placeholder="Nama Keluarga" />
    		<input class="form-control" type="text" name="bpjs[]" placeholder="No. BPJS" />
    	</div>
	</div>
	<div class="form-group text-center">
		<a href="#" class="btn btn-info" onclick="dupkeluarga();">Tambah Anggota Keluarga</a>
	</div>
