<section class="content">

		  <div class="row">
    <div class="col-md-12">
    <div class="box">
    	<div class="box-header with-border">
    	  <h3 class="box-title">Filter</h3>
    	</div>
    	<div class="box-body">
            <form method="post" action="<?=base_url('admin/penugasan/rekap/rekap')?>">
				<!--div class="form-group">
					<label>Nomor Surat Perjanjian</label>
					<select class="form-control ajaxjmk" name="nojmk" style="width: 100%;" required>
						<option value="">--Pilih No JMK--</option>
					</select>
				</div-->
				<input type="hidden" name="nojmk" value=""/>
				<div class="form-group">
				  <label for="spp">Surat Permohonan Pemeriksaan</label>
				   <input id="spp" type="text" name="spp" class="inmdl form-control" placeholder="Surat Permohonan Pemeriksaan" />
				</div>
				<div class="form-group">
				  <label for="tg">Tanggal Permohonan</label>
				   <input id="tg" type="date" name="tg" class="inmdl form-control" required="true"/>
				</div>
				<div class="form-group">
				  <label for="bulan">Bulan</label>
				  <select name="bulan" class="inmdl form-control" required>
					<option value="1" >Januari</option>
					<option value="2" >Februari</option>
					<option value="3" >Maret</option>
					<option value="4" >April</option>
					<option value="5" >Mei</option>
					<option value="6" >Juni</option>
					<option value="7" >Juli</option>
					<option value="8" >Agustus</option>
					<option value="9" >September</option>
					<option value="10" >Oktober</option>
					<option value="11" >November</option>
					<option value="11" >Desember</option>
				  </select>
				</div>
				<div class="form-group">
					<label>Tahun</label>
					<input class="form-control" type="number" min="2016" value="2016" name="tahun" required="true"/>
				</div>
				
				<div class="form-group text-center">
					<input class="btn btn-primary" id="btnSubmit" type="submit" name="submit" value="submit"/>
				</div>

			</form>
    	</div>
    </div>
    </div>
    
</div>
          
        </section>