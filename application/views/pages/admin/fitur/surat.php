<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Tambah Surat</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<form id="fillForm" action="<?php echo site_url('admin/fitur/surat/save'); ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nomor Surat</label>
							<input type="text" class="form-control" autocomplete="off" name="nosurat" id="nosurat" placeholder="Nomor Surat" autofocus="true">
						</div>
						<div class="form-group">
							<label>Perihal</label>
							<textarea type="textbox" class="form-control" autocomplete="off" name="perihal" id="perihal" placeholder="Perihal"></textarea>
						</div>
						<div class="form-group">
							<label>Tanggal Surat</label>
							<input type="date" class="form-control" autocomplete="off" name="tanggalsurat" id="tanggalsurat" placeholder="Tanggal Surat">
						</div>
						<div class="form-group">
							<label>Tanggal Mulai</label>
							<input type="date" class="form-control" autocomplete="off" name="tanggalmulai" id="tanggalsurat" placeholder="Tanggal Surat">
						</div>
						<div class="form-group">
							<label>Tanggal Akhir</label>
							<input type="date" class="form-control" autocomplete="off" name="tanggalakhir" id="tanggalsurat" placeholder="Tanggal Surat">
						</div>
						<div class="form-group">
							<label>Scan Surat</label>
							<input type="file" class="form-control" autocomplete="off" name="scansurat" id="scansurat" placeholder="Scan Surat">
						</div>
						<hr>
						<div id="formtenagaahli">
							<div class="tenagaahli-group">
								<div class="form-group ">
									<label>Tenaga Ahli yang dibutuhkan</label>
									<select name="tenagaahli[]" id="ajaxtenaga1" class="form-control">
										// MONGGO EDIT DISINI
										<option value="">--Ketik untuk mencari--</option>
										<option value="Ahli Muda">Ahli Muda</option>
										<?php
										// foreach ($kualifikasi as $kualifikasi)
										// 	echo "<option value=" . $kualifikasi->NAMA_KUALIFIKASI . ">" . $kualifikasi->NAMA_KUALIFIKASI . "</option>"
										?>
									</select>
								</div>
								<div class="form-group ">
									<label>Jabatan</label>
									<select name="jabatan[]" id="ajaxjabatan1" class="form-control ">
										<option value="">--Ketik untuk mencari--</option>
										<?php
										foreach ($jabatan as $jabatan)
											echo "<option value=" . $jabatan->ID_JABATAN . ">" . $jabatan->NAMA_JABATAN . "</option>"
											?>
									</select>
								</div>
								<div class="form-group">
									<label>Jumlah</label>
									<input type="number" class="form-control" autocomplete="off" name="jumlah[]" id="jumlah" placeholder="Jumlah">
								</div>
							</div>
						</div>
						<div class="form-group text-center">
							<a href="#" class="btn btn-info" onclick="duptenagaahli();">Tambah Tenaga Ahli</a>
						</div>

						<div class="form-group text-center">
							<input type="hidden" value="0" name="idKualifikasi" id="idKualifikasi" />
							<input type="submit" value="Submit" class="btn btn-primary" />
							<a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Reset</a>
						</div>
					</form>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<div class="col-md-9">
			<div id="msgAlert" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
				<p id="msgContent"></p>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Surat</h3>
					<!-- <div class="col-xs-12 left-text" style="padding:0;">
						<form>
							<div class="form-group">
								<div class="col-xs-3" style="padding:0;">
									<label class="control-label">Tampilkan : </label>
								</div>
								<div class="col-xs-9">
									<select class="form-control" onchange="changeData(this.value)">
										<option value="0">Semua Kualifikasi</option>
										<option value="1">Kualifikasi Aktif</option>
										<option value="2">Kualifikasi Nonaktif</option>
									</select>
								</div>
							</div>
						</form>
					</div> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="tblUtama" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Id Surat</th>
								<th>No surat</th>
								<th>Tanggal</th>
								<th>Perihal</th>
								<th>Scan Surat</th>
								<th>Detail</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
					<!-- <img src="<?php echo base_url() ?>assets/images/surat/2019-06-30/20190630_061818.jpg"> -->
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>
