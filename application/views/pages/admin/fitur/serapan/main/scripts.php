<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js') ?>"></script>
<script>
	$(document).ready(function() {

		$(".ajaxlokasi").select2({
			ajax: {
				url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						q: params.term
					};
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(obj) {
							return {
								id: obj.id,
								text: obj.text
							};
						})
					};
				},
				cache: true
			},
			"language": "id"
		});
		ajaxpegawai();
	});

	var table;
	$(function() {
		table = $('#tblUtama').DataTable({})

		$('#tblUtama tbody').on('click', '.linkedit', function(e) {
			var data = table.row($(this).parents('tr')).data();
			$('#idAgama').val(data[1]);
			$('#agama').val(data[2]);
			$('#agama').focus();
		});
		$('#tblUtama tbody').on('click', '.linkdelete', function(e) {
			var data = table.row($(this).parents('tr')).data();
			upStatus(data[1], data[4])
		});
	})

	function changeData(index = 0) {
		table.ajax.url('<?= $_ajaxSource . '/' ?>' + index);
		table.ajax.reload();
	}

	function reset() {
		$('#idAgama').val(0);
		$('#agama').val('');

		$('#agama').focus();
	}

	$('#fillForm').submit(function(e) {
		e.preventDefault();
		var form = new FormData($('#fillForm')[0]);

		$.ajax({
			url: "<?php echo site_url('admin/basic/agama/save'); ?>",
			type: 'POST',
			data: form,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				if (data.result == false) {
					errmsg(data.msg);
				} else {
					sucmsg(data.msg);
					reset();
					table.ajax.reload(null, false);
				}
			}
		});
	});

	function upStatus(id, status) {

		$.ajax({
			url: "<?php echo site_url('admin/basic/agama/upstatus'); ?>",
			type: "GET",
			dataType: 'json',
			data: {
				id: id,
				status: status
			},
			success: function(data) {
				if (data.result == false) {
					errmsg(data.msg);
				} else {
					sucmsg(data.msg);
					reset();
					table.ajax.reload(null, false);
				}

			}

		});
	}

	$('#msgAlert').hide();
	$('#msgAlert').on("close.bs.alert", function() {
		$('#msgAlert').hide();
		return false;
	});

	function sucmsg(msg) {
		$('#msgAlert').show();
		$('#msgAlert').removeClass('alert-danger');
		$('#msgAlert').addClass('alert-success');

		$('#msgIcon').removeClass('fa-ban');
		$('#msgIcon').addClass('fa-check');

		$('#msgContent').html(msg);
	}

	function errmsg(msg) {
		$('#msgAlert').show();
		$('#msgAlert').removeClass('alert-success');
		$('#msgAlert').addClass('alert-danger');

		$('#msgIcon').addClass('fa-ban');
		$('#msgIcon').removeClass('fa-check');

		$('#msgContent').html(msg);
	}
</script>
