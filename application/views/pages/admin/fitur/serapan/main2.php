<section class="content">
	<div class="row">
		<div class="col-md-4">
			<div class="box" style="height: 300px;">
				<div class="box-header with-border">
					<h3 class="box-title">Filter</h3>
				</div>
				<div class="box-body">
					<form method="post" action="<?php echo base_url() ?>admin/fitur/serapan_penggajian">
						<div class="form-group">
							<label>Provinsi</label>
							<select class="form-control" name="lokasi" style="width: 100%;" required>
								<option value="">--Ketik untuk mencari--</option>
								<?php foreach ($propinsi as $prop) { ?>
									<option value="<?= $prop->NAMA_PROP ?>"><?= $prop->NAMA_PROP ?></option>
								<?php } ?>
							</select>
						</div>

						<div class="form-group">
							<label>Tahun</label>
							<input class="form-control" type="number" min="2016" value="0" name="tahun" required="true" />
						</div>

						<div class="form-group text-center">
							<input class="btn btn-primary" id="btnSubmit" type="submit" name="submit" value="submit" />
						</div>

					</form>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="box" style="height: 300px;">
				<div class="box-header with-border">
					<h3 class="box-title">Chart</h3>

					<?php
					$month = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
					if (!empty($chart)) {
						foreach ($chart as $hasil) {
							$kualifikasi1[] = $hasil->gaji;
							$bulan[] = $month[$hasil->BULAN_GAJI];
						}
					} else { }
					?>
				</div>
				<div class="box-body">
					<div class="col-md-12">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption caption-md font-red-sunglo">
									<i class="icon-bar-chart theme-font hide"></i>
								</div>
							</div>
							<canvas id="myChart" height="100"></canvas>
							<script>
								var ctx = document.getElementById('myChart').getContext('2d');
								var myChart = new Chart(ctx, {
									type: 'bar',
									data: {
										labels: <?php echo json_encode($bulan); ?>,
										datasets: [{
											label: 'Total Gaji',
											data: <?php echo json_encode($kualifikasi1); ?>,
											backgroundColor: [
												'rgba(255, 99, 132, 0.2)',
												'rgba(54, 162, 235, 0.2)',
												'rgba(255, 206, 86, 0.2)',
												'rgba(75, 192, 192, 0.2)',
												'rgba(153, 102, 255, 0.2)',
												'rgba(255, 159, 64, 0.2)'
											],
											borderColor: [
												'rgba(255, 99, 132, 1)',
												'rgba(54, 162, 235, 1)',
												'rgba(255, 206, 86, 1)',
												'rgba(75, 192, 192, 1)',
												'rgba(153, 102, 255, 1)',
												'rgba(255, 159, 64, 1)'
											],
											borderWidth: 1
										}]
									},
									options: {
										scales: {
											yAxes: [{
												ticks: {
													beginAtZero: true
												}
											}]
										}
									}
								});
							</script>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="Modalid" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Export Excel</h4>
				</div>
				<div class="modal-body">
					<form method="post" action="<?= base_url('admin/fitur/serapan_penggajian/rekap2') ?>">
						<div class="form-group">
							<label for="hk">Provinsi</label>
							<select class="form-control" name="lokasi" style="width: 100%;" required>
								<option value="">--Ketik untuk mencari--</option>
								<?php foreach ($propinsi as $prop) { ?>
									<option value="<?= $prop->NAMA_PROP ?>"><?= $prop->NAMA_PROP ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="hk">Tahun</label>
							<select name="tahun" class="form-control">
								<option value="">--Pilih Tahun--</option>
								<?php
								$thn_skr = date('Y') + 1;
								for ($x = $thn_skr; $x >= 2010; $x--) {
									?>
									<option value="<?php echo $x ?>"><?php echo $x ?></option>
								<?php
								}
								?>
							</select>
						</div>
						<input type="submit" value="Export" class="btn btn-success" />
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border" style="background-color: #3C8DBC; color: #ffffff;">
					<div class="pull-left">
						<h3 class="box-title">Data Gaji</h3>
					</div>
					<div class="pull-right">
						<a class="btn btn-success" data-toggle="modal" data-target="#Modalid">Export Excel</a>
					</div>
				</div>
				<div class="box-body">
					<table id="tblUtama" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Kualifikasi</th>
								<th>Jumlah Pembayaran incl ppn</th>
								<th>ID Lokasi</th>
								<th>Provinsi</th>
								<th>Bulan</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							if (!empty($tabel)) {
								foreach ($tabel as $row) {
									?>
									<tr>
										<td>
											<?php echo $no ?>
										</td>
										<td>
											<?php echo $row->KUALIFIKASI_GAJI ?>
										</td>
										<td>
											<?php echo number_format($row->gaji) ?>
										</td>
										<td>
											<?php echo $row->ID_LOKASI ?>
										</td>
										<td>
											<?php echo $row->NAMA_PROP ?>
										</td>
										<td>
											<?php
											$month = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
											echo $month[$row->BULAN_GAJI] ?>
										</td>
									</tr>
									<?php
									$no++;
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</section>
