<section class="content">
	<div class="row">
		<div class="col-md-3" id="boxform">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Update Data</h3>
					<div class="pull-right">
						<a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Batal</a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<form id="fillForm">
						<div class="form-group">
							<label>Lokasi</label>
							<select class="form-control ajaxlokasi" id="lokasi" name="lokasi" style="width: 100%;" required>
								<option value="">--Pilih Lokasi--</option>
							</select>
						</div>

						<div class="form-group">
							<label for="hk">Jumlah Hari Kerja</label>
							<input type="hidden" name="id" id="id" class="form-control" value="0" />
							<input type="hidden" name="nip" id="nip" class="form-control" value="0" />
							<input id="hk" type="number" name="hk" step="0.01" class="inmdl form-control" placeholder="Jumlah Hari Kerja" value="0" required="true" />
						</div>

						<div class="form-group" style="padding-bottom: 20px;">
							<label for="kh">Jumlah Kehadiran</label>
							<input id="kh" type="number" name="kh" step="0.01" class="inmdl form-control" placeholder="Jumlah Kehadiran" value="0" required="true" />
						</div>

						<div class="form-group" style="padding-bottom: 20px;">
							<label for="index">Index</label>
							<input id="index" type="number" name="index" step="0.001" class="inmdl form-control" placeholder="Index" value="0" required="true" />
						</div>
						<div class="form-group" style="padding-bottom: 20px;">
							<label for="pph">PPh 21</label>
							<input id="pph" type="number" name="pph" class="inmdl form-control" placeholder="PPh 21" value="0" required="true" />
						</div>

						<div class="form-group" style="padding-bottom: 20px;">
							<label for="jamsostek">Jamsostek</label>
							<input id="jamsostek" type="number" name="jamsostek" class="inmdl form-control" placeholder="Jamsostek" value="0" required="true" />
						</div>

						<div class="form-group" style="padding-bottom: 20px;">
							<label for="koreksi">Koreksi</label>
							<input id="koreksi" type="number" name="koreksi" class="inmdl form-control" placeholder="Koreksi" value="0" required="true" />
						</div>
						<div class="form-group text-center">
							<input type="submit" value="Submit" class="btn btn-primary" />
							<a href="javascript:void(0)" class="btn btn-danger" onClick="reset()">Batal</a>
						</div>
					</form>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<div class="col-md-12" id="boxdata">
			<div id="msgAlert" class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i id="msgIcon" class="icon fa fa-ban"></i> Laporan</h4>
				<p id="msgContent"></p>
			</div>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Penagihan</h3>
					<div class="pull-right">
						<a class="btn btn-success" data-toggle="modal" data-target="#Modalid">Export Excel</a>
						<div id="Modalid" class="modal fade" role="dialog">
							<div class="modal-dialog">
								<!-- konten modal-->
								<div class="modal-content">
									<!-- heading modal -->
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Export Excel</h4>
									</div>
									<div class="modal-body">
										<form method="post" action="<?= base_url('admin/fitur/penagihan/rekap2') ?>">
											<div class="form-group">
												<label for="hk">Bulan</label>
												<select class="form-control" name="bulan">
													<option selected="selected">--Bulan--</option>
													<?php
													$bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
													$jlh_bln = count($bulan);
													for ($c = 0; $c < $jlh_bln; $c += 1) {
														$d = $c + 1;
														echo "<option value=$d> $bulan[$c] </option>";
													}
													?>
												</select>
											</div>
											<div class="form-group">
												<label for="hk">Tahun</label>
												<select name="tahun" class="form-control">
													<option value="">--Pilih Tahun--</option>
													<?php
													$thn_skr = date('Y') + 1;
													for ($x = $thn_skr; $x >= 2010; $x--) {
														?>
														<option value="<?php echo $x ?>"><?php echo $x ?></option>
													<?php
													}
													?>
												</select>
											</div>
											<input type="submit" value="Export" class="btn btn-success" />
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal -->
						<div id="myModal" class="modal fade" role="dialog">
							<div class="modal-dialog">
								<!-- konten modal-->
								<div class="modal-content">
									<!-- heading modal -->
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Upload data gaji pegawai</h4>
									</div>
									<!-- body modal -->
									<div class="modal-body">
										<form id="fillForm1">
											<div class="form-group">
												<label>Pegawai</label>
												<select class="form-control ajaxpegawai" id="nip1" name="nip1" style="width: 100%;">
													<option value="">--Pilih Pegawai--</option>
												</select>
											</div>
											<div class="form-group">
												<label for="hk">Kualifikasi</label>
												<input id="kulifikasi1" type="text" class="inmdl form-control" placeholder="Jumlah Hari Kerja" value=" " disabled />
											</div>
											<div class="form-group">
												<label>Lokasi</label>
												<select class="form-control ajaxlokasi1" id="lokasi1" name="lokasi1" style="width: 100%;">
													<option value="">--Pilih Lokasi--</option>
												</select>
											</div>
											<div class="form-group">
												<label for="hk">Bulan</label>
												<select class="form-control" name="bulan">
													<option selected="selected">--Bulan--</option>
													<?php
													$bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
													$jlh_bln = count($bulan);
													for ($c = 0; $c < $jlh_bln; $c += 1) {
														$d = $c + 1;
														echo "<option value=$d> $bulan[$c] </option>";
													}
													?>
												</select>
											</div>
											<div class="form-group">
												<label for="hk">Tahun</label>
												<select name="tahun" class="form-control">
													<option value="">--Pilih Tahun--</option>
													<?php
													$thn_skr = date('Y') + 1;
													for ($x = $thn_skr; $x >= 2010; $x--) {
														?>
														<option value="<?php echo $x ?>"><?php echo $x ?></option>
													<?php
													}
													?>
												</select>
											</div>
											<div class="form-group">
												<label>Tanggal Penagihan Gaji</label>
												<input class="form-control" type="date" name="tglapgaji" required="true" />
											</div>
											<div class="form-group">
												<label>Tanggal Gaji</label>
												<input class="form-control" type="date" name="tglgaji" required="true" />
											</div>
											<div class="form-group">
												<label for="hk">Jumlah Hari Kerja</label>
												<input id="hk1" type="number" name="hk1" step="0.01" class="inmdl form-control" placeholder="Jumlah Hari Kerja" value="0" />
											</div>

											<div class="form-group" style="padding-bottom: 20px;">
												<label for="kh">Jumlah Kehadiran</label>
												<input id="kh1" type="number" name="kh1" step="0.01" class="inmdl form-control" placeholder="Jumlah Kehadiran" value="0" />
											</div>

											<div class="form-group" style="padding-bottom: 20px;">
												<label for="index">Index</label>
												<input id="index1" type="number" name="index1" step="0.001" class="inmdl form-control" placeholder="Index" value="0" disabled />
												<input id="index2" type="hidden" name="index2" step="0.001" class="inmdl form-control" placeholder="Index" value="0" />
											</div>
											<div class="form-group" style="padding-bottom: 20px;">
												<label for="pph">Harga Kualifikasi</label>
												<input id="gajikulifikasi1" type="number" class="inmdl form-control pph1" value="0" disabled />
											</div>
											<div class="form-group" style="padding-bottom: 20px;">
												<label for="pph">PPh 21</label>
												<input id="pph1" type="number" name="pph1" class="inmdl form-control pph1" placeholder="PPh 21" value="0" />
											</div>

											<div class="form-group" style="padding-bottom: 20px;">
												<label for="jamsostek">Jamsostek</label>
												<input id="jamsostek1" type="number" name="jamsostek1" class="inmdl form-control jamsostek1" placeholder="Jamsostek" value="0" />
											</div>

											<div class="form-group" style="padding-bottom: 20px;">
												<label for="koreksi">Koreksi</label>
												<input id="koreksi1" type="number" name="koreksi1" class="inmdl form-control koreksi1" placeholder="Koreksi" value="0" />
											</div>
											<div class="form-group" style="padding-bottom: 20px;">
												<label for="koreksi">Pengurangan Gaji</label>
												<input id="pengurangangaji1" type="number" class="inmdl form-control koreksi1" value="0" disabled />
											</div>
											<div class="form-group" style="padding-bottom: 20px;">
												<label for="koreksi">THP Gaji</label>
												<input id="thpgaji1" type="number" name="thpgaji1" class="inmdl form-control thpgaji1" placeholder="Koreksi" value="0" disabled />
												<input id="thpgaji2" type="hidden" name="thpgaji2" class="inmdl form-control thpgaji2" placeholder="Koreksi" value="0" />
											</div>
											<input id="pengurangangaji" type="hidden" name="pengurangangaji" class="inmdl form-control pengurangangaji" />
											<input id="kulifikasi" type="hidden" name="kualifikasi" class="inmdl form-control kulifikasi" />
											<input id="kualifikasigaji" type="hidden" name="kualifikasigaji" class="inmdl form-control kualifikasigaji" />
											<input id="UPAH_TARIF" type="hidden" name="UPAH_TARIF" class="inmdl form-control " />
											<input id="TUMUM_TARIF" type="hidden" name="TUMUM_TARIF" class="inmdl form-control " />
											<input id="TMAKAN_TARIF" type="hidden" name="TMAKAN_TARIF" class="inmdl form-control " />
											<input id="TKOMUNIKASI_TARIF" type="hidden" name="TKOMUNIKASI_TARIF" class="inmdl form-control " />

											<input type="submit" value="Submit" class="btn btn-primary" />
										</form>
									</div>
									<!-- footer modal -->
									<div class="modal-footer">

										<button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 left-text" style="padding:0;">
						<form>
							<div class="form-group">
								<div class="col-xs-2" style="padding:0;">
									<label class="control-label">Tampilkan : </label>
								</div>
								<div class="col-xs-5 ">
									<select class="form-control tampilbulan">
										<option value="0">--Pilih Bulan--</option>
										<?php
										$bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
										$jlh_bln = count($bulan);
										for ($c = 0; $c < $jlh_bln; $c += 1) {
											$d = $c + 1;
											echo "<option value=$d> $bulan[$c] </option>";
										}
										?>
									</select>
								</div>
								<div class="col-xs-5">
									<select class="form-control tampiltahun">
										<option value="0">--Pilih Tahun--</option>
										<?php
										$thn_skr = date('Y') + 1;
										for ($x = $thn_skr; $x >= 2010; $x--) {
											?>
											<option value="<?php echo $x ?>"><?php echo $x ?></option>
										<?php
										}
										?>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="tblUtama" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Penugasan</th>
								<th>Kode Lokasi</th>
								<th>Tagihan per bulan</th>
								<th>Provinsi</th>
								<th>Kualifikasi</th>
								<th>Harga Satuan (Incl Ppn)</th>
								<th>Jumlah Hari Kerja</th>
								<th>Jumlah Hari Kehadiran</th>
								<th>MM</th>
								<th>Jumlah Pembayaran incl PPN</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>
