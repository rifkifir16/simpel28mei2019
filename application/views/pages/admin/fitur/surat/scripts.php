<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js') ?>"></script>
<script>
	var table;
	$('.ajaxtenaga').select2({
		ajax: {
			url: "<?php echo site_url('admin/basic/jabatan/ajax'); ?>",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: $.map(data, function(obj) {
						return {
							id: obj.id,
							text: obj.text
						};
					})
				};
			},
			cache: true
		},
		"language": "id"
	});
	$('.ajaxjabatan').select2({
		ajax: {
			url: "<?php echo site_url('admin/fitur/surat/ajax'); ?>",
			dataType: 'json',
			delay: 250,
			data: function(params) {
				return {
					q: params.term
				};
			},
			processResults: function(data) {
				return {
					results: $.map(data, function(obj) {
						return {
							id: obj.id,
							text: obj.text
						};
					})
				};
			},
			cache: true
		},
		"language": "id"
	});

	function duptenagaahli() {
		$(".tenagaahli-group").clone().removeClass('tenagaahli-group').appendTo("#formtenagaahli");
	}
	$(function() {
		table = $('#tblUtama').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "<?= $_ajaxSource ?>",
				"type": "POST"
			},
			"columnDefs": [{
				"targets": -1,
				"defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
				"render": function(data, type, full, meta) {
					//if(data==null) return '<a href="#">Terdapat kesalahan pada data ini</a>';
					var pesan = 'Aktifkan';
					//if(data==1) pesan='Nonaktifkan';
					return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkdetail">Detail Surat</a></li></ul></div>';
				}
			}],
			"language": {
				"lengthMenu": "Menampilkan _MENU_ data per halaman",
				"zeroRecords": "Data masih kosong atau tidak ditemukan",
				"info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
				"infoEmpty": "Data masih kosong"
			}
		})

		$('#tblUtama tbody').on('click', '.linkedit', function(e) {
			var data = table.row($(this).parents('tr')).data();
			$('#idKualifikasi').val(data[1]);
			$('#kualifikasi').val(data[2]);
			var str = data[3];
			var res = str.replace(/,/g, "");
			//console.log(res);

			$('#harga').val(res);
			$('#kualifikasi').focus();
		});
		$('#tblUtama tbody').on('click', '.linkdetail', function(e) {
			var data = table.row($(this).parents('tr')).data();
			window.location.href = "<?= base_url('admin/fitur/surat/detail') ?>/" + data[1] + ".html";
		});

		$('#tblUtama tbody').on('click', '.linkdelete', function(e) {
			var data = table.row($(this).parents('tr')).data();
			upStatus(data[1], data[5])
		});
	})

	function changeData(index = 0) {
		table.ajax.url('<?= $_ajaxSource . ' / ' ?>' + index);
		table.ajax.reload();
	}

	function reset() {
		$('#nosurat').val('');
		$('#tanggalsurat').val('');
		$('#scansurat').val('');
		$('#ajaxProp').val('');
	}

	// $('#fillForm').submit(function(e) {
	// 	e.preventDefault();
	// 	var form = new FormData($('#fillForm')[0]);
	// 	var nosurat = $('#nosurat').val();
	// 	var perihal = $('#perihal').val();
	// 	var tanggalsurat = $('#tanggalsurat').val();
	// 	// var scansurat = $('#scansurat').val();
	// 	$.ajax({
	// 		url: "<?php echo site_url('admin/fitur/surat/save'); ?>",
	// 		type: 'POST',
	// 		// data: form,
	// 		data: {
	// 			'nosurat': nosurat,
	// 			'perihal': perihal,
	// 			'tanggalsurat': tanggalsurat,
	// 			// 'scansurat': scansurat
	// 		},
	// 		dataType: 'json',
	// 		cache: false,
	// 		contentType: false,
	// 		processData: false,
	// 		success: function(data) {
	// 			if (data.result == false) {
	// 				errmsg(data.msg);
	// 			} else {
	// 				sucmsg(data.msg);
	// 				reset();
	// 				table.ajax.reload(null, false);
	// 			}
	// 		}
	// 	});
	// });

	function upStatus(id, status) {

		$.ajax({
			url: "<?php echo site_url('admin/basic/Kualifikasi/upstatus'); ?>",
			type: "GET",
			dataType: 'json',
			data: {
				id: id,
				status: status
			},
			success: function(data) {
				if (data.result == false) {
					errmsg(data.msg);
				} else {
					sucmsg(data.msg);
					reset();
					table.ajax.reload(null, false);
				}

			}

		});
	}

	$('#msgAlert').hide();
	$('#msgAlert').on("close.bs.alert", function() {
		$('#msgAlert').hide();
		return false;
	});

	function sucmsg(msg) {
		$('#msgAlert').show();
		$('#msgAlert').removeClass('alert-danger');
		$('#msgAlert').addClass('alert-success');

		$('#msgIcon').removeClass('fa-ban');
		$('#msgIcon').addClass('fa-check');

		$('#msgContent').html(msg);
	}

	function errmsg(msg) {
		$('#msgAlert').show();
		$('#msgAlert').removeClass('alert-success');
		$('#msgAlert').addClass('alert-danger');

		$('#msgIcon').addClass('fa-ban');
		$('#msgIcon').removeClass('fa-check');

		$('#msgContent').html(msg);
	}
</script>
