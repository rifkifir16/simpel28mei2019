<section class="content">
	<div class="row">

		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Surat</h3>
				</div>
				<style>
					blink {
						animation: blinker 0.6s linear infinite;
						color: #1c87c9;
					}

					@keyframes blinker {
						50% {
							opacity: 0;
						}
					}

					.blink-one {
						animation: blinker-one 1s linear infinite;
					}

					@keyframes blinker-one {
						0% {
							opacity: 0;
						}
					}

					.blink-two {
						animation: blinker-two 1.4s linear infinite;
					}

					@keyframes blinker-two {
						100% {
							opacity: 0;
						}
					}
				</style>
				<div class="box-body">
					<div>
						<center>
							<h2>Waktu kurang <blink><b><?= $selisih ?></b></blink> hari</h2>
						</center>
					</div>
					<div class="table-responsive">
						<table class="table table-borderless">
							<tr>
								<td><b>Nomor Surat</b></td>
								<td><?= $detailsurat[0]['NO_SURAT'] ?></td>
							</tr>
							<tr>
								<td><b>Tanggal</b></td>
								<td><?= $detailsurat[0]['TANGGAL'] ?></td>
							</tr>
							<tr>
								<td><b>Tanggal Mulai</b></td>
								<td><?= $detailsurat[0]['TANGGAL_MULAI'] ?></td>
							</tr>
							<tr>
								<td><b>Tanggal Akhir</b></td>
								<td><?= $detailsurat[0]['TANGGAL_AKHIR'] ?></td>
							</tr>
							<tr>
								<td><b>Perihal</b></td>
								<td><?= $detailsurat[0]['PERIHAL'] ?></td>
							</tr>
							<tr>
								<td><b>Scan Surat</b></td>
								<td><?= "<a target='_blank' href='" . $detailsurat[0]['SCAN_SURAT'] . "'>Download</a>" ?></td>
							</tr>
							<HR>
							<?php foreach ($detailtenagaahli as $ahli) {
								// echo $ahli['JUMLAH']
								?>
								<tr>
									<td><b>Nama Jabatan</b></td>
									<td><?= $ahli['NAMA_JABATAN'] ?></td>
								</tr>
								<tr>
									<td><b>Nama Kualifikasi</b></td>
									<td><?= $ahli['ID_KUALIFIKASI'] ?></td>
								</tr>
								<tr>
									<td><b>Jumlah</b></td>
									<td><?= $ahli['JUMLAH'] ?></td>
								</tr>
								<tr>
									<td><b>Terpilih</b></td>
									<td><?= $ahli['TERPILIH'] ?></td>
								</tr>
								<tr>
									<td colspan="2">
										<form method="POST" action="<?= base_url('admin/fitur/surat/updatetenagaahli') ?> ">
											<input name="ID_SURAT" type="hidden" value="<?= $detailsurat[0]['ID_SURAT'] ?>" />
											<input name="ID_TENAGAAHLI" type="hidden" value="<?= $ahli['ID_TENAGAAHLI'] ?>" />
											<input name="TERPILIH" type="hidden" value="<?= $ahli['TERPILIH'] ?>" />
											<?php if ($ahli['TERPILIH'] >= $ahli['JUMLAH']) {
												?>
												<center><input type="submit" value="Sudah terpenuh-i" class="btn btn-danger" disabled /></center>
											<?php
											} else { ?>
												<center><input type="submit" value="Update" class="btn btn-primary" /></center>
											<?php } ?>

										</form>
									</td>
								</tr>
							<?php } ?>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
