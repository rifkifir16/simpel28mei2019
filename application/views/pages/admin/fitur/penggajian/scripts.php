<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js') ?>"></script>
<script>
	var table;
	$("#boxform").hide();
	$(function() {

		$(document).ready(function() {

			$(".ajaxlokasi1").select2({
				ajax: {
					url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
					dataType: 'json',
					delay: 250,
					data: function(params) {
						return {
							q: params.term
						};
					},
					processResults: function(data) {
						return {
							results: $.map(data, function(obj) {
								return {
									id: obj.id,
									text: obj.kode_lokasi
								};
							})
						};
					},
					cache: true
				},
				"language": "id"
			});
			$(".updateajaxlokasi1").select2({
				ajax: {
					url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
					dataType: 'json',
					delay: 250,
					data: function(params) {
						return {
							q: params.term
						};
					},
					processResults: function(data) {
						return {
							results: $.map(data, function(obj) {
								return {
									id: obj.id,
									text: obj.kode_lokasi
								};
							})
						};
					},
					cache: true
				},
				"language": "id"
			});
			$(".kualifikasi").select2({
				ajax: {
					url: "<?php echo site_url('admin/basic/kualifikasi/ajax'); ?>",
					dataType: 'json',
					delay: 250,
					data: function(params) {
						return {
							q: params.term
						};
					},
					processResults: function(data) {
						return {
							results: $.map(data, function(obj) {
								return {
									id: obj.id,
									text: obj.text
								};
							})
						};
					},
					cache: true
				},
				"language": "id"
			});
			$(".kualifikasi").change(function() {
				var id = $(this).children("option:selected").val();
				$.ajax({
					url: "<?php echo site_url('admin/fitur/penggajian/kualifikasi'); ?>",
					type: 'POST',
					dataType: 'json',
					data: {
						id: id
					},
					success: function(data) {
						$("#gajikulifikasi1").val(data[0].HARGA_KUALIFIKASI);
						$("#kualifikasigaji").val(data[0].HARGA_KUALIFIKASI);
						$("#kulifikasi1").val(data[0].NAMA_KUALIFIKASI);
						$("#kulifikasi").val(data[0].NAMA_KUALIFIKASI);
					}
				});
			});
			$(".updatekualifikasi").select2({
				ajax: {
					url: "<?php echo site_url('admin/basic/kualifikasi/ajax'); ?>",
					dataType: 'json',
					delay: 250,
					data: function(params) {
						return {
							q: params.term
						};
					},
					processResults: function(data) {
						return {
							results: $.map(data, function(obj) {
								return {
									id: obj.id,
									text: obj.text
								};
							})
						};
					},
					cache: true
				},
				"language": "id"
			});
			$(".updatekualifikasi").change(function() {
				var id = $(this).children("option:selected").val();
				$.ajax({
					url: "<?php echo site_url('admin/fitur/penggajian/kualifikasi'); ?>",
					type: 'POST',
					dataType: 'json',
					data: {
						id: id
					},
					success: function(data) {
						$("#updategajikulifikasi1").val(data[0].HARGA_KUALIFIKASI);
						$("#updatekualifikasigaji").val(data[0].HARGA_KUALIFIKASI);
						$("#updatekulifikasi1").val(data[0].NAMA_KUALIFIKASI);
						$("#updatekulifikasi").val(data[0].NAMA_KUALIFIKASI);
					}
				});
			});

			$(".ajaxlokasi1").change(function() {
				var id = $(this).children("option:selected").val();
				$.ajax({
					url: "<?php echo site_url('admin/fitur/penggajian/lokasi'); ?>",
					type: 'POST',
					dataType: 'json',
					data: {
						id: id
					},
					success: function(data) {
						$("#index1").val(data[0].INDEX_PROP);
						$("#index2").val(data[0].INDEX_PROP);
						console.log(data[0].INDEX_PROP);
					}
				});
				// console.log('coba ' + $(this).children("option:selected").val())
			});
			$(".updateajaxlokasi1").change(function() {
				var id = $(this).children("option:selected").val();
				$.ajax({
					url: "<?php echo site_url('admin/fitur/penggajian/lokasi'); ?>",
					type: 'POST',
					dataType: 'json',
					data: {
						id: id
					},
					success: function(data) {
						$("#updateindex1").val(data[0].INDEX_PROP);
						$("#updateindex2").val(data[0].INDEX_PROP);
						console.log(data[0].INDEX_PROP);
					}
				});
				// console.log('coba ' + $(this).children("option:selected").val())
			});

			$(".ajaxpegawai").change(function() {
				var id = $(this).children("option:selected").val();
				var pph = parseInt($("#pph1").val());
				var jamsostek = parseInt($("#jamsostek1").val());
				var koreksi = parseInt($("#koreksi1").val());
				var thpgaji = parseInt($("#thpgaji1").val());
				var hasilpengurangan = pph + jamsostek + koreksi;
				$.ajax({
					url: "<?php echo site_url('admin/fitur/penggajian/pegawai'); ?>",
					type: 'POST',
					dataType: 'json',
					data: {
						id: id
					},
					success: function(data) {

						if (typeof(data) != "undefined" && data !== null) {
							var hasil = data[0].JUMLAH_TARIF - hasilpengurangan;
							$("#pengurangangaji").val(hasilpengurangan);
							$("#pengurangangaji1").val(hasilpengurangan);
							// $("#gajikulifikasi1").val(data[0].HARGA_KUALIFIKASI);
							// $("#kualifikasigaji").val(data[0].HARGA_KUALIFIKASI);
							// $("#kulifikasi1").val(data[0].NAMA_KUALIFIKASI);
							// $("#kulifikasi").val(data[0].NAMA_KUALIFIKASI);
							$("#UPAH_TARIF").val(data[0].UPAH_TARIF);
							$("#TUMUM_TARIF").val(data[0].TUMUM_TARIF);
							$("#TMAKAN_TARIF").val(data[0].TMAKAN_TARIF);
							$("#TKOMUNIKASI_TARIF").val(data[0].TKOMUNIKASI_TARIF);
							// $("#thpgaji1").val(hasil);
							// $("#thpgaji2").val(hasil);
							var pph = parseInt($("#pph1").val());
							var jamsostek = parseInt($("#jamsostek1").val());
							var koreksi = parseInt($("#koreksi1").val());
							var thpgaji = parseInt($("#thpgaji1").val());
							var hk = parseInt($("#hk1").val());
							var kh = parseInt($("#kh1").val());
							var UPAH_TARIF = parseInt($("#UPAH_TARIF").val());
							var TUMUM_TARIF = parseInt($("#TUMUM_TARIF").val());
							var TMAKAN_TARIF = parseInt($("#TMAKAN_TARIF").val());
							var TKOMUNIKASI_TARIF = parseInt($("#TKOMUNIKASI_TARIF").val());
							var hasilpengurangan = pph + jamsostek + koreksi;
							var mmkasar = kh / hk;
							var mm = mmkasar.toFixed(2);
							var hasil1 = (mm * (UPAH_TARIF + TUMUM_TARIF + TMAKAN_TARIF)) + TKOMUNIKASI_TARIF;
							var hasil = hasil1 - hasilpengurangan;
							$("#pengurangangaji").val(hasilpengurangan);
							$("#pengurangangaji1").val(hasilpengurangan);
							$("#thpgaji1").val(hasil);
							$("#thpgaji2").val(hasil);
						} else {
							alert('Maaf ada data yang kurang, coba cek data pegawai');
						}
						// console.log(data);

					}
				});
				// console.log(id);
			});

			$("#pph1 ,#jamsostek1 ,#koreksi1, #ajaxpegawai,#hk1 ,#kh1, #nip1").change(function() {
				var pph = parseInt($("#pph1").val());
				var jamsostek = parseInt($("#jamsostek1").val());
				var koreksi = parseInt($("#koreksi1").val());
				var thpgaji = parseInt($("#thpgaji1").val());
				var hk = parseInt($("#hk1").val());
				var kh = parseInt($("#kh1").val());
				var UPAH_TARIF = parseInt($("#UPAH_TARIF").val());
				var TUMUM_TARIF = parseInt($("#TUMUM_TARIF").val());
				var TMAKAN_TARIF = parseInt($("#TMAKAN_TARIF").val());
				var TKOMUNIKASI_TARIF = parseInt($("#TKOMUNIKASI_TARIF").val());
				var hasilpengurangan = pph + jamsostek + koreksi;
				var mmkasar = kh / hk;
				var mm = mmkasar;
				var hasil1 = (mm * (UPAH_TARIF + TUMUM_TARIF + TMAKAN_TARIF)) + TKOMUNIKASI_TARIF;
				console.log(hasil1);
				var hasil = hasil1 - hasilpengurangan;
				$("#pengurangangaji").val(hasilpengurangan);
				$("#pengurangangaji1").val(hasilpengurangan);
				$("#thpgaji1").val(hasil);
				$("#thpgaji2").val(hasil);
				console.log(hasil);
			});
			$("#updatepph1 ,#updatejamsostek1 ,#updatekoreksi1, #updateajaxpegawai,#hupdatek1 ,#updatekh1, #updatenip1").change(function() {
				var pph = parseInt($("#updatepph1").val());
				var jamsostek = parseInt($("#updatejamsostek1").val());
				var koreksi = parseInt($("#updatekoreksi1").val());
				var thpgaji = parseInt($("#updatethpgaji1").val());
				var hk = parseInt($("#updatehk1").val());
				var kh = parseInt($("#updatekh1").val());
				var UPAH_TARIF = parseInt($("#updateUPAH_TARIF").val());
				var TUMUM_TARIF = parseInt($("#updateTUMUM_TARIF").val());
				var TMAKAN_TARIF = parseInt($("#updateTMAKAN_TARIF").val());
				var TKOMUNIKASI_TARIF = parseInt($("#updateTKOMUNIKASI_TARIF").val());
				var hasilpengurangan = pph + jamsostek + koreksi;
				var mmkasar = kh / hk;
				var mm = mmkasar;
				var hasil1 = (mm * (UPAH_TARIF + TUMUM_TARIF + TMAKAN_TARIF)) + TKOMUNIKASI_TARIF;
				console.log(hasil1);
				var hasil = hasil1 - hasilpengurangan;
				$("#updatepengurangangaji").val(hasilpengurangan);
				$("#updatepengurangangaji1").val(hasilpengurangan);
				$("#updatethpgaji1").val(hasil);
				$("#updatethpgaji2").val(hasil);
				console.log(hasil);
			});

			// $("#hk1 ,#kh1").change(function() {
			// 	var hk = parseInt($("#hk1").val());
			// 	var kh = parseInt($("#kh1").val());
			// 	var UPAH_TARIF = parseInt($("#UPAH_TARIF").val());
			// 	var TUMUM_TARIF = parseInt($("#TUMUM_TARIF").val());
			// 	var TMAKAN_TARIF = parseInt($("#TMAKAN_TARIF").val());
			// 	var TKOMUNIKASI_TARIF = parseInt($("#TKOMUNIKASI_TARIF").val());
			// 	var hasil = ((kh / hk) * (UPAH_TARIF + TUMUM_TARIF + TMAKAN_TARIF)) + TKOMUNIKASI_TARIF;
			// 	$("#thpgaji1").val(hasil);
			// 	$("#thpgaji2").val(hasil);
			// });

			$(".ajaxlokasi").select2({
				ajax: {
					url: "<?php echo site_url('admin/basic/lokasi/ajax'); ?>",
					dataType: 'json',
					delay: 250,
					data: function(params) {
						return {
							q: params.term
						};
					},
					processResults: function(data) {
						return {
							results: $.map(data, function(obj) {
								return {
									id: obj.id,
									text: obj.text
								};
							})
						};
					},
					cache: true
				},
				"language": "id"
			});
			$(".ajaxpegawai").select2({
				ajax: {
					url: "<?php echo site_url('admin/user/pegawai/ajax'); ?>",
					dataType: 'json',
					delay: 250,
					data: function(params) {
						return {
							q: params.term
						};
					},
					processResults: function(data) {
						return {
							results: $.map(data, function(obj) {
								return {
									id: obj.id,
									text: obj.text
								};
							})
						};
					},
					cache: true
				},
				"language": "id"
			});

		});

		table = $('#tblUtama').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "<?= $_ajaxSource ?>",
				"type": "POST"
			},
			"columnDefs": [
				// {
				// 	"targets": -2,
				// 	"render": function(data, type, full, meta) {
				// 		if (data == null)
				// 			return '<a href="#">Terdapat kesalahan pada data ini</a>';
				// 		else if (data == 0)
				// 			return '<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i> Pending</a>';
				// 		else
				// 			return '<a href="#" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Disetujui</a>';
				// 	}
				// },
				{
					"targets": -1,
					"defaultContent": '<a href="#">Terdapat kesalahan pada data ini</a>',
					"render": function(data, type, full, meta) {
						if (data == null) return '<a href="#">Terdapat kesalahan pada data ini</a>';
						var pesan = 'Approve';
						if (data == 1) pesan = 'Nonapprove';
						return '<div class="btn-group"><button type="button" class="btn btn-default">Pilihan</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="linkedit">Edit</a></li><li><a href="#" class="linkdelete">Delete</a></li></ul></div>';
					}

				},
				{
					"targets": [1, 2, 3, 4, 9, 10, 15, 11, 16],
					"visible": false,
					"searchable": false
				}

			],
			"language": {
				"lengthMenu": "Menampilkan _MENU_ data per halaman",
				"zeroRecords": "Data masih kosong atau tidak ditemukan",
				"info": "Menampilkan halaman ke _PAGE_ dari _PAGES_ halaman",
				"infoEmpty": "Data masih kosong"
			}
		})

		$('#tblUtama tbody').on('click', '.linkedit', function(e) {
			var data = table.row($(this).parents('tr')).data();
			$('#id').val(data[1]);
			$.ajax({
				url: "<?php echo site_url('admin/fitur/penggajian/ajaxedit'); ?>",
				type: "post",
				dataType: 'json',
				data: {
					id: data[1],
					nip: data[5]
				},
				success: function(data) {
					console.log(data.data[0]);
					console.log(data.datapegawai);
					$('#id').val(data.data[0].ID_GAJI);
					$('#updatenip').val(data.data[0].NAMA_PEG);
					$('#updatelokasi1').html("<option value='" + data.data[0].ID_LOKASI + "' selected ='selected'>" + data.data[0].KODE_LOKASI + " </option>");
					$('#updatekualifikasi2').html("<option value='' selected ='selected'>" + data.data[0].KUALIFIKASI_GAJI + " </option>");
					$('#updatekulifikasi').val(data.data[0].KUALIFIKASI_GAJI);
					$('#updateglapgaji').val(data.data[0].TGLAP_GAJI);
					$('#updatetglgaji').val(data.data[0].TGL_GAJI);
					$('#updatehk1').val(data.data[0].JUMHARI_GAJI);
					$('#updatekh1').val(data.data[0].JUMHADIR_GAJI);
					$('#updateindex1').val(data.data[0].INDEX_GAJI);
					$('#updateindex2').val(data.data[0].INDEX_GAJI);
					$('#updategajikulifikasi1').val(data.data[0].NKUALIFIKASI_GAJI);
					$('#updatekualifikasigaji').val(data.data[0].NKUALIFIKASI_GAJI);
					$('#updatepph1').val(data.data[0].PPH_GAJI);
					$('#updatejamsostek1').val(data.data[0].JAMSOSTEK_GAJI);
					$('#updatekoreksi1').val(data.data[0].COR_GAJI);
					$('#updatepengurangangaji1').val(data.data[0].TPENGURANGAN_GAJI);
					$('#updatepengurangangaji').val(data.data[0].TPENGURANGAN_GAJI);
					$('#updatethpgaji1').val(data.data[0].THP_GAJI);
					$('#updatethpgaji2').val(data.data[0].THP_GAJI);
					$('#updateUPAH_TARIF').val(data.datapegawai[0].UPAH_TARIF);
					$('#updateTUMUM_TARIF').val(data.datapegawai[0].TUMUM_TARIF);
					$('#updateTMAKAN_TARIF').val(data.datapegawai[0].TMAKAN_TARIF);
					$('#updateTKOMUNIKASI_TARIF').val(data.datapegawai[0].TKOMUNIKASI_TARIF);
				}

			});
			$("#boxdata").attr('class', 'col-md-9');
			$("#boxform").show();
		});
		$('#tblUtama tbody').on('click', '.linkdelete', function(e) {
			var data = table.row($(this).parents('tr')).data();
			alert("Status on going" + data[1]);
			$.ajax({
				url: "<?php echo site_url('admin/fitur/penggajian/delete'); ?>",
				type: "post",
				dataType: 'json',
				data: {
					id: data[1]
				},
				success: function(data) {
					if (data.result == false) {
						errmsg(data.msg);
					} else {
						sucmsg(data.msg);
						reset();
						table.ajax.reload(null, false);
					}

				}

			});
			//upStatus(data[1],data[4])
		});
	})
	$(".tampiltahun,.tampilbulan").change(function() {
		var index = $(".tampilbulan option:selected").val();
		var index1 = $(".tampiltahun option:selected").val();
		table.ajax.url('<?= $_ajaxSource . "/" ?>' + index + "/" + index1);
		table.ajax.reload();
		console.log('<?= $_ajaxSource . "/" ?>' + index + "/" + index1);
		console.log($(".tampilbulan option:selected").val());
		console.log($(".tampiltahun option:selected").val());
	});

	function changeData(index = null) {
		table.ajax.url('<?= $_ajaxSource . "/" ?>' + index);
		// console.log(index);
		table.ajax.reload();
		return index;
	}

	function changeData1(index = null) {
		console.log($(".tampilbulan option:selected").val());
	}

	function reset() {
		$('#id').val(0);
		$('#nip').val(0);
		$('#lokasi').html("<option value=''> --Ketik untuk mencari--</option>");
		$('#hk').val(0);
		$('#kh').val(0);
		$('#koreksi').val(0);
		$('#index').val(0);
		$('#pph').val(0);
		$('#jamsostek').val(0);
		$("#boxdata").attr('class', 'col-md-12');
		$("#boxform").hide();
	}

	$('#fillForm').submit(function(e) {
		e.preventDefault();
		var form = new FormData($('#fillForm')[0]);
		// console.log(form);

		$.ajax({
			url: "<?php echo site_url('admin/fitur/penggajian/upstatus'); ?>",
			type: 'POST',
			data: form,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				if (data.result == false) {
					errmsg(data.msg);
					// console.log(data.check);
				} else {
					sucmsg(data.msg);
					reset();
					// console.log(data.check);
					table.ajax.reload(null, false);
				}
			}
		});
	});

	$('#fillForm1').submit(function(e) {
		e.preventDefault();
		var form = new FormData($('#fillForm1')[0]);

		$.ajax({
			url: "<?php echo site_url('admin/fitur/penggajian/save'); ?>",
			type: 'POST',
			data: form,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				if (data.result == false) {
					errmsg(data.msg);
					$('#myModal').modal('toggle');
					alert(data.msg);
				} else {
					sucmsg(data.msg);
					// reset();
					$('#myModal').modal('toggle');
					table.ajax.reload(null, false);
				}
				// console.log(data);
			}
		});
	});
	$('#fillForm2').submit(function(e) {
		e.preventDefault();
		var form = new FormData($('#fillForm2')[0]);

		$.ajax({
			url: "<?php echo site_url('admin/fitur/penggajian/rekap2'); ?>",
			type: 'POST',
			data: form,
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				if (data.result == false) {
					errmsg(data);
					$('#Modalid').modal('toggle');
					alert(data);
				} else {
					sucmsg(data);
					// reset();
					$('#Modalid').modal('toggle');
					console.log(data);
					// table.ajax.reload(null, false);
				}
				// console.log(data);
			}
		});
	});

	function upStatus() {
		var form = new FormData($('#fillForm')[0]);
		$.ajax({
			url: "<?php echo site_url('admin/fitur/penggajian/upstatus'); ?>",
			type: "post",
			dataType: 'json',
			data: form,
			success: function(data) {
				if (data.result == false) {
					errmsg(data.msg);
				} else {
					sucmsg(data.msg);
					reset();
					table.ajax.reload(null, false);
				}

			}

		});
	}

	$('#msgAlert').hide();
	$('#msgAlert').on("close.bs.alert", function() {
		$('#msgAlert').hide();
		return false;
	});

	function sucmsg(msg) {
		$('#msgAlert').show();
		$('#msgAlert').removeClass('alert-danger');
		$('#msgAlert').addClass('alert-success');

		$('#msgIcon').removeClass('fa-ban');
		$('#msgIcon').addClass('fa-check');

		$('#msgContent').html(msg);
	}

	function errmsg(msg) {
		$('#msgAlert').show();
		$('#msgAlert').removeClass('alert-success');
		$('#msgAlert').addClass('alert-danger');

		$('#msgIcon').addClass('fa-ban');
		$('#msgIcon').removeClass('fa-check');

		$('#msgContent').html(msg);
	}
</script>
