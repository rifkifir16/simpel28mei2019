<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div id="alerts" class="alert alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4>	<i class="icon fa fa-check"></i> Alert!</h4>
				<t id="almsg"></t>
			</div>
			<div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Laporan Eksternal</h3>
				</div>
				<div class="box-body">
					<form method="post" action="eksternal.php"  target="_blank">
						<?php
						$bulan=isset($_POST['bulan'])?$_POST['bulan']:'';
						$tahun=isset($_POST['tahun'])?$_POST['tahun']:'';
						?>
						<div class="form-group ">
							<label>Nomor Surat Perjanjian</label>
							<select class="form-control ajaxjmk" name="jmk" style="width: 100%;">
								<option value="">--Pilih JMK--</option>
							</select>
						</div>
						<div class="form-group">
						  <label for="spp">Surat Permohonan Pemeriksaan</label>
						   <input id="spp" type="text" name="spp" class="inmdl form-control" placeholder="Surat Permohonan Pemeriksaan"/>
						</div>
						<div class="form-group">
						  <label for="tg">Tanggal Permohonan</label>
						   <input id="tg" type="date" name="tg" class="inmdl form-control" required="true"/>
						</div>
						<div class="form-group">
							<div class="form-group">
							  <label for="bulan">Bulan</label>
							  <select name="bulan" class="inmdl form-control" required>
								<option value="1" <?php if($bulan==1) echo 'selected';?>>Januari</option>
								<option value="2" <?php if($bulan==2) echo 'selected';?>>Februari</option>
								<option value="3" <?php if($bulan==3) echo 'selected';?>>Maret</option>
								<option value="4" <?php if($bulan==4) echo 'selected';?>>April</option>
								<option value="5" <?php if($bulan==5) echo 'selected';?>>Mei</option>
								<option value="6" <?php if($bulan==6) echo 'selected';?>>Juni</option>
								<option value="7" <?php if($bulan==7) echo 'selected';?>>Juli</option>
								<option value="8" <?php if($bulan==8) echo 'selected';?>>Agustus</option>
								<option value="9" <?php if($bulan==9) echo 'selected';?>>September</option>
								<option value="10" <?php if($bulan==10) echo 'selected';?>>Oktober</option>
								<option value="11" <?php if($bulan==11) echo 'selected';?>>November</option>
								<option value="11" <?php if($bulan==12) echo 'selected';?>>Desember</option>
							  </select>
							</div>
							
							<div class="form-group">
							  <label for="tahun">Tahun</label>
							   <input id="tahun" type="number" min="2016" max="9999" name="tahun" class="inmdl form-control" placeholder="Tahun" value="<?=$tahun?>" required="true"/>
							</div>
							<div class="form-group col-md-2">
								<label style="color:transparent;">_</label>
								<button class="form-control btn btn-success" id="submit" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>