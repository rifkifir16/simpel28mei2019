<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="<?=base_url('assets/dist/img/user4-128x128.jpg')?>" alt="User profile picture">

          <h3 class="profile-username text-center">Nina Mcintire</h3>

          <p class="text-muted text-center">Software Engineer</p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Followers</b> <a class="pull-right">1,322</a>
            </li>
            <li class="list-group-item">
              <b>Teman</b> <a class="pull-right">543</a>
            </li>
            <li class="list-group-item">
              <b>Teman yang sama</b> <a class="pull-right">13,287</a>
            </li>
          </ul>
              
              <a href="#" class="btn btn-primary btn-block"><i class="fa fa-user-plus"></i> <b>Tambahkan sebagai teman</b></a>
              
               <div class="btn-group col-sm-12" style="padding:0;">
                  <a class="btn btn-danger col-sm-10"><i class="fa fa-user-times"></i> <b>Hapus teman </b>

                  </a>
                  <button type="button" class="btn btn-default dropdown-toggle col-sm-2" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="#"><i class="fa fa-plus text-yellow"></i>Tambah hutang</a></li>
                    <li><a href="#"><i class="fa fa-plus text-primary"></i>Tambah piutang</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-ban text-red"></i>Block</a></li>
                  </ul>
                </div>
                
              
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <!-- About Me Box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">About Me</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

          <p class="text-muted">
            B.S. in Computer Science from the University of Tennessee at Knoxville
          </p>

          <hr>

          <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

          <p class="text-muted">Malibu, California</p>

          <hr>

          <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

          <p>
            <span class="label label-danger">UI Design</span>
            <span class="label label-success">Coding</span>
            <span class="label label-info">Javascript</span>
            <span class="label label-warning">PHP</span>
            <span class="label label-primary">Node.js</span>
          </p>

          <hr>

          <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <?php include 'profil/detail.php';?>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</section>
<!-- /.content -->