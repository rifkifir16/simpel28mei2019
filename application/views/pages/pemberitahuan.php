<section class="content">
  <div class="row">
    <div class="col-sm-6">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Pemberitahuan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tr>
              <th style="width: 10px">#</th>
              <th>Konten</th>
            </tr>
            <tr>
              <td>1.</td>
              <td>Mas Joko menyentuh anda</td>
            </tr>
            <tr>
              <td>2.</td>
              <td>Mbak Vina mencintai orang lain</td>

            </tr>
            <tr>
              <td>3.</td>
              <td>Om Jono sudah kurus</td>

            </tr>
            <tr>
              <td>4.</td>
              <td>Ki budi mahmud bukan pejuang kemerdekaan</td>

            </tr>
            <tr class="clickable-row" data-href='<?=site_url('pengajuan')?>'>
              <td>5.</td>
              <td>Mbak Vina ingin meminjam bahu anda</td></a>
            </tr>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>