<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Lembar Pengesahan Penelitian</title>
<style type="text/css">
  .style1 {font-family: Arial, Helvetica, sans-serif}
  .style3 {font-size: 14px}
  .style4 {font-size: 14}
  .style7 {font-family: "Times New Roman", Times, serif; font-size: 14; }
</style>
</head>

<body>
<div align="center">
  <p>&nbsp;</p>
  <p><strong><a href="javascript:window.print()" style="text-decoration:none; color:#000000">HALAMAN PENGESAHAN</a></strong></p>
  
  <p><strong>PENELITIAN <?=strtoupper($datas['NAMA_JENIS'])?></strong></p>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
  	<tr>
      <td class="style3">&nbsp;</td>
      <td colspan="4" class="style3">&nbsp;</td>
      <td class="style3">&nbsp;</td>
      <td colspan="2" class="style3">&nbsp;</td>
    </tr>
  <tr>
    <td width="6%" class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">JUDUL PENELITIAN </span></td>
    <td width="1%" class="style3"><span class="style7">:</span></td>
    <td width="71%" colspan="2" class="style3"><span class="style7"><?=$datas['JUDUL_PENELITIAN']?>

</span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">BIDANG PENELITIAN</span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3" colspan="2"><span class="style7"><?=$datas['JENIS_BIDANG']?></span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">KETUA PENELITI </span></td>
    <td class="style3">&nbsp;</td>
    <td class="style3" colspan="2"><span class="style4"></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td width="2%" class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">a. Nama Lengkap </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"> <?=$datas['NAMA_DOSEN']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">b. NIDN </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"> <?=$datas['NIDN']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">c. Jabatan Fungsional / Pangkat </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['PANGKAT']?> / <?=$datas['NAMA_GOLONGAN']?>
          </span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">d. Program Studi </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['NAMA_JURUSAN']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">e. Nomor Hp </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['NO_TLP']?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td colspan="3" class="style3"><span class="style7">f. Alamat surel <i>(e-mail)</i> </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$datas['EMAIL']?></span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <!--start anggota-->
  <?php
  $num=1;
  foreach($anggota as $ag){ ?>
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">ANGGOTA PENELITI (<?=$num?>) </span></td>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td width="1%" class="style3"><span class="style7">a.</span></td>
    <td colspan="2" class="style3"><span class="style7">Nama Lengkap </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$ag['NAMA_DOSEN'];?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td width="1%" class="style3"><span class="style7">b.</span></td>
    <td colspan="2" class="style3"><span class="style7">NIDN </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$ag['NIDN'];?></span></td>
  </tr>
  <tr>
    <td height="23" class="style3">&nbsp;</td>
    <td class="style3"><span class="style4"></span></td>
    <td class="style3"><span class="style7">c.</span></td>
    <td colspan="2" class="style3"><span class="style7">Program Studi </span></td>
    <td class="style3"><span class="style7">:</span></td>
    <td class="style3"><span class="style7"><?=$ag['NAMA_JURUSAN'];?></span></td>
  </tr>
  <tr><td><pre></pre></td></tr>
  <?php 
    $num++;
    }
  ?>
  <!--end anggota-->
  <tr>
    <td class="style3">&nbsp;</td>
    <td colspan="4" class="style3"><span class="style7">BIAYA PENGABDIAN</span></td>
    <td class="style3"><span class="style7">:</span></td>
	
    <td class="style3"><span class="style7"><?=$datas['SUMBER_BIAYA_PENELITIAN']?> </span></td>
	<td class="style3"><span class="style7">Rp <?=number_format($datas['BIAYA_PENELITIAN'])?></span></td>
  </tr>
  <tr>
    <td class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
    <td width="15%" class="style3">&nbsp;</td>
    <td width="4%" class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
    <td class="style3">&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4"><?=$datas['tanggal'];?></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3">Mengetahui,</span></td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4"></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3"><?=$_data['pdekan']?></span></td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4">Ketua Pelaksana</span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3"></span></td>
      <td>&nbsp;</td>
      <td class="style3"><p class="style4">&nbsp;</p>
      <p class="style4">&nbsp;</p>                    </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style3"><?=$_data['ndekan']?></span></td>
      <td>&nbsp;</td>
      <td class="style3"><span class="style4"> <?=$datas['NAMA_DOSEN']?></span></td>
    </tr>
    <tr>
      <td width="5%">&nbsp;</td>
      <td width="45%"><span class="style3">NIP.<?=$_data['npdekan']?></span></td>
      <td width="14%">&nbsp;</td>
      <td width="36%" class="style3"><span class="style4">NIP.<?=$datas['NIP']?></span></td>
    </tr>
</table>
<table width="50%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td width="85%">&nbsp;</td>
      <td width="15%">&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3">Menyetujui,</div></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3"><?=$_data['plppm']?></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><p class="style3">&nbsp;</p>
      <p class="style3">&nbsp;</p>                    </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3"><?=$_data['nlppm']?></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center" class="style3">NIP.<?=$_data['nplppm']?></div></td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <p class="style1">&nbsp;</p>
  <p class="style1">&nbsp;</p>
  <blockquote>
    <p align="right" class="style1">&nbsp;</p>
    <p align="right" class="style1">&nbsp;</p>
    <p align="right" class="style1">&nbsp;</p>
    <p align="right" class="style1">&nbsp;</p>
    <p align="right" class="style1">&nbsp; </p>
  </blockquote>
</body>
<script src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js');?>"></script>
<script src="<?=base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<script>
	$(document).ready(function() {
		window.print();
		
	});
	</script>
</html>

