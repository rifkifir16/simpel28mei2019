<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $_title="PT. Surveyor Indonesia";
	protected $data=array();
	
	function __construct(){
		parent::__construct();
		//check is logged in
		if($this->session->userdata('userId')==null || $this->session->userdata('nip')==null || $this->session->userdata('hakAkses')==null)
			redirect('front');
		$this->data['isAdmin']=false;
		date_default_timezone_set('Asia/Jakarta');

		$sesi=array();
		$sesi['userid']=$this->session->userdata('userId');
		$sesi['nip']=$this->session->userdata('nip');
		$sesi['nama']=$this->session->userdata('nama');
		$sesi['hakakses']=$this->session->userdata('hakAkses');
		$this->data['sesi']=$sesi;
	}

	/*public function index()
	{
		$this->load->view('home/index');['isAdmin']
	}*/
}
