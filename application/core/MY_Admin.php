<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Admin extends MY_Controller
{

	protected $_title = "PT. Surveyor Indonesia";
	protected $data = array();
	protected $_sidebar = 'inc/sidemenu.php';

	function __construct()
	{
		parent::__construct();
		$session = $this->session->userdata();
		switch ($session['hakAkses']) {
			case 'admin':
				$this->data['_sidebar'] = $this->_sidebar;
				break;
			case 'pelamar':
				$this->data['_sidebar'] = 'inc/sidemenu-pelamar.php';
				break;
			case 'pln':
				$this->data['_sidebar'] = 'inc/sidemenu-pln.php';
				break;
		}
	}

	/*public function index()
	{
		$this->load->view('home/index');['isAdmin']
	}*/
}
